# README #

## What is this repository for? ##

This repository contains some of the lab projects from the Graphics Programming courses offered by The Game Institute
(https://www.gameinstitute.com) completely rewritten using C++ 11/14 and Vulkan (instead of DirectX 9). The main
purpose is educational. That is, to learn more about graphics programming and Vulkan.

### How do I get set up? ###

Libraries / tools used

* CMake with Visual Studio Community 2017: https://cmake.org/
* Vulkan SDK: https://vulkan.lunarg.com/
* GLFW for window creation, mouse and keyboard input: http://www.glfw.org/
* GLM (OpenGL mathematics library): http://glm.g-truc.net/0.9.8/index.html
* Various files from STB e.g. stb_image.h: https://github.com/nothings/stb
* PVS-Studio Static Code Analyzer (free version): http://www.viva64.com

Some library paths and header file include paths are hard coded in CMakeLists.txt. These would need to be modified
depending on where the user installed the libraries on their system. For example,

    set(glfw_include_path f:/glfw-3.2.1.bin.WIN64/include)
    set(glfw_lib_path f:/glfw-3.2.1.bin.WIN64/lib-vc2015)
    set(stb_include_path F:/code/repo/other)

SPIR-V files are created in CMAKE_CURRENT_BINARY_DIR e.g. vert.spv. If the application is started inside Visual Studio
then simply specifying the file name as vert.spv works. If the application is started outside Visual Studio it fails to
find vert.spv. This is because the application executable is located in CMAKE_CURRENT_BINARY_DIR/Debug. If you want to
run a program from the command line ensure that any SPIR-V files required are in the same directory as the executable.

All classes/functions are wrapped in the namespace Jade to help prevent name conflicts.

### Testing ###

I mainly work on my laptop, which has a GeForce GTX 780M (nVidia GPU).

The code could probably be modified to run on other platforms such as Linux without too much extra effort since all
libraries/tools used are cross platform. However, right now it only works on Windows 10.

### Projects ###

Project                 |About                                                                                                                                                                                                                                                                                                                                                                                                       |Screen Shot
------------------------|------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------|------------------------------------------------------------------------------------------
terrain_detail_texturing|This application renders a terrain (as indexed triangle strips) using a large base texture that is stretched across the entire terrain and a detail texture that is repeated every six quads horizontally and vertically to provide additional detail. The sky box technique (cube map) is used to make the scene more immersive.                                                                           |![Terrain Detail Texturing](gp1/terrain_detail_texturing/doc/terrain_detail_texturing.PNG)
scene_lighting          |Loads a scene from a file stored in Interchangeable World Format (IWF). Geometry is batch rendered by light group, then material. A light group stores the N most influential lights in the scene for a list of triangles. The triangles in each light group are further grouped by material. The reason for doing this is to minimise the number of vkCmdDrawIndexed calls required to draw the scene.     |![Scene lighting](gp1/scene_lighting/doc/scene_lighting.PNG)

### Who do I talk to? ###

* Repo owner or admin