// This is a personal academic project. Dear PVS-Studio, please check it.
// PVS-Studio Static Code Analyzer for C, C++ and C#: http://www.viva64.com
#include "timer.h"

// C++ Standard Library
#include <algorithm>
#include <cassert>
#include <cmath>
#include <cstring>
#include <numeric>

// GLFW
#include <GLFW/glfw3.h>

namespace Jade
{

void Timer::start(double previous_time)
{
    m_previous_time = previous_time;
}

void Timer::tick()
{
    m_time = glfwGetTime();
    assert(m_time >= m_previous_time);
    auto frame_time = m_time - m_previous_time;
    m_previous_time = m_time;
    auto delta = std::fabs(frame_time - m_time_elapsed);
    if (delta <= 1.0f)
    {
        // Example
        // Assume that m_frame_times holds 5 elements
        // 0 1 2 3 4
        // x x x x x start of frame 0
        //   x x x x shift to right
        // a x x x x write a to first element of m_frame_times
        // a x x x x start of frame 1
        //   a x x x shift to right
        // b a x x x write b to first element of m_frame_times
        // b a x x x start of frame 2
        //   b a x x shift to right
        // c b a x x write c to first element of m_frame_times
        // etc.
        // At the end of frame 4, a will be at index 4. It will be shifted off the end of m_frame_times in frame 5.
        // parameter 0: destination, parameter 1: source, parameter 2: size in bytes
        // The source and destination ranges can overlap.
        std::memmove(m_frame_times.data() + 1, m_frame_times.data(), sizeof(double) * (m_frame_times.size() - 1));
        m_frame_times.at(0) = frame_time;
    }
    
    m_time_elapsed = std::accumulate(m_frame_times.cbegin(), m_frame_times.cend(), 0.0);
    m_time_elapsed /= static_cast<double>(m_frame_times.size());
    ++m_frame_count;
    m_frames_per_second_time += m_time_elapsed;
    if (m_frames_per_second_time >= 1.0)
    {
        m_frame_rate = m_frame_count;
        m_frames_per_second_time = 0.0;
        m_frame_count = 0;
    }
}

int Timer::get_frame_rate() const
{
    return m_frame_rate;
}

double Timer::get_time_elapsed() const
{
    return m_time_elapsed;
}

}
