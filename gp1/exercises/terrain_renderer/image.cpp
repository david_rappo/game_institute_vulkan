// This is a personal academic project. Dear PVS-Studio, please check it.
// PVS-Studio Static Code Analyzer for C, C++ and C#: http://www.viva64.com
#include "image.h"

// C++ Standard Library
#include <cassert>

namespace Jade
{

void Image::destroy(VkDevice device)
{
    assert(nullptr != device);
    if (nullptr != m_sampler)
    {
        vkDestroySampler(device, m_sampler, nullptr);
        m_sampler = nullptr;
    }

    if (nullptr != m_image_view)
    {
        vkDestroyImageView(device, m_image_view, nullptr);
        m_image_view = nullptr;
    }

    if (nullptr != m_device_memory)
    {
        vkFreeMemory(device, m_device_memory, nullptr);
        m_device_memory = nullptr;
    }

    if (nullptr != m_image)
    {
        vkDestroyImage(device, m_image, nullptr);
        m_image = nullptr;
    }
}

}
