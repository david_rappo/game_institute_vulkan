#pragma once

// Vulkan
#include "vulkan/vulkan.h"

// Jade
#include "buffer.h"

namespace Jade
{

class Mesh
{
public:
    static const VkIndexType index_type{VK_INDEX_TYPE_UINT16};

    Mesh();
    Mesh(const Mesh&) = delete;
    Mesh& operator=(const Mesh&) = delete;

    void destroy(VkDevice device);

    Jade::Buffer m_vertex_buffer;
    Jade::Buffer m_staging_vertex_buffer;
    Jade::Buffer m_index_buffer;
    Jade::Buffer m_staging_index_buffer;
};

}