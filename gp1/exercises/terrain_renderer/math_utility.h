#pragma once

// GLM
#include <glm/mat4x4.hpp>
#include <glm/vec3.hpp>
#include <glm/vec4.hpp>

namespace Jade
{

namespace Math_utility
{

glm::mat4 perspective_projection_left_handed(float vertical_field_of_view,
    float aspect_ratio,
    float near_view_plane,
    float far_view_plane);

glm::mat4 look_at_left_handed(const glm::vec3 &eye, const glm::vec3 &at, const glm::vec3 &up);

glm::mat4 invert_y_matrix();

glm::vec3 transform_coordinate(const glm::mat4 &m, const glm::vec3 &v);

glm::vec3 transform_normal(const glm::mat4 &m, const glm::vec3 &v);

void regenerate_orientation_vectors(const glm::vec3 &right,
    const glm::vec3 &up,
    const glm::vec3 &look,
    glm::vec3 &out_right,
    glm::vec3 &out_up,
    glm::vec3 &out_look);

void copy(const glm::mat4 &source, float *out_data);

}

}
