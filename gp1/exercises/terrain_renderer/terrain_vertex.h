#pragma once

// GLM
#include <glm/vec2.hpp>
#include <glm/vec4.hpp>

namespace Jade
{

class Terrain_vertex
{
public:
    Terrain_vertex() = default;
    Terrain_vertex(const glm::vec3 &position, const glm::vec3 &normal): m_position{position}, m_normal{normal}
    {

    }

    glm::vec3 m_position;
    glm::vec3 m_normal;
};

}