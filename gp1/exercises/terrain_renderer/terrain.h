#pragma once

// C++ Standard Library
#include <cstddef>
#include <string>
#include <utility>
#include <vector>

// Vulkan
#include "vulkan/vulkan.h"

// GLM
#include <glm/vec3.hpp>

// Jade
#include "buffer.h"
#include "terrain_vertex.h"
#include "volume.h"

namespace Jade
{

class Camera;
class Player;

}

namespace Jade
{

class Terrain
{
public:
    // elapsed_time is measured in seconds
    static void update_player(Terrain *terrain, Player *player, float elapsed_time);
    // elapsed_time is measured in seconds
    static void update_camera(Terrain *terrain, Camera *camera, const Player *player, float elapsed_time);
    
    Terrain();
    Terrain(const Terrain&) = delete;
    Terrain& operator=(const Terrain&) = delete;
    bool load_height_map(const std::string &file_name, std::size_t width, std::size_t height);
    bool build_buffers(VkDevice vulkan_device,
        const VkPhysicalDeviceMemoryProperties &physical_device_memory_properties);
    bool copy_buffers(VkDevice vulkan_device, VkQueue graphics_queue, VkCommandBuffer command_buffer);
    void destroy(VkDevice device);
    void draw(VkCommandBuffer command_buffer, Camera *camera) const;
    float calculate_height(const glm::vec2 &world_position) const;

private:
    // The data type of the indices in the terrain index buffers.
    static const VkIndexType index_type{VK_INDEX_TYPE_UINT16};

    glm::vec3 generate_normal(std::size_t row_index, std::size_t column_index) const;
    
    // m_scale_vector.x and m_scale_vector.z are used to scale the width and height of the terrain quads.
    // m_scale_vector.y is used to scale the height of each terrain vertex.
    glm::vec3 m_scale_vector{0.0f};
    // Stores the number of indices used to draw a terrain sub mesh.
    uint32_t m_mesh_index_count{};
    // Stores the number of vertexes in the horizontal dimension in a terrain sub mesh
    std::size_t m_mesh_width{65};
    // Stores the number of vertexes in the vertical dimension in a terrain sub mesh
    std::size_t m_mesh_height{65};
    // width of m_data
    std::size_t m_width{};
    // height of m_data
    std::size_t m_height{};
    // Stores the number of sub meshes in the horizontal dimension of the terrain
    std::size_t m_horizontal_mesh_count{};
    // Stores the number of sub meshes in the vertical dimension of the terrain
    std::size_t m_vertical_mesh_count{};
    // Stores the terrain vertices in world space
    std::vector<Jade::Terrain_vertex> m_data;
    std::vector<Jade::Buffer> m_vertex_buffers;
    std::vector<Jade::Buffer> m_index_buffers;
    // m_staging_vertex_buffers is used to copy terrain vertex data from host memory to device memory (m_vertex_buffers)
    std::vector<Jade::Buffer> m_staging_vertex_buffers;
    // m_staging_index_buffers is used to copy terrain index data from host memory to device memory (m_index_buffers)
    std::vector<Jade::Buffer> m_staging_index_buffers;
    // Stores (m_horizontal_mesh_count * m_vertical_mesh_count) world space bounding volumes.
    // m_bounding_volumes[i] stores the axis aligned bounding box (AABB) for m_vertex_buffers[i]
    std::vector<Jade::Volume> m_bounding_volumes;
};

}