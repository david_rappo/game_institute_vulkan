// This is a personal academic project. Dear PVS-Studio, please check it.
// PVS-Studio Static Code Analyzer for C, C++ and C#: http://www.viva64.com
#include "player.h"

// C++ Standard Library
#include <algorithm>
#include <cassert>
#include <cmath>

// GLM
#include <glm/glm.hpp>
#include <glm/gtc/matrix_transform.hpp>
#include <glm/vec4.hpp>

// Jade
#include "first_person_camera.h"
#include "log_manager.h"
#include "math_utility.h"
#include "space_craft_camera.h"
#include "terrain.h"
#include "third_person_camera.h"

const float Jade::Player::minimum_pitch{glm::radians(-89.0f)};
const float Jade::Player::maximum_pitch{glm::radians(89.0f)};
const float Jade::Player::minimum_yaw{0.0f};
const float Jade::Player::maximum_yaw{glm::radians(360.0f)};
const float Jade::Player::minimum_roll{glm::radians(-20.0f)};
const float Jade::Player::maximum_roll{glm::radians(20.0f)};

namespace Jade
{

Player::Player() = default;

Player::~Player() = default;

void Player::set_camera_type(Camera::Type type)
{
    if ((nullptr != m_camera) && (type == m_camera->get_type()))
    {
        return;
    }
    
    auto previous_type = (nullptr != m_camera) ? m_camera->get_type() : Camera::Type::undefined;
    switch (type)
    {
    case Camera::Type::first_person:
        if (nullptr == m_camera)
        {
            m_camera.reset(new First_person_camera);
        }
        else
        {
            std::unique_ptr<Camera> old_camera(m_camera.release());
            m_camera.reset(new First_person_camera(old_camera.get()));
        }
        
        break;
    
    case Camera::Type::space_craft:
        if (nullptr == m_camera)
        {
            m_camera.reset(new Space_craft_camera);
        }
        else
        {
            std::unique_ptr<Camera> old_camera(m_camera.release());
            m_camera.reset(new Space_craft_camera(old_camera.get()));
        }

        break;

    case Camera::Type::third_person:
        if (nullptr == m_camera)
        {
            m_camera.reset(new Third_person_camera);
        }
        else
        {
            std::unique_ptr<Camera> old_camera(m_camera.release());
            m_camera.reset(new Third_person_camera(old_camera.get()));
        }

        break;

    default:
        assert(false);
        break;
    }

    if (Camera::Type::space_craft == previous_type)
    {
        // Set the up vector to <0, 1, 0> because the orientation of the player's up vector when it's attached camera
        // is a space craft camera might be unsuitable for different types of camera's.
        m_up = glm::vec3{0.0f, 1.0f, 0.0f};
        // Flatten the right and look vectors
        m_right.y = 0.0f;
        m_right = glm::normalize(m_right);
        m_look.y = 0.0f;
        m_look = glm::normalize(m_look);
        m_pitch = 0.0f;
        // Calculate the angle between <0, 0, 1> and m_look
        auto cos_q = glm::dot(m_look, glm::vec3{0.0f, 0.0f, 1.0f});
        m_yaw = std::acosf(cos_q);
        // m_yaw will be in the range: [0, PI]. If m_look.x is negative, then m_yaw should actually be -m_yaw.
        if (m_look.x < 0.0f)
        {
            m_yaw *= -1.0f;
        }

        m_roll = 0.0f;
    }
    else if (Camera::Type::space_craft == m_camera->get_type())
    {
        // Switching to a space craft camera. The players orientation vectors should match the camera's orientation
        // vectors.
        m_right = m_camera->get_right();
        m_up = m_camera->get_up();
        m_look = m_camera->get_look();
    }
}

// Set the position of the camera relative to the player.
void Player::set_camera_offset(const glm::vec3 &camera_offset)
{
    m_camera_offset = camera_offset;
    m_camera->set_position(m_position + m_camera_offset);
}

void Player::add_update_player_callback(Terrain *terrain)
{
    m_update_player_callbacks.push_back(std::bind(Terrain::update_player, terrain, this, std::placeholders::_1));
}

void Player::add_update_camera_callback(Terrain *terrain)
{
    m_update_camera_callbacks.push_back(std::bind(Terrain::update_camera,
        terrain,
        std::placeholders::_1,
        this,
        std::placeholders::_2));
}

void Player::set_position(const glm::vec3 &position)
{
    auto v = position - m_position;
    move(v);
}

void Player::rotate(float x, float y, float z)
{
    switch (m_camera->get_type())
    {
    
    case Camera::Type::first_person:
    case Camera::Type::third_person:
    {
        if (x)
        {
            m_pitch += x;
            if (m_pitch > maximum_pitch)
            {
                x = 0.0;
                m_pitch = maximum_pitch;
            }

            if (m_pitch < minimum_pitch)
            {
                x = 0.0;
                m_pitch = minimum_pitch;
            }
        }
        
        if (y)
        {
            m_yaw += y;
            if (m_yaw > maximum_yaw)
            {
                m_yaw -= maximum_yaw;
            }
            
            if (m_yaw < minimum_yaw)
            {
                m_yaw += maximum_yaw;
            }
        }
        
        if (z)
        {
            m_roll += z;
            if (m_roll > maximum_roll)
            {
                z = 0.0;
                m_roll = maximum_roll;
            }

            if (m_roll < minimum_roll)
            {
                z = 0.0;
                m_roll = minimum_roll;
            }
        }
        
        // rotate uses the player orientation vector to perform it's calculations
        m_camera->rotate(x, y, z, *this);
        if (y)
        {
            auto m = glm::rotate(glm::mat4{}, y, m_up);
            m_look = Math_utility::transform_normal(m, m_look);
            m_right = Math_utility::transform_normal(m, m_right);
        }
    }
    break;

    case Camera::Type::space_craft:
    {
        // rotate uses the player orientation vector to perform it's calculations
        m_camera->rotate(x, y, z, *this);
        if (x)
        {
            auto m = glm::rotate(glm::mat4{}, x, m_right);
            m_up = Math_utility::transform_normal(m, m_up);
            m_look = Math_utility::transform_normal(m, m_look);
        }

        if (y)
        {
            auto m = glm::rotate(glm::mat4{}, y, m_up);
            m_right = Math_utility::transform_normal(m, m_right);
            m_look = Math_utility::transform_normal(m, m_look);
        }

        if (z)
        {
            auto m = glm::rotate(glm::mat4{}, z, m_look);
            m_right = Math_utility::transform_normal(m, m_right);
            m_up = Math_utility::transform_normal(m, m_up);
        }
    }
    break;

    default:
        assert(false);
        break;
    }

    Math_utility::regenerate_orientation_vectors(m_right, m_up, m_look, m_right, m_up, m_look);
}

void Player::update(float time_elapsed)
{
    m_velocity += (m_gravity * time_elapsed);
    auto xz_vec = glm::vec2{m_velocity.x, m_velocity.z};
    auto length_xz = glm::length(xz_vec);
    if (length_xz > m_maximum_velocity_xz)
    {
        xz_vec /= length_xz;
        xz_vec *= m_maximum_velocity_xz;
        m_velocity.x = xz_vec.x;
        m_velocity.z = xz_vec.y; // Use .y because xz_vec is glm::vec2
    }

    // Clamp m_velocity.y to m_maximum_velocity_y
    if (m_velocity.y > m_maximum_velocity_y)
    {
        m_velocity.y = m_maximum_velocity_y;
    }
    
    move(m_velocity * time_elapsed);
    // Update the position of the player if it has moved into the terrain
    for (auto iter = m_update_player_callbacks.cbegin(); iter != m_update_player_callbacks.cend(); ++iter)
    {
        (*iter)(time_elapsed);
    }

    m_camera->update(time_elapsed, m_camera_lag, *this);
    // Update the position of the camera if it has moved into the terrain
    for (auto iter = m_update_camera_callbacks.cbegin(); iter != m_update_camera_callbacks.cend(); ++iter)
    {
        (*iter)(m_camera.get(), time_elapsed);
    }

    auto length = glm::length(m_velocity);
    auto friction = m_friction * time_elapsed;
    // Clamp friction to length
    if (friction > length)
    {
        friction = length;
    }
    
    auto reverse_velocity = -m_velocity;
    auto reverse_velocity_length = glm::length(reverse_velocity);
    auto delta = std::fabs(0.0f - reverse_velocity_length);
    if (delta >= 0.01f)
    {
        // Normalising reverse_velocity was returning <NaN, NaN, NaN> if reverse_velocity was zero. Appending
        // <NaN, NaN, NaN> to m_velocity corrupted m_velocity.
        reverse_velocity = glm::normalize(reverse_velocity);
        m_velocity += (reverse_velocity * friction);
    }
}

void Player::move(Direction direction, float distance)
{
    auto shift_vector = create_shift_vector(direction, distance);
    move(shift_vector);
}

void Player::move_velocity(Direction direction, float distance)
{
    auto shift_vector = create_shift_vector(direction, distance);
    move_velocity(shift_vector);
}

void Player::set_first_person_defaults()
{
    m_friction = 250.0f;
    m_gravity = glm::vec3{0.0f, -400.0f, 0.0f};
    m_maximum_velocity_xz = 125.0f;
    m_maximum_velocity_y = 400.0f;
    set_camera_offset(glm::vec3{0.0f, 10.0f, 0.0f});
    m_camera_lag = 0.0f;
}

void Player::set_third_person_defaults()
{
    m_friction = 250.0f;
    m_gravity = glm::vec3{0.0f, -400.0f, 0.0f};
    m_maximum_velocity_xz = 125.0f;
    m_maximum_velocity_y = 400.0f;
    set_camera_offset(glm::vec3{0.0f, 40.0f, -60.0f});
    m_camera_lag = 0.25f;
}

void Player::set_space_craft_defaults()
{
    m_friction = 125.0f;
    m_gravity = glm::vec3{0.0f, 0.0f, 0.0f};
    m_maximum_velocity_xz = 400.0f;
    m_maximum_velocity_y = 400.0f;
    set_camera_offset(glm::vec3{0.0f, 0.0f, 0.0f});
    m_camera_lag = 0.0f;
}

glm::mat4 Player::get_world_matrix() const
{
    // Direct3D world matrix
    //
    // right.x    right.y    right.z    0
    // up.x       up.y       up.z       0
    // look.x     look.y     look.z     0
    // position.x position.y position.z 1
    //
    // get_world_matrix returns the transpose of the Direct3D world matrix because GLSL uses column vectors
    auto column_0 = glm::vec4{m_right.x, m_right.y, m_right.z, 0.0f};
    auto column_1 = glm::vec4{m_up.x, m_up.y, m_up.z, 0.0f};
    auto column_2 = glm::vec4{m_look.x, m_look.y, m_look.z, 0.0f};
    auto column_3 = glm::vec4{m_position.x, m_position.y, m_position.z, 1.0f};
    return glm::mat4{column_0, column_1, column_2, column_3};
}

void Player::move(const glm::vec3 &v)
{
    m_position += v;
    m_camera->move(v);
}

void Player::move_velocity(const glm::vec3 &v)
{
    m_velocity += v;
}

glm::vec3 Player::create_shift_vector(Direction direction, float distance) const
{
    glm::vec3 shift_vector{0.0f};
    // No bits set
    if (0 == direction)
    {
        return shift_vector;
    }
    
    if (direction & Direction::forward)
    {
        shift_vector += (m_look * distance);
    }

    if (direction & Direction::back)
    {
        shift_vector += (-m_look * distance);
    }

    if (direction & Direction::right)
    {
        shift_vector += (m_right * distance);
    }

    if (direction & Direction::left)
    {
        shift_vector += (-m_right * distance);
    }

    if (direction & Direction::up)
    {
        shift_vector += (m_up * distance);
    }

    if (direction & Direction::down)
    {
        shift_vector += (-m_up * distance);
    }

    return shift_vector;
}

}