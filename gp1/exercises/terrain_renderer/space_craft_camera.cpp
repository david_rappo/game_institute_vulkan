// This is a personal academic project. Dear PVS-Studio, please check it.
// PVS-Studio Static Code Analyzer for C, C++ and C#: http://www.viva64.com
#include "space_craft_camera.h"

// GLM
#include <glm/glm.hpp>
#include <glm/gtc/matrix_transform.hpp>
#include <glm/mat4x4.hpp>

// Jade
#include "math_utility.h"
#include "player.h"

namespace Jade
{

Space_craft_camera::Space_craft_camera() = default;

Space_craft_camera::Space_craft_camera(const Camera *camera): Camera{camera}
{
}

Camera::Type Space_craft_camera::get_type() const
{
    return Type::space_craft;
}

void Space_craft_camera::rotate(float x, float y, float z, const Player &player)
{
    if (x)
    {
        // Pitch around the player's right vector
        auto m = glm::rotate(glm::mat4{}, x, player.get_right());
        auto up = Math_utility::transform_normal(m, get_up());
        set_up(up);
        auto look = Math_utility::transform_normal(m, get_look());
        set_look(look);
        auto const &player_position = player.get_position();
        auto camera_position = get_position();
        camera_position -= player_position;
        camera_position = Math_utility::transform_coordinate(m, camera_position);
        camera_position += player_position;
        set_position(camera_position);
    }

    if (y)
    {
        // Yaw around the player's up vector
        auto m = glm::rotate(glm::mat4{}, y, player.get_up());
        auto right = Math_utility::transform_normal(m, get_right());
        set_right(right);
        auto look = Math_utility::transform_normal(m, get_look());
        set_look(look);
        auto const &player_position = player.get_position();
        auto camera_position = get_position();
        camera_position -= player_position;
        camera_position = Math_utility::transform_coordinate(m, camera_position);
        camera_position += player_position;
        set_position(camera_position);
    }

    if (z)
    {
        // Roll around the player's look vector
        auto m = glm::rotate(glm::mat4{}, z, player.get_look());
        auto right = Math_utility::transform_normal(m, get_right());
        set_right(right);
        auto up = Math_utility::transform_normal(m, get_up());
        set_up(up);
        auto const &player_position = player.get_position();
        auto camera_position = get_position();
        camera_position -= player_position;
        camera_position = Math_utility::transform_coordinate(m, camera_position);
        camera_position += player_position;
        set_position(camera_position);
    }

    set_view_matrix_dirty(true);
}

}