#pragma once

// C++ Standard Library
#include <string>

namespace Jade
{

namespace Unicode
{

std::string wide_to_multi_byte(const std::wstring &wide_string);
std::wstring multi_byte_to_wide(const std::string &multi_byte_string);

}

}