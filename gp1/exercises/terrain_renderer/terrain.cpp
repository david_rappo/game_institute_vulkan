// This is a personal academic project. Dear PVS-Studio, please check it.
// PVS-Studio Static Code Analyzer for C, C++ and C#: http://www.viva64.com
#include "terrain.h"

// C++ Standard Library
#include <algorithm>
#include <cassert>
#include <fstream>
#include <iterator>
#include <limits>

// GLM
#include <glm/geometric.hpp>

// Jade
#include "log_manager.h"
#include "player.h"
#include "third_person_camera.h"
#include "vulkan_utility.h"

namespace Jade
{

// Test if "player" moved into "terrain". If so, adjust the players position and velocity.
void Terrain::update_player(Terrain *terrain, Player *player, float elapsed_time)
{
    auto player_position = player->get_position();
    auto height = terrain->calculate_height(glm::vec2{player_position.x, player_position.z});
    // Check if the players bounding volume is intersecting the terrain. player->get_volume().m_minimum.y should be
    // negative e.g. -3
    auto y = player_position.y + player->get_volume().m_minimum.y;
    if (y < height)
    {
        auto velocity = player->get_velocity();
        velocity.y = 0.0f;
        player->set_velocity(velocity);
        // player->get_volume().m_minimum.y should be negative e.g. -3
        player_position.y = height - player->get_volume().m_minimum.y;
        player->set_position(player_position);
    }
}

void Terrain::update_camera(Terrain *terrain, Camera *camera, const Player *player, float elapsed_time)
{
    if (Camera::Type::third_person != camera->get_type())
    {
        return;
    }

    auto camera_position = camera->get_position();
    auto height = terrain->calculate_height(glm::vec2{camera_position.x, camera_position.z});
    // Check if the camera's bounding volume is intersecting the terrain. camera->get_volume().m_minimum.y should be
    // negative e.g. -3
    auto y = camera_position.y + camera->get_volume().m_minimum.y;
    if (y < height)
    {
        camera_position.y = height - camera->get_volume().m_minimum.y;
        camera->set_position(camera_position);
    }

    // Even if the position of the camera was not updated in this function, the position of the player might have
    // changed in Terrain::update_player. Player::update calls Terrain::update_player before Terrain::update_camera.
    // Rotate the camera to look at the current position of the player.
    auto third_person_camera = dynamic_cast<Third_person_camera*>(camera);
    assert(nullptr != third_person_camera);
    third_person_camera->look_at(player->get_position(), player->get_up());
}

Terrain::Terrain() = default;

// file_name: the name of the raw file (.RAW) to load.
// width: number of bytes in the horizontal direction
// height: number of bytes in the vertical direction
bool Terrain::load_height_map(const std::string &file_name, std::size_t width, std::size_t height)
{
    std::ifstream ifs(file_name, std::ios_base::in|std::ios_base::binary);
    if (!ifs)
    {
        return false;
    }

    auto size = width * height;
    std::vector<char> data(size, char{});
    ifs.read(data.data(), static_cast<std::streamsize>(size));
    if (!ifs)
    {
        return false;
    }

    m_data.resize(size);
    m_width = width;
    m_height = height;
    // If m_width = 257, m_height = 257, m_mesh_width = 17, m_mesh_height = 17
    // m_horizontal_mesh_count = 256 / 16 = 16
    // m_vertical_mesh_count = 256 / 16 = 16
    m_horizontal_mesh_count = (m_width - 1) / (m_mesh_width - 1);
    m_vertical_mesh_count = (m_height - 1) / (m_mesh_height - 1);
    // Four is roughly the best size for a terrain with dimensions 512 by 512 quads. So, x_scale and z_scale will be
    // eight.
    auto x_scale = 4.0f * static_cast<float>(512 / (m_width - 1));
    auto y_scale = 2.0f;
    auto z_scale = 4.0f * static_cast<float>(512 / (m_height - 1));
    m_scale_vector = glm::vec3{x_scale, y_scale, z_scale};
    for (std::size_t row_index = 0; row_index != m_height; ++row_index)
    {
        for (std::size_t column_index = 0; column_index != m_width; ++column_index)
        {
            auto index = (row_index * m_width) + column_index;
            auto value = glm::vec3{static_cast<float>(column_index),
                static_cast<float>(data.at(index)),
                static_cast<float>(row_index)};
            value *= m_scale_vector;
            auto &terrain_vertex = m_data.at(index);
            terrain_vertex.m_position = value;
        }
    }
    
    for (std::size_t row_index = 0; row_index != m_height; ++row_index)
    {
        for (std::size_t column_index = 0; column_index != m_width; ++column_index)
        {
            auto index = (row_index * m_width) + column_index;
            auto &terrain_vertex = m_data.at(index);
            auto normal = generate_normal(row_index, column_index);
            terrain_vertex.m_normal = normal;
        }
    }
    
    return true;
}

// build_buffers should be called after load_height_map.
bool Terrain::build_buffers(VkDevice vulkan_device,
    const VkPhysicalDeviceMemoryProperties &physical_device_memory_properties)
{
    destroy(vulkan_device);
    auto mesh_count = m_horizontal_mesh_count * m_vertical_mesh_count;
    m_vertex_buffers.resize(mesh_count);
    m_staging_vertex_buffers.resize(mesh_count);
    m_index_buffers.resize(mesh_count);
    m_staging_index_buffers.resize(mesh_count);
    VkDeviceSize vertex_buffer_size = m_mesh_width * m_mesh_height * sizeof(Jade::Terrain_vertex);
    // Each terrain sub mesh is going to be rendered using triangle strips. Each sub mesh is (m_mesh_width - 1)
    // quads wide and (m_mesh_height - 1) quads high.
    // triangle_count = index_count - 2
    // index_count = triangle_count + 2
    auto quad_count_per_row = m_mesh_width - 1;
    auto triangle_count_per_row = quad_count_per_row * 2;
    auto index_count_per_row = triangle_count_per_row + 2;
    auto vertical_quad_count = m_mesh_height - 1;
    auto index_count = index_count_per_row * vertical_quad_count;
    // Add one primitive restart index for every row of quads except the last.
    if (vertical_quad_count > 1)
    {
        index_count += (vertical_quad_count - 1);
    }
    
    m_mesh_index_count = static_cast<uint32_t>(index_count);
    auto index_size = sizeof(uint16_t);
    auto index_buffer_size = index_count * index_size;
    for (std::size_t i = 0; i != mesh_count; ++i)
    {
        auto result = Jade::Vulkan_utility::create_buffer(vulkan_device,
            physical_device_memory_properties,
            VK_BUFFER_USAGE_VERTEX_BUFFER_BIT|VK_BUFFER_USAGE_TRANSFER_DST_BIT,
            VK_MEMORY_PROPERTY_DEVICE_LOCAL_BIT,
            vertex_buffer_size,
            m_vertex_buffers.at(i));
        if (!result)
        {
            return false;
        }

        result = Jade::Vulkan_utility::create_buffer(vulkan_device,
            physical_device_memory_properties,
            VK_BUFFER_USAGE_TRANSFER_SRC_BIT,
            VK_MEMORY_PROPERTY_HOST_VISIBLE_BIT,
            vertex_buffer_size,
            m_staging_vertex_buffers.at(i));
        if (!result)
        {
            return false;
        }
        
        result = Jade::Vulkan_utility::create_buffer(vulkan_device,
            physical_device_memory_properties,
            VK_BUFFER_USAGE_INDEX_BUFFER_BIT|VK_BUFFER_USAGE_TRANSFER_DST_BIT,
            VK_MEMORY_PROPERTY_DEVICE_LOCAL_BIT,
            static_cast<VkDeviceSize>(index_buffer_size),
            m_index_buffers.at(i));
        if (!result)
        {
            return false;
        }

        result = Jade::Vulkan_utility::create_buffer(vulkan_device,
            physical_device_memory_properties,
            VK_BUFFER_USAGE_TRANSFER_SRC_BIT,
            VK_MEMORY_PROPERTY_HOST_VISIBLE_BIT,
            index_buffer_size,
            m_staging_index_buffers.at(i));
        if (!result)
        {
            return false;
        }
    }
    
    return true;
}

// command_buffer: Assumes that vkBeginCommandBuffer has already been called on command_buffer.
//
// copy_buffers should be called after build_buffers.
bool Terrain::copy_buffers(VkDevice vulkan_device, VkQueue graphics_queue, VkCommandBuffer command_buffer)
{
    std::size_t buffer_index{0};
    m_bounding_volumes.clear();
    for (std::size_t mesh_index_z = 0; mesh_index_z != m_vertical_mesh_count; ++mesh_index_z)
    {
        for (std::size_t mesh_index_x = 0; mesh_index_x != m_horizontal_mesh_count; ++mesh_index_x)
        {
            // Copy the vertex data for the current terrain mesh to device memory.
            std::size_t begin_z = mesh_index_z * (m_mesh_height - 1);
            std::size_t begin_x = mesh_index_x * (m_mesh_width - 1);
            std::size_t end_z = begin_z + m_mesh_height;
            std::size_t end_x = begin_x + m_mesh_width;
            void *data{nullptr};
            const auto &vertex_buffer = m_vertex_buffers.at(buffer_index);
            const auto &staging_vertex_buffer = m_staging_vertex_buffers.at(buffer_index);
            auto vk_result = vkMapMemory(vulkan_device,
                staging_vertex_buffer.m_device_memory,
                0,
                vertex_buffer.m_memory_requirements.size,
                0,
                &data);
            if (VK_SUCCESS != vk_result)
            {
                return false;
            }
            
            const auto maximum_float = std::numeric_limits<float>::max();
            const auto minimum_float = std::numeric_limits<float>::min();
            // volume will store an AABB for the current terrain mesh
            Jade::Volume volume{glm::vec3{maximum_float}, glm::vec3{minimum_float}};
            Jade::Terrain_vertex *terrain_data = reinterpret_cast<Jade::Terrain_vertex*>(data);
            for (std::size_t z = begin_z; z != end_z; ++z)
            {
                for (std::size_t x = begin_x; x != end_x; ++x)
                {
                    const auto &terrain_vertex = m_data.at((z * m_width) + x);
                    const auto &position = terrain_vertex.m_position;
                    // Update volume (AABB) data if necessary
                    for (glm::vec3::length_type i = 0; i != position.length(); ++i)
                    {
                        if (position[i] < volume.m_minimum[i])
                        {
                            volume.m_minimum[i] = position[i];
                        }

                        if (position[i] > volume.m_maximum[i])
                        {
                            volume.m_maximum[i] = position[i];
                        }
                    }
                    
                    *terrain_data = terrain_vertex;
                    ++terrain_data;
                }
            }
            
            m_bounding_volumes.push_back(volume);
            VkMappedMemoryRange mapped_memory_range = {};
            mapped_memory_range.sType = VK_STRUCTURE_TYPE_MAPPED_MEMORY_RANGE;
            mapped_memory_range.pNext = nullptr;
            mapped_memory_range.memory = staging_vertex_buffer.m_device_memory;
            mapped_memory_range.offset = 0;
            mapped_memory_range.size = vertex_buffer.m_memory_requirements.size;
            vk_result = vkFlushMappedMemoryRanges(vulkan_device, 1, &mapped_memory_range);
            if (VK_SUCCESS != vk_result)
            {
                return false;
            }

            vkUnmapMemory(vulkan_device, staging_vertex_buffer.m_device_memory);
            VkBufferCopy buffer_copy = {};
            buffer_copy.srcOffset = 0;
            buffer_copy.dstOffset = 0;
            buffer_copy.size = vertex_buffer.m_memory_requirements.size;
            vkCmdCopyBuffer(command_buffer, staging_vertex_buffer.m_buffer, vertex_buffer.m_buffer, 1, &buffer_copy);
            VkBufferMemoryBarrier buffer_memory_barrier = {};
            buffer_memory_barrier.sType = VK_STRUCTURE_TYPE_BUFFER_MEMORY_BARRIER;
            buffer_memory_barrier.pNext = nullptr;
            buffer_memory_barrier.srcAccessMask = VK_ACCESS_TRANSFER_WRITE_BIT;
            buffer_memory_barrier.dstAccessMask = VK_ACCESS_VERTEX_ATTRIBUTE_READ_BIT;
            buffer_memory_barrier.srcQueueFamilyIndex = VK_QUEUE_FAMILY_IGNORED;
            buffer_memory_barrier.dstQueueFamilyIndex = VK_QUEUE_FAMILY_IGNORED;
            buffer_memory_barrier.buffer = vertex_buffer.m_buffer;
            buffer_memory_barrier.offset = 0;
            buffer_memory_barrier.size = vertex_buffer.m_memory_requirements.size;
            vkCmdPipelineBarrier(command_buffer,
                VK_PIPELINE_STAGE_TRANSFER_BIT,
                VK_PIPELINE_STAGE_VERTEX_INPUT_BIT,
                0,
                0,
                nullptr,
                1,
                &buffer_memory_barrier,
                0,
                nullptr);
            std::vector<uint16_t> indices;
            // Copy the index data for the current terrain mesh to device memory.
            const auto vertical_quad_count = m_mesh_height - 1;
            for (std::size_t z = 0; z != vertical_quad_count; ++z)
            {
                for (std::size_t x = 0; x != m_mesh_width; ++x)
                {
                    // Example (if m_mesh_width = 3, m_mesh_height = 3)
                    // vertex_buffer = { v0, v1, v2, z = 0 * z_scale
                    //                   v3, v4, v5, z = 1 * z_scale
                    //                   v6, v7, v8 } z = 2 * z_scale
                    // In world space the vertices are laid out like this:
                    //
                    // 2 * z_scale: v6 v7 v8
                    // 1 * z_scale: v3 v4 v5
                    // 0 * z_scale: v0 v1 v2
                    // indices = { v0, v3, v1, v4, v2, v5, primitive_restart, v3, v6, v4, v7, v5, v8 }
                    // (v0, v3, v1) is CW. The terrain pipeline treats triangles with a CW winding as front facing.
                    // (v3, v1, v4) is CCW, which is also correct because the terrain pipeline topology is
                    // VK_PRIMITIVE_TOPOLOGY_TRIANGLE_STRIP.
                    //
                    // When reading the code be careful not to get confused between the winding order of triangles in
                    // the vertex buffer, and the winding order of triangles in world space. For example, (v0, v3, v1)
                    // is CCW in the vertex buffer but CW in world space. The world space winding order is the correct
                    // winding order.
                    std::size_t index_0 = (z * m_mesh_width) + x;
                    std::size_t index_1 = ((z + 1) * m_mesh_width) + x;
                    indices.push_back(static_cast<uint16_t>(index_0));
                    indices.push_back(static_cast<uint16_t>(index_1));
                }

                // Add one primitive restart index for every row of quads except the last.
                if ((vertical_quad_count - 1) != z)
                {
                    auto primitive_restart_index = std::numeric_limits<uint16_t>::max();
                    indices.push_back(primitive_restart_index);
                }
            }
            
            assert(m_mesh_index_count == static_cast<uint32_t>(indices.size()));
            const auto &index_buffer = m_index_buffers.at(buffer_index);
            const auto &staging_index_buffer = m_staging_index_buffers.at(buffer_index);
            vk_result = vkMapMemory(vulkan_device,
                staging_index_buffer.m_device_memory,
                0,
                index_buffer.m_memory_requirements.size,
                0,
                &data);
            if (VK_SUCCESS != vk_result)
            {
                return false;
            }

            std::memcpy(data, indices.data(), static_cast<std::size_t>(sizeof(uint16_t) * indices.size()));
            mapped_memory_range = {};
            mapped_memory_range.sType = VK_STRUCTURE_TYPE_MAPPED_MEMORY_RANGE;
            mapped_memory_range.pNext = nullptr;
            mapped_memory_range.memory = staging_index_buffer.m_device_memory;
            mapped_memory_range.offset = 0;
            mapped_memory_range.size = index_buffer.m_memory_requirements.size;
            vk_result = vkFlushMappedMemoryRanges(vulkan_device, 1, &mapped_memory_range);
            if (VK_SUCCESS != vk_result)
            {
                return false;
            }

            vkUnmapMemory(vulkan_device, staging_index_buffer.m_device_memory);
            buffer_copy = {};
            buffer_copy.srcOffset = 0;
            buffer_copy.dstOffset = 0;
            buffer_copy.size = index_buffer.m_memory_requirements.size;
            vkCmdCopyBuffer(command_buffer, staging_index_buffer.m_buffer, index_buffer.m_buffer, 1, &buffer_copy);
            buffer_memory_barrier = {};
            buffer_memory_barrier.sType = VK_STRUCTURE_TYPE_BUFFER_MEMORY_BARRIER;
            buffer_memory_barrier.pNext = nullptr;
            buffer_memory_barrier.srcAccessMask = VK_ACCESS_TRANSFER_WRITE_BIT;
            buffer_memory_barrier.dstAccessMask = VK_ACCESS_VERTEX_ATTRIBUTE_READ_BIT;
            buffer_memory_barrier.srcQueueFamilyIndex = VK_QUEUE_FAMILY_IGNORED;
            buffer_memory_barrier.dstQueueFamilyIndex = VK_QUEUE_FAMILY_IGNORED;
            buffer_memory_barrier.buffer = index_buffer.m_buffer;
            buffer_memory_barrier.offset = 0;
            buffer_memory_barrier.size = index_buffer.m_memory_requirements.size;
            vkCmdPipelineBarrier(command_buffer,
                VK_PIPELINE_STAGE_TRANSFER_BIT,
                VK_PIPELINE_STAGE_VERTEX_INPUT_BIT,
                0,
                0,
                nullptr,
                1,
                &buffer_memory_barrier,
                0,
                nullptr);
            ++buffer_index;
        }
    }
    
    assert(m_bounding_volumes.size() == m_vertex_buffers.size());
    return true;
}

void Terrain::destroy(VkDevice device)
{
    assert(m_vertex_buffers.size() == m_index_buffers.size());
    for (std::size_t i = 0; i != m_vertex_buffers.size(); ++i)
    {
        auto &vertex_buffer = m_vertex_buffers.at(i);
        vertex_buffer.destroy(device);
        auto &index_buffer = m_index_buffers.at(i);
        index_buffer.destroy(device);
        auto &staging_vertex_buffer = m_staging_vertex_buffers.at(i);
        staging_vertex_buffer.destroy(device);
        auto &staging_index_buffer = m_staging_index_buffers.at(i);
        staging_index_buffer.destroy(device);
    }

    m_vertex_buffers.clear();
    m_staging_vertex_buffers.clear();
    m_index_buffers.clear();
    m_staging_index_buffers.clear();
}

void Terrain::draw(VkCommandBuffer command_buffer, Camera *camera) const
{
    for (std::size_t i = 0; i != m_index_buffers.size(); ++i)
    {
        const auto &volume = m_bounding_volumes.at(i);
        if (camera->bounds_in_frustum(volume.m_minimum, volume.m_maximum))
        {
            const auto &vertex_buffer = m_vertex_buffers.at(i);
            const auto &index_buffer = m_index_buffers.at(i);
            const VkDeviceSize offset{0};
            vkCmdBindVertexBuffers(command_buffer, 0, 1, &vertex_buffer.m_buffer, &offset);
            vkCmdBindIndexBuffer(command_buffer, index_buffer.m_buffer, 0, index_type);
            vkCmdDrawIndexed(command_buffer, m_mesh_index_count, 1, 0, 0, 0);
        }
    }
}

// world_position.x: x position in world space
// world_position.y: z position in world space
float Terrain::calculate_height(const glm::vec2 &world_position) const
{
    // Example. Let m_width = 3, m_height = 3, m_scale_vector = <8, 4, 8>
    // 
    // row 2 z = 16: v0 v1 v2
    // row 1 z = 8 : v0 v1 v2
    // row 0 z = 0 : v0 v1 v2
    auto x = world_position.x / m_scale_vector.x;
    auto z = world_position.y / m_scale_vector.z;
    if ((x < 0.0f) || (z < 0.0f) || (x > static_cast<float>(m_width - 1)) || (z > static_cast<float>(m_height - 1)))
    {
        return 0.0f;
    }
    
    auto x_size_t = static_cast<std::size_t>(x);
    auto z_size_t = static_cast<std::size_t>(z);
    // Example. Let x = 1.6. Then x_size_t = 1 (casting x to std::size_t truncates the fractional part of x) and
    // percent_x = 0.6
    auto percent_x = x - static_cast<float>(x_size_t);
    auto percent_z = z - static_cast<float>(z_size_t);
    // vertex_buffer = { v0, v1, v2, z = 0 * z_scale
    //                   v3, v4, v5, z = 1 * z_scale
    //                   v6, v7, v8 } z = 2 * z_scale
    // In world space the vertices are laid out like this:
    //
    // 2 * z_scale: v6 v7 v8
    // 1 * z_scale: v3 v4 v5
    // 0 * z_scale: v0 v1 v2
    // indices = { v0, v3, v1, v4, v2, v5, primitive_restart, v3, v6, v4, v7, v5, v8 }
    // (v0, v3, v1) is CW. The terrain pipeline treats triangles with a CW winding as front facing.
    // (v3, v1, v4) is CCW, which is also correct because the terrain pipeline topology is
    // VK_PRIMITIVE_TOPOLOGY_TRIANGLE_STRIP.
    std::size_t top_left_index = x_size_t + (z_size_t * m_width);
    std::size_t top_right_index = (x_size_t + 1) + (z_size_t * m_width);
    std::size_t bottom_left_index = x_size_t + ((z_size_t + 1) * m_width);
    std::size_t bottom_right_index = (x_size_t + 1) + ((z_size_t + 1) * m_width);
    const auto &top_left = m_data.at(bottom_left_index);
    const auto &top_right = m_data.at(bottom_right_index);
    const auto &bottom_left = m_data.at(top_left_index);
    const auto &bottom_right = m_data.at(top_right_index);
    auto top_height = top_left.m_position.y + percent_x * (top_right.m_position.y - top_left.m_position.y);
    auto bottom_height = bottom_left.m_position.y + percent_x * (bottom_right.m_position.y - bottom_left.m_position.y);
    return bottom_height + percent_z * (top_height - bottom_height);
}

// Generate a normal for the vertex at position (row_index, column_index) in m_data. Each vertex in m_data should
// contain a valid position before calling generate_normal.
glm::vec3 Terrain::generate_normal(std::size_t row_index, std::size_t column_index) const
{
    int add_row{};
    if (row_index < (m_height - 1))
    {
        add_row = 1;
    }
    else
    {
        add_row = -1;
    }

    int add_column{};
    if (column_index < (m_width - 1))
    {
        add_column = 1;
    }
    else
    {
        add_column = -1;
    }

    // If row_index = 0 and column_index = 0, then:
    // v0 v1
    // v2
    auto index_0 = (row_index * m_width) + column_index;
    auto row_index_int = static_cast<int>(row_index);
    auto column_index_int = static_cast<int>(column_index);
    auto width_int = static_cast<int>(m_width);
    auto index_1 = static_cast<std::size_t>((row_index_int * width_int) + column_index_int + add_column);
    auto index_2 = static_cast<std::size_t>(((row_index_int + add_row) * width_int) + column_index_int);
    const auto &v0 = m_data.at(index_0);
    const auto &v1 = m_data.at(index_1);
    const auto &v2 = m_data.at(index_2);
    auto edge0 = v2.m_position - v0.m_position;
    auto edge1 = v1.m_position - v0.m_position;
    auto normal = glm::cross(edge0, edge1);
    return glm::normalize(normal);
}

}