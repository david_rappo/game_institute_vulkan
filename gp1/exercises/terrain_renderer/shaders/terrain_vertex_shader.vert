#version 450
layout (location = 0) in vec3 in_position;
layout (location = 1) in vec3 in_normal;

layout (location = 0) out vec4 vs_out_color;

layout (push_constant) uniform Uniform_push_constant
{

// m_matrix = invert_y_matrix * projection_matrix * view_matrix * model_matrix
mat4 m_matrix;

} uniform_push_constant;

out gl_PerVertex
{

vec4 gl_Position;

};

// light_direction is a unit vector
const vec3 light_direction = vec3(0.650945f, -0.390567f, 0.650945f);
const vec3 terrain_color = vec3(1.0f, 0.8f, 0.6f);

void main()
{
    gl_Position = uniform_push_constant.m_matrix * vec4(in_position, 1.0f);
	float cos_q = dot(-light_direction, in_normal);
	cos_q = clamp(cos_q, 0.25f, 1.0f);
	vs_out_color = vec4(terrain_color * cos_q, 1.0f);
}