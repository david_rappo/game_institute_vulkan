#pragma once

// C++ Standard Library
#include <cstdlib>
#include <string>
#include <map>
#include <random>
#include <vector>

// GLFW
#include <GLFW/glfw3.h>

// GLM
#include <glm/mat4x4.hpp>
#include <glm/vec2.hpp>
#include <glm/vec4.hpp>

// Jade
#include "buffer.h"
#include "frame.h"
#include "image.h"
#include "mesh.h"
#include "player.h"
#include "terrain.h"
#include "timer.h"
#include "vertex.h"

namespace Jade
{

class Stb_image;

}

namespace Jade
{

class Application
{
public:
    Application();
    ~Application();
    bool initialize(const std::vector<std::string> &arguments);
    void run();

private:
    static const std::string texture_path;
    static const uint32_t command_buffer_count{3};

    static glm::vec3 random_color(std::uniform_real_distribution<float> &distribution,
        std::default_random_engine &engine);
    static void glfw_error_callback(int error, const char *description);
    static void glfw_window_close_callback(GLFWwindow *window);
    static void glfw_frame_buffer_size_callback(GLFWwindow *window, int width, int height);
    static void glfw_key_callback(GLFWwindow *window, int key, int scan_code, int action, int modifiers);
    static void glfw_mouse_button_callback(GLFWwindow *window, int button, int action, int modifiers);
    static void glfw_mouse_cursor_position_callback(GLFWwindow *window, double x, double y);
    static bool check_instance_layers(const std::vector<std::string> &instance_layer_names);
    static bool check_instance_extensions(const std::vector<std::string> &extension_names);
    static bool check_device_extensions(VkPhysicalDevice device, const std::vector<std::string> &extension_names);
    static VkBool32 VKAPI_PTR vulkan_debug_report_callback(VkDebugReportFlagsEXT flags,
        VkDebugReportObjectTypeEXT object_type,
        uint64_t object,
        size_t location,
        int32_t message_code,
        const char *layer_prefix,
        const char *message,
        void *user_data);
    void key_callback(GLFWwindow *window, int key, int scan_code, int action, int modifiers);
    void mouse_button_callback(GLFWwindow *window, int button, int action, int modifiers);
    void mouse_cursor_position_callback(GLFWwindow *window, double x, double y);
    void frame_buffer_resized(int new_width, int new_height);
    bool create_instance(const std::vector<std::string> &instance_extensions, VkInstance &out_instance) const;
    bool check_physical_device(VkPhysicalDevice physical_device,
        const std::vector<std::string> &device_extensions,
        VkSurfaceKHR present_surface,
        uint32_t &out_graphics_queue_family_index,
        uint32_t &out_present_queue_family_index) const;
    bool create_debug_report_callback(VkInstance instance, VkDebugReportCallbackEXT &out_callback) const;
    bool create_device(VkPhysicalDevice &out_physical_device,
        VkDevice &out_vulkan_device,
        uint32_t &out_graphics_queue_family_index,
        uint32_t &out_present_queue_family_index) const;
    bool create_swapchain(
        uint32_t width,
        uint32_t height,
        VkSwapchainKHR &out_old_swapchain,
        VkFormat &out_swapchain_image_format,
        std::vector<VkImage> &out_swapchain_images,
        std::vector<VkImageView> &out_swapchain_image_views,
        VkSwapchainKHR &out_swapchain) const;
    bool create_render_pass(VkRenderPass &out_render_pass) const;
    bool create_framebuffer(VkDevice device,
        VkRenderPass render_pass,
        uint32_t width,
        uint32_t height,
        VkImageView image_view,
        VkImageView depth_stencil_view,
        VkFramebuffer &out_framebuffer) const;
    bool create_shader_modules(const std::vector<std::string> &shader_file_names,
        std::vector<VkShaderModule> &out_shader_modules) const;
    bool create_terrain_pipeline(VkPipelineLayout &out_pipeline_layout, VkPipeline &out_pipeline) const;
    bool create_player_mesh_pipeline(VkPipelineLayout &out_pipeline_layout, VkPipeline &out_pipeline) const;
    bool create_semaphores(uint32_t create_count, std::vector<VkSemaphore> &out_semaphores) const;
    bool create_fences(uint32_t create_count, std::vector<VkFence> &out_vec_fence) const;
    bool create_command_pool(VkCommandPool &out_command_pool) const;
    bool allocate_command_buffers(uint32_t count, std::vector<VkCommandBuffer> &out_command_buffers) const;
    bool record_command_buffer(uint32_t framebuffer_width,
        uint32_t framebuffer_height,
        VkFramebuffer framebuffer,
        VkImage swapchain_image,
        VkCommandBuffer command_buffer);
    bool draw(uint32_t framebuffer_width, uint32_t framebuffer_height, Jade::Frame &out_frame);
    bool recreate_swap_chain(uint32_t framebuffer_width, uint32_t framebuffer_height);
    bool create_2d_image(VkDevice device,
        const VkPhysicalDeviceMemoryProperties &physical_device_memory_properties,
        VkFormat image_format, // e.g. VK_FORMAT_R8G8B8A8_UNORM
        VkImageTiling image_tiling, // e.g. VK_IMAGE_TILING_OPTIMAL
        VkImageUsageFlags image_usage_flags, // e.g. VK_IMAGE_USAGE_TRANSFER_DST_BIT|VK_IMAGE_USAGE_SAMPLED_BIT
        VkImageAspectFlags image_aspect_flags, // e.g. VK_IMAGE_ASPECT_COLOR_BIT
        VkMemoryPropertyFlags memory_property_flags, // e.g. VK_MEMORY_PROPERTY_DEVICE_LOCAL_BIT
        uint32_t image_width,
        uint32_t image_height,
        Jade::Image &out_image) const;
    bool create_depth_stencil_image(VkDevice device,
        const VkPhysicalDeviceMemoryProperties &physical_device_memory_properties,
        VkFormat image_format,
        uint32_t image_width,
        uint32_t image_height,
        Jade::Image &out_image) const;
    void process_input();
    void animate_objects();
    bool is_mouse_button_pressed(int button) const;
    bool create_player_mesh_buffers();
    bool copy_player_mesh_buffers(VkCommandBuffer command_buffer);
    bool find_depth_stencil_image_format(VkFormat &out_format) const;

    // begin: see Application()
    std::default_random_engine m_random_engine;
    std::uniform_real_distribution<float> m_distribution;
    glm::mat4 m_invert_y_matrix{};
    glm::mat4 m_view_matrix{};
    std::vector<Jade::Vertex> m_player_mesh_vertices;
    std::vector<uint16_t> m_player_mesh_indices;
    // end: see Application()
    Player m_player;
    std::string m_window_title;
    VkFormat m_depth_stencil_image_format{};
    bool m_window_should_close{false};
    GLFWwindow *m_glfw_window{nullptr};
    std::string m_application_name{"Jade"};
    std::string m_engine_name{"Jade"};
    std::vector<std::string> m_instance_layer_names{"VK_LAYER_LUNARG_standard_validation"};
    std::vector<std::string> m_device_extension_names{"VK_KHR_swapchain"};
    uint32_t m_graphics_queue_family_index{};
    uint32_t m_present_queue_family_index{};
    VkInstance m_vulkan_instance{nullptr};
    VkSurfaceKHR m_surface{nullptr};
    // The application does not need to free/destroy the physical device
    VkPhysicalDevice m_physical_device{nullptr};
    VkDevice m_vulkan_device{nullptr};
    // The application does not need to free/destroy the queues
    VkQueue m_graphics_queue{nullptr};
    VkQueue m_present_queue{nullptr};
    VkFormat m_swapchain_image_format{};
    VkSwapchainKHR m_swapchain{nullptr};
    // The application does not need to free/destroy the swapchain images
    std::vector<VkImage> m_swapchain_images;
    std::vector<VkImageView> m_swapchain_image_views;
    VkRenderPass m_render_pass{nullptr};
    std::vector<VkShaderModule> m_terrain_shader_modules;
    std::vector<VkShaderModule> m_player_mesh_shader_modules;
    VkPipelineLayout m_terrain_pipeline_layout{nullptr};
    VkPipeline m_terrain_pipeline{nullptr};
    VkPipelineLayout m_player_mesh_pipeline_layout{nullptr};
    VkPipeline m_player_mesh_pipeline{nullptr};
    VkCommandPool m_command_pool{nullptr};
    VkPhysicalDeviceMemoryProperties m_physical_device_memory_properties{};
    VkPhysicalDeviceProperties m_physical_device_properties{};
    std::vector<Jade::Frame> m_vec_frame;
    Jade::Image m_depth_stencil;
    Jade::Timer m_timer;
    // Key = GLFW identifier of keyboard key
    // Value = state
    std::map<int, int> m_key_map;
    Jade::Terrain m_terrain;
    Jade::Mesh m_player_mesh;
    glm::dvec2 m_previous_mouse_cursor_position{0.0, 0.0};
    // Updated in Application::mouse_cursor_position_callback if the left mouse button is pressed.
    glm::dvec2 m_mouse_cursor_shift_vector{0.0};
    // Key = button e.g. GLFW_MOUSE_BUTTON_LEFT
    // Value = state e.g. GLFW_PRESS
    std::map<int, int> m_mouse_button_map;
#ifdef _DEBUG
    // Destroyed in ~Application
    VkDebugReportCallbackEXT m_debug_report_callback{nullptr};
#endif
};

}
