#pragma once

// GLM
#include <glm/vec3.hpp>

namespace Jade
{

// Vertex is used to draw the cube representing the player in third person camera mode.
class Vertex
{
public:
    Vertex() = default;
    Vertex(const glm::vec3 &position, const glm::vec3 &color): m_position{position}, m_color{color}
    {

    }
    
    glm::vec3 m_position;
    glm::vec3 m_color;
};

}