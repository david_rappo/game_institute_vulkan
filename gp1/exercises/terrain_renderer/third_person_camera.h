#pragma once

// Jade
#include "camera.h"

// GLM
#include <glm/vec3.hpp>

namespace Jade
{

class Player;

}

namespace Jade
{

class Third_person_camera : public Camera
{
public:
    Third_person_camera();
    explicit Third_person_camera(const Camera *camera);
    Type get_type() const override;
    void rotate(float x, float y, float z, const Player &player) override;
    void update(float time_elapsed, float camera_lag, const Player &player) override;
    void move(const glm::vec3 &v) override;
    void look_at(const glm::vec3 &at, const glm::vec3 &up);
};

}