readme.txt

Application: terrain_renderer

About

Rewrite of Exercise 3.4 Simple Terrain Renderer from Graphics Programming with DirectX 9 Part 1
(https://www.gameinstitute.com/) using Vulkan.

Constructs N terrain meshes from a 257 x 257 height map in raw format. The terrain meshes are rendered as indexed
triangle strips. The terrain is lit using vertex lighting. There is a single static directional light in the scene.

Controls

The player can switch between three different camera modes:

- First person (press the '1' key)
- Third person. In third person mode the player is represented as a coloured cube (press the '2' key)
- Space craft (press the '3' key)

Keyboard Controls

Strafe left: left arrow key
Strafe right: right arrow key
Forward: up arrow key
Back: down arrow key

Mouse Controls

Rotate

Hold down the left mouse button only. Vertical movements pitch the camera. Horizontal movements yaw the camera.
Hold down the left mouse button and the right mouse button. Vertical movements pitch the camera. Horizontal movements
roll the camera.