#pragma once

// Vulkan
#include "vulkan/vulkan.h"

namespace Jade
{

class Buffer
{
public:
    void destroy(VkDevice device);

    VkBuffer m_buffer{nullptr};
    VkDeviceMemory m_device_memory{nullptr};
    // This is the size used to create m_buffer e.g. VkBufferCreateInfo::size. m_memory_requirements is initialized by
    // calling vkGetBufferMemoryRequirements. m_memory_requirements.size is likely to be larger than m_size.
    VkDeviceSize m_size{};
    VkMemoryRequirements m_memory_requirements{};
};

}
