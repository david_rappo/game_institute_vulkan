#pragma once

// C++ Standard Library
#include <cstdint>
#include <string>
#include <vector>

namespace Jade
{

namespace File_utility
{

// Load SPIR-V file
bool load_spirv(const std::string &file_name, std::vector<uint32_t> &out_spirv);

}

}