// This is a personal academic project. Dear PVS-Studio, please check it.
// PVS-Studio Static Code Analyzer for C, C++ and C#: http://www.viva64.com
// C++ Standard Library
#include <cassert>
#include <cstddef>
#include <cstdlib>
#include <iostream>
#include <string>
#include <vector>

// Windows
// Windows.h must be included before glfw3.h
#include <Windows.h>
#include <GLFW/glfw3.h>

// Jade
#include "application.h"
#include "unicode_utility.h"

int CALLBACK WinMain(_In_ HINSTANCE hInstance,
    _In_ HINSTANCE hPrevInstance,
    _In_ LPSTR lpCmdLine,
    _In_ int nCmdShow)
{
    auto command_line = GetCommandLineW();
    int number_of_arguments{0};
    auto argv = CommandLineToArgvW(command_line, &number_of_arguments);
    assert(number_of_arguments > -1);
    std::vector<std::wstring> wide_arguments(argv, argv + static_cast<std::ptrdiff_t>(number_of_arguments));
    std::vector<std::string> arguments;
    for (auto iter = wide_arguments.cbegin(); iter != wide_arguments.cend(); ++iter)
    {
        auto str = Jade::Unicode::wide_to_multi_byte(*iter);
        arguments.push_back(str);
    }

    Jade::Application application;
    auto result = application.initialize(arguments);
    if (!result)
    {
        return 0;
    }

    application.run();
    return 0;
}