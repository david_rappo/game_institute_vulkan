// This is a personal academic project. Dear PVS-Studio, please check it.
// PVS-Studio Static Code Analyzer for C, C++ and C#: http://www.viva64.com
#include "application.h"

// C++ Standard Library
#include <algorithm>
#include <array>
#include <bitset>
#include <cassert>
#include <chrono>
#include <cinttypes>
#include <cstdio>
#include <cstring>
#include <fstream>
#include <iterator>
#include <limits>
#include <memory>

// GLM
#include <glm/gtc/matrix_transform.hpp>
#include <glm/gtc/type_ptr.hpp>
#include <glm/vec2.hpp>
#include <glm/vec3.hpp>

// Jade
#include "file_utility.h"
#include "log_manager.h"
#include "math_utility.h"
#include "stb_image_wrapper.h"

namespace Jade
{

Application::Application(): //-V730
    m_random_engine{static_cast<unsigned>(std::chrono::system_clock::now().time_since_epoch().count())},
    m_distribution{0.0f, 1.0f},
    m_invert_y_matrix{Jade::Math_utility::invert_y_matrix()},
    m_cube{create_cube()},
    m_model_matrices{glm::translate(glm::mat4{}, glm::vec3{-3.5f, 2.0f, 14.0f}),
        glm::translate(glm::mat4{}, glm::vec3{3.5f, -2.0f, 14.0f})},
    m_animate{true, true}
{

}

Application::~Application()
{
    // Destroy resources in reverse order of creation
    // Creation order
    // m_glfw_window
    // m_vulkan_instance
    // m_surface
    // m_debug_report_callback
    // m_vulkan_device
    // m_descriptor_pool
    // m_descriptor_set_layout
    // m_descriptor_set
    // m_swap_chain
    // m_swap_chain_image_views
    // m_render_pass
    // m_shader_modules
    // m_pipeline_layout
    // m_pipeline
    // m_vertex_buffer
    // m_uniform_buffer
    // m_staging_buffer
    // m_depth_stencil
    // m_command_pool
    // m_vec_frame
    if (nullptr != m_vulkan_device)
    {
        vkDeviceWaitIdle(m_vulkan_device);
        for (auto iter = m_vec_frame.cbegin(); iter != m_vec_frame.cend(); ++iter)
        {
            if (nullptr != m_command_pool)
            {
                iter->destroy(m_vulkan_device, m_command_pool);
            }
        }
        
        if (nullptr != m_command_pool)
        {
            vkDestroyCommandPool(m_vulkan_device, m_command_pool, nullptr);
        }

        m_depth_stencil.destroy(m_vulkan_device);
        m_staging_buffer.destroy(m_vulkan_device);
        m_uniform_buffer.destroy(m_vulkan_device);
        m_vertex_buffer.destroy(m_vulkan_device);
        if (nullptr != m_pipeline)
        {
            vkDestroyPipeline(m_vulkan_device, m_pipeline, nullptr);
        }
        
        if (nullptr != m_pipeline_layout)
        {
            vkDestroyPipelineLayout(m_vulkan_device, m_pipeline_layout, nullptr);
        }

        for (auto iter = m_shader_modules.cbegin(); iter != m_shader_modules.cend(); ++iter)
        {
            vkDestroyShaderModule(m_vulkan_device, *iter, nullptr);
        }
        
        if (nullptr != m_render_pass)
        {
            vkDestroyRenderPass(m_vulkan_device, m_render_pass, nullptr);
        }
        
        for (auto iter = m_swapchain_image_views.cbegin(); iter != m_swapchain_image_views.cend(); ++iter)
        {
            auto image_view = *iter;
            if (nullptr != image_view)
            {
                vkDestroyImageView(m_vulkan_device, image_view, nullptr);
            }
        }

        if (nullptr != m_swapchain)
        {
            vkDestroySwapchainKHR(m_vulkan_device, m_swapchain, nullptr);
        }

        if (nullptr != m_descriptor_set)
        {
            assert(nullptr != m_descriptor_pool);
            // It is not necessary to call vkFreeDescriptorSets unless VkDescriptorPoolCreateInfo.flags contains
            // VK_DESCRIPTOR_POOL_CREATE_FREE_DESCRIPTOR_SET_BIT
            // vkFreeDescriptorSets(m_vulkan_device, m_descriptor_pool, 1, &m_descriptor_set);
        }

        if (nullptr != m_descriptor_set_layout)
        {
            vkDestroyDescriptorSetLayout(m_vulkan_device, m_descriptor_set_layout, nullptr);
        }
        
        if (nullptr != m_descriptor_pool)
        {
            vkDestroyDescriptorPool(m_vulkan_device, m_descriptor_pool, nullptr);
        }
        
        vkDestroyDevice(m_vulkan_device, nullptr);
    }
#ifdef _DEBUG
    if (nullptr != m_debug_report_callback)
    {
        if (nullptr != m_vulkan_instance)
        {
            auto proc_address = glfwGetInstanceProcAddress(m_vulkan_instance,
                "vkDestroyDebugReportCallbackEXT");
            if (nullptr != proc_address)
            {
                auto Jade_vkDestroyDebugReportCallbackEXT = reinterpret_cast<PFN_vkDestroyDebugReportCallbackEXT>(proc_address);
                Jade_vkDestroyDebugReportCallbackEXT(m_vulkan_instance,
                    m_debug_report_callback,
                    nullptr);
            }
        }
    }
#endif
    if (nullptr != m_vulkan_instance)
    {
        if (nullptr != m_surface)
        {
            vkDestroySurfaceKHR(m_vulkan_instance, m_surface, nullptr);
        }
        
        vkDestroyInstance(m_vulkan_instance, nullptr);
    }

    if (nullptr != m_glfw_window)
    {
        glfwDestroyWindow(m_glfw_window);
    }

    glfwTerminate();
}

bool Application::initialize(const std::vector<std::string> &arguments)
{
    // It is OK to call glfwSetErrorCallback before glfwInit
    glfwSetErrorCallback(Application::glfw_error_callback);
    if (GLFW_TRUE != glfwInit())
    {
        return false;
    }

    auto title = m_application_name;
    if (!arguments.empty())
    {
        title = arguments.at(0);
    }

    glfwWindowHint(GLFW_CLIENT_API, GLFW_NO_API);
    glfwWindowHint(GLFW_VISIBLE, GLFW_FALSE);
    m_glfw_window = glfwCreateWindow(800, 600, title.c_str(), nullptr, nullptr);
    if (nullptr == m_glfw_window)
    {
        return false;
    }

    glfwSetWindowUserPointer(m_glfw_window, this);
    glfwSetWindowCloseCallback(m_glfw_window, Application::glfw_window_close_callback);
    glfwSetFramebufferSizeCallback(m_glfw_window, Application::glfw_frame_buffer_size_callback);
    std::vector<std::string> instance_extension_names;
    uint32_t glfw_required_instance_extensions_count{};
    // The returned array will always contain VK_KHR_surface.
    auto glfw_required_instance_extensions = glfwGetRequiredInstanceExtensions(
        &glfw_required_instance_extensions_count);
    if (nullptr == glfw_required_instance_extensions)
    {
        return false;
    }

    std::copy(glfw_required_instance_extensions,
        glfw_required_instance_extensions + static_cast<ptrdiff_t>(glfw_required_instance_extensions_count),
        std::back_inserter(instance_extension_names));
    if (!create_instance(instance_extension_names, m_vulkan_instance))
    {
        return false;
    }

    if (VK_SUCCESS != glfwCreateWindowSurface(m_vulkan_instance, m_glfw_window, nullptr, &m_surface))
    {
        return false;
    }

    glfwSetKeyCallback(m_glfw_window, glfw_key_callback);
#ifdef _DEBUG
    if (!create_debug_report_callback(m_vulkan_instance, m_debug_report_callback))
    {
        return false;
    }
#endif
    if (!create_device(m_physical_device, m_vulkan_device, m_graphics_queue_family_index, m_present_queue_family_index))
    {
        return false;
    }

    vkGetDeviceQueue(m_vulkan_device, m_graphics_queue_family_index, 0, &m_graphics_queue);
    vkGetDeviceQueue(m_vulkan_device, m_present_queue_family_index, 0, &m_present_queue);
    vkGetPhysicalDeviceProperties(m_physical_device, &m_physical_device_properties);
    vkGetPhysicalDeviceMemoryProperties(m_physical_device, &m_physical_device_memory_properties);
    m_uniform_buffer_offset = determine_uniform_buffer_offset(
        m_physical_device_properties.limits.minUniformBufferOffsetAlignment);
    if (!create_descriptor_pool(m_descriptor_pool))
    {
        return false;
    }

    if (!create_descriptor_set_layout(m_descriptor_set_layout))
    {
        return false;
    }

    if (!allocate_descriptor_set(m_descriptor_set))
    {
        return false;
    }
    
    int width{};
    int height{};
    glfwGetFramebufferSize(m_glfw_window, &width, &height);
    auto old_swapchain = m_swapchain;
    assert(nullptr == old_swapchain);
    if (!create_swapchain(
        static_cast<uint32_t>(width),
        static_cast<uint32_t>(height),
        old_swapchain,
        m_swapchain_image_format,
        m_swapchain_images,
        m_swapchain_image_views,
        m_swapchain))
    {
        return false;
    }

    if (!find_depth_stencil_image_format(m_depth_stencil_image_format))
    {
        return false;
    }

    if (!create_buffer(VK_BUFFER_USAGE_VERTEX_BUFFER_BIT|VK_BUFFER_USAGE_TRANSFER_DST_BIT,
        VK_MEMORY_PROPERTY_DEVICE_LOCAL_BIT,
        sizeof(Jade::Vertex) * m_cube.size(),
        m_vertex_buffer))
    {
        return false;
    }
    
    {
        // descriptor_binding_model_matrix is VK_DESCRIPTOR_TYPE_UNIFORM_BUFFER_DYNAMIC. There are two matrices
        // associated with the descriptor.
        auto matrix_count =
            descriptor_type_uniform_buffer_count + (descriptor_type_uniform_buffer_dynamic_count * 2);
        auto size = m_uniform_buffer_offset * static_cast<VkDeviceSize>(matrix_count);
        if (!create_buffer(VK_BUFFER_USAGE_UNIFORM_BUFFER_BIT|VK_BUFFER_USAGE_TRANSFER_DST_BIT,
            VK_MEMORY_PROPERTY_DEVICE_LOCAL_BIT,
            size,
            m_uniform_buffer))
        {
            return false;
        }
    }

    {
        std::vector<VkDeviceSize> v;
        v.push_back(m_vertex_buffer.m_memory_requirements.size);
        v.push_back(m_uniform_buffer.m_memory_requirements.size);
        auto iter = std::max_element(v.cbegin(), v.cend());
        if (!create_buffer(VK_BUFFER_USAGE_TRANSFER_SRC_BIT,
            VK_MEMORY_PROPERTY_HOST_VISIBLE_BIT,
            *iter,
            m_staging_buffer))
        {
            return false;
        }
    }
    
    if (!create_render_pass(m_render_pass))
    {
        return false;
    }

    std::vector<std::string> file_names = { "vert.spv", "frag.spv" };
    if (!create_shader_modules(file_names, m_shader_modules))
    {
        return false;
    }
    
    std::vector<VkShaderStageFlagBits> stages = { VK_SHADER_STAGE_VERTEX_BIT, VK_SHADER_STAGE_FRAGMENT_BIT };
    if (!create_pipeline(stages, m_pipeline_layout, m_pipeline))
    {
        return false;
    }

    if (!create_command_pool(m_command_pool))
    {
        return false;
    }

    std::vector<VkCommandBuffer> vec_command_buffer;
    if (!allocate_command_buffers(command_buffer_count, vec_command_buffer))
    {
        return false;
    }

    for (std::size_t i = 0; i != vec_command_buffer.size(); ++i)
    {
        Jade::Frame frame;
        frame.m_command_buffer = vec_command_buffer.at(i);
        m_vec_frame.push_back(frame);
    }

    std::vector<VkSemaphore> semaphores;
    if (!create_semaphores(static_cast<uint32_t>(m_vec_frame.size() * 2), semaphores))
    {
        return false;
    }

    for (std::size_t i = 0; i != m_vec_frame.size(); ++i)
    {
        auto &frame = m_vec_frame.at(i);
        auto j = i * 2;
        frame.m_image_available_semaphore = semaphores.at(j);
        frame.m_rendering_finished_semaphore = semaphores.at(j + 1);
    }

    std::vector<VkFence> vec_fence;
    if (!create_fences(static_cast<uint32_t>(m_vec_frame.size()), vec_fence))
    {
        return false;
    }

    for (std::size_t i = 0; i != vec_fence.size(); ++i)
    {
        auto &frame = m_vec_frame.at(i);
        frame.m_fence = vec_fence.at(i);
    }

    {
        const auto &frame = m_vec_frame.at(0);
        if (!copy_vertex_data(frame.m_command_buffer))
        {
            return false;
        }
    }

    update_descriptor_sets();
    return true;
}

void Application::run()
{
    // Calling glfwShowWindow causes Application::frame_buffer_resized to be called.
    glfwShowWindow(m_glfw_window);
    std::size_t frame_index{};
    m_timer.start(glfwGetTime());
    while (!m_window_should_close)
    {
        m_timer.tick();
        int width{};
        int height{};
        glfwGetFramebufferSize(m_glfw_window, &width, &height);
        // Assume that the application is minimized if width or height is zero.
        bool is_minimized = ((0 == width) || (0 == height));
        if (!is_minimized)
        {
            process_input();
            animate_objects();
            if (!draw(static_cast<uint32_t>(width), static_cast<uint32_t>(height), m_vec_frame.at(frame_index)))
            {
                return;
            }

            ++frame_index;
            assert(m_vec_frame.size() == static_cast<std::size_t>(command_buffer_count));
            frame_index %= m_vec_frame.size();
        }
        
        glfwPollEvents();
    }
}

void Application::glfw_error_callback(int error, const char *description)
{
    auto &os = Log_manager::get_instance()->get_stream(Log_manager::Log_type::glfw_error);
    os << jade_function_signature << ") error: " << error << " description: " << description << std::endl;
    std::exit(0);
}

void Application::glfw_window_close_callback(GLFWwindow *window)
{
    auto user_data = glfwGetWindowUserPointer(window);
    if (nullptr != user_data)
    {
        auto application = reinterpret_cast<Application*>(user_data);
        application->m_window_should_close = true;
    }
}

void Application::glfw_frame_buffer_size_callback(GLFWwindow *window, int width, int height)
{
    auto user_data = glfwGetWindowUserPointer(window);
    if (nullptr != user_data)
    {
        auto application = reinterpret_cast<Application*>(user_data);
        application->frame_buffer_resized(width, height);
    }
}

void Application::glfw_key_callback(GLFWwindow *window, int key, int scan_code, int action, int modifiers)
{
    auto user_data = glfwGetWindowUserPointer(window);
    if (nullptr != user_data)
    {
        auto application = reinterpret_cast<Application*>(user_data);
        application->key_callback(window, key, scan_code, action, modifiers);
    }
}

// Returns true if all of the instance layers in instance_layer_names are supported.
bool Application::check_instance_layers(const std::vector<std::string> &instance_layer_names)
{
    uint32_t property_count{};
    auto result = vkEnumerateInstanceLayerProperties(&property_count, nullptr);
    if (VK_SUCCESS != result)
    {
        return false;
    }

    std::vector<VkLayerProperties> layer_properties(static_cast<std::size_t>(property_count));
    result = vkEnumerateInstanceLayerProperties(&property_count, layer_properties.data());
    if (VK_SUCCESS != result)
    {
        return false;
    }

    for (auto iter = instance_layer_names.cbegin(); iter != instance_layer_names.cend(); ++iter)
    {
        auto find_result = std::find_if(layer_properties.cbegin(),
            layer_properties.cend(),
            [iter](const VkLayerProperties &layer_property)
        { return 0 == std::strcmp(layer_property.layerName, iter->c_str()); });
        if (layer_properties.cend() == find_result)
        {
            return false;
        }
    }

    return true;
}

// Returns true if every extension in extension_names is supported by the Vulkan
// implementation.
bool Application::check_instance_extensions(const std::vector<std::string> &extension_names)
{
    uint32_t property_count{};
    auto result = vkEnumerateInstanceExtensionProperties(nullptr,
        &property_count,
        nullptr);
    if (VK_SUCCESS != result)
    {
        return false;
    }

    std::vector<VkExtensionProperties> properties(static_cast<std::size_t>(property_count));
    result = vkEnumerateInstanceExtensionProperties(nullptr,
        &property_count,
        properties.data());
    if (VK_SUCCESS != result)
    {
        return false;
    }

    for (auto iter = extension_names.cbegin(); iter != extension_names.cend(); ++iter)
    {
        auto j = std::find_if(properties.cbegin(),
            properties.cend(),
            [iter](const VkExtensionProperties &property) { return 0 == std::strcmp(iter->c_str(), property.extensionName); });
        if (properties.cend() == j)
        {
            return false;
        }
    }

    return true;
}

// Returns true if every extension in extension_names is supported by the Vulkan implementation.
bool Application::check_device_extensions(VkPhysicalDevice device,
    const std::vector<std::string> &extension_names)
{
    uint32_t property_count{};
    if (VK_SUCCESS != vkEnumerateDeviceExtensionProperties(device, nullptr, &property_count, nullptr))
    {
        return false;
    }

    std::vector<VkExtensionProperties> properties(static_cast<std::size_t>(property_count));
    if (VK_SUCCESS != vkEnumerateDeviceExtensionProperties(device, nullptr, &property_count, properties.data()))
    {
        return false;
    }

    for (auto iter = extension_names.cbegin(); iter != extension_names.cend(); ++iter)
    {
        auto j = std::find_if(properties.cbegin(),
            properties.cend(),
            [iter](const VkExtensionProperties &property) { return 0 == std::strcmp(property.extensionName, iter->c_str()); });
        if (properties.cend() == j)
        {
            return false;
        }
    }

    return true;
}

VkBool32 Application::vulkan_debug_report_callback(VkDebugReportFlagsEXT flags,
    VkDebugReportObjectTypeEXT object_type,
    uint64_t object,
    size_t location,
    int32_t message_code,
    const char *layer_prefix,
    const char *message,
    void *user_data)
{
    auto &os = Log_manager::get_instance()->get_stream(Log_manager::Log_type::vulkan_debug_report_all);
    os << "flags: " << flags << std::endl;
    os << "object_type: " << object_type << std::endl;
    os << "object: " << object << std::endl;
    os << "location: " << location << std::endl; //-V128
    os << "message_code: " << message_code << std::endl;
    os << "layer_prefix: " << layer_prefix << std::endl;
    os << message << std::endl;
    os << std::endl;
    switch (flags)
    {
    case VK_DEBUG_REPORT_WARNING_BIT_EXT:
    case VK_DEBUG_REPORT_PERFORMANCE_WARNING_BIT_EXT:
    case VK_DEBUG_REPORT_ERROR_BIT_EXT:
        std::exit(EXIT_FAILURE);
        break;
    }

    // Tell the calling layer not to abort the Vulkan call so that the
    // application behaves the same with and without validation layers enabled.
    return VK_FALSE;
}

// Find the first VkMemoryType required by memory_requirements that satisfies requirements e.g. must be host visible.
bool Application::find_memory_type(const VkPhysicalDeviceMemoryProperties &physical_device_memory_properties,
    const VkMemoryRequirements &memory_requirements,
    VkFlags requirements,
    uint32_t &memory_type_index)
{
    std::bitset<sizeof(std::uint32_t) * 8> b{static_cast<unsigned long long>(memory_requirements.memoryTypeBits)};
    for (uint32_t i = 0; i != physical_device_memory_properties.memoryTypeCount; ++i)
    {
        if (b[static_cast<std::size_t>(i)])
        {
            auto &memory_type = physical_device_memory_properties.memoryTypes[i];
            auto result = (memory_type.propertyFlags & requirements);
            if (result == requirements)
            {
                memory_type_index = i;
                return true;
            }
        }
    }

    return false;
}

void Application::key_callback(GLFWwindow *window, int key, int scan_code, int action, int modifiers)
{
    switch (key)
    {
    
    // Toggle animation of the first cube.
    case GLFW_KEY_0:
    {
        if (GLFW_PRESS == action)
        {
            m_animate.at(0) = !m_animate.at(0);
        }
        break;
    }
    
    // Toggle animation of the second cube.
    case GLFW_KEY_1:
    {
        if (GLFW_PRESS == action)
        {
            m_animate.at(1) = !m_animate.at(1);
        }
        break;
    }
    
    case GLFW_KEY_LEFT:
    case GLFW_KEY_RIGHT:
        m_key_map[key] = action;
        break;
    }
}

void Application::frame_buffer_resized(int new_width, int new_height)
{
    auto delta = std::fabs(0.0f - new_height);
    if (delta < 0.01f)
    {
        // avoid divide by zero
        m_projection_matrix = glm::mat4{};
    }
    else
    {
        auto aspect_ratio = static_cast<float>(new_width) / static_cast<float>(new_height);
        m_projection_matrix = Math_utility::perspective_projection_left_handed(glm::radians(60.0f),
            aspect_ratio,
            1.01f,
            1000.0f);
    }
    
    // Assume that the application is minimized if width or height is zero.
    if ((0 == new_width) || (0 == new_height))
    {
        return;
    }

    if (!recreate_swap_chain(static_cast<uint32_t>(new_width), static_cast<uint32_t>(new_height)))
    {
        std::exit(EXIT_FAILURE);
    }

    // recreate_swap_chain calls vkDeviceWaitIdle, so m_vulkan_device should be idle now.
    m_depth_stencil.destroy(m_vulkan_device);
    if (!create_depth_stencil_image(m_vulkan_device,
        m_physical_device_memory_properties,
        m_depth_stencil_image_format,
        static_cast<uint32_t>(new_width),
        static_cast<uint32_t>(new_height),
        m_depth_stencil))
    {
        std::exit(EXIT_FAILURE);
    }
    
    const auto &frame = m_vec_frame.at(0);
    if (!copy_uniform_data(frame.m_command_buffer))
    {
        std::exit(EXIT_FAILURE);
    }
}

// instance_extensions: List of extensions that the newly created VkInstance must support.
bool Application::create_instance(const std::vector<std::string> &instance_extensions, VkInstance &out_instance) const
{
    if (!check_instance_layers(m_instance_layer_names))
    {
        return false;
    }

    if (!check_instance_extensions(instance_extensions))
    {
        return false;
    }

    VkApplicationInfo application_info = {};
    application_info.sType = VK_STRUCTURE_TYPE_APPLICATION_INFO;
    application_info.pNext = nullptr;
    application_info.pApplicationName = m_application_name.c_str();
    application_info.applicationVersion = VK_MAKE_VERSION(1, 0, 0);
    application_info.pEngineName = m_engine_name.c_str();
    application_info.engineVersion = VK_MAKE_VERSION(1, 0, 0);
    application_info.apiVersion = VK_API_VERSION_1_0;
    VkInstanceCreateInfo instance_create_info = {};
    instance_create_info.sType = VK_STRUCTURE_TYPE_INSTANCE_CREATE_INFO;
    instance_create_info.pNext = nullptr;
    instance_create_info.flags = 0;
    instance_create_info.pApplicationInfo = &application_info;
    std::vector<const char*> instance_layers_char;
#ifdef _DEBUG
    std::transform(m_instance_layer_names.cbegin(),
        m_instance_layer_names.cend(),
        std::back_inserter(instance_layers_char),
        [](const std::string &layer) { return layer.c_str(); });
#endif
    instance_create_info.enabledLayerCount = static_cast<uint32_t>(instance_layers_char.size());
    instance_create_info.ppEnabledLayerNames = instance_layers_char.data();
    std::vector<const char*> instance_extensions_char;
    std::transform(instance_extensions.cbegin(),
        instance_extensions.cend(),
        std::back_inserter(instance_extensions_char),
        [](const std::string &extension) { return extension.c_str(); });
#ifdef _DEBUG
    instance_extensions_char.push_back("VK_EXT_debug_report");
#endif
    instance_create_info.enabledExtensionCount = static_cast<uint32_t>(instance_extensions_char.size());
    instance_create_info.ppEnabledExtensionNames = instance_extensions_char.data();
    auto result = vkCreateInstance(&instance_create_info,
        nullptr,
        &out_instance);
    return VK_SUCCESS == result;
}

// Returns true if physical_device is a suitable physical device e.g.
// - has a queue family that supports graphics
// - has a queue family that supports presenting to m_surface
//
// Each VkPhysicalDevice can have multiple queue families. Each queue family can
// have multiple queues.
//
// device_extensions - list of extensions that physical_device must support
// present_surface - handle to surface that physical_device must be able to
// present to.
bool Application::check_physical_device(VkPhysicalDevice physical_device,
    const std::vector<std::string> &device_extensions,
    VkSurfaceKHR present_surface,
    uint32_t &out_graphics_queue_family_index,
    uint32_t &out_present_queue_family_index) const
{
    out_graphics_queue_family_index = std::numeric_limits<uint32_t>::max();
    out_present_queue_family_index = std::numeric_limits<uint32_t>::max();
    if (!check_device_extensions(physical_device, device_extensions))
    {
        return false;
    }

    uint32_t queue_family_property_count{};
    vkGetPhysicalDeviceQueueFamilyProperties(physical_device, &queue_family_property_count, nullptr);
    if (0 == queue_family_property_count)
    {
        return false;
    }

    std::vector<VkQueueFamilyProperties> queue_family_properties(static_cast<std::size_t>(queue_family_property_count));
    vkGetPhysicalDeviceQueueFamilyProperties(physical_device,
        &queue_family_property_count,
        queue_family_properties.data());
    uint32_t queue_family_index{};
    std::vector<VkBool32> present_supported_vec;
    std::vector<bool> graphics_supported_vec;
    std::vector<bool> both_supported_vec;
    for (auto iter = queue_family_properties.cbegin(); iter != queue_family_properties.cend(); ++iter)
    {
        VkBool32 present_supported{VK_FALSE};
        // Check if the queue family queue_family_index can present to m_surface
        if (VK_SUCCESS != vkGetPhysicalDeviceSurfaceSupportKHR(physical_device,
            queue_family_index,
            present_surface,
            &present_supported))
        {
            return false;
        }
        
        present_supported_vec.push_back(present_supported);
        auto graphics_supported = (iter->queueFlags & VK_QUEUE_GRAPHICS_BIT);
		// Fix for warning C4800: forcing value to bool
		graphics_supported_vec.push_back((graphics_supported != 0) ? true : false);
        auto both_supported = (graphics_supported && (VK_TRUE == present_supported));
        both_supported_vec.push_back(both_supported);
        ++queue_family_index;
    }

    // Prefer to use a queue family that supports both graphics and present
    for (std::size_t i = 0; i != both_supported_vec.size(); ++i)
    {
        // Pick the first queue family found that supports both graphics and
        // present.
        if (both_supported_vec.at(i))
        {
            out_graphics_queue_family_index = static_cast<uint32_t>(i);
            out_present_queue_family_index = static_cast<uint32_t>(i);
            return true;
        }
    }

    for (std::size_t i = 0; i != present_supported_vec.size(); ++i)
    {
        auto present_supported = present_supported_vec.at(i);
        // Pick the first queue family found that supports present.
        if (VK_TRUE == present_supported)
        {
            out_present_queue_family_index = static_cast<uint32_t>(i);
            break;
        }
    }

    for (std::size_t i = 0; i != graphics_supported_vec.size(); ++i)
    {
        // Pick the first queue family found that supports graphics.
        if (graphics_supported_vec.at(i))
        {
            out_graphics_queue_family_index = static_cast<uint32_t>(i);
        }
    }

    if (std::numeric_limits<uint32_t>::max() == out_graphics_queue_family_index)
    {
        return false;
    }

    if (std::numeric_limits<uint32_t>::max() == out_present_queue_family_index)
    {
        return false;
    }

    return true;
}

bool Application::create_debug_report_callback(VkInstance instance,
    VkDebugReportCallbackEXT &out_callback) const
{
    VkDebugReportCallbackCreateInfoEXT debug_report_callback_create_info = {};
    debug_report_callback_create_info.sType = VK_STRUCTURE_TYPE_DEBUG_REPORT_CALLBACK_CREATE_INFO_EXT;
    debug_report_callback_create_info.pNext = nullptr;
    debug_report_callback_create_info.flags = VK_DEBUG_REPORT_INFORMATION_BIT_EXT|
        VK_DEBUG_REPORT_WARNING_BIT_EXT|
        VK_DEBUG_REPORT_PERFORMANCE_WARNING_BIT_EXT|
        VK_DEBUG_REPORT_ERROR_BIT_EXT|
        VK_DEBUG_REPORT_DEBUG_BIT_EXT;
    debug_report_callback_create_info.pfnCallback = Application::vulkan_debug_report_callback;
    debug_report_callback_create_info.pUserData = nullptr;
    auto proc_address = glfwGetInstanceProcAddress(instance, "vkCreateDebugReportCallbackEXT");
    if (nullptr == proc_address)
    {
        return false;
    }
    
    auto Jade_vkCreateDebugReportCallbackEXT = reinterpret_cast<PFN_vkCreateDebugReportCallbackEXT>(proc_address);
    auto result = Jade_vkCreateDebugReportCallbackEXT(instance,
        &debug_report_callback_create_info,
        nullptr,
        &out_callback);
    return VK_SUCCESS == result;
}

// out_graphics_queue_family_index: the queue family index belonging to the selected VkPhysicalDevice to use for
// graphics.
//
// out_present_queue_family_index: the queue family index belonging to the selected VkPhysicalDevice to use for present.
//
// out_graphics_queue_family_index and out_present_queue_family_index can be the same.
bool Application::create_device(VkPhysicalDevice &out_physical_device,
    VkDevice &out_vulkan_device,
    uint32_t &out_graphics_queue_family_index,
    uint32_t &out_present_queue_family_index) const
{
    uint32_t physical_device_count{};
    std::vector<VkPhysicalDevice> physical_devices;
    if (VK_SUCCESS != vkEnumeratePhysicalDevices(m_vulkan_instance, &physical_device_count, nullptr))
    {
        return false;
    }

    physical_devices.resize(static_cast<std::size_t>(physical_device_count), nullptr);
    if (VK_SUCCESS != vkEnumeratePhysicalDevices(m_vulkan_instance, &physical_device_count, physical_devices.data()))
    {
        return false;
    }

    std::size_t physical_device_index{};
    for (auto iter = physical_devices.cbegin(); iter != physical_devices.cend(); ++iter)
    {
        auto check_physical_device_result = check_physical_device(*iter,
            m_device_extension_names,
            m_surface,
            out_graphics_queue_family_index,
            out_present_queue_family_index);
        if (check_physical_device_result)
        {
            break;
        }

        ++physical_device_index;
    }

    if (physical_devices.size() == physical_device_index)
    {
        return false;
    }

    if ((std::numeric_limits<uint32_t>::max() == out_graphics_queue_family_index) ||
        (std::numeric_limits<uint32_t>::max() == out_present_queue_family_index))
    {
        return false;
    }

    auto priority = 1.0f;
    std::vector<VkDeviceQueueCreateInfo> device_queue_create_infos;
    VkDeviceQueueCreateInfo device_queue_create_info = {};
    device_queue_create_info.sType = VK_STRUCTURE_TYPE_DEVICE_QUEUE_CREATE_INFO;
    device_queue_create_info.pNext = nullptr;
    device_queue_create_info.flags = 0;
    device_queue_create_info.queueFamilyIndex = out_graphics_queue_family_index;
    device_queue_create_info.queueCount = 1;
    device_queue_create_info.pQueuePriorities = &priority;
    device_queue_create_infos.push_back(device_queue_create_info);
    if (out_graphics_queue_family_index != out_present_queue_family_index)
    {
        device_queue_create_info.queueFamilyIndex = out_present_queue_family_index;
        device_queue_create_infos.push_back(device_queue_create_info);
    }

    VkDeviceCreateInfo device_create_info = {};
    device_create_info.sType = VK_STRUCTURE_TYPE_DEVICE_CREATE_INFO;
    device_create_info.pNext = nullptr;
    device_create_info.flags = 0;
    device_create_info.queueCreateInfoCount = static_cast<uint32_t>(device_queue_create_infos.size());
    device_create_info.pQueueCreateInfos = device_queue_create_infos.data();
    device_create_info.enabledLayerCount = 0;
    device_create_info.ppEnabledLayerNames = nullptr;
    std::vector<const char*> extension_names_char;
    std::transform(m_device_extension_names.cbegin(),
        m_device_extension_names.cend(),
        std::back_inserter(extension_names_char),
        [](const std::string &layer) { return layer.c_str(); });
    device_create_info.enabledExtensionCount = static_cast<uint32_t>(extension_names_char.size());
    device_create_info.ppEnabledExtensionNames = extension_names_char.data();
    device_create_info.pEnabledFeatures = nullptr;
    if (VK_SUCCESS != vkCreateDevice(physical_devices.at(physical_device_index),
        &device_create_info,
        nullptr,
        &out_vulkan_device))
    {
        return false;
    }

    out_physical_device = physical_devices.at(physical_device_index);
    return true;
}

// out_old_swapchain: create_swapchain will destroy out_old_swapchain after the new swapchain is created.
bool Application::create_swapchain(
    uint32_t width,
    uint32_t height,
    VkSwapchainKHR &out_old_swapchain,
    VkFormat &out_swapchain_image_format,
    std::vector<VkImage> &out_swapchain_images,
    std::vector<VkImageView> &out_swapchain_image_views,
    VkSwapchainKHR &out_swapchain) const
{
    uint32_t formats_count{};
    if (VK_SUCCESS != vkGetPhysicalDeviceSurfaceFormatsKHR(m_physical_device, m_surface, &formats_count, nullptr))
    {
        return false;
    }

    std::vector<VkSurfaceFormatKHR> formats(static_cast<std::size_t>(formats_count));
    if (VK_SUCCESS != vkGetPhysicalDeviceSurfaceFormatsKHR(m_physical_device,
        m_surface,
        &formats_count,
        formats.data()))
    {
        return false;
    }

    if (0 == formats_count)
    {
        return false;
    }

    // If surface has no preferred format, use VK_FORMAT_B8G8R8A8_UNORM
    if (1 == formats_count)
    {
        const auto &surface_format = formats.at(0);
        if (VK_FORMAT_UNDEFINED == surface_format.format)
        {
            out_swapchain_image_format = VK_FORMAT_B8G8R8A8_UNORM;
        }
        else
        {
            out_swapchain_image_format = surface_format.format;
        }
    }
    else
    {
        const auto &surface_format = formats.at(0);
        out_swapchain_image_format = surface_format.format;
    }

    VkSurfaceCapabilitiesKHR surface_capabilities = {};
    if (VK_SUCCESS != vkGetPhysicalDeviceSurfaceCapabilitiesKHR(m_physical_device, m_surface, &surface_capabilities))
    {
        return false;
    }

    VkExtent2D extent = {};
    // If width is 0xFFFF_FFFF, so is height.
    if (std::numeric_limits<uint32_t>::max() == surface_capabilities.currentExtent.width)
    {
        extent.width = width;
        extent.height = height;
        extent.width = glm::clamp(extent.width,
            surface_capabilities.minImageExtent.width,
            surface_capabilities.maxImageExtent.width);
        extent.height = glm::clamp(extent.height,
            surface_capabilities.minImageExtent.height,
            surface_capabilities.maxImageExtent.height);
    }
    else
    {
        extent = surface_capabilities.currentExtent;
    }

    auto number_of_images = surface_capabilities.minImageCount;
    VkSurfaceTransformFlagBitsKHR surface_transform{};
    if (surface_capabilities.supportedTransforms & VK_SURFACE_TRANSFORM_IDENTITY_BIT_KHR)
    {
        surface_transform = VK_SURFACE_TRANSFORM_IDENTITY_BIT_KHR;
    }
    else
    {
        surface_transform = surface_capabilities.currentTransform;
    }

    VkSwapchainCreateInfoKHR swap_chain_create_info = {};
    swap_chain_create_info.sType = VK_STRUCTURE_TYPE_SWAPCHAIN_CREATE_INFO_KHR;
    swap_chain_create_info.pNext = nullptr;
    swap_chain_create_info.surface = m_surface;
    swap_chain_create_info.minImageCount = number_of_images;
    swap_chain_create_info.imageFormat = out_swapchain_image_format;
    swap_chain_create_info.imageExtent = extent;
    swap_chain_create_info.preTransform = surface_transform;
    swap_chain_create_info.compositeAlpha = VK_COMPOSITE_ALPHA_OPAQUE_BIT_KHR;
    swap_chain_create_info.imageArrayLayers = 1;
    // VK_PRESENT_MODE_FIFO_KHR is always supported
    swap_chain_create_info.presentMode = VK_PRESENT_MODE_FIFO_KHR;
    swap_chain_create_info.oldSwapchain = out_old_swapchain;
    swap_chain_create_info.clipped = VK_TRUE;
    swap_chain_create_info.imageColorSpace = VK_COLORSPACE_SRGB_NONLINEAR_KHR;
    swap_chain_create_info.imageUsage = VK_IMAGE_USAGE_COLOR_ATTACHMENT_BIT|VK_IMAGE_USAGE_TRANSFER_SRC_BIT;
    swap_chain_create_info.imageSharingMode = VK_SHARING_MODE_EXCLUSIVE;
    swap_chain_create_info.queueFamilyIndexCount = 0;
    swap_chain_create_info.pQueueFamilyIndices = nullptr;
    std::vector<uint32_t> queue_family_indices = { m_graphics_queue_family_index, m_present_queue_family_index };
    if (m_graphics_queue_family_index != m_present_queue_family_index)
    {
        swap_chain_create_info.imageSharingMode = VK_SHARING_MODE_CONCURRENT;
        swap_chain_create_info.queueFamilyIndexCount = static_cast<uint32_t>(queue_family_indices.size());
        swap_chain_create_info.pQueueFamilyIndices = queue_family_indices.data();
    }

    if (VK_SUCCESS != vkCreateSwapchainKHR(m_vulkan_device, &swap_chain_create_info, nullptr, &out_swapchain))
    {
        return false;
    }

    if (nullptr != out_old_swapchain)
    {
        vkDestroySwapchainKHR(m_vulkan_device, out_old_swapchain, nullptr);
        out_old_swapchain = nullptr;
    }

    uint32_t swap_chain_image_count{};
    if (VK_SUCCESS != vkGetSwapchainImagesKHR(m_vulkan_device, out_swapchain, &swap_chain_image_count, nullptr))
    {
        return false;
    }

    out_swapchain_images.resize(static_cast<std::size_t>(swap_chain_image_count));
    if (VK_SUCCESS != vkGetSwapchainImagesKHR(m_vulkan_device,
        out_swapchain,
        &swap_chain_image_count,
        out_swapchain_images.data()))
    {
        return false;
    }

    out_swapchain_image_views.clear();
    for (uint32_t i = 0; i != swap_chain_image_count; ++i)
    {
        VkImageViewCreateInfo image_view_create_info = {};
        image_view_create_info.sType = VK_STRUCTURE_TYPE_IMAGE_VIEW_CREATE_INFO;
        image_view_create_info.pNext = nullptr;
        image_view_create_info.flags = 0;
        image_view_create_info.image = out_swapchain_images.at(static_cast<std::size_t>(i));
        image_view_create_info.viewType = VK_IMAGE_VIEW_TYPE_2D;
        image_view_create_info.format = out_swapchain_image_format;
        image_view_create_info.components.r = VK_COMPONENT_SWIZZLE_R;
        image_view_create_info.components.g = VK_COMPONENT_SWIZZLE_G;
        image_view_create_info.components.b = VK_COMPONENT_SWIZZLE_B;
        image_view_create_info.components.a = VK_COMPONENT_SWIZZLE_A;
        image_view_create_info.subresourceRange.aspectMask = VK_IMAGE_ASPECT_COLOR_BIT;
        image_view_create_info.subresourceRange.baseMipLevel = 0;
        image_view_create_info.subresourceRange.levelCount = 1;
        image_view_create_info.subresourceRange.baseArrayLayer = 0;
        image_view_create_info.subresourceRange.layerCount = 1;
        VkImageView image_view{nullptr};
        if (VK_SUCCESS != vkCreateImageView(m_vulkan_device, &image_view_create_info, nullptr, &image_view))
        {
            return false;
        }

        out_swapchain_image_views.push_back(image_view);
    }

    return true;
}

bool Application::create_render_pass(VkRenderPass &out_render_pass) const
{
    std::vector<VkAttachmentDescription> vec_attachment_description;
    
    {
        VkAttachmentDescription attachment_description = {};
        attachment_description.flags = 0;
        attachment_description.format = m_swapchain_image_format;
        attachment_description.samples = VK_SAMPLE_COUNT_1_BIT;
        attachment_description.loadOp = VK_ATTACHMENT_LOAD_OP_CLEAR;
        attachment_description.storeOp = VK_ATTACHMENT_STORE_OP_STORE;
        attachment_description.stencilLoadOp = VK_ATTACHMENT_LOAD_OP_DONT_CARE;
        attachment_description.stencilStoreOp = VK_ATTACHMENT_STORE_OP_DONT_CARE;
        attachment_description.initialLayout = VK_IMAGE_LAYOUT_UNDEFINED;
        attachment_description.finalLayout = VK_IMAGE_LAYOUT_PRESENT_SRC_KHR;
        vec_attachment_description.push_back(attachment_description);
    }

    {
        VkAttachmentDescription attachment_description = {};
        attachment_description.flags = 0;
        attachment_description.format = m_depth_stencil_image_format;
        attachment_description.samples = VK_SAMPLE_COUNT_1_BIT;
        attachment_description.loadOp = VK_ATTACHMENT_LOAD_OP_CLEAR;
        attachment_description.storeOp = VK_ATTACHMENT_STORE_OP_STORE;
        // This should be OK since the application does not do anything with the stencil buffer.
        attachment_description.stencilLoadOp = VK_ATTACHMENT_LOAD_OP_DONT_CARE;
        attachment_description.stencilStoreOp = VK_ATTACHMENT_STORE_OP_DONT_CARE;
        attachment_description.initialLayout = VK_IMAGE_LAYOUT_UNDEFINED;
        attachment_description.finalLayout = VK_IMAGE_LAYOUT_PRESENT_SRC_KHR;
        vec_attachment_description.push_back(attachment_description);
    }
    
    VkAttachmentReference attachment_reference = {};
    // attachment is the index of the element in VkRenderPassCreateInfo.pAttachments
    attachment_reference.attachment = 0;
    // layout is the layout used by the attachment during the subpass.
    attachment_reference.layout = VK_IMAGE_LAYOUT_COLOR_ATTACHMENT_OPTIMAL;
    VkAttachmentReference depth_stencil_attachment_reference = {};
    depth_stencil_attachment_reference.attachment = 1;
    depth_stencil_attachment_reference.layout = VK_IMAGE_LAYOUT_DEPTH_STENCIL_ATTACHMENT_OPTIMAL;
    VkSubpassDescription subpass_description = {};
    subpass_description.flags = 0;
    subpass_description.pipelineBindPoint = VK_PIPELINE_BIND_POINT_GRAPHICS;
    subpass_description.inputAttachmentCount = 0;
    subpass_description.pInputAttachments = nullptr;
    subpass_description.colorAttachmentCount = 1;
    subpass_description.pColorAttachments = &attachment_reference;
    subpass_description.pResolveAttachments = nullptr;
    subpass_description.pDepthStencilAttachment = &depth_stencil_attachment_reference;
    subpass_description.preserveAttachmentCount = 0;
    subpass_description.pPreserveAttachments = nullptr;
    std::vector<VkSubpassDependency> vec_subpass_dependency;

    {
        VkSubpassDependency subpass_dependency = {};
        subpass_dependency.srcSubpass = VK_SUBPASS_EXTERNAL;
        subpass_dependency.dstSubpass = 0;
        subpass_dependency.srcStageMask = VK_PIPELINE_STAGE_BOTTOM_OF_PIPE_BIT;
        subpass_dependency.dstStageMask = VK_PIPELINE_STAGE_COLOR_ATTACHMENT_OUTPUT_BIT;
        subpass_dependency.srcAccessMask = VK_ACCESS_MEMORY_READ_BIT;
        subpass_dependency.dstAccessMask = VK_ACCESS_COLOR_ATTACHMENT_WRITE_BIT;
        subpass_dependency.dependencyFlags = VK_DEPENDENCY_BY_REGION_BIT;
        vec_subpass_dependency.push_back(subpass_dependency);
    }

    {
        VkSubpassDependency subpass_dependency = {};
        subpass_dependency.srcSubpass = 0;
        subpass_dependency.dstSubpass = VK_SUBPASS_EXTERNAL;
        subpass_dependency.srcStageMask = VK_PIPELINE_STAGE_COLOR_ATTACHMENT_OUTPUT_BIT;
        subpass_dependency.dstStageMask = VK_PIPELINE_STAGE_BOTTOM_OF_PIPE_BIT;
        subpass_dependency.srcAccessMask = VK_ACCESS_COLOR_ATTACHMENT_WRITE_BIT;
        subpass_dependency.dstAccessMask = VK_ACCESS_MEMORY_READ_BIT;
        subpass_dependency.dependencyFlags = VK_DEPENDENCY_BY_REGION_BIT;
        vec_subpass_dependency.push_back(subpass_dependency);
    }

    VkRenderPassCreateInfo render_pass_create_info = {};
    render_pass_create_info.sType = VK_STRUCTURE_TYPE_RENDER_PASS_CREATE_INFO;
    render_pass_create_info.pNext = nullptr;
    render_pass_create_info.flags = 0;
    render_pass_create_info.attachmentCount = static_cast<uint32_t>(vec_attachment_description.size());
    render_pass_create_info.pAttachments = vec_attachment_description.data();
    render_pass_create_info.subpassCount = 1;
    render_pass_create_info.pSubpasses = &subpass_description;
    render_pass_create_info.dependencyCount = static_cast<uint32_t>(vec_subpass_dependency.size());
    render_pass_create_info.pDependencies = vec_subpass_dependency.data();
    return VK_SUCCESS == vkCreateRenderPass(m_vulkan_device, &render_pass_create_info, nullptr, &out_render_pass);
}

bool Application::create_framebuffer(VkDevice device,
    VkRenderPass render_pass,
    uint32_t width,
    uint32_t height,
    VkImageView image_view,
    VkImageView depth_stencil_view,
    VkFramebuffer &out_framebuffer) const
{
    VkFramebufferCreateInfo framebuffer_create_info = {};
    framebuffer_create_info.sType = VK_STRUCTURE_TYPE_FRAMEBUFFER_CREATE_INFO;
    framebuffer_create_info.pNext = nullptr;
    framebuffer_create_info.flags = 0;
    framebuffer_create_info.renderPass = render_pass;
    std::vector<VkImageView> vec_image_view = { image_view, depth_stencil_view };
    framebuffer_create_info.attachmentCount = static_cast<uint32_t>(vec_image_view.size());
    framebuffer_create_info.pAttachments = vec_image_view.data();
    framebuffer_create_info.width = width;
    framebuffer_create_info.height = height;
    framebuffer_create_info.layers = 1;
    return VK_SUCCESS == vkCreateFramebuffer(device, &framebuffer_create_info, nullptr, &out_framebuffer);
}

// shader_file_names: SPIR-V files
// out_shader_modules: the caller must destroy these using vkDestroyShaderModule
bool Application::create_shader_modules(const std::vector<std::string> &shader_file_names,
    std::vector<VkShaderModule> &out_shader_modules) const
{
    out_shader_modules.clear();
    for (auto iter = shader_file_names.cbegin(); iter != shader_file_names.cend(); ++iter)
    {
        std::vector<uint32_t> spirv;
        if (!File_utility::load_spirv(*iter, spirv))
        {
            return false;
        }

        VkShaderModuleCreateInfo shader_module_create_info = {};
        shader_module_create_info.sType = VK_STRUCTURE_TYPE_SHADER_MODULE_CREATE_INFO;
        shader_module_create_info.pNext = nullptr;
        shader_module_create_info.flags = 0;
        shader_module_create_info.codeSize = spirv.size() * sizeof(uint32_t);
        shader_module_create_info.pCode = spirv.data();
        VkShaderModule shader_module = nullptr;
        auto vk_result = vkCreateShaderModule(m_vulkan_device, &shader_module_create_info, nullptr, &shader_module);
        if (VK_SUCCESS != vk_result)
        {
            return false;
        }

        out_shader_modules.push_back(shader_module);
    }

    return true;
}

// shader_modules, shader_stages: If shader_modules[i] stores the SPIR-V for a vertex shader, shader_stages[i] should be
// VK_SHADER_STAGE_VERTEX_BIT.
bool Application::create_pipeline(const std::vector<VkShaderStageFlagBits> &shader_stages,
    VkPipelineLayout &out_pipeline_layout,
    VkPipeline &out_pipeline) const
{
    std::vector<VkPipelineShaderStageCreateInfo> vec_pipeline_shader_stage_create_info;
    for (std::size_t i = 0; i != m_shader_modules.size(); ++i)
    {
        VkPipelineShaderStageCreateInfo pipeline_shader_stage_create_info = {};
        pipeline_shader_stage_create_info.sType = VK_STRUCTURE_TYPE_PIPELINE_SHADER_STAGE_CREATE_INFO;
        pipeline_shader_stage_create_info.pNext = nullptr;
        pipeline_shader_stage_create_info.pSpecializationInfo = nullptr;
        pipeline_shader_stage_create_info.flags = 0;
        pipeline_shader_stage_create_info.stage = shader_stages.at(i);
        pipeline_shader_stage_create_info.pName = "main";
        pipeline_shader_stage_create_info.module = m_shader_modules.at(i);
        vec_pipeline_shader_stage_create_info.push_back(pipeline_shader_stage_create_info);
    }

    VkVertexInputBindingDescription vertex_input_binding_description = {};
    vertex_input_binding_description.binding = 0;
    vertex_input_binding_description.stride = sizeof(Jade::Vertex);
    vertex_input_binding_description.inputRate = VK_VERTEX_INPUT_RATE_VERTEX;
    std::vector<VkVertexInputAttributeDescription> vec_vertex_input_attribute_description;

    {
        VkVertexInputAttributeDescription vertex_input_attribute_description;
        vertex_input_attribute_description.location = 0;
        vertex_input_attribute_description.binding = vertex_input_binding_description.binding;
        vertex_input_attribute_description.format = VK_FORMAT_R32G32B32A32_SFLOAT;
        vertex_input_attribute_description.offset = 0;
        vec_vertex_input_attribute_description.push_back(vertex_input_attribute_description);
    }

    {
        VkVertexInputAttributeDescription vertex_input_attribute_description;
        vertex_input_attribute_description.location = 1;
        vertex_input_attribute_description.binding = vertex_input_binding_description.binding;
        vertex_input_attribute_description.format = VK_FORMAT_R32G32B32A32_SFLOAT;
        vertex_input_attribute_description.offset = sizeof(glm::vec4);
        vec_vertex_input_attribute_description.push_back(vertex_input_attribute_description);
    }

    VkPipelineVertexInputStateCreateInfo pipeline_vertex_input_state_create_info = {};
    pipeline_vertex_input_state_create_info.sType = VK_STRUCTURE_TYPE_PIPELINE_VERTEX_INPUT_STATE_CREATE_INFO;
    pipeline_vertex_input_state_create_info.pNext = nullptr;
    pipeline_vertex_input_state_create_info.flags = 0;
    pipeline_vertex_input_state_create_info.vertexBindingDescriptionCount = 1;
    pipeline_vertex_input_state_create_info.pVertexBindingDescriptions = &vertex_input_binding_description;
    pipeline_vertex_input_state_create_info.vertexAttributeDescriptionCount = static_cast<uint32_t>(vec_vertex_input_attribute_description.size());
    pipeline_vertex_input_state_create_info.pVertexAttributeDescriptions = vec_vertex_input_attribute_description.data();
    VkPipelineInputAssemblyStateCreateInfo pipeline_input_assembly_state_create_info = {};
    pipeline_input_assembly_state_create_info.sType = VK_STRUCTURE_TYPE_PIPELINE_INPUT_ASSEMBLY_STATE_CREATE_INFO;
    pipeline_input_assembly_state_create_info.pNext = nullptr;
    pipeline_input_assembly_state_create_info.flags = 0;
    pipeline_input_assembly_state_create_info.topology = VK_PRIMITIVE_TOPOLOGY_TRIANGLE_LIST;
    pipeline_input_assembly_state_create_info.primitiveRestartEnable = VK_FALSE;
    VkPipelineViewportStateCreateInfo pipeline_viewport_state_create_info = {};
    pipeline_viewport_state_create_info.sType = VK_STRUCTURE_TYPE_PIPELINE_VIEWPORT_STATE_CREATE_INFO;
    pipeline_viewport_state_create_info.pNext = nullptr;
    pipeline_viewport_state_create_info.viewportCount = 1;
    pipeline_viewport_state_create_info.pViewports = nullptr;
    pipeline_viewport_state_create_info.scissorCount = 1;
    pipeline_viewport_state_create_info.pScissors = nullptr;
    VkPipelineRasterizationStateCreateInfo pipeline_rasterization_state_create_info = {};
    pipeline_rasterization_state_create_info.sType = VK_STRUCTURE_TYPE_PIPELINE_RASTERIZATION_STATE_CREATE_INFO;
    pipeline_rasterization_state_create_info.pNext = nullptr;
    pipeline_rasterization_state_create_info.flags = 0;
    pipeline_rasterization_state_create_info.depthClampEnable = VK_FALSE;
    pipeline_rasterization_state_create_info.rasterizerDiscardEnable = VK_FALSE;
    pipeline_rasterization_state_create_info.polygonMode = VK_POLYGON_MODE_FILL;
    pipeline_rasterization_state_create_info.cullMode = VK_CULL_MODE_BACK_BIT;
    pipeline_rasterization_state_create_info.frontFace = VK_FRONT_FACE_CLOCKWISE;
    pipeline_rasterization_state_create_info.depthBiasEnable = VK_FALSE;
    pipeline_rasterization_state_create_info.depthBiasConstantFactor = 0.0f;
    pipeline_rasterization_state_create_info.depthBiasClamp = 0.0f;
    pipeline_rasterization_state_create_info.depthBiasSlopeFactor = 0.0f;
    pipeline_rasterization_state_create_info.lineWidth = 1.0f;
    VkPipelineMultisampleStateCreateInfo pipeline_multisample_state_create_info = {};
    pipeline_multisample_state_create_info.sType = VK_STRUCTURE_TYPE_PIPELINE_MULTISAMPLE_STATE_CREATE_INFO;
    pipeline_multisample_state_create_info.pNext = nullptr;
    pipeline_multisample_state_create_info.flags = 0;
    pipeline_multisample_state_create_info.rasterizationSamples = VK_SAMPLE_COUNT_1_BIT;
    pipeline_multisample_state_create_info.sampleShadingEnable = VK_FALSE;
    pipeline_multisample_state_create_info.minSampleShading = 1.0f;
    pipeline_multisample_state_create_info.pSampleMask = nullptr;
    pipeline_multisample_state_create_info.alphaToCoverageEnable = VK_FALSE;
    pipeline_multisample_state_create_info.alphaToOneEnable = VK_FALSE;
    VkPipelineColorBlendAttachmentState pipeline_color_blend_attachment_state = {};
    pipeline_color_blend_attachment_state.blendEnable = VK_FALSE;
    pipeline_color_blend_attachment_state.srcColorBlendFactor = VK_BLEND_FACTOR_ONE;
    pipeline_color_blend_attachment_state.dstColorBlendFactor = VK_BLEND_FACTOR_ZERO;
    pipeline_color_blend_attachment_state.colorBlendOp = VK_BLEND_OP_ADD;
    pipeline_color_blend_attachment_state.srcAlphaBlendFactor = VK_BLEND_FACTOR_ONE;
    pipeline_color_blend_attachment_state.dstAlphaBlendFactor = VK_BLEND_FACTOR_ZERO;
    pipeline_color_blend_attachment_state.alphaBlendOp = VK_BLEND_OP_ADD;
    pipeline_color_blend_attachment_state.colorWriteMask = VK_COLOR_COMPONENT_R_BIT|
        VK_COLOR_COMPONENT_G_BIT|
        VK_COLOR_COMPONENT_B_BIT|
        VK_COLOR_COMPONENT_A_BIT;
    VkPipelineColorBlendStateCreateInfo pipeline_color_blend_state_create_info = {};
    pipeline_color_blend_state_create_info.sType = VK_STRUCTURE_TYPE_PIPELINE_COLOR_BLEND_STATE_CREATE_INFO;
    pipeline_color_blend_state_create_info.pNext = nullptr;
    pipeline_color_blend_state_create_info.flags = 0;
    pipeline_color_blend_state_create_info.logicOpEnable = VK_FALSE;
    pipeline_color_blend_state_create_info.logicOp = VK_LOGIC_OP_CLEAR;
    pipeline_color_blend_state_create_info.attachmentCount = 1;
    pipeline_color_blend_state_create_info.pAttachments = &pipeline_color_blend_attachment_state;
    std::fill(pipeline_color_blend_state_create_info.blendConstants,
        pipeline_color_blend_state_create_info.blendConstants + 4,
        0.0f);
    std::vector<VkDynamicState> vec_dynamic_state = { VK_DYNAMIC_STATE_VIEWPORT, VK_DYNAMIC_STATE_SCISSOR };
    VkPipelineDynamicStateCreateInfo pipeline_dynamic_state_create_info = {};
    pipeline_dynamic_state_create_info.sType = VK_STRUCTURE_TYPE_PIPELINE_DYNAMIC_STATE_CREATE_INFO;
    pipeline_dynamic_state_create_info.pNext = nullptr;
    pipeline_dynamic_state_create_info.flags = 0;
    pipeline_dynamic_state_create_info.dynamicStateCount = static_cast<uint32_t>(vec_dynamic_state.size());
    pipeline_dynamic_state_create_info.pDynamicStates = vec_dynamic_state.data();
    VkPipelineLayoutCreateInfo pipeline_layout_create_info = {};
    pipeline_layout_create_info.sType = VK_STRUCTURE_TYPE_PIPELINE_LAYOUT_CREATE_INFO;
    pipeline_layout_create_info.pNext = nullptr;
    pipeline_layout_create_info.flags = 0;
    pipeline_layout_create_info.setLayoutCount = 1;
    pipeline_layout_create_info.pSetLayouts = &m_descriptor_set_layout;
    pipeline_layout_create_info.pushConstantRangeCount = 0;
    pipeline_layout_create_info.pPushConstantRanges = nullptr;
    auto vk_result = vkCreatePipelineLayout(m_vulkan_device,
        &pipeline_layout_create_info,
        nullptr,
        &out_pipeline_layout);
    if (VK_SUCCESS != vk_result)
    {
        return false;
    }

    VkPipelineDepthStencilStateCreateInfo pipeline_depth_stencil_state_create_info = {};
    pipeline_depth_stencil_state_create_info.sType = VK_STRUCTURE_TYPE_PIPELINE_DEPTH_STENCIL_STATE_CREATE_INFO;
    pipeline_depth_stencil_state_create_info.pNext = nullptr;
    pipeline_depth_stencil_state_create_info.flags = 0;
    pipeline_depth_stencil_state_create_info.depthTestEnable = VK_TRUE;
    pipeline_depth_stencil_state_create_info.depthWriteEnable = VK_TRUE;
    pipeline_depth_stencil_state_create_info.depthCompareOp = VK_COMPARE_OP_LESS_OR_EQUAL;
    pipeline_depth_stencil_state_create_info.depthBoundsTestEnable = VK_FALSE;
    pipeline_depth_stencil_state_create_info.stencilTestEnable = VK_FALSE;
    VkStencilOpState stencil_op_state = {};
    stencil_op_state.failOp = VK_STENCIL_OP_KEEP;
    stencil_op_state.passOp = VK_STENCIL_OP_KEEP;
    stencil_op_state.compareOp = VK_COMPARE_OP_ALWAYS;
    stencil_op_state.compareMask = 0;
    stencil_op_state.reference = 0;
    stencil_op_state.depthFailOp = VK_STENCIL_OP_KEEP;
    stencil_op_state.writeMask = 0;
    pipeline_depth_stencil_state_create_info.back = stencil_op_state;
    pipeline_depth_stencil_state_create_info.front = stencil_op_state;
    VkGraphicsPipelineCreateInfo graphics_pipeline_create_info = {};
    graphics_pipeline_create_info.sType = VK_STRUCTURE_TYPE_GRAPHICS_PIPELINE_CREATE_INFO;
    graphics_pipeline_create_info.pNext = nullptr;
    graphics_pipeline_create_info.flags = 0;
    graphics_pipeline_create_info.stageCount = static_cast<uint32_t>(vec_pipeline_shader_stage_create_info.size());
    graphics_pipeline_create_info.pStages = vec_pipeline_shader_stage_create_info.data();
    graphics_pipeline_create_info.pVertexInputState = &pipeline_vertex_input_state_create_info;
    graphics_pipeline_create_info.pInputAssemblyState = &pipeline_input_assembly_state_create_info;
    graphics_pipeline_create_info.pTessellationState = nullptr;
    graphics_pipeline_create_info.pViewportState = &pipeline_viewport_state_create_info;
    graphics_pipeline_create_info.pRasterizationState = &pipeline_rasterization_state_create_info;
    graphics_pipeline_create_info.pMultisampleState = &pipeline_multisample_state_create_info;
    graphics_pipeline_create_info.pDepthStencilState = &pipeline_depth_stencil_state_create_info;
    graphics_pipeline_create_info.pColorBlendState = &pipeline_color_blend_state_create_info;
    graphics_pipeline_create_info.pDynamicState = &pipeline_dynamic_state_create_info;
    graphics_pipeline_create_info.layout = out_pipeline_layout;
    graphics_pipeline_create_info.renderPass = m_render_pass;
    graphics_pipeline_create_info.subpass = 0;
    graphics_pipeline_create_info.basePipelineHandle = nullptr;
    graphics_pipeline_create_info.basePipelineIndex = -1;
    vk_result = vkCreateGraphicsPipelines(m_vulkan_device,
        nullptr,
        1,
        &graphics_pipeline_create_info,
        nullptr,
        &out_pipeline);
    if (VK_SUCCESS != vk_result)
    {
        return false;
    }
    
    return true;
}

// Semaphores are used for synchronization. A semaphore has two states:
// - signaled
// - unsignaled
// A queue can wait on a particular semaphore to be signaled before proceeding to process command buffers submitted to
// it.
bool Application::create_semaphores(uint32_t create_count, std::vector<VkSemaphore> &out_semaphores) const
{
    assert(out_semaphores.empty());
    for (uint32_t i = 0; i != create_count; ++i)
    {
        VkSemaphoreCreateInfo create_info = {};
        create_info.sType = VK_STRUCTURE_TYPE_SEMAPHORE_CREATE_INFO;
        create_info.pNext = nullptr;
        create_info.flags = 0;
        VkSemaphore semaphore;
        auto result = vkCreateSemaphore(m_vulkan_device, &create_info, nullptr, &semaphore);
        if (VK_SUCCESS != result)
        {
            return false;
        }

        out_semaphores.push_back(semaphore);
    }

    return true;
}

bool Application::create_fences(uint32_t create_count, std::vector<VkFence> &out_vec_fence) const
{
    assert(out_vec_fence.empty());
    for (uint32_t i = 0; i != create_count; ++i)
    {
        VkFenceCreateInfo fence_create_info = {};
        fence_create_info.sType = VK_STRUCTURE_TYPE_FENCE_CREATE_INFO;
        fence_create_info.pNext = nullptr;
        fence_create_info.flags = VK_FENCE_CREATE_SIGNALED_BIT;
        VkFence fence{nullptr};
        auto result = vkCreateFence(m_vulkan_device, &fence_create_info, nullptr, &fence);
        if (VK_SUCCESS != result)
        {
            return false;
        }

        out_vec_fence.push_back(fence);
    }

    return true;
}

bool Application::create_command_pool(VkCommandPool &out_command_pool) const
{
    VkCommandPoolCreateInfo create_info = {};
    create_info.sType = VK_STRUCTURE_TYPE_COMMAND_POOL_CREATE_INFO;
    create_info.pNext = nullptr;
    create_info.flags = VK_COMMAND_POOL_CREATE_RESET_COMMAND_BUFFER_BIT|VK_COMMAND_POOL_CREATE_TRANSIENT_BIT;
    create_info.queueFamilyIndex = m_graphics_queue_family_index;
    auto result = vkCreateCommandPool(m_vulkan_device, &create_info, nullptr, &out_command_pool);
    return VK_SUCCESS == result;
}

bool Application::allocate_command_buffers(uint32_t count, std::vector<VkCommandBuffer> &out_command_buffers) const
{
    out_command_buffers.resize(static_cast<std::size_t>(count));
    VkCommandBufferAllocateInfo command_buffer_allocate_info = {};
    command_buffer_allocate_info.sType = VK_STRUCTURE_TYPE_COMMAND_BUFFER_ALLOCATE_INFO;
    command_buffer_allocate_info.pNext = nullptr;
    command_buffer_allocate_info.commandPool = m_command_pool;
    command_buffer_allocate_info.level = VK_COMMAND_BUFFER_LEVEL_PRIMARY;
    command_buffer_allocate_info.commandBufferCount = count;
    auto result = vkAllocateCommandBuffers(m_vulkan_device, &command_buffer_allocate_info, out_command_buffers.data());
    return VK_SUCCESS == result;
}

bool Application::record_command_buffer(uint32_t framebuffer_width,
    uint32_t framebuffer_height,
    VkFramebuffer framebuffer,
    VkImage swapchain_image,
    VkCommandBuffer command_buffer) const
{
    VkCommandBufferBeginInfo command_buffer_begin_info = {};
    command_buffer_begin_info.sType = VK_STRUCTURE_TYPE_COMMAND_BUFFER_BEGIN_INFO;
    command_buffer_begin_info.pNext = nullptr;
    command_buffer_begin_info.flags = VK_COMMAND_BUFFER_USAGE_ONE_TIME_SUBMIT_BIT;
    command_buffer_begin_info.pNext = nullptr;
    auto vk_result = vkBeginCommandBuffer(command_buffer, &command_buffer_begin_info);
    if (VK_SUCCESS != vk_result)
    {
        return false;
    }

    if (m_graphics_queue_family_index != m_present_queue_family_index)
    {
        // Barrier from present to draw
        VkImageMemoryBarrier image_memory_barrier = {};
        image_memory_barrier.sType = VK_STRUCTURE_TYPE_IMAGE_MEMORY_BARRIER;
        image_memory_barrier.pNext = nullptr;
        // srcAccessMask: the first operation protected by the barrier
        // dstAccessMask: the second operation protected by the barrier
        image_memory_barrier.srcAccessMask = VK_ACCESS_MEMORY_READ_BIT;
        image_memory_barrier.dstAccessMask = VK_ACCESS_COLOR_ATTACHMENT_WRITE_BIT;
        image_memory_barrier.oldLayout = VK_IMAGE_LAYOUT_UNDEFINED;
        image_memory_barrier.newLayout = VK_IMAGE_LAYOUT_PRESENT_SRC_KHR;
        image_memory_barrier.srcQueueFamilyIndex = m_present_queue_family_index;
        image_memory_barrier.dstQueueFamilyIndex = m_graphics_queue_family_index;
        image_memory_barrier.image = swapchain_image;
        VkImageSubresourceRange image_subresource_range = {};
        image_subresource_range.aspectMask = VK_IMAGE_ASPECT_COLOR_BIT;
        image_subresource_range.baseMipLevel = 0;
        image_subresource_range.levelCount = 1;
        image_subresource_range.baseArrayLayer = 0;
        image_subresource_range.layerCount = 1;
        image_memory_barrier.subresourceRange = image_subresource_range;
        vkCmdPipelineBarrier(command_buffer,
            VK_PIPELINE_STAGE_COLOR_ATTACHMENT_OUTPUT_BIT, // which stage wrote to the resource last
            VK_PIPELINE_STAGE_COLOR_ATTACHMENT_OUTPUT_BIT, // which stage will read from the resource next
            0,
            0,
            nullptr,
            0,
            nullptr,
            1,
            &image_memory_barrier);
    }

    VkRenderPassBeginInfo render_pass_begin_info = {};
    render_pass_begin_info.sType = VK_STRUCTURE_TYPE_RENDER_PASS_BEGIN_INFO;
    render_pass_begin_info.pNext = nullptr;
    render_pass_begin_info.renderPass = m_render_pass;
    render_pass_begin_info.framebuffer = framebuffer;
    VkRect2D render_area = {};
    render_area.offset.x = 0;
    render_area.offset.y = 0;
    render_area.extent.width = framebuffer_width;
    render_area.extent.height = framebuffer_height;
    render_pass_begin_info.renderArea = render_area;
    std::vector<VkClearValue> vec_clear_value;
    
    {
        VkClearValue clear_value = {};
        // RGBA
        clear_value.color.float32[0] = 0.4f; //-V525
        clear_value.color.float32[1] = 0.4f;
        clear_value.color.float32[2] = 0.4f;
        clear_value.color.float32[3] = 1.0f;
        vec_clear_value.push_back(clear_value);
    }

    {
        VkClearValue clear_value = {};
        clear_value.depthStencil.depth = 1.0f;
        clear_value.depthStencil.stencil = 0;
        vec_clear_value.push_back(clear_value);
    }
    
    render_pass_begin_info.clearValueCount = static_cast<uint32_t>(vec_clear_value.size());
    render_pass_begin_info.pClearValues = vec_clear_value.data();

    {
        // Update the model matrices for both cubes and the view matrix.
        std::vector<VkDeviceSize> offsets = { 0, m_uniform_buffer_offset, m_uniform_buffer_offset * 2 };
        std::vector<glm::mat4> matrices = { m_model_matrices.at(0), m_model_matrices.at(1), m_view_matrix };
        if (!copy_uniform_data_started_recording(command_buffer, offsets, matrices))
        {
            return false;
        }
    }
    
    vkCmdBeginRenderPass(command_buffer, &render_pass_begin_info, VK_SUBPASS_CONTENTS_INLINE);
    vkCmdBindPipeline(command_buffer, VK_PIPELINE_BIND_POINT_GRAPHICS, m_pipeline);
    VkViewport viewport = {};
    viewport.x = 0.0f;
    viewport.y = 0.0f;
    viewport.width = static_cast<float>(framebuffer_width);
    viewport.height = static_cast<float>(framebuffer_height);
    viewport.minDepth = 0.0f;
    viewport.maxDepth = 1.0f;
    vkCmdSetViewport(command_buffer, 0, 1, &viewport);
    VkRect2D scissor = {};
    scissor.offset.x = 0;
    scissor.offset.y = 0;
    scissor.extent.width = framebuffer_width;
    scissor.extent.height = framebuffer_height;
    vkCmdSetScissor(command_buffer, 0, 1, &scissor);
    const VkDeviceSize offset{0};
    vkCmdBindVertexBuffers(command_buffer, 0, 1, &m_vertex_buffer.m_buffer, &offset);
    
    {
        // Use a different model matrix to draw each cube.
        std::array<uint32_t, 2> dynamic_offsets = { 0, static_cast<uint32_t>(m_uniform_buffer_offset) };
        for (auto iter = dynamic_offsets.cbegin(); iter != dynamic_offsets.cend(); ++iter)
        {
            const auto &dynamic_offset = *iter;
            vkCmdBindDescriptorSets(command_buffer,
                VK_PIPELINE_BIND_POINT_GRAPHICS,
                m_pipeline_layout,
                0,
                1,
                &m_descriptor_set,
                1,
                &dynamic_offset);
            vkCmdDraw(command_buffer, static_cast<uint32_t>(m_cube.size()), 1, 0, 0);
        }
    }
    
    vkCmdEndRenderPass(command_buffer);
    if (m_graphics_queue_family_index != m_present_queue_family_index)
    {
        // Barrier from draw to present
        VkImageMemoryBarrier image_memory_barrier = {};
        image_memory_barrier.sType = VK_STRUCTURE_TYPE_IMAGE_MEMORY_BARRIER;
        image_memory_barrier.pNext = nullptr;
        // srcAccessMask: the first operation protected by the barrier
        // dstAccessMask: the second operation protected by the barrier
        image_memory_barrier.srcAccessMask = VK_ACCESS_COLOR_ATTACHMENT_WRITE_BIT;
        image_memory_barrier.dstAccessMask = VK_ACCESS_MEMORY_READ_BIT;
        image_memory_barrier.oldLayout = VK_IMAGE_LAYOUT_PRESENT_SRC_KHR;
        image_memory_barrier.newLayout = VK_IMAGE_LAYOUT_PRESENT_SRC_KHR;
        image_memory_barrier.srcQueueFamilyIndex = m_graphics_queue_family_index;
        image_memory_barrier.dstQueueFamilyIndex = m_present_queue_family_index;
        image_memory_barrier.image = swapchain_image;
        VkImageSubresourceRange image_subresource_range = {};
        image_subresource_range.aspectMask = VK_IMAGE_ASPECT_COLOR_BIT;
        image_subresource_range.baseMipLevel = 0;
        image_subresource_range.levelCount = 1;
        image_subresource_range.baseArrayLayer = 0;
        image_subresource_range.layerCount = 1;
        image_memory_barrier.subresourceRange = image_subresource_range;
        vkCmdPipelineBarrier(command_buffer,
            VK_PIPELINE_STAGE_COLOR_ATTACHMENT_OUTPUT_BIT, // which stage wrote to the resource last
            VK_PIPELINE_STAGE_BOTTOM_OF_PIPE_BIT, // which stage will read from the resource next
            0,
            0,
            nullptr,
            0,
            nullptr,
            1,
            &image_memory_barrier);
    }

    vk_result = vkEndCommandBuffer(command_buffer);
    if (VK_SUCCESS != vk_result)
    {
        return false;
    }

    return true;
}

bool Application::draw(uint32_t framebuffer_width, uint32_t framebuffer_height, Jade::Frame &out_frame)
{
    auto vk_result = vkWaitForFences(m_vulkan_device,
        1,
        &out_frame.m_fence,
        VK_TRUE,
        std::numeric_limits<uint64_t>::max());
    switch (vk_result)
    {
    case VK_SUCCESS:
        break;
    
    case VK_TIMEOUT:
        // This should never happen since the timeout argument to vkWaitForFences is UINT64_MAX
        return false;
        break;
    
    default:
        // Failure
        return false;
        break;
    }
    
    vk_result = vkResetFences(m_vulkan_device, 1, &out_frame.m_fence);
    if (VK_SUCCESS != vk_result)
    {
        return false;
    }
    
    uint32_t image_index{};
    vk_result = vkAcquireNextImageKHR(m_vulkan_device,
        m_swapchain,
        std::numeric_limits<uint64_t>::max(),
        out_frame.m_image_available_semaphore, // will be signaled when the application can use the image
        nullptr,
        &image_index);
    switch (vk_result)
    {
    case VK_SUCCESS:
        break;
    
    case VK_SUBOPTIMAL_KHR:
    case VK_ERROR_OUT_OF_DATE_KHR:
        return recreate_swap_chain(framebuffer_width, framebuffer_height);
        break;

    default:
        // Failure
        return false;
        break;
    }
    
    if (nullptr != out_frame.m_framebuffer)
    {
        vkDestroyFramebuffer(m_vulkan_device, out_frame.m_framebuffer, nullptr);
        out_frame.m_framebuffer = nullptr;
    }

    if (!create_framebuffer(m_vulkan_device,
        m_render_pass,
        framebuffer_width,
        framebuffer_height,
        m_swapchain_image_views.at(static_cast<std::size_t>(image_index)),
        m_depth_stencil.m_image_view,
        out_frame.m_framebuffer))
    {
        return false;
    }
    
    if (!record_command_buffer(framebuffer_width,
        framebuffer_height,
        out_frame.m_framebuffer,
        m_swapchain_images.at(static_cast<std::size_t>(image_index)),
        out_frame.m_command_buffer))
    {
        return false;
    }
    
    VkSubmitInfo submit_info = {};
    submit_info.sType = VK_STRUCTURE_TYPE_SUBMIT_INFO;
    submit_info.pNext = nullptr;
    submit_info.waitSemaphoreCount = 1;
    // Wait for image_available_semaphore to be signaled before the command buffers for this batch are executed.
    submit_info.pWaitSemaphores = &out_frame.m_image_available_semaphore;
    VkPipelineStageFlags wait_destination_stage_mask = VK_PIPELINE_STAGE_COLOR_ATTACHMENT_OUTPUT_BIT;
    // pWaitDstStageMask: pointer to an array with waitSemaphoreCount elements. pWaitSemaphores[i] will wait to be
    // signaled at pWaitDstStageMask[i]
    submit_info.pWaitDstStageMask = &wait_destination_stage_mask;
    submit_info.commandBufferCount = 1;
    submit_info.pCommandBuffers = &out_frame.m_command_buffer;
    submit_info.signalSemaphoreCount = 1;
    // Signal rendering_finished_semaphore when the command buffers for this batch have finished executing.
    submit_info.pSignalSemaphores = &out_frame.m_rendering_finished_semaphore;
    vk_result = vkQueueSubmit(m_graphics_queue, 1, &submit_info, out_frame.m_fence);
    if (VK_SUCCESS != vk_result)
    {
        return false;
    }
    
    VkPresentInfoKHR present_info = {};
    present_info.sType = VK_STRUCTURE_TYPE_PRESENT_INFO_KHR;
    present_info.pNext = nullptr;
    present_info.waitSemaphoreCount = 1;
    // Wait for rendering_finished_semaphore to be signaled before issuing the present request.
    present_info.pWaitSemaphores = &out_frame.m_rendering_finished_semaphore;
    present_info.swapchainCount = 1;
    present_info.pSwapchains = &m_swapchain;
    present_info.pImageIndices = &image_index;
    present_info.pResults = nullptr;
    vk_result = vkQueuePresentKHR(m_present_queue, &present_info);
    switch (vk_result)
    {
    case VK_SUCCESS:
        break;

    case VK_SUBOPTIMAL_KHR:
    case VK_ERROR_OUT_OF_DATE_KHR:
        return recreate_swap_chain(framebuffer_width, framebuffer_height);
        break;

    default:
        // Failure
        return false;
        break;
    }

    return true;
}

bool Application::create_buffer(VkBufferUsageFlags buffer_usage_flags,
    VkMemoryPropertyFlags memory_property_flags,
    VkDeviceSize buffer_size,
    Jade::Buffer &out_buffer) const
{
    VkBufferCreateInfo buffer_create_info = {};
    buffer_create_info.sType = VK_STRUCTURE_TYPE_BUFFER_CREATE_INFO;
    buffer_create_info.pNext = nullptr;
    buffer_create_info.flags = 0;
    buffer_create_info.size = buffer_size;
    out_buffer.m_size = buffer_size;
    buffer_create_info.usage = buffer_usage_flags;
    buffer_create_info.sharingMode = VK_SHARING_MODE_EXCLUSIVE;
    buffer_create_info.queueFamilyIndexCount = 0;
    buffer_create_info.pQueueFamilyIndices = nullptr;
    auto vk_result = vkCreateBuffer(m_vulkan_device, &buffer_create_info, nullptr, &out_buffer.m_buffer);
    if (VK_SUCCESS != vk_result)
    {
        return false;
    }

    vkGetBufferMemoryRequirements(m_vulkan_device, out_buffer.m_buffer, &out_buffer.m_memory_requirements);
    uint32_t memory_type_index{};
    if (!find_memory_type(m_physical_device_memory_properties,
        out_buffer.m_memory_requirements,
        memory_property_flags,
        memory_type_index))
    {
        return false;
    }

    VkMemoryAllocateInfo memory_allocate_info = {};
    memory_allocate_info.sType = VK_STRUCTURE_TYPE_MEMORY_ALLOCATE_INFO;
    memory_allocate_info.pNext = nullptr;
    memory_allocate_info.allocationSize = out_buffer.m_memory_requirements.size;
    memory_allocate_info.memoryTypeIndex = memory_type_index;
    vk_result = vkAllocateMemory(m_vulkan_device, &memory_allocate_info, nullptr, &out_buffer.m_device_memory);
    if (VK_SUCCESS != vk_result)
    {
        return false;
    }

    vk_result = vkBindBufferMemory(m_vulkan_device, out_buffer.m_buffer, out_buffer.m_device_memory, 0);
    if (VK_SUCCESS != vk_result)
    {
        return false;
    }

    return true;
}

bool Application::recreate_swap_chain(uint32_t framebuffer_width, uint32_t framebuffer_height)
{
    auto vk_result = vkDeviceWaitIdle(m_vulkan_device);
    if (VK_SUCCESS != vk_result)
    {
        return false;
    }
    
    for (auto iter = m_swapchain_image_views.cbegin(); iter != m_swapchain_image_views.cend(); ++iter)
    {
        auto image_view = *iter;
        if (nullptr != image_view)
        {
            vkDestroyImageView(m_vulkan_device, image_view, nullptr);
        }
    }

    m_swapchain_image_views.clear();
    m_swapchain_images.clear();
    auto old_swapchain = m_swapchain;
    if (!create_swapchain(
        framebuffer_width,
        framebuffer_height,
        old_swapchain,
        m_swapchain_image_format,
        m_swapchain_images,
        m_swapchain_image_views,
        m_swapchain))
    {
        return false;
    }

    return true;
}

bool Application::copy_vertex_data(VkCommandBuffer command_buffer) const
{
    void *data{nullptr};
    auto vk_result = vkMapMemory(m_vulkan_device,
        m_staging_buffer.m_device_memory,
        0,
        m_vertex_buffer.m_memory_requirements.size,
        0,
        &data);
    if (VK_SUCCESS != vk_result)
    {
        return false;
    }

    std::memcpy(data, m_cube.data(), static_cast<std::size_t>(sizeof(Jade::Vertex) * m_cube.size()));
    VkMappedMemoryRange mapped_memory_range = {};
    mapped_memory_range.sType = VK_STRUCTURE_TYPE_MAPPED_MEMORY_RANGE;
    mapped_memory_range.pNext = nullptr;
    mapped_memory_range.memory = m_staging_buffer.m_device_memory;
    mapped_memory_range.offset = 0;
    mapped_memory_range.size = m_vertex_buffer.m_memory_requirements.size;
    vk_result = vkFlushMappedMemoryRanges(m_vulkan_device, 1, &mapped_memory_range);
    if (VK_SUCCESS != vk_result)
    {
        return false;
    }

    vkUnmapMemory(m_vulkan_device, m_staging_buffer.m_device_memory);
    VkCommandBufferBeginInfo command_buffer_begin_info = {};
    command_buffer_begin_info.sType = VK_STRUCTURE_TYPE_COMMAND_BUFFER_BEGIN_INFO;
    command_buffer_begin_info.pNext = nullptr;
    command_buffer_begin_info.flags = VK_COMMAND_BUFFER_USAGE_ONE_TIME_SUBMIT_BIT;
    command_buffer_begin_info.pInheritanceInfo = nullptr;
    vk_result = vkBeginCommandBuffer(command_buffer, &command_buffer_begin_info);
    if (VK_SUCCESS != vk_result)
    {
        return false;
    }
    
    VkBufferCopy buffer_copy = {};
    buffer_copy.srcOffset = 0;
    buffer_copy.dstOffset = 0;
    buffer_copy.size = m_vertex_buffer.m_memory_requirements.size;
    vkCmdCopyBuffer(command_buffer, m_staging_buffer.m_buffer, m_vertex_buffer.m_buffer, 1, &buffer_copy);
    VkBufferMemoryBarrier buffer_memory_barrier = {};
    buffer_memory_barrier.sType = VK_STRUCTURE_TYPE_BUFFER_MEMORY_BARRIER;
    buffer_memory_barrier.pNext = nullptr;
    // The IntroductionToVulkan.Uniform_Buffers uses VK_ACCESS_MEMORY_WRITE_BIT. I think that
    // VK_ACCESS_TRANSFER_WRITE_BIT is more appropriate.
    buffer_memory_barrier.srcAccessMask = VK_ACCESS_TRANSFER_WRITE_BIT;
    buffer_memory_barrier.dstAccessMask = VK_ACCESS_VERTEX_ATTRIBUTE_READ_BIT;
    buffer_memory_barrier.srcQueueFamilyIndex = VK_QUEUE_FAMILY_IGNORED;
    buffer_memory_barrier.dstQueueFamilyIndex = VK_QUEUE_FAMILY_IGNORED;
    buffer_memory_barrier.buffer = m_vertex_buffer.m_buffer;
    buffer_memory_barrier.offset = 0;
    buffer_memory_barrier.size = m_vertex_buffer.m_memory_requirements.size;
    vkCmdPipelineBarrier(command_buffer,
        VK_PIPELINE_STAGE_TRANSFER_BIT,
        VK_PIPELINE_STAGE_VERTEX_INPUT_BIT,
        0,
        0,
        nullptr,
        1,
        &buffer_memory_barrier,
        0,
        nullptr);
    vk_result = vkEndCommandBuffer(command_buffer);
    if (VK_SUCCESS != vk_result)
    {
        return false;
    }

    VkSubmitInfo submit_info = {};
    submit_info.sType = VK_STRUCTURE_TYPE_SUBMIT_INFO;
    submit_info.pNext = nullptr;
    submit_info.waitSemaphoreCount = 0;
    submit_info.pWaitSemaphores = nullptr;
    submit_info.pWaitDstStageMask = nullptr;
    submit_info.commandBufferCount = 1;
    submit_info.pCommandBuffers = &command_buffer;
    submit_info.signalSemaphoreCount = 0;
    submit_info.pSignalSemaphores = nullptr;
    vk_result = vkQueueSubmit(m_graphics_queue, 1, &submit_info, nullptr);
    if (VK_SUCCESS != vk_result)
    {
        return false;
    }

    vk_result = vkDeviceWaitIdle(m_vulkan_device);
    if (VK_SUCCESS != vk_result)
    {
        return false;
    }

    return true;
}

bool Application::copy_uniform_data(VkCommandBuffer command_buffer) const
{
    float *begin_data{nullptr};
    auto vk_result = vkMapMemory(m_vulkan_device,
        m_staging_buffer.m_device_memory,
        0,
        m_uniform_buffer.m_memory_requirements.size,
        0,
        reinterpret_cast<void**>(&begin_data));
    if (VK_SUCCESS != vk_result)
    {
        return false;
    }

    {
        // 0: model matrix #1
        // 1: model matrix #2
        // 2: view matrix
        // 3: projection matrix
        // 4: invert y matrix
        std::vector<VkDeviceSize> offsets = { 0,
            m_uniform_buffer_offset,
            m_uniform_buffer_offset * 2,
            m_uniform_buffer_offset * 3,
            m_uniform_buffer_offset * 4 };
        std::vector<glm::mat4> matrices = { m_model_matrices.at(0),
            m_model_matrices.at(1),
            m_view_matrix,
            m_projection_matrix,
            m_invert_y_matrix };
        for (std::size_t i = 0; i != offsets.size(); ++i)
        {
            auto byte_offset = offsets.at(i);
            assert(0 == (byte_offset % sizeof(float)));
            auto float_offset = byte_offset / sizeof(float);
            auto data = begin_data + static_cast<ptrdiff_t>(float_offset);
            const auto &m = matrices.at(i);
            Math_utility::copy(m, data);
        }
    }
    
    VkMappedMemoryRange mapped_memory_range = {};
    mapped_memory_range.sType = VK_STRUCTURE_TYPE_MAPPED_MEMORY_RANGE;
    mapped_memory_range.pNext = nullptr;
    mapped_memory_range.memory = m_staging_buffer.m_device_memory;
    mapped_memory_range.offset = 0;
    mapped_memory_range.size = m_uniform_buffer.m_memory_requirements.size;
    vk_result = vkFlushMappedMemoryRanges(m_vulkan_device, 1, &mapped_memory_range);
    if (VK_SUCCESS != vk_result)
    {
        return false;
    }

    vkUnmapMemory(m_vulkan_device, m_staging_buffer.m_device_memory);
    VkCommandBufferBeginInfo command_buffer_begin_info = {};
    command_buffer_begin_info.sType = VK_STRUCTURE_TYPE_COMMAND_BUFFER_BEGIN_INFO;
    command_buffer_begin_info.pNext = nullptr;
    command_buffer_begin_info.flags = VK_COMMAND_BUFFER_USAGE_ONE_TIME_SUBMIT_BIT;
    command_buffer_begin_info.pInheritanceInfo = nullptr;
    vk_result = vkBeginCommandBuffer(command_buffer, &command_buffer_begin_info);
    if (VK_SUCCESS != vk_result)
    {
        return false;
    }

    VkBufferCopy buffer_copy = {};
    buffer_copy.srcOffset = 0;
    buffer_copy.dstOffset = 0;
    buffer_copy.size = m_uniform_buffer.m_memory_requirements.size;
    vkCmdCopyBuffer(command_buffer, m_staging_buffer.m_buffer, m_uniform_buffer.m_buffer, 1, &buffer_copy);
    VkBufferMemoryBarrier buffer_memory_barrier = {};
    buffer_memory_barrier.sType = VK_STRUCTURE_TYPE_BUFFER_MEMORY_BARRIER;
    buffer_memory_barrier.pNext = nullptr;
    // The IntroductionToVulkan.Uniform_Buffers uses VK_ACCESS_MEMORY_WRITE_BIT. I think that
    // VK_ACCESS_TRANSFER_WRITE_BIT is more appropriate.
    buffer_memory_barrier.srcAccessMask = VK_ACCESS_TRANSFER_WRITE_BIT;
    buffer_memory_barrier.dstAccessMask = VK_ACCESS_UNIFORM_READ_BIT;
    buffer_memory_barrier.srcQueueFamilyIndex = VK_QUEUE_FAMILY_IGNORED;
    buffer_memory_barrier.dstQueueFamilyIndex = VK_QUEUE_FAMILY_IGNORED;
    buffer_memory_barrier.buffer = m_uniform_buffer.m_buffer;
    buffer_memory_barrier.offset = 0;
    buffer_memory_barrier.size = m_uniform_buffer.m_memory_requirements.size;
    vkCmdPipelineBarrier(command_buffer,
        VK_PIPELINE_STAGE_TRANSFER_BIT,
        VK_PIPELINE_STAGE_VERTEX_INPUT_BIT,
        0,
        0,
        nullptr,
        1,
        &buffer_memory_barrier,
        0,
        nullptr);
    vk_result = vkEndCommandBuffer(command_buffer);
    if (VK_SUCCESS != vk_result)
    {
        return false;
    }

    VkSubmitInfo submit_info = {};
    submit_info.sType = VK_STRUCTURE_TYPE_SUBMIT_INFO;
    submit_info.pNext = nullptr;
    submit_info.waitSemaphoreCount = 0;
    submit_info.pWaitSemaphores = nullptr;
    submit_info.pWaitDstStageMask = nullptr;
    submit_info.commandBufferCount = 1;
    submit_info.pCommandBuffers = &command_buffer;
    submit_info.signalSemaphoreCount = 0;
    submit_info.pSignalSemaphores = nullptr;
    vk_result = vkQueueSubmit(m_graphics_queue, 1, &submit_info, nullptr);
    if (VK_SUCCESS != vk_result)
    {
        return false;
    }

    vk_result = vkDeviceWaitIdle(m_vulkan_device);
    if (VK_SUCCESS != vk_result)
    {
        return false;
    }

    return true;
}

bool Application::copy_uniform_data_started_recording(VkCommandBuffer command_buffer,
    const std::vector<VkDeviceSize> &offsets,
    const std::vector<glm::mat4> &matrices) const
{
    float *begin_data{nullptr};
    auto vk_result = vkMapMemory(m_vulkan_device,
        m_staging_buffer.m_device_memory,
        0,
        m_uniform_buffer.m_memory_requirements.size,
        0,
        reinterpret_cast<void**>(&begin_data));
    if (VK_SUCCESS != vk_result)
    {
        return false;
    }
    
    for (std::size_t i = 0; i != offsets.size(); ++i)
    {
        auto byte_offset = offsets.at(i);
        assert(0 == (byte_offset % sizeof(float)));
        auto float_offset = byte_offset / sizeof(float);
        auto data = begin_data + static_cast<ptrdiff_t>(float_offset);
        const auto &m = matrices.at(i);
        Math_utility::copy(m, data);
    }

    VkMappedMemoryRange mapped_memory_range = {};
    mapped_memory_range.sType = VK_STRUCTURE_TYPE_MAPPED_MEMORY_RANGE;
    mapped_memory_range.pNext = nullptr;
    mapped_memory_range.memory = m_staging_buffer.m_device_memory;
    mapped_memory_range.offset = 0;
    mapped_memory_range.size = m_uniform_buffer.m_memory_requirements.size;
    vk_result = vkFlushMappedMemoryRanges(m_vulkan_device, 1, &mapped_memory_range);
    if (VK_SUCCESS != vk_result)
    {
        return false;
    }

    vkUnmapMemory(m_vulkan_device, m_staging_buffer.m_device_memory);
    VkBufferCopy buffer_copy = {};
    buffer_copy.srcOffset = 0;
    buffer_copy.dstOffset = 0;
    buffer_copy.size = m_uniform_buffer.m_memory_requirements.size;
    vkCmdCopyBuffer(command_buffer, m_staging_buffer.m_buffer, m_uniform_buffer.m_buffer, 1, &buffer_copy);
    VkBufferMemoryBarrier buffer_memory_barrier = {};
    buffer_memory_barrier.sType = VK_STRUCTURE_TYPE_BUFFER_MEMORY_BARRIER;
    buffer_memory_barrier.pNext = nullptr;
    // The IntroductionToVulkan.Uniform_Buffers uses VK_ACCESS_MEMORY_WRITE_BIT. I think that
    // VK_ACCESS_TRANSFER_WRITE_BIT is more appropriate.
    buffer_memory_barrier.srcAccessMask = VK_ACCESS_TRANSFER_WRITE_BIT;
    buffer_memory_barrier.dstAccessMask = VK_ACCESS_UNIFORM_READ_BIT;
    buffer_memory_barrier.srcQueueFamilyIndex = VK_QUEUE_FAMILY_IGNORED;
    buffer_memory_barrier.dstQueueFamilyIndex = VK_QUEUE_FAMILY_IGNORED;
    buffer_memory_barrier.buffer = m_uniform_buffer.m_buffer;
    buffer_memory_barrier.offset = 0;
    buffer_memory_barrier.size = m_uniform_buffer.m_memory_requirements.size;
    vkCmdPipelineBarrier(command_buffer,
        VK_PIPELINE_STAGE_TRANSFER_BIT,
        VK_PIPELINE_STAGE_VERTEX_INPUT_BIT,
        0,
        0,
        nullptr,
        1,
        &buffer_memory_barrier,
        0,
        nullptr);
    return true;
}

bool Application::create_2d_image(VkDevice device,
    const VkPhysicalDeviceMemoryProperties &physical_device_memory_properties,
    VkFormat image_format,
    VkImageTiling image_tiling,
    VkImageUsageFlags image_usage_flags,
    VkImageAspectFlags image_aspect_flags,
    VkMemoryPropertyFlags memory_property_flags,
    uint32_t image_width,
    uint32_t image_height,
    Jade::Image &out_image) const
{
    VkImageCreateInfo image_create_info = {};
    image_create_info.sType = VK_STRUCTURE_TYPE_IMAGE_CREATE_INFO;
    image_create_info.pNext = nullptr;
    image_create_info.flags = 0;
    image_create_info.imageType = VK_IMAGE_TYPE_2D;
    image_create_info.format = image_format;
    // extent is measured in texels
    image_create_info.extent.width = image_width;
    image_create_info.extent.height = image_height;
    image_create_info.extent.depth = 1;
    image_create_info.mipLevels = 1;
    image_create_info.arrayLayers = 1;
    image_create_info.samples = VK_SAMPLE_COUNT_1_BIT;
    image_create_info.tiling = VK_IMAGE_TILING_OPTIMAL;
    image_create_info.usage = image_usage_flags;
    image_create_info.sharingMode = VK_SHARING_MODE_EXCLUSIVE;
    image_create_info.queueFamilyIndexCount = 0;
    image_create_info.pQueueFamilyIndices = nullptr;
    image_create_info.initialLayout = VK_IMAGE_LAYOUT_UNDEFINED;
    auto vk_result = vkCreateImage(device, &image_create_info, nullptr, &out_image.m_image);
    if (VK_SUCCESS != vk_result)
    {
        return false;
    }

    vkGetImageMemoryRequirements(device, out_image.m_image, &out_image.m_memory_requirements);
    uint32_t memory_type_index{};
    if (!find_memory_type(physical_device_memory_properties,
        out_image.m_memory_requirements,
        memory_property_flags,
        memory_type_index))
    {
        return false;
    }

    VkMemoryAllocateInfo memory_allocate_info = {};
    memory_allocate_info.sType = VK_STRUCTURE_TYPE_MEMORY_ALLOCATE_INFO;
    memory_allocate_info.pNext = nullptr;
    memory_allocate_info.allocationSize = out_image.m_memory_requirements.size;
    memory_allocate_info.memoryTypeIndex = memory_type_index;
    vk_result = vkAllocateMemory(device, &memory_allocate_info, nullptr, &out_image.m_device_memory);
    if (VK_SUCCESS != vk_result)
    {
        return false;
    }

    vk_result = vkBindImageMemory(device, out_image.m_image, out_image.m_device_memory, 0);
    if (VK_SUCCESS != vk_result)
    {
        return false;
    }

    VkImageViewCreateInfo image_view_create_info = {};
    image_view_create_info.sType = VK_STRUCTURE_TYPE_IMAGE_VIEW_CREATE_INFO;
    image_view_create_info.pNext = nullptr;
    image_view_create_info.flags = 0;
    image_view_create_info.image = out_image.m_image;
    image_view_create_info.viewType = VK_IMAGE_VIEW_TYPE_2D;
    image_view_create_info.format = image_create_info.format;
    image_view_create_info.components.r = VK_COMPONENT_SWIZZLE_IDENTITY;
    image_view_create_info.components.g = VK_COMPONENT_SWIZZLE_IDENTITY;
    image_view_create_info.components.b = VK_COMPONENT_SWIZZLE_IDENTITY;
    image_view_create_info.components.a = VK_COMPONENT_SWIZZLE_IDENTITY;
    image_view_create_info.subresourceRange.aspectMask = image_aspect_flags;
    image_view_create_info.subresourceRange.baseMipLevel = 0;
    image_view_create_info.subresourceRange.levelCount = 1;
    image_view_create_info.subresourceRange.baseArrayLayer = 0;
    image_view_create_info.subresourceRange.layerCount = 1;
    vk_result = vkCreateImageView(device, &image_view_create_info, nullptr, &out_image.m_image_view);
    if (VK_SUCCESS != vk_result)
    {
        return false;
    }

    return true;
}

bool Application::create_depth_stencil_image(VkDevice device,
    const VkPhysicalDeviceMemoryProperties &physical_device_memory_properties,
    VkFormat image_format,
    uint32_t image_width,
    uint32_t image_height,
    Jade::Image &out_image) const
{
    auto image_usage_flags = VK_IMAGE_USAGE_DEPTH_STENCIL_ATTACHMENT_BIT;
    auto image_aspect_flags = VK_IMAGE_ASPECT_DEPTH_BIT|VK_IMAGE_ASPECT_STENCIL_BIT;
    auto memory_property_flags = 0;
    auto image_tiling = VK_IMAGE_TILING_OPTIMAL;
    VkImageFormatProperties image_format_properties = {};
    auto vk_result = vkGetPhysicalDeviceImageFormatProperties(m_physical_device,
        image_format,
        VK_IMAGE_TYPE_2D,
        image_tiling,
        image_usage_flags,
        0,
        &image_format_properties);
    // If vkGetPhysicalDeviceImageFormatProperties returns VK_ERROR_FORMAT_NOT_SUPPORTED, could try again with
    // different arguments.
    if (VK_SUCCESS != vk_result)
    {
        return false;
    }

    return create_2d_image(m_vulkan_device,
        physical_device_memory_properties,
        image_format,
        image_tiling,
        image_usage_flags,
        image_aspect_flags,
        memory_property_flags,
        image_width,
        image_height,
        out_image);
}

bool Application::create_descriptor_pool(VkDescriptorPool &out_descriptor_pool) const
{
    std::vector<VkDescriptorPoolSize> vec_descriptor_pool_size;

    {
        VkDescriptorPoolSize descriptor_pool_size = {};
        descriptor_pool_size.type = VK_DESCRIPTOR_TYPE_UNIFORM_BUFFER;
        descriptor_pool_size.descriptorCount = descriptor_type_uniform_buffer_count;
        vec_descriptor_pool_size.push_back(descriptor_pool_size);
    }

    {
        VkDescriptorPoolSize descriptor_pool_size = {};
        descriptor_pool_size.type = VK_DESCRIPTOR_TYPE_UNIFORM_BUFFER_DYNAMIC;
        descriptor_pool_size.descriptorCount = descriptor_type_uniform_buffer_dynamic_count;
        vec_descriptor_pool_size.push_back(descriptor_pool_size);
    }

    VkDescriptorPoolCreateInfo descriptor_pool_create_info = {};
    descriptor_pool_create_info.sType = VK_STRUCTURE_TYPE_DESCRIPTOR_POOL_CREATE_INFO;
    descriptor_pool_create_info.pNext = nullptr;
    descriptor_pool_create_info.flags = 0;
    descriptor_pool_create_info.maxSets = 1;
    descriptor_pool_create_info.poolSizeCount = static_cast<uint32_t>(vec_descriptor_pool_size.size());
    descriptor_pool_create_info.pPoolSizes = vec_descriptor_pool_size.data();
    return VK_SUCCESS == vkCreateDescriptorPool(m_vulkan_device,
        &descriptor_pool_create_info,
        nullptr,
        &out_descriptor_pool);
}

bool Application::create_descriptor_set_layout(VkDescriptorSetLayout &descriptor_set_layout) const
{
    std::vector<VkDescriptorSetLayoutBinding> vec_descriptor_set_layout_binding;

    {
        VkDescriptorSetLayoutBinding descriptor_set_layout_binding = {};
        descriptor_set_layout_binding.binding = descriptor_binding_model_matrix;
        descriptor_set_layout_binding.descriptorType = VK_DESCRIPTOR_TYPE_UNIFORM_BUFFER_DYNAMIC;
        descriptor_set_layout_binding.descriptorCount = descriptor_type_uniform_buffer_dynamic_count;
        descriptor_set_layout_binding.stageFlags = VK_SHADER_STAGE_VERTEX_BIT;
        descriptor_set_layout_binding.pImmutableSamplers = nullptr;
        vec_descriptor_set_layout_binding.push_back(descriptor_set_layout_binding);
    }
    
    std::array<uint32_t, 3> bindings = { descriptor_binding_view_matrix,
        descriptor_binding_projection_matrix,
        descriptor_binding_invert_y_matrix };
    for (auto iter = bindings.cbegin(); iter != bindings.cend(); ++iter)
    {
        VkDescriptorSetLayoutBinding descriptor_set_layout_binding = {};
        descriptor_set_layout_binding.binding = *iter;
        descriptor_set_layout_binding.descriptorType = VK_DESCRIPTOR_TYPE_UNIFORM_BUFFER;
        descriptor_set_layout_binding.descriptorCount = 1;
        descriptor_set_layout_binding.stageFlags = VK_SHADER_STAGE_VERTEX_BIT;
        descriptor_set_layout_binding.pImmutableSamplers = nullptr;
        vec_descriptor_set_layout_binding.push_back(descriptor_set_layout_binding);
    }
    
    VkDescriptorSetLayoutCreateInfo descriptor_set_layout_create_info = {};
    descriptor_set_layout_create_info.sType = VK_STRUCTURE_TYPE_DESCRIPTOR_SET_LAYOUT_CREATE_INFO;
    descriptor_set_layout_create_info.pNext = nullptr;
    descriptor_set_layout_create_info.flags = 0;
    descriptor_set_layout_create_info.bindingCount = static_cast<uint32_t>(vec_descriptor_set_layout_binding.size());
    descriptor_set_layout_create_info.pBindings = vec_descriptor_set_layout_binding.data();
    return VK_SUCCESS == vkCreateDescriptorSetLayout(m_vulkan_device,
        &descriptor_set_layout_create_info,
        nullptr,
        &descriptor_set_layout);
}

bool Application::allocate_descriptor_set(VkDescriptorSet &out_descriptor_set) const
{
    VkDescriptorSetAllocateInfo descriptor_set_allocate_info = {};
    descriptor_set_allocate_info.sType = VK_STRUCTURE_TYPE_DESCRIPTOR_SET_ALLOCATE_INFO;
    descriptor_set_allocate_info.pNext = nullptr;
    descriptor_set_allocate_info.descriptorPool = m_descriptor_pool;
    descriptor_set_allocate_info.descriptorSetCount = 1;
    descriptor_set_allocate_info.pSetLayouts = &m_descriptor_set_layout;
    return VK_SUCCESS == vkAllocateDescriptorSets(m_vulkan_device, &descriptor_set_allocate_info, &out_descriptor_set);
}

void Application::update_descriptor_sets() const
{
    std::vector<VkWriteDescriptorSet> vec_write_descriptor_set;
    std::vector<VkDescriptorBufferInfo> vec_descriptor_buffer_info;
    
    {
        VkWriteDescriptorSet write_descriptor_set = {};
        write_descriptor_set.sType = VK_STRUCTURE_TYPE_WRITE_DESCRIPTOR_SET;
        write_descriptor_set.pNext = nullptr;
        write_descriptor_set.dstSet = m_descriptor_set;
        write_descriptor_set.dstBinding = descriptor_binding_model_matrix;
        write_descriptor_set.dstArrayElement = 0;
        write_descriptor_set.descriptorCount = descriptor_type_uniform_buffer_dynamic_count;
        write_descriptor_set.descriptorType = VK_DESCRIPTOR_TYPE_UNIFORM_BUFFER_DYNAMIC;
        write_descriptor_set.pImageInfo = nullptr;
        write_descriptor_set.pTexelBufferView = nullptr;
        VkDescriptorBufferInfo descriptor_buffer_info = {};
        descriptor_buffer_info.buffer = m_uniform_buffer.m_buffer;
        // offset must be a multiple of VkPhysicalDeviceLimits::minUniformBufferOffsetAlignment
        descriptor_buffer_info.offset = 0;
        // descriptor_binding_model_matrix is VK_DESCRIPTOR_TYPE_UNIFORM_BUFFER_DYNAMIC. There are two matrices
        // associated with the descriptor.
        descriptor_buffer_info.range = 2 * m_uniform_buffer_offset;
        vec_descriptor_buffer_info.push_back(descriptor_buffer_info);
        write_descriptor_set.pBufferInfo = nullptr;
        vec_write_descriptor_set.push_back(write_descriptor_set);
    }

    std::array<uint32_t, 3> bindings = { descriptor_binding_view_matrix,
        descriptor_binding_projection_matrix,
        descriptor_binding_invert_y_matrix };
    // 0: model matrix #1
    // 1: model matrix #2
    // 2: view matrix
    // 3: projection matrix
    // 4: invert y matrix
    std::array<VkDeviceSize, 3> offsets = { 2 * m_uniform_buffer_offset,
        3 * m_uniform_buffer_offset,
        4 * m_uniform_buffer_offset };
    for (std::size_t i = 0; i != bindings.size(); ++i)
    {
        VkWriteDescriptorSet write_descriptor_set = {};
        write_descriptor_set.sType = VK_STRUCTURE_TYPE_WRITE_DESCRIPTOR_SET;
        write_descriptor_set.pNext = nullptr;
        write_descriptor_set.dstSet = m_descriptor_set;
        write_descriptor_set.dstBinding = bindings.at(i);
        write_descriptor_set.dstArrayElement = 0;
        write_descriptor_set.descriptorCount = 1;
        write_descriptor_set.descriptorType = VK_DESCRIPTOR_TYPE_UNIFORM_BUFFER;
        write_descriptor_set.pImageInfo = nullptr;
        write_descriptor_set.pTexelBufferView = nullptr;
        VkDescriptorBufferInfo descriptor_buffer_info = {};
        descriptor_buffer_info.buffer = m_uniform_buffer.m_buffer;
        // offset must be a multiple of VkPhysicalDeviceLimits::minUniformBufferOffsetAlignment
        descriptor_buffer_info.offset = offsets.at(i);
        descriptor_buffer_info.range = sizeof(glm::mat4);
        vec_descriptor_buffer_info.push_back(descriptor_buffer_info);
        write_descriptor_set.pBufferInfo = nullptr;
        vec_write_descriptor_set.push_back(write_descriptor_set);
    }

    for (std::size_t i = 0; i != vec_write_descriptor_set.size(); ++i)
    {
        auto &write_descriptor_set = vec_write_descriptor_set.at(static_cast<std::size_t>(i));
        auto &descriptor_buffer_info = vec_descriptor_buffer_info.at(static_cast<std::size_t>(i));
        write_descriptor_set.pBufferInfo = &descriptor_buffer_info;
    }

    vkUpdateDescriptorSets(m_vulkan_device,
        static_cast<uint32_t>(vec_write_descriptor_set.size()),
        vec_write_descriptor_set.data(),
        0,
        nullptr);
}

// Cannot be const because it uses m_distribution
std::vector<Jade::Vertex> Application::create_cube()
{
    // Cube data is from GP1 Lab Project 3.1
    // Front facing triangles are CW.
    std::vector<Jade::Vertex> v =
    {
        // Front face
        Jade::Vertex{glm::vec4{-2.0f, 2.0f, -2.0f, 1.0f}, glm::vec4{}},
        Jade::Vertex{glm::vec4{2.0f, 2.0f, -2.0f, 1.0f}, glm::vec4{}},
        Jade::Vertex{glm::vec4{2.0f, -2.0f, -2.0f, 1.0f}, glm::vec4{}},

        Jade::Vertex{glm::vec4{-2.0f, 2.0f, -2.0f, 1.0f}, glm::vec4{}},
        Jade::Vertex{glm::vec4{2.0f, -2.0f, -2.0f, 1.0f}, glm::vec4{}},
        Jade::Vertex{glm::vec4{-2.0f, -2.0f, -2.0f, 1.0f}, glm::vec4{}},

        // Top face
        Jade::Vertex{glm::vec4{-2.0f, 2.0f, 2.0f, 1.0f}, glm::vec4{}},
        Jade::Vertex{glm::vec4{2.0f, 2.0f, 2.0f, 1.0f}, glm::vec4{}},
        Jade::Vertex{glm::vec4{2.0f, 2.0f, -2.0f, 1.0f}, glm::vec4{}},

        Jade::Vertex{glm::vec4{-2.0f, 2.0f, 2.0f, 1.0f}, glm::vec4{}},
        Jade::Vertex{glm::vec4{2.0f, 2.0f, -2.0f, 1.0f}, glm::vec4{}},
        Jade::Vertex{glm::vec4{-2.0f, 2.0f, -2.0f, 1.0f}, glm::vec4{}},

        // Back face
        Jade::Vertex{glm::vec4{-2.0f, -2.0f, 2.0f, 1.0f}, glm::vec4{}},
        Jade::Vertex{glm::vec4{2.0f, -2.0f, 2.0f, 1.0f}, glm::vec4{}},
        Jade::Vertex{glm::vec4{2.0f, 2.0f, 2.0f, 1.0f}, glm::vec4{}},

        Jade::Vertex{glm::vec4{-2.0f, -2.0f, 2.0f, 1.0f}, glm::vec4{}},
        Jade::Vertex{glm::vec4{2.0f, 2.0f, 2.0f, 1.0f}, glm::vec4{}},
        Jade::Vertex{glm::vec4{-2.0f, 2.0f, 2.0f, 1.0f}, glm::vec4{}},

        // Bottom face
        Jade::Vertex{glm::vec4{-2.0f, -2.0f, -2.0f, 1.0f}, glm::vec4{}},
        Jade::Vertex{glm::vec4{2.0f, -2.0f, -2.0f, 1.0f}, glm::vec4{}},
        Jade::Vertex{glm::vec4{2.0f, -2.0f, 2.0f, 1.0f}, glm::vec4{}},

        Jade::Vertex{glm::vec4{-2.0f, -2.0f, -2.0f, 1.0f}, glm::vec4{}},
        Jade::Vertex{glm::vec4{2.0f, -2.0f, 2.0f, 1.0f}, glm::vec4{}},
        Jade::Vertex{glm::vec4{-2.0f, -2.0f, 2.0f, 1.0f}, glm::vec4{}},

        // Left face
        Jade::Vertex{glm::vec4{-2.0f, 2.0f, 2.0f, 1.0f}, glm::vec4{}},
        Jade::Vertex{glm::vec4{-2.0f, 2.0f, -2.0f, 1.0f}, glm::vec4{}},
        Jade::Vertex{glm::vec4{-2.0f, -2.0f, -2.0f, 1.0f}, glm::vec4{}},

        Jade::Vertex{glm::vec4{-2.0f, 2.0f, 2.0f, 1.0f}, glm::vec4{}},
        Jade::Vertex{glm::vec4{-2.0f, -2.0f, -2.0f, 1.0f}, glm::vec4{}},
        Jade::Vertex{glm::vec4{-2.0f, -2.0f, 2.0f, 1.0f}, glm::vec4{}},

        // Right face
        Jade::Vertex{glm::vec4{2.0f, 2.0f, -2.0f, 1.0f}, glm::vec4{}},
        Jade::Vertex{glm::vec4{2.0f, 2.0f, 2.0f, 1.0f}, glm::vec4{}},
        Jade::Vertex{glm::vec4{2.0f, -2.0f, 2.0f, 1.0f}, glm::vec4{}},

        Jade::Vertex{glm::vec4{2.0f, 2.0f, -2.0f, 1.0f}, glm::vec4{}},
        Jade::Vertex{glm::vec4{2.0f, -2.0f, 2.0f, 1.0f}, glm::vec4{}},
        Jade::Vertex{glm::vec4{2.0f, -2.0f, -2.0f, 1.0f}, glm::vec4{}}
    };
    
    for (auto iter = v.begin(); iter != v.end(); ++iter)
    {
        auto r = m_distribution(m_random_engine);
        auto g = m_distribution(m_random_engine);
        auto b = m_distribution(m_random_engine);
        // RGBA
        iter->m_color = glm::vec4{r, g, b, 1.0f};
    }

    return v;
}

// The application is going to store five mat4 objects in a single buffer. However, VkDescriptorBufferInfo::offset must
// be a multiple of VkPhysicalDeviceProperties::minUniformBufferOffsetAlignment.
// mat4 0 starts at 0
// mat4 1 starts at size
// mat4 2 starts at 2*size
// mat4 3 starts at 3*size
// mat4 4 starts at 4*size
VkDeviceSize Application::determine_uniform_buffer_offset(VkDeviceSize minimum_uniform_buffer_offset_alignment) const
{
    // Assuming that a float is 4 bytes, size_of_mat4 is 64 bytes.
    auto size_of_mat4 = static_cast<VkDeviceSize>(sizeof(glm::mat4));
    VkDeviceSize count{1};
    auto size = count * minimum_uniform_buffer_offset_alignment;
    while (size < size_of_mat4)
    {
        ++count;
        size = count * minimum_uniform_buffer_offset_alignment;
    }

    return size;
}

void Application::process_input()
{
    // Direct3D view matrix
    //
    // right.x,               up.x,               look.x,               0
    // right.y,               up.y,               look.y,               0
    // right.z,               up.z,               look.z,               0
    // -(position dot right), -(position dot up), -(position dot look), 1
    //
    // Inverted Direct3D view matrix
    //
    // right.x, right.y, right.z, -(position dot right),
    // up.x,    up.y,    up.z,    -(position dot up)
    // look.x,  look.y,  look.z,  -(position dot look)
    // 0,       0,       0,       1
    //
    // right is always <1, 0, 0> (in this application)
    // -(position dot right) = -(<position.x, position.y, position.z> dot <1, 0, 0>)
    // -(position dot right) = -(position.x * 1)
    // -(position dot right) = -(position.x)
    // Therefore if the view matrix is view_matrix,
    // view_matrix[3][0] += 25.0f * m_timer.get_time_elapsed() is equivalent to:
    // -position.x += 25.0f * m_timer.get_time_elapsed()
    const auto distance = 25.0f;
    for (auto iter = m_key_map.cbegin(); iter != m_key_map.cend(); ++iter)
    {
        switch (iter->first)
        {
        
        case GLFW_KEY_LEFT:
            if ((GLFW_PRESS == iter->second) || (GLFW_REPEAT == iter->second))
            {
                m_view_matrix[3][0] += (distance * static_cast<float>(m_timer.get_time_elapsed()));
            }
            break;

        case GLFW_KEY_RIGHT:
            if ((GLFW_PRESS == iter->second) || (GLFW_REPEAT == iter->second))
            {
                m_view_matrix[3][0] -= (distance * static_cast<float>(m_timer.get_time_elapsed()));
            }
            break;
        }
    }
}

void Application::animate_objects()
{
    if (m_animate.at(0))
    {
        auto yaw = glm::radians(75.0 * m_timer.get_time_elapsed());
        auto pitch = glm::radians(50.0 * m_timer.get_time_elapsed());
        auto roll = glm::radians(25.0 * m_timer.get_time_elapsed());
        auto matrix_yaw = glm::rotate(glm::mat4{}, static_cast<float>(yaw), glm::vec3{0.0f, 1.0f, 0.0f});
        auto matrix_pitch = glm::rotate(glm::mat4{}, static_cast<float>(pitch), glm::vec3{1.0f, 0.0f, 0.0f});
        auto matrix_roll = glm::rotate(glm::mat4{}, static_cast<float>(roll), glm::vec3{0.0f, 0.0f, 1.0f});
        // See D3DXMatrixRotationYawPitchRoll
        // (1) roll
        // (2) pitch
        // (3) yaw
        auto m = matrix_yaw * matrix_pitch * matrix_roll;
        m_model_matrices.at(0) = m_model_matrices.at(0) * m;
    }

    if (m_animate.at(1))
    {
        auto yaw = glm::radians(-25.0 * m_timer.get_time_elapsed());
        auto pitch = glm::radians(50.0 * m_timer.get_time_elapsed());
        auto roll = glm::radians(-75.0 * m_timer.get_time_elapsed());
        auto matrix_yaw = glm::rotate(glm::mat4{}, static_cast<float>(yaw), glm::vec3{0.0f, 1.0f, 0.0f});
        auto matrix_pitch = glm::rotate(glm::mat4{}, static_cast<float>(pitch), glm::vec3{1.0f, 0.0f, 0.0f});
        auto matrix_roll = glm::rotate(glm::mat4{}, static_cast<float>(roll), glm::vec3{0.0f, 0.0f, 1.0f});
        // See D3DXMatrixRotationYawPitchRoll
        // (1) roll
        // (2) pitch
        // (3) yaw
        auto m = matrix_yaw * matrix_pitch * matrix_roll;
        m_model_matrices.at(1) = m_model_matrices.at(1) * m;
    }
}

bool Application::find_depth_stencil_image_format(VkFormat &out_format) const
{
    auto image_usage_flags = VK_IMAGE_USAGE_DEPTH_STENCIL_ATTACHMENT_BIT;
    auto image_aspect_flags = VK_IMAGE_ASPECT_DEPTH_BIT | VK_IMAGE_ASPECT_STENCIL_BIT;
    auto memory_property_flags = 0;
    auto image_tiling = VK_IMAGE_TILING_OPTIMAL;
    VkImageFormatProperties image_format_properties = {};
    std::vector<VkFormat> image_formats = {VK_FORMAT_D24_UNORM_S8_UINT, VK_FORMAT_D16_UNORM_S8_UINT};
    for (auto iter = image_formats.cbegin(); iter != image_formats.cend(); ++iter)
    {
        auto vk_result = vkGetPhysicalDeviceImageFormatProperties(m_physical_device,
            *iter,
            VK_IMAGE_TYPE_2D,
            image_tiling,
            image_usage_flags,
            0,
            &image_format_properties);
        // vkGetPhysicalDeviceImageFormatProperties can return VK_ERROR_FORMAT_NOT_SUPPORTED
        if (VK_SUCCESS == vk_result)
        {
            out_format = *iter;
            return true;
        }
    }

    return false;
}

}