// This is a personal academic project. Dear PVS-Studio, please check it.
// PVS-Studio Static Code Analyzer for C, C++ and C#: http://www.viva64.com
#include "file_utility.h"

// C++ Standard Library
#include <fstream>

namespace Jade
{

namespace File_utility
{

bool load_spirv(const std::string &file_name, std::vector<uint32_t> &out_spirv)
{
    std::ifstream ifs(file_name, std::ios_base::in|std::ios_base::binary|std::ios_base::ate);
    if (!ifs)
    {
        return false;
    }

    auto position = ifs.tellg();
    if (-1 == position)
    {
        return false;
    }

    ifs.seekg(0);
    if (!ifs)
    {
        return false;
    }

    // position is the number of bytes in the file.
    // length is number of uint32_t in the file.
    // length * 4 = position
    // length = position / 4
    std::size_t length = static_cast<std::size_t>(position) / static_cast<std::size_t>(sizeof(uint32_t));
    out_spirv.resize(length);
    ifs.read(reinterpret_cast<char*>(out_spirv.data()), static_cast<std::streamsize>(position)); //-V206
    if (!ifs)
    {
        return false;
    }

    return true;
}

}

}