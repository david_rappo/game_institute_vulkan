#pragma once

// GLM
#include <glm/mat4x4.hpp>
#include <glm/vec4.hpp>

namespace Jade
{

namespace Math_utility
{

glm::mat4 perspective_projection_left_handed(float vertical_field_of_view,
    float aspect_ratio,
    float near_view_plane,
    float far_view_plane);

glm::mat4 invert_y_matrix();

void copy(const glm::mat4 &source, float *out_data);

}

}
