// This is a personal academic project. Dear PVS-Studio, please check it.
// PVS-Studio Static Code Analyzer for C, C++ and C#: http://www.viva64.com
#include "math_utility.h"

// C++ Standard Library
#include <cassert>
#include <cmath>

namespace Jade
{

namespace Math_utility
{

// Intended for use with a Direct3D coordinate system. Similar to D3DXMatrixPerspectiveFovLH.
//
// Vulkan is going to perform perspective division on the 4D vector output from the projection matrix e.g.
// v' = (v.x/v.w, v.y/v.w, v.z/v.w, v.w/v.w)
// If the z component of v' is outside the range [0.0f, 1.0f] v' will be rejected.
// m needs to do: (z - near_view_plane) * ratio
// ratio * (z - near_view_plane) = ratio * z - ratio * near_view_plane
// ratio * (z - near_view_plane) = (ratio * z) + (-ratio * near_view_plane)
//
// m:
// q / aspect_ratio 0 0     0
// 0                q 0     0
// 0                0 ratio -ratio * near_view_plane
// 0                0 1     0
//
// v:
// x
// y
// z
// 1
//
// 4x4 by 4x1 = 4x1
// m by v = V
// V.x = First row in m dot first column in v
// V.y = Second row in m dot first column in v
// V.z = Third row in m dot first column in v
// V.w = Fourth row in m dot first column in v
//
// The fourth row of m will copy v.z into V.w
//
// Example. near_view_plane = 10, far_view_plane = 100
// ratio = 100 / 90
// ratio = 10 / 9
// V.z = ratio * v.z + (-ratio * near_view_plane) * v.w
// V.z = ratio * v.z + (-ratio * near_view_plane)
// V.z = 10/9 * v.z + (-10/9 * 10)
// V.z = 10/9 * v.z + (-100/9)
// If v.z = 10, then:
// V.z = 10/9 * 10 + (-100/9)
// V.z = 100/9 + (-100/9)
// V.z = 0
// perspective projection
// V.z' = V.z / V.w
// V.z' = 0 / 10
// V.z' = 0
// If v.z = 100, then:
// V.z = 10/9 * 100 + (-100/9)
// V.z = 1000/9 + (-100/9)
// V.z = 900/9
// V.z = 100
// perspective projection
// V.z' = V.z / V.w
// V.z' = 100 / 100
// V.z' = 1
//
// vertical_field_of_view: Measured in Radians
// return: The return value is not the same as the matrix returned by D3DXMatrixPerspectiveFovLH because GLSL uses
// column vectors (not row vectors like Direct3D)
glm::mat4 perspective_projection_left_handed(float vertical_field_of_view,
    float aspect_ratio,
    float near_view_plane,
    float far_view_plane)
{
    glm::mat4 m;
    auto q = 1.0f / std::tan(vertical_field_of_view * 0.5f);
    // First column
    m[0][0] = q / aspect_ratio;
    m[0][1] = 0.0f;
    m[0][2] = 0.0f;
    m[0][3] = 0.0f;
    // Second column
    m[1][0] = 0.0f;
    m[1][1] = q;
    m[1][2] = 0.0f;
    m[1][3] = 0.0f;
    // Third column
    auto ratio = far_view_plane / (far_view_plane - near_view_plane);
    m[2][0] = 0.0f;
    m[2][1] = 0.0f;
    m[2][2] = ratio;
    m[2][3] = 1.0f;
    // Fourth column
    m[3][0] = 0.0f;
    m[3][1] = 0.0f;
    m[3][2] = -ratio * near_view_plane;
    m[3][3] = 0.0f;
    return m;
}

// return: Returns a matrix that inverts the y component of the vector that is multiplied by it. The returned matrix is
// intended to be used with column vectors.
glm::mat4 invert_y_matrix()
{
    // 4x4 by 4x1 = 4x1
    // m by v = V
    // V.x = First row in m dot first column in v
    // V.y = Second row in m dot first column in v
    // V.z = Third row in m dot first column in v
    // V.w = Fourth row in m dot first column in v
    //
    // 1 0  0 0
    // 0 -1 0 0
    // 0 0  1 0
    // 0 0  0 1
    glm::mat4 m{};
    m[1][1] = -1.0f;
    return m;
}

// Copy source into out_data.
void copy(const glm::mat4 &source, float *out_data)
{
    for (glm::mat4::length_type column_index = 0; column_index != source.length(); ++column_index)
    {
        const auto &column = source[column_index];
        for (glm::vec4::length_type row_index = 0; row_index != column.length(); ++row_index)
        {
            (*out_data) = column[static_cast<std::size_t>(row_index)];
            ++out_data;
        }
    }
}

}

}