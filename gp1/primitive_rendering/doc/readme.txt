readme.txt

Application: primitive_rendering

About

Vulkan port of Lab Project 3.1 from Graphics Programming with DirectX 9 Part 1 (https://www.gameinstitute.com/)

Draws two colored animating cubes.

Controls

0: Toggle animation of the first cube
1: Toggle animation of the second cube
Left arrow: move left
Right arrow: move right