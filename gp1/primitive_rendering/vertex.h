#pragma once

// GLM
#include <glm/vec2.hpp>
#include <glm/vec4.hpp>

namespace Jade
{

class Vertex
{
public:
    Vertex() = default;
    Vertex(const glm::vec4 &position, const glm::vec4 &color): m_position{position}, m_color{color}
    {

    }
    
    glm::vec4 m_position;
    glm::vec4 m_color;
};

}