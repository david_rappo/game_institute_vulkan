// This is a personal academic project. Dear PVS-Studio, please check it.
// PVS-Studio Static Code Analyzer for C, C++ and C#: http://www.viva64.com
#include "timer.h"

// C++ Standard Library
#include <cassert>
#include <cmath>

// GLFW
#include <GLFW/glfw3.h>

namespace Jade
{

void Timer::start(double previous_time)
{
    m_previous_time = previous_time;
}

void Timer::tick()
{
    m_time = glfwGetTime();
    assert(m_time >= m_previous_time);
    m_time_elapsed = m_time - m_previous_time;
    m_previous_time = m_time;
    ++m_frame_count;
    m_frames_per_second_time += m_time_elapsed;
    if (m_frames_per_second_time >= 1.0)
    {
        m_frame_rate = m_frame_count;
        m_frames_per_second_time = 0.0;
        m_frame_count = 0;
    }
}

int Timer::get_frame_rate() const
{
    return m_frame_rate;
}

double Timer::get_time_elapsed() const
{
    return m_time_elapsed;
}

}
