#pragma once

// C++ Standard Library
#include <cstdlib>
#include <string>
#include <map>
#include <random>
#include <vector>

// GLFW
#include <GLFW/glfw3.h>

// GLM
#include <glm/mat4x4.hpp>
#include <glm/vec4.hpp>

// Jade
#include "buffer.h"
#include "frame.h"
#include "image.h"
#include "timer.h"
#include "vertex.h"

namespace Jade
{

class Stb_image;

}

namespace Jade
{

class Application
{
public:
    Application();
    ~Application();
    bool initialize(const std::vector<std::string> &arguments);
    void run();

private:
    static const uint32_t command_buffer_count{3};
    // One view matrix
    // One projection matrix
    // One invert y matrix
    static const uint32_t descriptor_type_uniform_buffer_count{3};
    // Two model matrices are stored in the uniform buffer although there is only one descriptor.
    static const uint32_t descriptor_type_uniform_buffer_dynamic_count{1};
    static const uint32_t descriptor_binding_model_matrix{0};
    static const uint32_t descriptor_binding_view_matrix{1};
    static const uint32_t descriptor_binding_projection_matrix{2};
    static const uint32_t descriptor_binding_invert_y_matrix{3};

    static void glfw_error_callback(int error, const char *description);
    static void glfw_window_close_callback(GLFWwindow *window);
    static void glfw_frame_buffer_size_callback(GLFWwindow *window, int width, int height);
    static void glfw_key_callback(GLFWwindow *window, int key, int scan_code, int action, int modifiers);
    static bool check_instance_layers(const std::vector<std::string> &instance_layer_names);
    static bool check_instance_extensions(const std::vector<std::string> &extension_names);
    static bool check_device_extensions(VkPhysicalDevice device, const std::vector<std::string> &extension_names);
    static VkBool32 VKAPI_PTR vulkan_debug_report_callback(VkDebugReportFlagsEXT flags,
        VkDebugReportObjectTypeEXT object_type,
        uint64_t object,
        size_t location,
        int32_t message_code,
        const char *layer_prefix,
        const char *message,
        void *user_data);
    static bool find_memory_type(const VkPhysicalDeviceMemoryProperties &physical_device_memory_properties,
        const VkMemoryRequirements &memory_requirements,
        VkFlags requirements,
        uint32_t &memory_type_index);
    void key_callback(GLFWwindow *window, int key, int scan_code, int action, int modifiers);
    void frame_buffer_resized(int new_width, int new_height);
    bool create_instance(const std::vector<std::string> &instance_extensions, VkInstance &out_instance) const;
    bool check_physical_device(VkPhysicalDevice physical_device,
        const std::vector<std::string> &device_extensions,
        VkSurfaceKHR present_surface,
        uint32_t &out_graphics_queue_family_index,
        uint32_t &out_present_queue_family_index) const;
    bool create_debug_report_callback(VkInstance instance, VkDebugReportCallbackEXT &out_callback) const;
    bool create_device(VkPhysicalDevice &out_physical_device,
        VkDevice &out_vulkan_device,
        uint32_t &out_graphics_queue_family_index,
        uint32_t &out_present_queue_family_index) const;
    bool create_swapchain(
        uint32_t width,
        uint32_t height,
        VkSwapchainKHR &out_old_swapchain,
        VkFormat &out_swapchain_image_format,
        std::vector<VkImage> &out_swapchain_images,
        std::vector<VkImageView> &out_swapchain_image_views,
        VkSwapchainKHR &out_swapchain) const;
    bool create_render_pass(VkRenderPass &out_render_pass) const;
    bool create_framebuffer(VkDevice device,
        VkRenderPass render_pass,
        uint32_t width,
        uint32_t height,
        VkImageView image_view,
        VkImageView depth_stencil_view,
        VkFramebuffer &out_framebuffer) const;
    bool create_shader_modules(const std::vector<std::string> &shader_file_names,
        std::vector<VkShaderModule> &out_shader_modules) const;
    bool create_pipeline(const std::vector<VkShaderStageFlagBits> &shader_stages,
        VkPipelineLayout &out_pipeline_layout,
        VkPipeline &out_pipeline) const;
    bool create_semaphores(uint32_t create_count, std::vector<VkSemaphore> &out_semaphores) const;
    bool create_fences(uint32_t create_count, std::vector<VkFence> &out_vec_fence) const;
    bool create_command_pool(VkCommandPool &out_command_pool) const;
    bool allocate_command_buffers(uint32_t count, std::vector<VkCommandBuffer> &out_command_buffers) const;
    bool record_command_buffer(uint32_t framebuffer_width,
        uint32_t framebuffer_height,
        VkFramebuffer framebuffer,
        VkImage swapchain_image,
        VkCommandBuffer command_buffer) const;
    bool draw(uint32_t framebuffer_width, uint32_t framebuffer_height, Jade::Frame &out_frame);
    bool create_buffer(
        VkBufferUsageFlags buffer_usage_flags,
        VkMemoryPropertyFlags memory_property_flags,
        VkDeviceSize buffer_size,
        Jade::Buffer &out_buffer) const;
    bool recreate_swap_chain(uint32_t framebuffer_width, uint32_t framebuffer_height);
    bool copy_vertex_data(VkCommandBuffer command_buffer) const;
    bool copy_uniform_data(VkCommandBuffer command_buffer) const;
    bool copy_uniform_data_started_recording(VkCommandBuffer command_buffer,
        const std::vector<VkDeviceSize> &offsets,
        const std::vector<glm::mat4> &matrices) const;
    bool create_2d_image(VkDevice device,
        const VkPhysicalDeviceMemoryProperties &physical_device_memory_properties,
        VkFormat image_format, // e.g. VK_FORMAT_R8G8B8A8_UNORM
        VkImageTiling image_tiling, // e.g. VK_IMAGE_TILING_OPTIMAL
        VkImageUsageFlags image_usage_flags, // e.g. VK_IMAGE_USAGE_TRANSFER_DST_BIT|VK_IMAGE_USAGE_SAMPLED_BIT
        VkImageAspectFlags image_aspect_flags, // e.g. VK_IMAGE_ASPECT_COLOR_BIT
        VkMemoryPropertyFlags memory_property_flags, // e.g. VK_MEMORY_PROPERTY_DEVICE_LOCAL_BIT
        uint32_t image_width,
        uint32_t image_height,
        Jade::Image &out_image) const;
    bool create_depth_stencil_image(VkDevice device,
        const VkPhysicalDeviceMemoryProperties &physical_device_memory_properties,
        VkFormat image_format,
        uint32_t image_width,
        uint32_t image_height,
        Jade::Image &out_image) const;
    bool create_descriptor_pool(VkDescriptorPool &out_descriptor_pool) const;
    bool create_descriptor_set_layout(VkDescriptorSetLayout &descriptor_set_layout) const;
    bool allocate_descriptor_set(VkDescriptorSet &out_descriptor_set) const;
    void update_descriptor_sets() const;
    std::vector<Jade::Vertex> create_cube();
    VkDeviceSize determine_uniform_buffer_offset(VkDeviceSize minimum_uniform_buffer_offset_alignment) const;
    void process_input();
    void animate_objects();
    bool find_depth_stencil_image_format(VkFormat &out_format) const;

    // begin: see Application()
    std::default_random_engine m_random_engine;
    std::uniform_real_distribution<float> m_distribution;
    glm::mat4 m_invert_y_matrix{};
    std::vector<Jade::Vertex> m_cube;
    // Stores model matrices for each cube
    std::vector<glm::mat4> m_model_matrices;
    // Stores one boolean per cube. If m_animate[i] is true, animate the cube at index i.
    std::vector<bool> m_animate;
    // end: see Application()
    glm::mat4 m_view_matrix{};
    glm::mat4 m_projection_matrix{};
    VkFormat m_depth_stencil_image_format{};
    bool m_window_should_close{false};
    GLFWwindow *m_glfw_window{nullptr};
    std::string m_application_name{"Jade"};
    std::string m_engine_name{"Jade"};
    std::vector<std::string> m_instance_layer_names{"VK_LAYER_LUNARG_standard_validation"};
    std::vector<std::string> m_device_extension_names{"VK_KHR_swapchain"};
    uint32_t m_graphics_queue_family_index{};
    uint32_t m_present_queue_family_index{};
    VkInstance m_vulkan_instance{nullptr};
    VkSurfaceKHR m_surface{nullptr};
    // The application does not need to free/destroy the physical device
    VkPhysicalDevice m_physical_device{nullptr};
    VkDevice m_vulkan_device{nullptr};
    VkDescriptorPool m_descriptor_pool{nullptr};
    VkDescriptorSetLayout m_descriptor_set_layout{nullptr};
    VkDescriptorSet m_descriptor_set{nullptr};
    // The application does not need to free/destroy the queues
    VkQueue m_graphics_queue{nullptr};
    VkQueue m_present_queue{nullptr};
    VkFormat m_swapchain_image_format{};
    VkSwapchainKHR m_swapchain{nullptr};
    // The application does not need to free/destroy the swapchain images
    std::vector<VkImage> m_swapchain_images;
    std::vector<VkImageView> m_swapchain_image_views;
    VkRenderPass m_render_pass{nullptr};
    std::vector<VkShaderModule> m_shader_modules;
    VkPipelineLayout m_pipeline_layout{nullptr};
    VkPipeline m_pipeline{nullptr};
    Jade::Buffer m_vertex_buffer;
    Jade::Buffer m_uniform_buffer;
    Jade::Buffer m_staging_buffer;
    VkCommandPool m_command_pool{nullptr};
    VkPhysicalDeviceMemoryProperties m_physical_device_memory_properties{};
    VkPhysicalDeviceProperties m_physical_device_properties{};
    std::vector<Jade::Frame> m_vec_frame;
    Jade::Image m_depth_stencil;
    Jade::Timer m_timer;
    // Key = GLFW identifier of keyboard key
    // Value = state
    std::map<int, int> m_key_map;
    
    // The offset between elements in the uniform buffer in bytes e.g.
    // element 0 begins at 0
    // element 1 begins at m_uniform_buffer_offset
    // element 2 begins at 2 * m_uniform_buffer_offset
    // ...
    // This is calculated using VkDescriptorBufferInfo::offset.
    // See Application::determine_uniform_buffer_offset.
    VkDeviceSize m_uniform_buffer_offset{};
#ifdef _DEBUG
    // Destroyed in ~Application
    VkDebugReportCallbackEXT m_debug_report_callback{nullptr};
#endif
};

}
