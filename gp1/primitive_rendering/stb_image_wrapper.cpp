// This is a personal academic project. Dear PVS-Studio, please check it.
// PVS-Studio Static Code Analyzer for C, C++ and C#: http://www.viva64.com
#include "stb_image_wrapper.h"
#define STB_IMAGE_IMPLEMENTATION
#include "stb/stb_image.h"

// C++ Standard Library
#include <cassert>
#include <cstddef>
#include <memory>

namespace Jade
{

Stb_image::Stb_image() = default;

// number_of_components: The number of components desired e.g. specify 4 to force four components per pixel.
bool Stb_image::load(const std::string &file_name, int number_of_components)
{
    auto data = stbi_load(file_name.c_str(),
        &m_width,
        &m_number_of_scanlines,
        &m_number_of_components,
        number_of_components);
    if (nullptr == data)
    {
        return false;
    }

    // m_number_of_components stores the actual number of components in file_name immediately after calling stbi_load.
    // If number_of_components is not zero (meaning that the caller forced stbi_load to use a specific number of
    // components per pixel), overwrite m_number_of_components with number_of_components.
    if (0 != number_of_components)
    {
        m_number_of_components = number_of_components;
    }
    
    std::unique_ptr<unsigned char, void(*)(void*)> up_data(data, stbi_image_free);
    auto size = m_number_of_scanlines * m_width * m_number_of_components;
    m_data = std::vector<unsigned char>(data, data + static_cast<std::ptrdiff_t>(size));
    return true;
}

Stb_image::Pixel Stb_image::get_pixel(int x, int scanline) const
{
    auto index_int = (m_width * m_number_of_components * scanline) + (x * m_number_of_components);
    auto index = static_cast<std::size_t>(index_int);
    Stb_image::Pixel pixel;
    switch (m_number_of_components)
    {
    case 4:
        pixel.m_red = m_data.at(index);
        pixel.m_green = m_data.at(index + 1);
        pixel.m_blue = m_data.at(index + 2);
        pixel.m_alpha = m_data.at(index + 3);
        break;
    case 3:
        pixel.m_red = m_data.at(index);
        pixel.m_green = m_data.at(index + 1);
        pixel.m_blue = m_data.at(index + 2);
        break;
    case 2:
        pixel.m_red = m_data.at(index);
        pixel.m_alpha = m_data.at(index + 1);
        break;
    case 1:
        pixel.m_red = m_data.at(index);
        break;
    default:
        assert(false);
        break;
    }

    return pixel;
}

}