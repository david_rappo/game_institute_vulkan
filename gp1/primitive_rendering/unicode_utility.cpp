// This is a personal academic project. Dear PVS-Studio, please check it.
// PVS-Studio Static Code Analyzer for C, C++ and C#: http://www.viva64.com
#include "unicode_utility.h"

// C++ Standard Library
#include <vector>

// Windows
#include <Windows.h>

namespace Jade
{

namespace Unicode
{

std::string wide_to_multi_byte(const std::wstring &wide_string)
{
    auto n = WideCharToMultiByte(CP_UTF8, // code page
                                 0, // flags - must be zero for code page CP_UTF8
                                 wide_string.c_str(),
                                 static_cast<int>(wide_string.size()),
                                 nullptr, // pointer to a buffer that receives the converted string
                                 0, // size of buffer that receives the converted string
                                 nullptr, // default char
                                 nullptr); // used default char
    if (0 == n)
    {
        return std::string{};
    }

    std::vector<char> vec(n);
    n = WideCharToMultiByte(CP_UTF8, // code page
                            0, // flags - must be zero for code page CP_UTF8
                            wide_string.c_str(),
                            static_cast<int>(wide_string.size()),
                            vec.data(), // pointer to a buffer that receives the converted string
                            static_cast<int>(vec.size()), // size of buffer that receives the converted string
                            nullptr, // default char
                            nullptr); // used default char
    if (0 == n)
    {
        return std::string{};
    }
    
    return std::string(vec.data(), vec.size());
}

std::wstring multi_byte_to_wide(const std::string &multi_byte_string)
{
    auto n = MultiByteToWideChar(CP_UTF8, // code page
                                 0, // flags - must be zero for code page CP_UTF8
                                 multi_byte_string.c_str(),
                                 static_cast<int>(multi_byte_string.size()),
                                 nullptr,
                                 0);
    if (0 == n)
    {
        return std::wstring{};
    }
    
    std::vector<wchar_t> vec(n);
    n = MultiByteToWideChar(CP_UTF8, // code page
                            0, // flags - must be zero for code page CP_UTF8
                            multi_byte_string.c_str(),
                            static_cast<int>(multi_byte_string.size()),
                            vec.data(),
                            static_cast<int>(vec.size()));
    if (0 == n)
    {
        return std::wstring{};
    }

    return std::wstring(vec.data(), vec.size());
}

}

}
