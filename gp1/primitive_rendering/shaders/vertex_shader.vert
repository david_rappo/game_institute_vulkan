#version 450
layout (location = 0) in vec4 in_position;
layout (location = 1) in vec4 in_color;

layout (location = 0) out vec4 vs_out_color;

layout (set = 0, binding = 0) uniform uniform_buffer_model_matrix
{

mat4 m_model_matrix;

};

layout (set = 0, binding = 1) uniform uniform_buffer_view_matrix
{

mat4 m_view_matrix;

};

layout (set = 0, binding = 2) uniform uniform_buffer_projection_matrix
{

mat4 m_projection_matrix;

};

layout (set = 0, binding = 3) uniform uniform_buffer_invert_y_matrix
{

mat4 m_invert_y;

};

out gl_PerVertex
{

vec4 gl_Position;

};

void main()
{
    gl_Position = m_invert_y * m_projection_matrix * m_view_matrix * m_model_matrix * in_position;
	vs_out_color = in_color;
}