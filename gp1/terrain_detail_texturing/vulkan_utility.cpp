// This is a personal academic project. Dear PVS-Studio, please check it.
// PVS-Studio Static Code Analyzer for C, C++ and C#: http://www.viva64.com
#include "vulkan_utility.h"

// C++ Standard Library
#include <bitset>
#include <cassert>
#include <cstddef>
#include <limits>

// GLI
#include <gli/image.hpp>

// Jade
#include "buffer.h"
#include "image.h"
#include "log_manager.h"

namespace Jade
{

namespace Vulkan_utility
{

bool create_buffer(VkDevice vulkan_device,
    const VkPhysicalDeviceMemoryProperties &physical_device_memory_properties,
    VkBufferUsageFlags buffer_usage_flags,
    VkMemoryPropertyFlags memory_property_flags,
    VkDeviceSize buffer_size,
    Jade::Buffer &out_buffer)
{
    VkBufferCreateInfo buffer_create_info = {};
    buffer_create_info.sType = VK_STRUCTURE_TYPE_BUFFER_CREATE_INFO;
    buffer_create_info.pNext = nullptr;
    buffer_create_info.flags = 0;
    buffer_create_info.size = buffer_size;
    out_buffer.m_size = buffer_size;
    buffer_create_info.usage = buffer_usage_flags;
    buffer_create_info.sharingMode = VK_SHARING_MODE_EXCLUSIVE;
    buffer_create_info.queueFamilyIndexCount = 0;
    buffer_create_info.pQueueFamilyIndices = nullptr;
    auto vk_result = vkCreateBuffer(vulkan_device, &buffer_create_info, nullptr, &out_buffer.m_buffer);
    if (VK_SUCCESS != vk_result)
    {
        return false;
    }

    vkGetBufferMemoryRequirements(vulkan_device, out_buffer.m_buffer, &out_buffer.m_memory_requirements);
    uint32_t memory_type_index{};
    if (!find_memory_type(physical_device_memory_properties,
        out_buffer.m_memory_requirements,
        memory_property_flags,
        memory_type_index))
    {
        return false;
    }

    VkMemoryAllocateInfo memory_allocate_info = {};
    memory_allocate_info.sType = VK_STRUCTURE_TYPE_MEMORY_ALLOCATE_INFO;
    memory_allocate_info.pNext = nullptr;
    memory_allocate_info.allocationSize = out_buffer.m_memory_requirements.size;
    memory_allocate_info.memoryTypeIndex = memory_type_index;
    vk_result = vkAllocateMemory(vulkan_device, &memory_allocate_info, nullptr, &out_buffer.m_device_memory);
    if (VK_SUCCESS != vk_result)
    {
        return false;
    }

    vk_result = vkBindBufferMemory(vulkan_device, out_buffer.m_buffer, out_buffer.m_device_memory, 0);
    if (VK_SUCCESS != vk_result)
    {
        return false;
    }

    return true;
}

// Find the first VkMemoryType required by memory_requirements that satisfies requirements e.g. must be host visible.
bool find_memory_type(const VkPhysicalDeviceMemoryProperties &physical_device_memory_properties,
    const VkMemoryRequirements &memory_requirements,
    VkMemoryPropertyFlags flags,
    uint32_t &memory_type_index)
{
    // 32 elements since memory_requirements.memoryTypeBits has 32 bits
    std::bitset<sizeof(std::uint32_t) * 8> b{static_cast<unsigned long long>(memory_requirements.memoryTypeBits)};
    for (uint32_t i = 0; i != physical_device_memory_properties.memoryTypeCount; ++i)
    {
        if (b[static_cast<std::size_t>(i)])
        {
            auto &memory_type = physical_device_memory_properties.memoryTypes[i];
            // Does memory_type.propertyFlags have the required bits set?
            auto result = (memory_type.propertyFlags & flags);
            if (result == flags)
            {
                memory_type_index = i;
                return true;
            }
        }
    }

    return false;
}

// Returns the number of bytes required to store buffer_size bytes in a buffer taking minimum_offset_alignment into
// consideration.
//
// minimum_offset_alignment: A minimum offset alignment such as
//                           VkPhysicalDeviceLimits::minUniformBufferOffsetAlignment.
VkDeviceSize calculate_buffer_size(VkDeviceSize minimum_offset_alignment, VkDeviceSize buffer_size)
{
    assert(0 != buffer_size);
    // Example. Let minimum_offset_alignment = 256.
    // If buffer_size = 4, i = 0, remainder = 4, add = 256, ret = 256
    // If buffer_size = 256, i = 1, remainder = 0, add = 0, ret = 256
    // If buffer_size = 300, i = 1, remainder = 44, add = 1, ret = 512
    auto i = buffer_size / minimum_offset_alignment;
    auto remainder = buffer_size % minimum_offset_alignment;
    auto add = (0 != remainder) ? minimum_offset_alignment : 0;
    return (i * minimum_offset_alignment) + add;
}

// Returns true if flags is supported by the tiling / format combination.
bool is_image_format_supported(VkPhysicalDevice physical_device,
    VkImageTiling tiling,
    VkFormat format,
    VkFormatFeatureFlags flags)
{
    VkFormatProperties format_properties = {};
    vkGetPhysicalDeviceFormatProperties(physical_device, format, &format_properties);
    VkFormatFeatureFlags result{};
    switch (tiling)
    {
    case VK_IMAGE_TILING_OPTIMAL:
        result = format_properties.optimalTilingFeatures & flags;
        break;
    case VK_IMAGE_TILING_LINEAR:
        result = format_properties.linearTilingFeatures & flags;
        break;
    default:
        assert(false);
    }
    
    return result == flags;
}

// Creates:
// - VkImage
// - VkImageView
// - VkSampler
// sampler_address_mode: Use for U, V, and W
bool create_image_2d(VkDevice vulkan_device,
    VkPhysicalDevice physical_device,
    const VkPhysicalDeviceMemoryProperties &physical_device_memory_properties,
    VkFormat image_format,
    const gli::texture2d &texture,
    VkSamplerAddressMode sampler_address_mode,
    Jade::Image &out_image)
{
    assert(gli::target::TARGET_2D == texture.target());
    VkImageCreateInfo image_create_info = {};
    image_create_info.sType = VK_STRUCTURE_TYPE_IMAGE_CREATE_INFO;
    image_create_info.pNext = nullptr;
    image_create_info.flags = 0;
    image_create_info.imageType = VK_IMAGE_TYPE_2D;
    image_create_info.format = VK_FORMAT_R8G8B8A8_UNORM;
    if (!Vulkan_utility::is_image_format_supported(physical_device,
        VK_IMAGE_TILING_OPTIMAL,
        image_create_info.format,
        VK_FORMAT_FEATURE_SAMPLED_IMAGE_BIT | VK_FORMAT_FEATURE_SAMPLED_IMAGE_FILTER_LINEAR_BIT | VK_FORMAT_FEATURE_TRANSFER_DST_BIT_KHR))
    {
        auto &os = Log_manager::get_instance()->get_stream(Log_manager::Log_type::default_log);
        os << jade_function_signature << " VkFormat " << image_create_info.format << " is unsupported." << std::endl;
        return false;
    }

    auto image_0 = texture[0];
    auto extent = image_0.extent();
    image_create_info.extent = VkExtent3D{static_cast<uint32_t>(extent.x), static_cast<uint32_t>(extent.y), 1};
    image_create_info.mipLevels = static_cast<uint32_t>(texture.levels());
    image_create_info.arrayLayers = 1;
    image_create_info.samples = VK_SAMPLE_COUNT_1_BIT;
    image_create_info.tiling = VK_IMAGE_TILING_OPTIMAL;
    image_create_info.usage = VK_IMAGE_USAGE_TRANSFER_DST_BIT | VK_IMAGE_USAGE_SAMPLED_BIT;
    image_create_info.sharingMode = VK_SHARING_MODE_EXCLUSIVE;
    image_create_info.queueFamilyIndexCount = 0;
    image_create_info.pQueueFamilyIndices = nullptr;
    image_create_info.initialLayout = VK_IMAGE_LAYOUT_UNDEFINED;
    auto vk_result = vkCreateImage(vulkan_device, &image_create_info, nullptr, &out_image.m_image);
    if (VK_SUCCESS != vk_result)
    {
        return false;
    }

    vkGetImageMemoryRequirements(vulkan_device, out_image.m_image, &out_image.m_memory_requirements);
    uint32_t memory_type_index{};
    if (!Vulkan_utility::find_memory_type(physical_device_memory_properties,
        out_image.m_memory_requirements,
        VK_MEMORY_PROPERTY_DEVICE_LOCAL_BIT,
        memory_type_index))
    {
        return false;
    }

    VkMemoryAllocateInfo memory_allocate_info = {};
    memory_allocate_info.sType = VK_STRUCTURE_TYPE_MEMORY_ALLOCATE_INFO;
    memory_allocate_info.pNext = nullptr;
    memory_allocate_info.allocationSize = out_image.m_memory_requirements.size;
    memory_allocate_info.memoryTypeIndex = memory_type_index;
    vk_result = vkAllocateMemory(vulkan_device, &memory_allocate_info, nullptr, &out_image.m_device_memory);
    if (VK_SUCCESS != vk_result)
    {
        return false;
    }

    vk_result = vkBindImageMemory(vulkan_device, out_image.m_image, out_image.m_device_memory, 0);
    if (VK_SUCCESS != vk_result)
    {
        return false;
    }

    VkImageViewCreateInfo image_view_create_info = {};
    image_view_create_info.sType = VK_STRUCTURE_TYPE_IMAGE_VIEW_CREATE_INFO;
    image_view_create_info.pNext = nullptr;
    image_view_create_info.flags = 0;
    image_view_create_info.image = out_image.m_image;
    image_view_create_info.viewType = VK_IMAGE_VIEW_TYPE_2D;
    image_view_create_info.format = image_create_info.format;
    image_view_create_info.components.r = VK_COMPONENT_SWIZZLE_IDENTITY;
    image_view_create_info.components.g = VK_COMPONENT_SWIZZLE_IDENTITY;
    image_view_create_info.components.b = VK_COMPONENT_SWIZZLE_IDENTITY;
    image_view_create_info.components.a = VK_COMPONENT_SWIZZLE_IDENTITY;
    image_view_create_info.subresourceRange.aspectMask = VK_IMAGE_ASPECT_COLOR_BIT;
    image_view_create_info.subresourceRange.baseMipLevel = 0;
    image_view_create_info.subresourceRange.levelCount = image_create_info.mipLevels;
    image_view_create_info.subresourceRange.baseArrayLayer = 0;
    image_view_create_info.subresourceRange.layerCount = 1;
    vk_result = vkCreateImageView(vulkan_device, &image_view_create_info, nullptr, &out_image.m_image_view);
    if (VK_SUCCESS != vk_result)
    {
        return false;
    }
    
    VkSamplerCreateInfo sampler_create_info = {};
    sampler_create_info.sType = VK_STRUCTURE_TYPE_SAMPLER_CREATE_INFO;
    sampler_create_info.pNext = nullptr;
    sampler_create_info.flags = 0;
    sampler_create_info.magFilter = VK_FILTER_LINEAR;
    sampler_create_info.minFilter = VK_FILTER_LINEAR;
    sampler_create_info.mipmapMode = VK_SAMPLER_MIPMAP_MODE_LINEAR;
    sampler_create_info.addressModeU = sampler_address_mode;
    sampler_create_info.addressModeV = sampler_address_mode;
    sampler_create_info.addressModeW = sampler_address_mode;
    sampler_create_info.mipLodBias = 0.0f;
    sampler_create_info.anisotropyEnable = VK_FALSE;
    sampler_create_info.maxAnisotropy = 1.0f;
    sampler_create_info.compareEnable = VK_FALSE;
    sampler_create_info.compareOp = VK_COMPARE_OP_NEVER;
    sampler_create_info.minLod = 0.0f;
    sampler_create_info.maxLod = static_cast<float>(image_create_info.mipLevels);
    sampler_create_info.borderColor = VK_BORDER_COLOR_FLOAT_TRANSPARENT_BLACK;
    sampler_create_info.unnormalizedCoordinates = VK_FALSE;
    vk_result = vkCreateSampler(vulkan_device, &sampler_create_info, nullptr, &out_image.m_sampler);
    if (VK_SUCCESS != vk_result)
    {
        return false;
    }

    return true;
}

// A cube map is an array image of type VK_IMAGE_TYPE_2D with six layers (-x, +x, -y, +y, -z, +z)
// 
// texture: Six textures are required to create a cube map. texture is one of the textures in the cube map to be
//          created. Each texture in the cube map should have the same dimensions and format.
bool create_cube_map(VkDevice vulkan_device,
    VkPhysicalDevice physical_device,
    const VkPhysicalDeviceMemoryProperties &physical_device_memory_properties,
    VkFormat image_format,
    const gli::texture2d &texture,
    Jade::Image &out_image)
{
    VkImageCreateInfo image_create_info = {};
    image_create_info.sType = VK_STRUCTURE_TYPE_IMAGE_CREATE_INFO;
    image_create_info.pNext = nullptr;
    image_create_info.flags = VK_IMAGE_CREATE_CUBE_COMPATIBLE_BIT;
    image_create_info.imageType = VK_IMAGE_TYPE_2D;
    image_create_info.format = VK_FORMAT_R8G8B8A8_UNORM;
    if (!Vulkan_utility::is_image_format_supported(physical_device,
        VK_IMAGE_TILING_OPTIMAL,
        image_create_info.format,
        VK_FORMAT_FEATURE_SAMPLED_IMAGE_BIT | VK_FORMAT_FEATURE_SAMPLED_IMAGE_FILTER_LINEAR_BIT | VK_FORMAT_FEATURE_TRANSFER_DST_BIT_KHR))
    {
        auto &os = Log_manager::get_instance()->get_stream(Log_manager::Log_type::default_log);
        os << jade_function_signature << " VkFormat " << image_create_info.format << " is unsupported." << std::endl;
        return false;
    }

    auto image_0 = texture[0];
    auto extent = image_0.extent();
    image_create_info.extent = VkExtent3D{static_cast<uint32_t>(extent.x), static_cast<uint32_t>(extent.y), 1};
    image_create_info.mipLevels = static_cast<uint32_t>(texture.levels());
    image_create_info.arrayLayers = 6;
    image_create_info.samples = VK_SAMPLE_COUNT_1_BIT;
    image_create_info.tiling = VK_IMAGE_TILING_OPTIMAL;
    image_create_info.usage = VK_IMAGE_USAGE_TRANSFER_DST_BIT | VK_IMAGE_USAGE_SAMPLED_BIT;
    image_create_info.sharingMode = VK_SHARING_MODE_EXCLUSIVE;
    image_create_info.queueFamilyIndexCount = 0;
    image_create_info.pQueueFamilyIndices = nullptr;
    image_create_info.initialLayout = VK_IMAGE_LAYOUT_UNDEFINED;
    auto vk_result = vkCreateImage(vulkan_device, &image_create_info, nullptr, &out_image.m_image);
    if (VK_SUCCESS != vk_result)
    {
        return false;
    }
    
    vkGetImageMemoryRequirements(vulkan_device, out_image.m_image, &out_image.m_memory_requirements);
    uint32_t memory_type_index{};
    if (!Vulkan_utility::find_memory_type(physical_device_memory_properties,
        out_image.m_memory_requirements,
        VK_MEMORY_PROPERTY_DEVICE_LOCAL_BIT,
        memory_type_index))
    {
        return false;
    }

    VkMemoryAllocateInfo memory_allocate_info = {};
    memory_allocate_info.sType = VK_STRUCTURE_TYPE_MEMORY_ALLOCATE_INFO;
    memory_allocate_info.pNext = nullptr;
    memory_allocate_info.allocationSize = out_image.m_memory_requirements.size;
    memory_allocate_info.memoryTypeIndex = memory_type_index;
    vk_result = vkAllocateMemory(vulkan_device, &memory_allocate_info, nullptr, &out_image.m_device_memory);
    if (VK_SUCCESS != vk_result)
    {
        return false;
    }

    vk_result = vkBindImageMemory(vulkan_device, out_image.m_image, out_image.m_device_memory, 0);
    if (VK_SUCCESS != vk_result)
    {
        return false;
    }
    
    VkImageViewCreateInfo image_view_create_info = {};
    image_view_create_info.sType = VK_STRUCTURE_TYPE_IMAGE_VIEW_CREATE_INFO;
    image_view_create_info.pNext = nullptr;
    image_view_create_info.flags = 0;
    image_view_create_info.image = out_image.m_image;
    image_view_create_info.viewType = VK_IMAGE_VIEW_TYPE_CUBE;
    image_view_create_info.format = image_create_info.format;
    image_view_create_info.components.r = VK_COMPONENT_SWIZZLE_IDENTITY;
    image_view_create_info.components.g = VK_COMPONENT_SWIZZLE_IDENTITY;
    image_view_create_info.components.b = VK_COMPONENT_SWIZZLE_IDENTITY;
    image_view_create_info.components.a = VK_COMPONENT_SWIZZLE_IDENTITY;
    image_view_create_info.subresourceRange.aspectMask = VK_IMAGE_ASPECT_COLOR_BIT;
    image_view_create_info.subresourceRange.baseMipLevel = 0;
    image_view_create_info.subresourceRange.levelCount = image_create_info.mipLevels;
    image_view_create_info.subresourceRange.baseArrayLayer = 0;
    image_view_create_info.subresourceRange.layerCount = image_create_info.arrayLayers;
    vk_result = vkCreateImageView(vulkan_device, &image_view_create_info, nullptr, &out_image.m_image_view);
    if (VK_SUCCESS != vk_result)
    {
        return false;
    }
    
    VkSamplerCreateInfo sampler_create_info = {};
    sampler_create_info.sType = VK_STRUCTURE_TYPE_SAMPLER_CREATE_INFO;
    sampler_create_info.pNext = nullptr;
    sampler_create_info.flags = 0;
    sampler_create_info.magFilter = VK_FILTER_LINEAR;
    sampler_create_info.minFilter = VK_FILTER_LINEAR;
    sampler_create_info.mipmapMode = VK_SAMPLER_MIPMAP_MODE_LINEAR;
    sampler_create_info.addressModeU = VK_SAMPLER_ADDRESS_MODE_CLAMP_TO_EDGE;
    sampler_create_info.addressModeV = VK_SAMPLER_ADDRESS_MODE_CLAMP_TO_EDGE;
    sampler_create_info.addressModeW = VK_SAMPLER_ADDRESS_MODE_CLAMP_TO_EDGE;
    sampler_create_info.mipLodBias = 0.0f;
    sampler_create_info.anisotropyEnable = VK_FALSE;
    sampler_create_info.maxAnisotropy = 1.0f;
    sampler_create_info.compareEnable = VK_FALSE;
    sampler_create_info.compareOp = VK_COMPARE_OP_NEVER;
    sampler_create_info.minLod = 0.0f;
    sampler_create_info.maxLod = static_cast<float>(image_create_info.mipLevels);
    sampler_create_info.borderColor = VK_BORDER_COLOR_FLOAT_TRANSPARENT_BLACK;
    sampler_create_info.unnormalizedCoordinates = VK_FALSE;
    vk_result = vkCreateSampler(vulkan_device, &sampler_create_info, nullptr, &out_image.m_sampler);
    if (VK_SUCCESS != vk_result)
    {
        return false;
    }
    
    return true;
}

}

}