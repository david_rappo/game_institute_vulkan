#version 450

layout (location = 0) in vec3 vs_out_texture_0;

layout (location = 0) out vec4 out_color;

layout (set = 0, binding = 0) uniform samplerCube sampler_cube;

void main()
{
	vec4 texel = texture(sampler_cube, vs_out_texture_0);
	out_color = texel; // RGBA
}