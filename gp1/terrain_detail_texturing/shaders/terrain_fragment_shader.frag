#version 450

layout (location = 0) in vec4 vs_out_color;
layout (location = 1) in vec2 vs_out_texture_0;
layout (location = 2) in vec2 vs_out_texture_1;

layout (location = 0) out vec4 out_color;

layout (set = 0, binding = 0) uniform sampler2D base_sampler;
layout (set = 0, binding = 1) uniform sampler2D detail_sampler;

void main()
{
	vec4 base_texel = texture(base_sampler, vs_out_texture_0);
	vec4 detail_texel = texture(detail_sampler, vs_out_texture_1);
	// D3DTOP_ADDSIGNED: Arg1 + Arg2 - 0.5
	// Arg1 = base_texel, Arg2 = detail_texel
	// Lab Project 6.2 (Terrain Detail Texturing) does not use lighting
	// out_color = base_texel + detail_texel - vec4(0.5f); // RGBA

	// D3DTOP_MODULATE, R = Arg1 x Arg2
	// D3DTOP_MODULATE2X, R = (Arg1 x Arg2) << 1
	// D3DTOP_MODULATE4X, R = (Arg1 x Arg2) << 2
	
	// vec4 arg1 = vs_out_color * base_texel; // D3DTOP_MODULATE - too dark
	vec4 arg1 = vs_out_color * base_texel * vec4(2.0f); // D3DTOP_MODULATE2X - about right
	// vec4 arg1 = vs_out_color * base_texel * vec4(4.0f); // D3DTOP_MODULATE4X - too bright
	out_color = arg1 + detail_texel - vec4(0.5f); // RGBA
}