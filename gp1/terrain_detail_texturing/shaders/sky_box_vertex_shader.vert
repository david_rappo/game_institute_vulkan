#version 450
layout (location = 0) in vec3 in_position;

layout (location = 0) out vec3 vs_out_texture_0;

// TODO: This is unused and should be deleted.
layout (set = 0, binding = 1) uniform Uniform_matrices
{

// matrix = view_matrix * model_matrix
mat4 m_model_view_matrix;

} uniform_matrices;

layout (push_constant) uniform Uniform_push_constant
{

// m_matrix = invert_y_matrix * projection_matrix * view_matrix * model_matrix
mat4 m_matrix;

} uniform_push_constant;

out gl_PerVertex
{

vec4 gl_Position;

};

void main()
{
    gl_Position = uniform_push_constant.m_matrix * vec4(in_position, 1.0f);
	vs_out_texture_0 = in_position;
}