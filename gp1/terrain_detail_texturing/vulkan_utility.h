#pragma

// GLI
#include <gli/texture2d.hpp>

// Vulkan
#include "vulkan/vulkan.h"

namespace Jade
{

class Buffer;
class Image;

}

namespace Jade
{

namespace Vulkan_utility
{

bool create_buffer(
    VkDevice vulkan_device,
    const VkPhysicalDeviceMemoryProperties &physical_device_memory_properties,
    VkBufferUsageFlags buffer_usage_flags,
    VkMemoryPropertyFlags memory_property_flags,
    VkDeviceSize buffer_size,
    Jade::Buffer &out_buffer);

bool find_memory_type(const VkPhysicalDeviceMemoryProperties &physical_device_memory_properties,
    const VkMemoryRequirements &memory_requirements,
    VkMemoryPropertyFlags flags,
    uint32_t &memory_type_index);

VkDeviceSize calculate_buffer_size(VkDeviceSize minimum_offset_alignment, VkDeviceSize buffer_size);

bool is_image_format_supported(VkPhysicalDevice physical_device,
    VkImageTiling tiling,
    VkFormat format,
    VkFormatFeatureFlags flags);

bool create_image_2d(VkDevice vulkan_device,
    VkPhysicalDevice physical_device,
    const VkPhysicalDeviceMemoryProperties &physical_device_memory_properties,
    VkFormat image_format,
    const gli::texture2d &texture,
    VkSamplerAddressMode sampler_address_mode,
    Jade::Image &out_image);

bool create_cube_map(VkDevice vulkan_device,
    VkPhysicalDevice physical_device,
    const VkPhysicalDeviceMemoryProperties &physical_device_memory_properties,
    VkFormat image_format,
    const gli::texture2d &texture,
    Jade::Image &out_image);

// Copy the contents of data into staging_buffer.
//
// staging_buffer: the memory for staging_buffer is assumed to have been created with the
// VK_MEMORY_PROPERTY_HOST_VISIBLE_BIT flag.
template<typename T>
bool fill_staging_buffer(VkDevice vulkan_device, const std::vector<T> &data, Jade::Buffer &staging_buffer)
{
    void *map_data{nullptr};
    auto vk_result = vkMapMemory(vulkan_device,
        staging_buffer.m_device_memory,
        0,
        VK_WHOLE_SIZE,
        0,
        &map_data);
    if (VK_SUCCESS != vk_result)
    {
        return false;
    }
    
    std::memcpy(map_data, data.data(), data.size() * sizeof(T));
    VkMappedMemoryRange mapped_memory_range = {};
    mapped_memory_range.sType = VK_STRUCTURE_TYPE_MAPPED_MEMORY_RANGE;
    mapped_memory_range.pNext = nullptr;
    mapped_memory_range.memory = staging_buffer.m_device_memory;
    mapped_memory_range.offset = 0;
    mapped_memory_range.size = VK_WHOLE_SIZE;
    vk_result = vkFlushMappedMemoryRanges(vulkan_device, 1, &mapped_memory_range);
    if (VK_SUCCESS != vk_result)
    {
        return false;
    }

    vkUnmapMemory(vulkan_device, staging_buffer.m_device_memory);
    return true;
}

}

}