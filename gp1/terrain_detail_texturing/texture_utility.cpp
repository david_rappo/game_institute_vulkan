// This is a personal academic project. Dear PVS-Studio, please check it.
// PVS-Studio Static Code Analyzer for C, C++ and C#: http://www.viva64.com
#include "texture_utility.h"

// C++ Standard Library
#include <cstddef>

namespace Jade
{

namespace Texture_utility
{

// Returns the number of bytes required to store all of the mip levels in texture_2d
VkDeviceSize calculate_size(const gli::texture2d &texture_2d)
{
    std::size_t size{};
    for (std::size_t i = 0; i != texture_2d.levels(); ++i)
    {
        auto image = texture_2d[i];
        size += image.size();
    }

    return static_cast<VkDeviceSize>(size);
}

}

}