#pragma once

// C++ Standard Library
#include <iosfwd>
#include <string>

// GLM
#include <glm/mat4x4.hpp>
#include <glm/vec3.hpp>
#include <glm/vec4.hpp>

namespace Jade
{

namespace Debug_utility
{

std::ostream &operator<<(std::ostream &out, const glm::mat4 &m);
std::ostream &operator<<(std::ostream &out, const glm::vec4 &v);

}

}