// This is a personal academic project. Dear PVS-Studio, please check it.
// PVS-Studio Static Code Analyzer for C, C++ and C#: http://www.viva64.com
#include "first_person_camera.h"

// GLM
#include <glm/glm.hpp>
#include <glm/gtc/matrix_transform.hpp>
#include <glm/mat4x4.hpp>

// Jade
#include "math_utility.h"
#include "player.h"

namespace Jade
{

First_person_camera::First_person_camera() = default;

First_person_camera::First_person_camera(const Camera *camera): Camera{camera}
{
    // If the previous camera was a space craft camera...
    if (Camera::Type::space_craft == camera->get_type())
    {
        // Set the up vector to <0, 1, 0> because the orientation of the space craft camera's up vector might be
        // unsuitable for a first person camera.
        set_up(glm::vec3{0.0f, 1.0f, 0.0f});
        // Flatten the right and look vectors
        auto right = get_right();
        right.y = 0.0f;
        right = glm::normalize(right);
        set_right(right);
        auto look = get_look();
        look.y = 0.0f;
        look = glm::normalize(look);
        set_look(look);
    }
}

Camera::Type First_person_camera::get_type() const
{
    return Camera::Type::first_person;
}

void First_person_camera::rotate(float x, float y, float z, const Player &player)
{
    if (x)
    {
        // Pitch around the camera's right vector
        auto m = glm::rotate(glm::mat4{}, x, get_right());
        auto right = Math_utility::transform_normal(m, get_right());
        set_right(right);
        auto up = Math_utility::transform_normal(m, get_up());
        set_up(up);
        auto look = Math_utility::transform_normal(m, get_look());
        set_look(look);
    }

    if (y)
    {
        // Yaw around the players up vector
        auto m = glm::rotate(glm::mat4{}, y, player.get_up());
        auto right = Math_utility::transform_normal(m, get_right());
        set_right(right);
        auto up = Math_utility::transform_normal(m, get_up());
        set_up(up);
        auto look = Math_utility::transform_normal(m, get_look());
        set_look(look);
    }

    if (z)
    {
        // Rotate the camera around the players look vector
        auto m = glm::rotate(glm::mat4{}, z, player.get_look());
        auto position = get_position();
        position -= player.get_position();
        position = Math_utility::transform_coordinate(m, position);
        position += player.get_position();
        set_position(position);
        // Roll around the player's look vector
        auto right = Math_utility::transform_normal(m, get_right());
        set_right(right);
        auto up = Math_utility::transform_normal(m, get_up());
        set_up(up);
        auto look = Math_utility::transform_normal(m, get_look());
        set_look(look);
    }

    set_view_matrix_dirty(true);
}

}