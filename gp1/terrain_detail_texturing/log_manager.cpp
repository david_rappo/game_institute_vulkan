// This is a personal academic project. Dear PVS-Studio, please check it.
// PVS-Studio Static Code Analyzer for C, C++ and C#: http://www.viva64.com
#include "log_manager.h"

// C++ Standard Library
#include <array>
#include <cassert>
#include <utility>

std::unique_ptr<Jade::Log_manager> Jade::Log_manager::s_instance{};

namespace Jade
{

Log_manager *Log_manager::get_instance()
{
    if (false == Log_manager::s_instance)
    {
        Log_manager::s_instance.reset(new Log_manager{});
    }

    return Log_manager::s_instance.get();
}

std::ostream &Log_manager::get_stream(Log_type log_type)
{
    return m_streams.at(static_cast<std::size_t>(log_type));
}

Log_manager::Log_manager(): m_streams(file_count)
{
    // The order of file names is important because Log_type will be used to
    // index into m_streams.
    std::array<const char*, file_count> file_names = { "default_log.txt",
        "glfw_error.txt",
        "vulkan_debug_report_all_log.txt" };
    for (std::size_t i = 0; i != file_names.size(); ++i)
    {
        auto &ofs = m_streams.at(i);
        auto &file_name = file_names.at(i);
        ofs.open(file_name);
        if (ofs.fail())
        {
            return;
        }
    }
}

}
