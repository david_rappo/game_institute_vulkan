// This is a personal academic project. Dear PVS-Studio, please check it.
// PVS-Studio Static Code Analyzer for C, C++ and C#: http://www.viva64.com
#include "mesh.h"

// C++ Standard Library
#include <cassert>

namespace Jade
{

Mesh::Mesh() = default;

void Mesh::destroy(VkDevice device)
{
    assert(nullptr != device);
    m_staging_index_buffer.destroy(device);
    m_index_buffer.destroy(device);
    m_staging_vertex_buffer.destroy(device);
    m_vertex_buffer.destroy(device);
}

}