// This is a personal academic project. Dear PVS-Studio, please check it.
// PVS-Studio Static Code Analyzer for C, C++ and C#: http://www.viva64.com
#include "debug_utility.h"

// C++ Standard Library
#include <iomanip>
#include <iostream>

namespace Jade
{

namespace Debug_utility
{

std::ostream &operator<<(std::ostream &out, const glm::mat4 &m)
{
    const int w{10};
    glm::mat4::length_type r{0};
    out << std::setw(w) << m[0][r] << std::setw(w) << m[1][r] << std::setw(w) << m[2][r] << std::setw(w) << m[3][r] << "\n";
    ++r;
    out << std::setw(w) << m[0][r] << std::setw(w) << m[1][r] << std::setw(w) << m[2][r] << std::setw(w) << m[3][r] << "\n";
    ++r;
    out << std::setw(w) << m[0][r] << std::setw(w) << m[1][r] << std::setw(w) << m[2][r] << std::setw(w) << m[3][r] << "\n";
    ++r;
    out << std::setw(w) << m[0][r] << std::setw(w) << m[1][r] << std::setw(w) << m[2][r] << std::setw(w) << m[3][r];
    return out;
}

std::ostream &operator<<(std::ostream &out, const glm::vec4 &v)
{
    out << "<" << v.x << ", " << v.y << ", " << v.z << ", " << v.w << ">";
    return out;
}

}

}