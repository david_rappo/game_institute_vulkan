#pragma once

// GLM
#include <glm/vec4.hpp>

namespace Jade
{

// Stores the same data as the D3DXPLANE structure
class Plane
{
public:
    Plane() = default;
    explicit Plane(const glm::vec4 &v): m_a{v.x}, m_b{v.y}, m_c{v.z}, m_d{v.w} {}
    Plane(float a, float b, float c, float d): m_a{a}, m_b{b}, m_c{c}, m_d{d} {}

    static Plane normalize(const Plane &plane);
    
    float m_a{};
    float m_b{};
    float m_c{};
    float m_d{};
};

}
