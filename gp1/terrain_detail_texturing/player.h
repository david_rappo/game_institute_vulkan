#pragma once

// C++ Standard Library
#include <functional>
#include <memory>
#include <vector>

// GLM
#include <glm/mat4x4.hpp>
#include <glm/vec3.hpp>

// Jade
#include "camera.h"
#include "volume.h"

namespace Jade
{

class Terrain;

}

namespace Jade
{

class Player
{
public:
    // All angles are measured in radians
    static const float minimum_pitch;
    static const float maximum_pitch;
    static const float minimum_yaw;
    static const float maximum_yaw;
    static const float minimum_roll;
    static const float maximum_roll;

    enum Direction : unsigned
    {
        forward = 1,
        back = 2,
        right = 4,
        left = 8,
        up = 16,
        down = 32
    };

    Player();
    ~Player();

    void set_camera_type(Camera::Type type);
    void set_friction(float friction)
    {
        m_friction = friction;
    }

    void set_gravity(const glm::vec3 &gravity)
    {
        m_gravity = gravity;
    }

    void set_maximum_velocity_xz(float maximum_velocity_xz)
    {
        m_maximum_velocity_xz = maximum_velocity_xz;
    }

    void set_maximum_velocity_y(float maximum_velocity_y)
    {
        m_maximum_velocity_y = maximum_velocity_y;
    }

    void set_camera_offset(const glm::vec3 &camera_offset);
    const glm::vec3 &get_camera_offset() const
    {
        return m_camera_offset;
    }

    void set_camera_lag(float camera_lag)
    {
        m_camera_lag = camera_lag;
    }

    const Volume &get_volume() const
    {
        return m_volume;
    }

    void set_volume(const Volume &volume)
    {
        m_volume = volume;
    }

    Camera *get_camera()
    {
        return m_camera.get();
    }
    
    const Camera *get_const_camera() const
    {
        return m_camera.get();
    }

    void add_update_player_callback(Terrain *terrain);
    void add_update_camera_callback(Terrain *terrain);
    const glm::vec3 &get_position() const
    {
        return m_position;
    }
    
    void set_position(const glm::vec3 &position);
    // x, y, and z are measured in radians
    void rotate(float x, float y, float z);

    const glm::vec3 &get_right() const
    {
        return m_right;
    }

    const glm::vec3 &get_up() const
    {
        return m_up;
    }

    const glm::vec3 &get_look() const
    {
        return m_look;
    }

    const glm::vec3 &get_velocity() const
    {
        return m_velocity;
    }

    void set_velocity(const glm::vec3 &velocity)
    {
        m_velocity = velocity;
    }

    void update(float time_elapsed);
    void move(Direction direction, float distance);
    void move_velocity(Direction direction, float distance);
    void set_first_person_defaults();
    void set_third_person_defaults();
    void set_space_craft_defaults();
    glm::mat4 get_world_matrix() const;

private:
    void move(const glm::vec3 &v);
    void move_velocity(const glm::vec3 &v);
    glm::vec3 create_shift_vector(Direction direction, float distance) const;

    // Friction per second
    float m_friction{};
    // Units per second
    float m_maximum_velocity_xz{};
    // Units per second
    float m_maximum_velocity_y{};
    // Measured in seconds
    float m_camera_lag{};
    // Angles are stored in Radians
    float m_pitch{};
    float m_yaw{};
    float m_roll{};
    std::unique_ptr<Camera> m_camera;
    glm::vec3 m_gravity;
    glm::vec3 m_camera_offset;
    glm::vec3 m_position;
    glm::vec3 m_right{1.0f, 0.0f, 0.0f};
    glm::vec3 m_up{0.0f, 1.0f, 0.0f};
    glm::vec3 m_look{0.0f, 0.0f, 1.0f};
    // Units per second
    glm::vec3 m_velocity;
    Volume m_volume;
    std::vector<std::function<void(float)>> m_update_player_callbacks;
    std::vector<std::function<void(Camera*, float)>> m_update_camera_callbacks;
};

}
