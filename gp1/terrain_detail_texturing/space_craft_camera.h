#pragma once

// Jade
#include "camera.h"

namespace Jade
{

class Player;

}

namespace Jade
{

class Space_craft_camera : public Camera
{
public:
    Space_craft_camera();
    explicit Space_craft_camera(const Camera *camera);
    Type get_type() const override;
    void rotate(float x, float y, float z, const Player &player) override;
};

}