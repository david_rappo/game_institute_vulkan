// This is a personal academic project. Dear PVS-Studio, please check it.
// PVS-Studio Static Code Analyzer for C, C++ and C#: http://www.viva64.com
#include "buffer.h"

// C++ Standard Library
#include <cassert>

namespace Jade
{

void Buffer::destroy(VkDevice device)
{
    assert(nullptr != device);
    if (nullptr != m_buffer)
    {
        vkDestroyBuffer(device, m_buffer, nullptr);
        m_buffer = nullptr;
    }

    if (nullptr != m_device_memory)
    {
        vkFreeMemory(device, m_device_memory, nullptr);
        m_device_memory = nullptr;
    }
}

}
