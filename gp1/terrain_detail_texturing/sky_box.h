#pragma once

// C++ Standard Library
#include <vector>

// Vulkan
#include "vulkan/vulkan.h"

// GLI
#include <gli/texture2d.hpp>

// GLM
#include <glm/mat4x4.hpp>

// Jade
#include "buffer.h"
#include "image.h"
#include "sky_box_vertex.h"

namespace Jade
{

class Sky_box
{
public:
    // The number of layers in the cube map image - one per face.
    static constexpr uint32_t layer_count{6};
    // The data type of the indices stored in the index buffer
    static constexpr VkIndexType index_type{VK_INDEX_TYPE_UINT16};

    static glm::mat4 create_world_matrix(const glm::vec3 &camera_position);

    static VkDeviceSize determine_uniform_buffer_offset(VkDeviceSize minimum_uniform_buffer_offset_alignment);

    Sky_box();

    Sky_box(const Sky_box&) = delete;
    Sky_box& operator=(const Sky_box&) = delete;

    void draw(VkCommandBuffer command_buffer) const;
    bool create_resources(VkDevice vulkan_device,
        VkPhysicalDevice physical_device,
        const VkPhysicalDeviceMemoryProperties &physical_device_memory_properties,
        VkDeviceSize minimum_uniform_buffer_offset_alignment);
    void copy_resources_to_device(VkDevice vulkan_device, VkQueue graphics_queue, VkCommandBuffer command_buffer) const;
    bool copy_model_view_matrix_to_device(VkDevice vulkan_device,
        VkQueue graphics_queue,
        const glm::mat4 &matrix,
        VkCommandBuffer command_buffer);
    void destroy(VkDevice device);

    const Jade::Image &get_image() const
    {
        return m_image;
    }

    const Jade::Buffer &get_uniform_buffer() const
    {
        return m_uniform_buffer;
    }

private:
    bool create_image(VkDevice vulkan_device,
        VkPhysicalDevice physical_device,
        const VkPhysicalDeviceMemoryProperties &physical_device_memory_properties);
    void copy_image_staging_buffer_to_device(VkDevice vulkan_device,
        VkQueue graphics_queue,
        VkCommandBuffer command_buffer) const;
    bool copy_image_to_staging_buffer(VkDevice vulkan_device,
        const std::vector<const gli::texture2d*> &textures,
        const Jade::Buffer &staging_buffer,
        uint32_t &out_mip_levels,
        std::vector<VkBufferImageCopy> &out_buffer_image_copies) const;

    std::vector<Sky_box_vertex> m_vertices;
    std::vector<uint16_t> m_indices;
    // Stores the number of mip levels in each image used by the sky box (each image should have the same number of mip
    // levels).
    uint32_t m_mip_levels{};
    Jade::Buffer m_vertex_buffer;
    Jade::Buffer m_staging_vertex_buffer;
    Jade::Buffer m_index_buffer;
    Jade::Buffer m_staging_index_buffer;
    Jade::Image m_image;
    Jade::Buffer m_image_staging_buffer;
    // offset: 0
    // matrix: model view matrix
    Jade::Buffer m_uniform_buffer;
    Jade::Buffer m_staging_uniform_buffer;
    std::vector<VkBufferImageCopy> m_buffer_image_copies;
};

}