#pragma once

// GLM
#include <glm/vec2.hpp>
#include <glm/vec3.hpp>

namespace Jade
{

class Terrain_vertex
{
public:
    Terrain_vertex() = default;
    Terrain_vertex(const glm::vec3 &position,
        const glm::vec3 &normal,
        const glm::vec2 &texture_0,
        const glm::vec2 &texture_1):
        m_position{position},
        m_normal{normal},
        m_texture_0{texture_0},
        m_texture_1{texture_1}
    {

    }

    glm::vec3 m_position;
    glm::vec3 m_normal;
    glm::vec2 m_texture_0;
    glm::vec2 m_texture_1;
};

}