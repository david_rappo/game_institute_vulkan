// This is a personal academic project. Dear PVS-Studio, please check it.
// PVS-Studio Static Code Analyzer for C, C++ and C#: http://www.viva64.com
#include "camera.h"

// C++ Standard Library
#include <vector>

// GLM
#include <glm/glm.hpp>
#include <glm/trigonometric.hpp>

// Jade
#include "math_utility.h"

namespace Jade
{

Camera::Camera() = default;

Camera::Camera(const Camera *camera):
    m_vertical_field_of_view{camera->m_vertical_field_of_view},
    m_near_view_plane{camera->m_near_view_plane},
    m_far_view_plane{camera->m_far_view_plane},
    m_aspect_ratio{camera->m_aspect_ratio},
    //m_view_matrix_dirty{camera->m_view_matrix_dirty}, -- initialised in the body of the constructor
    //m_projection_matrix_dirty{camera->m_projection_matrix_dirty}, -- initialised in the body of the constructor
    //m_frustum_dirty{camera->m_frustum_dirty}, -- initialised in the body of the constructor
    m_position{camera->m_position},
    m_right{camera->m_right},
    m_up{camera->m_up},
    m_look{camera->m_look},
    m_view_matrix{camera->m_view_matrix},
    m_projection_matrix{camera->m_projection_matrix},
    m_volume{camera->m_volume}
{
    set_view_matrix_dirty(true);
    set_projection_matrix_dirty(true);
}

Camera::~Camera() = default;

void Camera::set_vertical_field_of_view(float vertical_field_of_view)
{
    m_vertical_field_of_view = vertical_field_of_view;
    set_projection_matrix_dirty(true);
}

void Camera::set_aspect_ratio(float aspect_ratio)
{
    m_aspect_ratio = aspect_ratio;
    set_projection_matrix_dirty(true);
}

void Camera::set_near_view_plane(float near_view_plane)
{
    m_near_view_plane = near_view_plane;
    set_projection_matrix_dirty(true);
}

void Camera::set_far_view_plane(float far_view_plane)
{
    m_far_view_plane = far_view_plane;
    set_projection_matrix_dirty(true);
}

void Camera::set_position(const glm::vec3 &position)
{
    m_position = position;
    set_view_matrix_dirty(true);
}

const glm::mat4 &Camera::get_view_matrix()
{
    if (!m_view_matrix_dirty)
    {
        return m_view_matrix;
    }

    Math_utility::regenerate_orientation_vectors(m_right, m_up, m_look, m_right, m_up, m_look);
    // Direct3D view matrix
    // right.x               up.x               look.x               0
    // right.y               up.y               look.y               0
    // right.z               up.z               look.z               0
    // -(position dot right) -(position dot up) -(position dot look) 1
    //
    // Transpose the Direct3D view matrix because GLSL uses column vectors (not row vectors)
    // right.x right.y right.z -(position dot right)
    // up.x    up.y    up.z    -(position dot up)
    // look.x  look.y  look.z  -(position dot look)
    // 0       0       0       1
    auto &column_0 = m_view_matrix[0];
    column_0.x = m_right.x;
    column_0.y = m_up.x;
    column_0.z = m_look.x;
    column_0.w = 0.0f;
    auto &column_1 = m_view_matrix[1];
    column_1.x = m_right.y;
    column_1.y = m_up.y;
    column_1.z = m_look.y;
    column_1.w = 0.0f;
    auto &column_2 = m_view_matrix[2];
    column_2.x = m_right.z;
    column_2.y = m_up.z;
    column_2.z = m_look.z;
    column_2.w = 0.0f;
    auto &column_3 = m_view_matrix[3];
    column_3.x = -glm::dot(m_position, m_right);
    column_3.y = -glm::dot(m_position, m_up);
    column_3.z = -glm::dot(m_position, m_look);
    column_3.w = 1.0f;
    set_view_matrix_dirty(false);
    return m_view_matrix;
}

const glm::mat4 &Camera::get_projection_matrix()
{
    if (!m_projection_matrix_dirty)
    {
        return m_projection_matrix;
    }

    m_projection_matrix = Math_utility::perspective_projection_left_handed(m_vertical_field_of_view,
        m_aspect_ratio,
        m_near_view_plane,
        m_far_view_plane);
    set_projection_matrix_dirty(false);
    return m_projection_matrix;
}

// minimum: the minimum point on an axis aligned bounding box (AABB)
// maximum: the minimum point on an axis aligned bounding box
//
// Returns true if the AABB represented by minimum and maximum is fully or partially inside the frustum.
bool Camera::bounds_in_frustum(const glm::vec3 &minimum, const glm::vec3 &maximum)
{
    calculate_frustum_planes();
    std::vector<Jade::Plane> planes = {m_left_frustum_plane, m_right_frustum_plane, m_top_frustum_plane,
        m_bottom_frustum_plane, m_near_frustum_plane, m_far_frustum_plane};
    for (auto iter = planes.cbegin(); iter != planes.cend(); ++iter)
    {
        // plane_normal faces outwards
        auto plane_normal = glm::vec3{iter->m_a, iter->m_b, iter->m_c};
        glm::vec3 near_point{};
        near_point.x = (plane_normal.x < 0.0f) ? maximum.x : minimum.x;
        near_point.y = (plane_normal.y < 0.0f) ? maximum.y : minimum.y;
        near_point.z = (plane_normal.z < 0.0f) ? maximum.z : minimum.z;
        auto distance = glm::dot(plane_normal, near_point) + iter->m_d;
        if (distance > 0.0f)
        {
            // The entire AABB is in front of the plane
            return false;
        }
    }
    
    // The AABB is fully or partially inside the frustum
    return true;
}

void Camera::update(float time_elapsed, float camera_lag, const Player &player)
{
    // Empty
}

void Camera::move(const glm::vec3 &v)
{
    m_position += v;
    set_view_matrix_dirty(true);
}

void Camera::set_view_matrix_dirty(bool view_matrix_dirty)
{
    m_view_matrix_dirty = view_matrix_dirty;
    if (m_view_matrix_dirty)
    {
        m_frustum_dirty = true;
    }
}

void Camera::set_projection_matrix_dirty(bool projection_matrix_dirty)
{
    m_projection_matrix_dirty = projection_matrix_dirty;
    if (m_projection_matrix_dirty)
    {
        m_frustum_dirty = true;
    }
}

// Extract the frustum planes in world space.
void Camera::calculate_frustum_planes()
{
    // Direct3D projection matrix
    //
    // X       Y       Z                        W
    //
    // x_scale 0       0                        0
    // 0       y_scale 0                        0
    // 0       0       z_f / (z_f - z_n)        1
    // 0       0       -z_n * z_f / (z_f - z_n) 0
    //
    // y_scale = 1 / tan(vertical_field_of_view * 0.5)
    // x_scale = y_scale / aspect_ratio
    // z_n = far_view_plane
    // z_f = near_view_plane
    //
    // X (the first three rows of the first column) is aligned with the camera space X axis.
    // Y (the first three rows of the second column) is aligned with the camera space Y axis.
    // W (the first three rows of the fourth column) is aligned with the camera space Z axis.
    //
    // bottom_plane_normal = -(column_3 + column_1)
    //                     = -(W + Y)
    // The result is inverted so that the normal faces outward
    //
    // top_plane_normal = -(column_3 + (-column_1))
    //                  = -(W + (-Y))
    //                  = -(W - Y)
    //
    // left_plane_normal = -(column_3 + column_0)
    //                   = -(W + X)
    //
    // right_plane_normal = -(column_3 + (-column_0))
    //                    = -(W + (-X))
    //                    = -(W - X)
    //
    // far_plane_normal = -(column_3 + (-column_2))
    //                  = -(W + (-Z))
    //                  = -(W - Z)
    //
    // near_plane_normal = -column_2
    //                   = -Z
    //
    // The inverted Direct3D projection matrix is:
    //
    // X row: x_scale 0       0                 0
    // Y row: 0       y_scale 0                 0
    // Z row: 0       0       z_f / (z_f - z_n) -z_n * z_f / (z_f - z_n)
    // W row: 0       0       1                 0
    //
    // Using the inverted Direct3D projection matrix the logic to extract the frustum planes is the same as for the
    // regular Direct3D projection matrix except that the X, Y, Z, and W columns are rows in the inverted Direct3D
    // projection matrix.
    if (!m_frustum_dirty)
    {
        return;
    }

    auto matrix = get_projection_matrix() * get_view_matrix();
    glm::vec4 x_row{matrix[0][0], matrix[1][0], matrix[2][0], matrix[3][0]};
    glm::vec4 y_row{matrix[0][1], matrix[1][1], matrix[2][1], matrix[3][1]};
    glm::vec4 z_row{matrix[0][2], matrix[1][2], matrix[2][2], matrix[3][2]};
    glm::vec4 w_row{matrix[0][3], matrix[1][3], matrix[2][3], matrix[3][3]};
    m_left_frustum_plane = Jade::Plane{-(w_row + x_row)};
    m_right_frustum_plane = Jade::Plane{-(w_row - x_row)};
    m_top_frustum_plane = Jade::Plane{-(w_row - y_row)};
    m_bottom_frustum_plane = Jade::Plane{-(w_row + y_row)};
    m_near_frustum_plane = Jade::Plane{-z_row};
    m_far_frustum_plane = Jade::Plane{-(w_row - z_row)};
    m_left_frustum_plane = Jade::Plane::normalize(m_left_frustum_plane);
    m_right_frustum_plane = Jade::Plane::normalize(m_right_frustum_plane);
    m_top_frustum_plane = Jade::Plane::normalize(m_top_frustum_plane);
    m_bottom_frustum_plane = Jade::Plane::normalize(m_bottom_frustum_plane);
    m_near_frustum_plane = Jade::Plane::normalize(m_near_frustum_plane);
    m_far_frustum_plane = Jade::Plane::normalize(m_far_frustum_plane);
    m_frustum_dirty = false;
}

}