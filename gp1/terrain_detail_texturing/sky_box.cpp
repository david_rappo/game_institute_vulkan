// This is a personal academic project. Dear PVS-Studio, please check it.
// PVS-Studio Static Code Analyzer for C, C++ and C#: http://www.viva64.com
#include "sky_box.h"

// C++ Standard Library
#include <cassert>
#include <cstddef>

// GLI
#include <gli/image.hpp>
#include <gli/load_ktx.hpp>

// GLM
#include <glm/gtc/type_ptr.hpp>
#include <glm/vec2.hpp>
#include <glm/vec3.hpp>
#include <glm/gtc/matrix_transform.hpp>

// Jade
#include "application.h"
#include "texture_utility.h"
#include "vulkan_utility.h"

namespace Jade
{

glm::mat4 Sky_box::create_world_matrix(const glm::vec3 &camera_position)
{
    // 1 0 0 t_x
    // 0 1 0 t_y
    // 0 0 1 t_z
    // 0 0 0 1
    return glm::translate(glm::mat4{}, camera_position + glm::vec3{0.0f, 1.3f, 0.0f});
}

// Calculate the starting offset of each element in the buffer. For example, if minimum_uniform_buffer_offset_alignment
// is 256 bytes then:
// Element 0 starts at address 0
// Element 1 starts at address 256
// Element 2 starts at address 512
// ...
// So, there might be some wasted space between elements in the buffer unless the size of each element is equal to
// minimum_uniform_buffer_offset_alignment.
//
// Return value: This must be a multiple of minimum_uniform_buffer_offset_alignment.
VkDeviceSize Sky_box::determine_uniform_buffer_offset(VkDeviceSize minimum_uniform_buffer_offset_alignment)
{
    auto size_of_mat4 = static_cast<VkDeviceSize>(sizeof(glm::mat4));
    VkDeviceSize ret{};
    // For example, 256 > 64
    if (minimum_uniform_buffer_offset_alignment > size_of_mat4)
    {
        ret = minimum_uniform_buffer_offset_alignment;
    }
    else
    {
        // For example,
        // minimum_uniform_buffer_offset_alignment = 40, size_of_mat4 = 64, remainder = 24, x = 1, ret = 80
        auto remainder = size_of_mat4 % minimum_uniform_buffer_offset_alignment;
        // minimum_uniform_buffer_offset_alignment * x = size_of_mat4
        // x = size_of_mat4 / minimum_uniform_buffer_offset_alignment
        auto x = size_of_mat4 / minimum_uniform_buffer_offset_alignment;
        ret = x * minimum_uniform_buffer_offset_alignment;
        ret += ((0 != remainder) ? minimum_uniform_buffer_offset_alignment : 0);
        assert(0 == (ret % minimum_uniform_buffer_offset_alignment));
    }
    
    return ret;
}

Sky_box::Sky_box(): m_vertices{Sky_box_vertex{glm::vec3{-10.0f, 10.0f, 10.0f}}, // Front quad (all quads point inward)
    Sky_box_vertex{glm::vec3{10.0f}},
    Sky_box_vertex{glm::vec3{10.0f, -10.0f, 10.0f}},
    Sky_box_vertex{glm::vec3{-10.0f, -10.0f, 10.0f}},
    Sky_box_vertex{glm::vec3{10.0f, 10.0f, -10.0f}}, // Back quad
    Sky_box_vertex{glm::vec3{-10.0f, 10.0f, -10.0f}},
    Sky_box_vertex{glm::vec3{-10.0f}},
    Sky_box_vertex{glm::vec3{10.0f, -10.0f, -10.0f}},
    Sky_box_vertex{glm::vec3{-10.0f, 10.0f, -10.0f}}, // Left quad
    Sky_box_vertex{glm::vec3{-10.0f, 10.0f, 10.0f}},
    Sky_box_vertex{glm::vec3{-10.0f, -10.0f, 10.0f}},
    Sky_box_vertex{glm::vec3{-10.0f}},
    Sky_box_vertex{glm::vec3{10.0f}}, // Right quad
    Sky_box_vertex{glm::vec3{10.0f, 10.0f, -10.0f}},
    Sky_box_vertex{glm::vec3{10.0f, -10.0f, -10.0f}},
    Sky_box_vertex{glm::vec3{10.0f, -10.0f, 10.0f}},
    Sky_box_vertex{glm::vec3{-10.0f, 10.0f, -10.0f}}, // Top quad
    Sky_box_vertex{glm::vec3{10.0f, 10.0f, -10.0f}},
    Sky_box_vertex{glm::vec3{10.0f}},
    Sky_box_vertex{glm::vec3{-10.0f, 10.0f, 10.0f}},
    Sky_box_vertex{glm::vec3{-10.0f, -10.0f, 10.0f}}, // Bottom quad
    Sky_box_vertex{glm::vec3{10.0f, -10.0f, 10.0f}},
    Sky_box_vertex{glm::vec3{10.0f, -10.0f, -10.0f}},
    Sky_box_vertex{glm::vec3{-10.0f}}}
{
    // Front quad
    // (0) -10, 10  (1) 10, 10
    // (3) -10, -10 (2) 10, -10
    // In DirectX 9 the top left corner of a quad is (0, 0) and the bottom right corner of a quad is (1, 1). Vulkan is
    // the same.
    // (0) 0, 0 (1) 1, 0
    // (3) 0, 1 (2) 1, 1

    for (uint16_t face = 0; face != layer_count; ++face)
    {
        // Triangle #0: 0, 1, 3
        // 0 1
        // 3 X
        // Triangle #1: 1, 3, 2
        // X 1 
        // 3 2
        auto i0 = face * 4; //-V112
        auto i1 = (face * 4) + 1; //-V112
        auto i2 = (face * 4) + 3; //-V112
        auto i3 = (face * 4) + 2; //-V112
        m_indices.push_back(i0);
        m_indices.push_back(i1);
        m_indices.push_back(i2);
        m_indices.push_back(i3);
    }
}

void Sky_box::draw(VkCommandBuffer command_buffer) const
{
    const VkDeviceSize offset{0};
    vkCmdBindVertexBuffers(command_buffer, 0, 1, &m_vertex_buffer.m_buffer, &offset);
    vkCmdBindIndexBuffer(command_buffer, m_index_buffer.m_buffer, 0, Sky_box::index_type);
    vkCmdDrawIndexed(command_buffer, static_cast<uint32_t>(m_indices.size()), 1, 0, 0, 0);
}

bool Sky_box::create_resources(VkDevice vulkan_device,
    VkPhysicalDevice physical_device,
    const VkPhysicalDeviceMemoryProperties &physical_device_memory_properties,
    VkDeviceSize minimum_uniform_buffer_offset_alignment)
{
    if (!create_image(vulkan_device, physical_device, physical_device_memory_properties))
    {
        return false;
    }

    VkDeviceSize buffer_size = m_vertices.size() * sizeof(Sky_box_vertex);
    auto result = Jade::Vulkan_utility::create_buffer(vulkan_device,
        physical_device_memory_properties,
        VK_BUFFER_USAGE_VERTEX_BUFFER_BIT | VK_BUFFER_USAGE_TRANSFER_DST_BIT,
        VK_MEMORY_PROPERTY_DEVICE_LOCAL_BIT,
        buffer_size,
        m_vertex_buffer);
    if (!result)
    {
        return false;
    }
    
    result = Jade::Vulkan_utility::create_buffer(vulkan_device,
        physical_device_memory_properties,
        VK_BUFFER_USAGE_TRANSFER_SRC_BIT,
        VK_MEMORY_PROPERTY_HOST_VISIBLE_BIT,
        buffer_size,
        m_staging_vertex_buffer);
    if (!result)
    {
        return false;
    }

    if (!Vulkan_utility::fill_staging_buffer(vulkan_device, m_vertices, m_staging_vertex_buffer))
    {
        return false;
    }

    buffer_size = m_indices.size() * sizeof(uint16_t);
    result = Jade::Vulkan_utility::create_buffer(vulkan_device,
        physical_device_memory_properties,
        VK_BUFFER_USAGE_INDEX_BUFFER_BIT | VK_BUFFER_USAGE_TRANSFER_DST_BIT,
        VK_MEMORY_PROPERTY_DEVICE_LOCAL_BIT,
        buffer_size,
        m_index_buffer);
    if (!result)
    {
        return false;
    }
    
    result = Jade::Vulkan_utility::create_buffer(vulkan_device,
        physical_device_memory_properties,
        VK_BUFFER_USAGE_TRANSFER_SRC_BIT,
        VK_MEMORY_PROPERTY_HOST_VISIBLE_BIT,
        buffer_size,
        m_staging_index_buffer);
    if (!result)
    {
        return false;
    }

    if (!Vulkan_utility::fill_staging_buffer(vulkan_device, m_indices, m_staging_index_buffer))
    {
        return false;
    }

    auto uniform_buffer_offset = determine_uniform_buffer_offset(minimum_uniform_buffer_offset_alignment);
    buffer_size = uniform_buffer_offset;
    result = Jade::Vulkan_utility::create_buffer(vulkan_device,
        physical_device_memory_properties,
        VK_BUFFER_USAGE_UNIFORM_BUFFER_BIT | VK_BUFFER_USAGE_TRANSFER_DST_BIT,
        VK_MEMORY_PROPERTY_DEVICE_LOCAL_BIT,
        buffer_size,
        m_uniform_buffer);
    if (!result)
    {
        return false;
    }

    result = Jade::Vulkan_utility::create_buffer(vulkan_device,
        physical_device_memory_properties,
        VK_BUFFER_USAGE_TRANSFER_SRC_BIT,
        VK_MEMORY_PROPERTY_HOST_VISIBLE_BIT,
        buffer_size,
        m_staging_uniform_buffer);
    if (!result)
    {
        return false;
    }
    
    return true;
}

// command_buffer: Assumes that vkBeginCommandBuffer has already been called on command_buffer.
void Sky_box::copy_resources_to_device(VkDevice vulkan_device, VkQueue graphics_queue, VkCommandBuffer command_buffer) const
{
    copy_image_staging_buffer_to_device(vulkan_device, graphics_queue, command_buffer);
    VkBufferCopy buffer_copy = {};
    buffer_copy.srcOffset = 0;
    buffer_copy.dstOffset = 0;
    buffer_copy.size = m_staging_vertex_buffer.m_memory_requirements.size;
    vkCmdCopyBuffer(command_buffer, m_staging_vertex_buffer.m_buffer, m_vertex_buffer.m_buffer, 1, &buffer_copy);
    buffer_copy.size = m_staging_index_buffer.m_memory_requirements.size;
    vkCmdCopyBuffer(command_buffer, m_staging_index_buffer.m_buffer, m_index_buffer.m_buffer, 1, &buffer_copy);
    std::vector<VkBufferMemoryBarrier> buffer_memory_barriers(2);
    auto &first_barrier = buffer_memory_barriers.front();
    first_barrier.sType = VK_STRUCTURE_TYPE_BUFFER_MEMORY_BARRIER;
    first_barrier.pNext = nullptr;
    first_barrier.srcAccessMask = VK_ACCESS_TRANSFER_WRITE_BIT;
    first_barrier.dstAccessMask = VK_ACCESS_VERTEX_ATTRIBUTE_READ_BIT;
    first_barrier.srcQueueFamilyIndex = VK_QUEUE_FAMILY_IGNORED;
    first_barrier.dstQueueFamilyIndex = VK_QUEUE_FAMILY_IGNORED;
    first_barrier.buffer = m_vertex_buffer.m_buffer;
    first_barrier.offset = 0;
    first_barrier.size = m_staging_vertex_buffer.m_memory_requirements.size;
    auto &second_barrier = buffer_memory_barriers.back();
    second_barrier.sType = VK_STRUCTURE_TYPE_BUFFER_MEMORY_BARRIER;
    second_barrier.pNext = nullptr;
    second_barrier.srcAccessMask = VK_ACCESS_TRANSFER_WRITE_BIT;
    second_barrier.dstAccessMask = VK_ACCESS_VERTEX_ATTRIBUTE_READ_BIT;
    second_barrier.srcQueueFamilyIndex = VK_QUEUE_FAMILY_IGNORED;
    second_barrier.dstQueueFamilyIndex = VK_QUEUE_FAMILY_IGNORED;
    second_barrier.buffer = m_index_buffer.m_buffer;
    second_barrier.offset = 0;
    second_barrier.size = m_staging_index_buffer.m_memory_requirements.size;
    vkCmdPipelineBarrier(command_buffer,
        VK_PIPELINE_STAGE_TRANSFER_BIT,
        VK_PIPELINE_STAGE_VERTEX_INPUT_BIT,
        0,
        0,
        nullptr,
        static_cast<uint32_t>(buffer_memory_barriers.size()),
        buffer_memory_barriers.data(),
        0,
        nullptr);
}

// copy_model_view_matrix_to_device is a non const member function because Vulkan_utility::fill_staging_buffer modifies
// m_staging_uniform_buffer.
//
// command_buffer: Assumes that vkBeginCommandBuffer has already been called on command_buffer.
bool Sky_box::copy_model_view_matrix_to_device(VkDevice vulkan_device,
    VkQueue graphics_queue,
    const glm::mat4 &matrix,
    VkCommandBuffer command_buffer)
{
    auto m = glm::value_ptr(matrix);
    std::vector<float> data(m, m + (sizeof(glm::mat4) / sizeof(float)));
    if (!Vulkan_utility::fill_staging_buffer(vulkan_device, data, m_staging_uniform_buffer))
    {
        return false;
    }
    
    VkBufferCopy buffer_copy = {};
    buffer_copy.srcOffset = 0;
    buffer_copy.dstOffset = 0;
    buffer_copy.size = m_staging_uniform_buffer.m_memory_requirements.size;
    vkCmdCopyBuffer(command_buffer, m_staging_uniform_buffer.m_buffer, m_uniform_buffer.m_buffer, 1, &buffer_copy);
    VkBufferMemoryBarrier buffer_memory_barrier = {};
    buffer_memory_barrier.sType = VK_STRUCTURE_TYPE_BUFFER_MEMORY_BARRIER;
    buffer_memory_barrier.pNext = nullptr;
    buffer_memory_barrier.srcAccessMask = VK_ACCESS_TRANSFER_WRITE_BIT;
    buffer_memory_barrier.dstAccessMask = VK_ACCESS_UNIFORM_READ_BIT;
    buffer_memory_barrier.srcQueueFamilyIndex = VK_QUEUE_FAMILY_IGNORED;
    buffer_memory_barrier.dstQueueFamilyIndex = VK_QUEUE_FAMILY_IGNORED;
    buffer_memory_barrier.buffer = m_uniform_buffer.m_buffer;
    buffer_memory_barrier.offset = 0;
    buffer_memory_barrier.size = m_staging_uniform_buffer.m_memory_requirements.size;
    vkCmdPipelineBarrier(command_buffer,
        VK_PIPELINE_STAGE_TRANSFER_BIT,
        VK_PIPELINE_STAGE_VERTEX_INPUT_BIT,
        0,
        0,
        nullptr,
        1,
        &buffer_memory_barrier,
        0,
        nullptr);
    return true;
}

// Order of creation
// (1) m_vertex_buffer
// (2) m_staging_vertex_buffer
// (3) m_index_buffer
// (4) m_staging_index_buffer
// (5) m_image
// (6) m_image_staging_buffer
// (7) m_uniform_buffer
// (8) m_staging_uniform_buffer
void Sky_box::destroy(VkDevice device)
{
    m_staging_uniform_buffer.destroy(device);
    m_uniform_buffer.destroy(device);
    m_image_staging_buffer.destroy(device);
    m_image.destroy(device);
    m_staging_index_buffer.destroy(device);
    m_index_buffer.destroy(device);
    m_staging_vertex_buffer.destroy(device);
    m_vertex_buffer.destroy(device);
}

// Creates:
// - m_image
// - m_staging_buffer
//
// In Lab Project 6.2 - Terrain Detail Texturing the sky box is rendered using the following sampler states:
// m_pD3DDevice->SetSamplerState(0, D3DSAMP_ADDRESSU, D3DTADDRESS_CLAMP);
// m_pD3DDevice->SetSamplerState(0, D3DSAMP_ADDRESSV, D3DTADDRESS_CLAMP);
bool Sky_box::create_image(VkDevice vulkan_device,
    VkPhysicalDevice physical_device,
    const VkPhysicalDeviceMemoryProperties &physical_device_memory_properties)
{
    // All sky box images have dimensions: 1024 by 1024
    // All sky box images have format: RGBA (8, 8, 8, 8)
    // All sky box images are fully opaque (alpha = 255)
    auto left_texture = gli::load_ktx(Application::texture_path + "/SkyBox_Left.ktx");
    if (left_texture.empty())
    {
        return false;
    }

    auto right_texture = gli::load_ktx(Application::texture_path + "/SkyBox_Right.ktx");
    if (right_texture.empty())
    {
        return false;
    }

    auto bottom_texture = gli::load_ktx(Application::texture_path + "/SkyBox_Bottom.ktx");
    if (bottom_texture.empty())
    {
        return false;
    }

    auto top_texture = gli::load_ktx(Application::texture_path + "/SkyBox_Top.ktx");
    if (top_texture.empty())
    {
        return false;
    }

    auto back_texture = gli::load_ktx(Application::texture_path + "/SkyBox_Back.ktx");
    if (back_texture.empty())
    {
        return false;
    }

    auto front_texture = gli::load_ktx(Application::texture_path + "/SkyBox_Front.ktx");
    if (front_texture.empty())
    {
        return false;
    }

    gli::texture2d left_texture_2d{left_texture};
    gli::texture2d right_texture_2d{right_texture};
    gli::texture2d bottom_texture_2d{bottom_texture};
    gli::texture2d top_texture_2d{top_texture};
    gli::texture2d back_texture_2d{back_texture};
    gli::texture2d front_texture_2d{front_texture};
    if (!Vulkan_utility::create_cube_map(vulkan_device,
        physical_device,
        physical_device_memory_properties,
        VK_FORMAT_R8G8B8A8_UNORM,
        left_texture_2d,
        m_image))
    {
        return false;
    }

    auto size_of_one_image = Texture_utility::calculate_size(left_texture_2d);
    auto image_size = size_of_one_image * static_cast<VkDeviceSize>(layer_count);
    if (!Vulkan_utility::create_buffer(vulkan_device,
        physical_device_memory_properties,
        VK_BUFFER_USAGE_TRANSFER_SRC_BIT,
        VK_MEMORY_PROPERTY_HOST_VISIBLE_BIT,
        image_size,
        m_image_staging_buffer))
    {
        return false;
    }

    // The sky box textures are from Game Institute Lab Project 6.2 - Terrain Detail Texturing, which uses DirectX 9.
    // To get the same results as Lab Project 6.2 I needed to flip the textures.
    std::vector<const gli::texture2d*> textures = {&right_texture_2d, // Use right instead of left
        &left_texture_2d, // Use left instead of right
        &top_texture_2d, // Use top instead of bottom
        &bottom_texture_2d, // Use bottom instead of top
        &front_texture_2d, // Use front instead of back
        &back_texture_2d}; // Use back instead of front
    if (!copy_image_to_staging_buffer(vulkan_device,
        textures,
        m_image_staging_buffer,
        m_mip_levels,
        m_buffer_image_copies))
    {
        return false;
    }

    return true;
}

void Sky_box::copy_image_staging_buffer_to_device(VkDevice vulkan_device,
    VkQueue graphics_queue,
    VkCommandBuffer command_buffer) const
{
    VkImageMemoryBarrier before_image_memory_barrier;
    VkImageMemoryBarrier after_image_memory_barrier;
    before_image_memory_barrier = {};
    before_image_memory_barrier.sType = VK_STRUCTURE_TYPE_IMAGE_MEMORY_BARRIER;
    before_image_memory_barrier.pNext = nullptr;
    before_image_memory_barrier.srcAccessMask = 0;
    before_image_memory_barrier.dstAccessMask = VK_ACCESS_TRANSFER_WRITE_BIT;
    before_image_memory_barrier.oldLayout = VK_IMAGE_LAYOUT_UNDEFINED;
    before_image_memory_barrier.newLayout = VK_IMAGE_LAYOUT_TRANSFER_DST_OPTIMAL;
    before_image_memory_barrier.srcQueueFamilyIndex = VK_QUEUE_FAMILY_IGNORED;
    before_image_memory_barrier.dstQueueFamilyIndex = VK_QUEUE_FAMILY_IGNORED;
    before_image_memory_barrier.image = m_image.m_image;
    VkImageSubresourceRange image_subresource_range = {};
    image_subresource_range.aspectMask = VK_IMAGE_ASPECT_COLOR_BIT;
    image_subresource_range.baseMipLevel = 0;
    image_subresource_range.levelCount = m_mip_levels;
    image_subresource_range.baseArrayLayer = 0;
    image_subresource_range.layerCount = layer_count;
    before_image_memory_barrier.subresourceRange = image_subresource_range;
    after_image_memory_barrier = before_image_memory_barrier;
    // Transition the image from VK_IMAGE_LAYOUT_TRANSFER_DST_OPTIMAL to VK_IMAGE_LAYOUT_SHADER_READ_ONLY_OPTIMAL
    after_image_memory_barrier.srcAccessMask = VK_ACCESS_TRANSFER_WRITE_BIT;
    after_image_memory_barrier.dstAccessMask = VK_ACCESS_SHADER_READ_BIT;
    after_image_memory_barrier.oldLayout = VK_IMAGE_LAYOUT_TRANSFER_DST_OPTIMAL;
    after_image_memory_barrier.newLayout = VK_IMAGE_LAYOUT_SHADER_READ_ONLY_OPTIMAL;
    // Transition the image from the layout in which it was created (VK_IMAGE_LAYOUT_UNDEFINED) to
    // VK_IMAGE_LAYOUT_TRANSFER_DST_OPTIMAL.
    vkCmdPipelineBarrier(command_buffer,
        VK_PIPELINE_STAGE_TOP_OF_PIPE_BIT, // source stage mask
        VK_PIPELINE_STAGE_TRANSFER_BIT, // destination stage mask
        0,
        0,
        nullptr,
        0,
        nullptr,
        1,
        &before_image_memory_barrier);
    vkCmdCopyBufferToImage(command_buffer,
        m_image_staging_buffer.m_buffer,
        m_image.m_image,
        VK_IMAGE_LAYOUT_TRANSFER_DST_OPTIMAL,
        static_cast<uint32_t>(m_buffer_image_copies.size()),
        m_buffer_image_copies.data());
    vkCmdPipelineBarrier(command_buffer,
        VK_PIPELINE_STAGE_TRANSFER_BIT, // source stage mask
        VK_PIPELINE_STAGE_FRAGMENT_SHADER_BIT, // destination stage mask
        0,
        0,
        nullptr,
        0,
        nullptr,
        1,
        &after_image_memory_barrier);
}

// textures: Contains six textures (-x, +x, -y, +y, -z, +z). Each texture corresponds to one of the array images in the
//           cube map image.
// out_mip_levels: See m_mip_levels
bool Sky_box::copy_image_to_staging_buffer(VkDevice vulkan_device,
    const std::vector<const gli::texture2d*> &textures,
    const Jade::Buffer &staging_buffer,
    uint32_t &out_mip_levels,
    std::vector<VkBufferImageCopy> &out_buffer_image_copies) const
{
    // Array layers: -x, +x, -y, +y, -z, +z
    // mip level 0: -x mip level 0, +x mip level 0, -y mip level 0, ...
    // mip level 1: -x mip level 1, +x mip level 1, -y mip level 1, ...
    // ...
    // [mip level 0 data for all array layers][mip level 1 data for all array layers] ...

    out_buffer_image_copies.clear();
    VkDeviceSize buffer_offset{};
    assert(!textures.empty());
    const gli::texture2d *first_texture = textures.front();
    for (std::size_t i = 0; i != first_texture->levels(); ++i)
    {
        auto image = (*first_texture)[i];
        VkBufferImageCopy buffer_image_copy = {};
        buffer_image_copy.bufferOffset = buffer_offset;
        buffer_offset += (static_cast<VkDeviceSize>(image.size()) * static_cast<VkDeviceSize>(layer_count));
        buffer_image_copy.bufferRowLength = 0; // tightly packed
        buffer_image_copy.bufferImageHeight = 0; // tightly packed
        VkImageSubresourceLayers image_subresource_layers = {};
        image_subresource_layers.aspectMask = VK_IMAGE_ASPECT_COLOR_BIT;
        image_subresource_layers.mipLevel = static_cast<uint32_t>(i);
        image_subresource_layers.baseArrayLayer = 0;
        image_subresource_layers.layerCount = layer_count;
        buffer_image_copy.imageSubresource = image_subresource_layers;
        buffer_image_copy.imageOffset = VkOffset3D{0, 0, 0};
        auto extent = image.extent();
        buffer_image_copy.imageExtent = VkExtent3D{static_cast<uint32_t>(extent.x), static_cast<uint32_t>(extent.y), 1};
        out_buffer_image_copies.push_back(buffer_image_copy);
    }

    uint32_t *data{nullptr};
    auto vk_result = vkMapMemory(vulkan_device,
        staging_buffer.m_device_memory,
        0,
        staging_buffer.m_memory_requirements.size,
        0,
        reinterpret_cast<void**>(&data));
    if (VK_SUCCESS != vk_result)
    {
        return false;
    }

    out_mip_levels = static_cast<uint32_t>(first_texture->levels());
    // Copy the mip level 0 data for all array layers into the staging buffer, then the mip level 1 data for all array
    // layers, and so on.
    for (std::size_t i = 0; i != static_cast<std::size_t>(out_mip_levels); ++i)
    {
        for (auto iter = textures.cbegin(); iter != textures.cend(); ++iter)
        {
            assert((*iter)->levels() == out_mip_levels);
            auto image = (**iter)[i];
            std::memcpy(data, image.data(), image.size());
            auto increment = (image.size() / sizeof(uint32_t));
            data += increment;
        }
    }

    VkMappedMemoryRange mapped_memory_range = {};
    mapped_memory_range.sType = VK_STRUCTURE_TYPE_MAPPED_MEMORY_RANGE;
    mapped_memory_range.pNext = nullptr;
    mapped_memory_range.memory = staging_buffer.m_device_memory;
    mapped_memory_range.offset = 0;
    mapped_memory_range.size = staging_buffer.m_memory_requirements.size;
    vk_result = vkFlushMappedMemoryRanges(vulkan_device, 1, &mapped_memory_range);
    if (VK_SUCCESS != vk_result)
    {
        return false;
    }

    vkUnmapMemory(vulkan_device, staging_buffer.m_device_memory);
    return true;
}

}