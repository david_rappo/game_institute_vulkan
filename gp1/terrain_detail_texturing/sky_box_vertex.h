#pragma once

// GLM
#include <glm/vec2.hpp>
#include <glm/vec3.hpp>

namespace Jade
{

class Sky_box_vertex
{
public:
    Sky_box_vertex() = default;
    explicit Sky_box_vertex(const glm::vec3 &position): m_position{position}
    {

    }

    glm::vec3 m_position;
};

}