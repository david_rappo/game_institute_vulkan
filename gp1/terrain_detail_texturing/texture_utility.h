#pragma once

// Vulkan
#include "vulkan/vulkan.h"

// GLI
#include <gli/texture2d.hpp>

namespace Jade
{

namespace Texture_utility
{

VkDeviceSize calculate_size(const gli::texture2d &texture_2d);

}

}