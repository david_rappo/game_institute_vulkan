// This is a personal academic project. Dear PVS-Studio, please check it.
// PVS-Studio Static Code Analyzer for C, C++ and C#: http://www.viva64.com
#include "math_utility.h"

// C++ Standard Library
#include <cassert>
#include <cmath>

// GLM
#include <glm/geometric.hpp>

namespace Jade
{

namespace Math_utility
{

// Direct3D projection matrix
//
// x_scale 0       0                        0
// 0       y_scale 0                        0
// 0       0       z_f / (z_f - z_n)        1
// 0       0       -z_n * z_f / (z_f - z_n) 0
//
// y_scale = 1 / tan(vertical_field_of_view * 0.5)
// x_scale = y_scale / aspect_ratio
// z_n = far_view_plane
// z_f = near_view_plane
//
// perspective_projection_left_handed returns the transpose of what D3DXMatrixPerspectiveFovLH would return because
// GLSL uses column vectors (not row vectors like Direct3D).
//
// vertical_field_of_view: Measured in Radians
glm::mat4 perspective_projection_left_handed(float vertical_field_of_view,
    float aspect_ratio,
    float near_view_plane,
    float far_view_plane)
{
    glm::mat4 m;
    auto y_scale = 1.0f / std::tan(vertical_field_of_view * 0.5f);
    auto x_scale = y_scale / aspect_ratio;
    // First column
    m[0][0] = x_scale;
    m[0][1] = 0.0f;
    m[0][2] = 0.0f;
    m[0][3] = 0.0f;
    // Second column
    m[1][0] = 0.0f;
    m[1][1] = y_scale;
    m[1][2] = 0.0f;
    m[1][3] = 0.0f;
    // Third column
    m[2][0] = 0.0f;
    m[2][1] = 0.0f;
    m[2][2] = far_view_plane / (far_view_plane - near_view_plane);
    m[2][3] = 1.0f;
    // Fourth column
    m[3][0] = 0.0f;
    m[3][1] = 0.0f;
    m[3][2] = -near_view_plane * far_view_plane / (far_view_plane - near_view_plane);
    m[3][3] = 0.0f;
    return m;
}

// return: Returns a matrix that inverts the y component of the vector that is multiplied by it. The returned matrix is
// intended to be used with column vectors.
glm::mat4 invert_y_matrix()
{
    // 4x4 by 4x1 = 4x1
    // m by v = V
    // V.x = First row in m dot first column in v
    // V.y = Second row in m dot first column in v
    // V.z = Third row in m dot first column in v
    // V.w = Fourth row in m dot first column in v
    //
    // 1 0  0 0
    // 0 -1 0 0
    // 0 0  1 0
    // 0 0  0 1
    glm::mat4 m{};
    m[1][1] = -1.0f;
    return m;
}

glm::vec3 transform_coordinate(const glm::mat4 &m, const glm::vec3 &v)
{
    auto result = m * glm::vec4{v, 1.0f};
    result /= result.w;
    return result;
}

glm::vec3 transform_normal(const glm::mat4 &m, const glm::vec3 &v)
{
    return m * glm::vec4{v, 0.0f};
}

// Ensure that the orientation vectors are orthogonal to one another and unit length.
void regenerate_orientation_vectors(const glm::vec3 &right,
    const glm::vec3 &up,
    const glm::vec3 &look,
    glm::vec3 &out_right,
    glm::vec3 &out_up,
    glm::vec3 &out_look)
{
    out_look = glm::normalize(look);
    out_right = glm::cross(up, out_look);
    out_right = glm::normalize(out_right);
    out_up = glm::cross(out_look, out_right);
    out_up = glm::normalize(out_up);
}

// look_at_left_handed does the same as D3DXMatrixLookAtLH except that the resulting matrix is transposed.
glm::mat4 look_at_left_handed(const glm::vec3 &eye, const glm::vec3 &at, const glm::vec3 &up)
{
    auto z_axis = at - eye;
    z_axis = glm::normalize(z_axis);
    auto x_axis = glm::cross(up, z_axis);
    x_axis = glm::normalize(x_axis);
    auto y_axis = glm::cross(z_axis, x_axis);
    // Direct3D view matrix.
    // x_axis.x y_axis.x z_axis.x, 0
    // x_axis.y y_axis.y z_axis.y, 0
    // x_axis.z y_axis.z z_axis.z, 0
    // -dot(x_axis, eye), -dot(y_axis, eye), -dot(z_axis, eye), 1
    glm::mat4 m;
    // Create the transpose of the Direct3D view matrix.
    // First column
    m[0][0] = x_axis.x;
    m[0][1] = y_axis.x;
    m[0][2] = z_axis.x;
    m[0][3] = 0.0f;
    // Second column
    m[1][0] = x_axis.y;
    m[1][1] = y_axis.y;
    m[1][2] = z_axis.y;
    m[1][3] = 0.0f;
    // Third column
    m[2][0] = x_axis.z;
    m[2][1] = y_axis.z;
    m[2][2] = z_axis.z;
    m[2][3] = 0.0f;
    // Fourth column
    m[3][0] = -glm::dot(eye, x_axis);
    m[3][1] = -glm::dot(eye, y_axis);
    m[3][2] = -glm::dot(eye, z_axis);
    m[3][3] = 1.0f;
    return m;
}

// Copy source into out_data.
void copy(const glm::mat4 &source, float *out_data)
{
    for (glm::mat4::length_type column_index = 0; column_index != source.length(); ++column_index)
    {
        const auto &column = source[column_index];
        for (glm::vec4::length_type row_index = 0; row_index != column.length(); ++row_index)
        {
            (*out_data) = column[static_cast<std::size_t>(row_index)];
            ++out_data;
        }
    }
}

}

}