// This is a personal academic project. Dear PVS-Studio, please check it.
// PVS-Studio Static Code Analyzer for C, C++ and C#: http://www.viva64.com
#include "third_person_camera.h"

// C++ Standard Library
#include <cassert>

// GLM
#include <glm/glm.hpp>
#include <glm/gtc/matrix_transform.hpp>
#include <glm/mat4x4.hpp>

// Jade
#include "math_utility.h"
#include "player.h"

namespace Jade
{

Third_person_camera::Third_person_camera() = default;

Third_person_camera::Third_person_camera(const Camera *camera): Camera{camera}
{
    // If the previous camera was a space craft camera...
    if (Camera::Type::space_craft == camera->get_type())
    {
        // Set the up vector to <0, 1, 0> because the orientation of the space craft camera's up vector might be
        // unsuitable for a third person camera.
        set_up(glm::vec3{0.0f, 1.0f, 0.0f});
        // Flatten the right and look vectors
        auto right = get_right();
        right.y = 0.0f;
        right = glm::normalize(right);
        set_right(right);
        auto look = get_look();
        look.y = 0.0f;
        look = glm::normalize(look);
        set_look(look);
    }
}

Camera::Type Third_person_camera::get_type() const
{
    return Camera::Type::third_person;
}

void Third_person_camera::rotate(float x, float y, float z, const Player &player)
{
    // Do nothing.
}

void Third_person_camera::update(float time_elapsed, float camera_lag, const Player &player)
{
    assert(camera_lag >= 0.0f);
    if (0.0f != camera_lag)
    {
        time_elapsed /= camera_lag;
    }

    // Direct3D 9 world matrix
    // right.x    right.y    right.z    0
    // up.x       up.y       up.z       0
    // look.x     look.y     look.z     0
    // position.x position.y position.z 1
    const auto &player_right = player.get_right();
    const auto &player_up = player.get_up();
    const auto &player_look = player.get_look();
    // Inverted because GLSL uses column vectors(not row vectors like Direct3D)
    // rotation_matrix
    // right.x up.x look.x 0
    // right.y up.y look.y 0
    // right.z up.z look.z 0
    // 0       0    0      1
    glm::mat4 rotation_matrix{
        glm::vec4{player_right.x, player_right.y, player_right.z, 0.0f}, // column 0
        glm::vec4{player_up.x, player_up.y, player_up.z, 0.0f}, // column 1
        glm::vec4{player_look.x, player_look.y, player_look.z, 0.0f}, // column 2
        glm::vec4{0.0f, 0.0f, 0.0f, 1.0f}}; // column 3
    // Calculate the new position of the camera offset vector based on the orientation of the player.
    auto offset = Math_utility::transform_coordinate(rotation_matrix, player.get_camera_offset());
    // The camera is located at: player.get_position() + player.get_camera_offset()
    // The new position of the camera should be: player.get_position() + offset
    auto new_camera_position = player.get_position() + offset;
    // Calculate a vector from the current camera position to the new position
    auto direction = new_camera_position - get_position();
    auto length = glm::length(direction);
    direction = glm::normalize(direction);
    auto distance_to_move = length * time_elapsed;
    if (distance_to_move > length)
    {
        distance_to_move = length;
    }

    // If there is only a very short distance to move, move all of the way
    if (length < 0.01f)
    {
        distance_to_move = length;
    }
    
    if (distance_to_move > 0.0f)
    {
        auto position = get_position();
        position += (direction * distance_to_move);
        set_position(position);
        // Rotate the camera to look at the player from it's new position.
        look_at(player.get_position(), player_up);
    }
}

void Third_person_camera::move(const glm::vec3 &v)
{
    // Do nothing.
}

// Rotate the camera to look at the position "at".
void Third_person_camera::look_at(const glm::vec3 &at, const glm::vec3 &up)
{
    // Direct3D view matrix.
    // x_axis.x y_axis.x z_axis.x, 0
    // x_axis.y y_axis.y z_axis.y, 0
    // x_axis.z y_axis.z z_axis.z, 0
    // -dot(x_axis, eye), -dot(y_axis, eye), -dot(z_axis, eye), 1
    //
    // The matrix returned by look_at_left_handed is the transpose of the Direct3D view matrix. Therefore, the right
    // vector is stored in the first row of the matrix, the up vector is stored in the second row of the matrix, and
    // the look vector is stored in the third row of the matrix.
    auto look_at_matrix = Math_utility::look_at_left_handed(get_position(), at, up);
    auto camera_right_vector = glm::vec3{look_at_matrix[0].x, look_at_matrix[1].x, look_at_matrix[2].x};
    auto camera_up_vector = glm::vec3{look_at_matrix[0].y, look_at_matrix[1].y, look_at_matrix[2].y};
    auto camera_look_vector = glm::vec3{look_at_matrix[0].z, look_at_matrix[1].z, look_at_matrix[2].z};
    set_right(camera_right_vector);
    set_up(camera_up_vector);
    set_look(camera_look_vector);
}

}