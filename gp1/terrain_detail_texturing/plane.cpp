// This is a personal academic project. Dear PVS-Studio, please check it.
// PVS-Studio Static Code Analyzer for C, C++ and C#: http://www.viva64.com
#include "plane.h"

// GLM
#include <glm/geometric.hpp>

namespace Jade
{

Plane Plane::normalize(const Plane &plane)
{
    auto v = glm::vec3{plane.m_a, plane.m_b, plane.m_c};
    auto length = glm::length(v);
    return Plane{glm::vec4{v, plane.m_d} / length};
}

}