#pragma once

// GLM
#include <glm/vec4.hpp>

namespace Jade
{

// Stores the same data as the D3DXPLANE structure
class D3d9_plane
{
public:
    D3d9_plane() = default;
    explicit D3d9_plane(const glm::vec4 &v): m_a{v.x}, m_b{v.y}, m_c{v.z}, m_d{v.w} {}
    D3d9_plane(float a, float b, float c, float d): m_a{a}, m_b{b}, m_c{c}, m_d{d} {}

    static D3d9_plane normalize(const D3d9_plane &plane);
    
    float m_a{};
    float m_b{};
    float m_c{};
    float m_d{};
};

}
