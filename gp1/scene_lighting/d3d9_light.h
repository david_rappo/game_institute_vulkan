#pragma once

// C++ Standard Library
#include <iosfwd>
#include <cstddef>
#include <cstdint>

// GLM
#include <glm/vec3.hpp>
#include <glm/vec4.hpp>

namespace Jade
{

// Stores the same data as D3DLIGHT9. It corresponds to the Light_struct structure used in the vertex shader.
class D3d9_light
{
public:
    static const std::size_t std_140_size{112};

    enum class Type: int32_t
    {
        point = 1,
        spot = 2,
        directional = 3
    };
                            // std140 layout
    glm::vec4 m_diffuse;    // base alignment: 16 aligned offset: 0
    glm::vec4 m_specular;   // base alignment: 16 aligned offset: 16
    glm::vec4 m_ambient;    // base alignment: 16 aligned offset: 32
    glm::vec4 m_position;   // base alignment: 16 aligned offset: 48
    glm::vec4 m_direction;  // base alignment: 16 aligned offset: 64
    Type m_type{};          // base alignment: 4  aligned offset: 80
    float m_range{};        // base alignment: 4  aligned offset: 84
    float m_falloff{};      // base alignment: 4  aligned offset: 88
    float m_attenuation0{}; // base alignment: 4  aligned offset: 92
    float m_attenuation1{}; // base alignment: 4  aligned offset: 96
    float m_attenuation2{}; // base alignment: 4  aligned offset: 100
    float m_theta{};        // base alignment: 4  aligned offset: 104
    float m_phi{};          // base alignment: 4  aligned offset: 108
                            //
                            // total size = 112
    // The largest member of D3d9_light is 16 bytes. 112 is a multiple of 16. So, an array of D3d9_light in the vertex
    // shader will be tightly packed.
};

std::ostream &operator<<(std::ostream &out, const D3d9_light &light);

}