#pragma once

// C++ Standard Library
#include <cstdint>

// GLM
#include <glm/mat4x4.hpp>

namespace Jade
{

// Corresponds to Uniform_push_constant in the vertex shader. Push constant blocks use the std430 layout.
class Push_constants
{
public:
    Push_constants();
    Push_constants(const glm::mat4 &matrix,
        uint32_t light_group_size,
        uint32_t light_group_index,
        uint32_t material_index);

    // m_matrix = invert_y_matrix * projection_matrix * view_matrix * model_matrix
    glm::mat4 m_matrix;
    uint32_t m_light_group_size{};
    uint32_t m_light_group_index{};
    uint32_t m_material_index{};
};

}