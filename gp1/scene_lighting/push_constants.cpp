// This is a personal academic project. Dear PVS-Studio, please check it.
// PVS-Studio Static Code Analyzer for C, C++ and C#: http://www.viva64.com
#include "push_constants.h"

namespace Jade
{

Push_constants::Push_constants() = default;

Push_constants::Push_constants(const glm::mat4 &matrix,
    uint32_t light_group_size,
    uint32_t light_group_index,
    uint32_t material_index):
    m_matrix{matrix},
    m_light_group_size{light_group_size},
    m_light_group_index{light_group_index},
    m_material_index{material_index}
{

}

}