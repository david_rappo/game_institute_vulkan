// This is a personal academic project. Dear PVS-Studio, please check it.
// PVS-Studio Static Code Analyzer for C, C++ and C#: http://www.viva64.com
#include "frame.h"

// C++ Standard Library
#include <cassert>

namespace Jade
{

void Frame::destroy(VkDevice device, VkCommandPool command_pool) const
{
    assert(nullptr != device);
    assert(nullptr != command_pool);
    if (nullptr != m_command_buffer)
    {
        vkFreeCommandBuffers(device, command_pool, 1, &m_command_buffer);
    }
    
    if (nullptr != m_framebuffer)
    {
        vkDestroyFramebuffer(device, m_framebuffer, nullptr);
    }

    if (nullptr != m_image_available_semaphore)
    {
        vkDestroySemaphore(device, m_image_available_semaphore, nullptr);
    }

    if (nullptr != m_rendering_finished_semaphore)
    {
        vkDestroySemaphore(device, m_rendering_finished_semaphore, nullptr);
    }

    if (nullptr != m_fence)
    {
        vkDestroyFence(device, m_fence, nullptr);
    }
}

}
