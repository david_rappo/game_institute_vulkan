// This is a personal academic project. Dear PVS-Studio, please check it.
// PVS-Studio Static Code Analyzer for C, C++ and C#: http://www.viva64.com
#include "vulkan_utility.h"

// C++ Standard Library
#include <bitset>
#include <cassert>
#include <cstddef>
#include <limits>

// Jade
#include "buffer.h"

namespace Jade
{

namespace Vulkan_utility
{

bool create_buffer(VkDevice vulkan_device,
    const VkPhysicalDeviceMemoryProperties &physical_device_memory_properties,
    VkBufferUsageFlags buffer_usage_flags,
    VkMemoryPropertyFlags memory_property_flags,
    VkDeviceSize buffer_size,
    Jade::Buffer &out_buffer)
{
    VkBufferCreateInfo buffer_create_info = {};
    buffer_create_info.sType = VK_STRUCTURE_TYPE_BUFFER_CREATE_INFO;
    buffer_create_info.pNext = nullptr;
    buffer_create_info.flags = 0;
    buffer_create_info.size = buffer_size;
    out_buffer.m_size = buffer_size;
    buffer_create_info.usage = buffer_usage_flags;
    buffer_create_info.sharingMode = VK_SHARING_MODE_EXCLUSIVE;
    buffer_create_info.queueFamilyIndexCount = 0;
    buffer_create_info.pQueueFamilyIndices = nullptr;
    auto vk_result = vkCreateBuffer(vulkan_device, &buffer_create_info, nullptr, &out_buffer.m_buffer);
    if (VK_SUCCESS != vk_result)
    {
        return false;
    }

    vkGetBufferMemoryRequirements(vulkan_device, out_buffer.m_buffer, &out_buffer.m_memory_requirements);
    uint32_t memory_type_index{};
    if (!find_memory_type(physical_device_memory_properties,
        out_buffer.m_memory_requirements,
        memory_property_flags,
        memory_type_index))
    {
        return false;
    }

    VkMemoryAllocateInfo memory_allocate_info = {};
    memory_allocate_info.sType = VK_STRUCTURE_TYPE_MEMORY_ALLOCATE_INFO;
    memory_allocate_info.pNext = nullptr;
    memory_allocate_info.allocationSize = out_buffer.m_memory_requirements.size;
    memory_allocate_info.memoryTypeIndex = memory_type_index;
    vk_result = vkAllocateMemory(vulkan_device, &memory_allocate_info, nullptr, &out_buffer.m_device_memory);
    if (VK_SUCCESS != vk_result)
    {
        return false;
    }

    vk_result = vkBindBufferMemory(vulkan_device, out_buffer.m_buffer, out_buffer.m_device_memory, 0);
    if (VK_SUCCESS != vk_result)
    {
        return false;
    }

    return true;
}

// Find the first VkMemoryType required by memory_requirements that satisfies requirements e.g. must be host visible.
bool find_memory_type(const VkPhysicalDeviceMemoryProperties &physical_device_memory_properties,
    const VkMemoryRequirements &memory_requirements,
    VkFlags requirements,
    uint32_t &memory_type_index)
{
    std::bitset<sizeof(std::uint32_t) * 8> b{static_cast<unsigned long long>(memory_requirements.memoryTypeBits)};
    for (uint32_t i = 0; i != physical_device_memory_properties.memoryTypeCount; ++i)
    {
        if (b[static_cast<std::size_t>(i)])
        {
            auto &memory_type = physical_device_memory_properties.memoryTypes[i];
            auto result = (memory_type.propertyFlags & requirements);
            if (result == requirements)
            {
                memory_type_index = i;
                return true;
            }
        }
    }

    return false;
}

// Returns the number of bytes required to store buffer_size bytes in a buffer taking minimum_offset_alignment into
// consideration.
//
// minimum_offset_alignment: A minimum offset alignment such as
//                           VkPhysicalDeviceLimits::minUniformBufferOffsetAlignment.
VkDeviceSize calculate_buffer_size(VkDeviceSize minimum_offset_alignment, VkDeviceSize buffer_size)
{
    assert(0 != buffer_size);
    // Example. Let minimum_offset_alignment = 256.
    // If buffer_size = 4, i = 0, remainder = 4, add = 256, ret = 256
    // If buffer_size = 256, i = 1, remainder = 0, add = 0, ret = 256
    // If buffer_size = 300, i = 1, remainder = 44, add = 1, ret = 512
    auto i = buffer_size / minimum_offset_alignment;
    auto remainder = buffer_size % minimum_offset_alignment;
    auto add = (0 != remainder) ? minimum_offset_alignment : 0;
    return (i * minimum_offset_alignment) + add;
}

}

}