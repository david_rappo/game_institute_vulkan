#pragma once

// GLM
#include <glm/vec3.hpp>

namespace Jade
{

class Vertex
{
public:
    Vertex() = default;
    Vertex(const glm::vec3 &position, const glm::vec3 &normal): m_position{position}, m_normal{normal}
    {

    }
    
    glm::vec3 m_position;
    glm::vec3 m_normal;
};

}