#pragma once

// C++ Standard Library
#include <array>

namespace Jade
{

// This class expects that GLFW has been initialized properly.
class Timer
{
public:
    void start(double previous_time);
    void tick();
    int get_frame_rate() const;
    double get_time_elapsed() const;

private:
    double m_previous_time{};
    double m_time{};
    // Stores the elapsed time in seconds since the last call to tick.
    double m_time_elapsed{};
    double m_frames_per_second_time{};
    int m_frame_count{};
    // Stores frames per second.
    int m_frame_rate{};
    // First in first out. See tick method.
    std::array<double, 50> m_frame_times{};
};

}
