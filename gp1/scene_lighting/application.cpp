// This is a personal academic project. Dear PVS-Studio, please check it.
// PVS-Studio Static Code Analyzer for C, C++ and C#: http://www.viva64.com
#include "application.h"

// C++ Standard Library
#include <algorithm>
#include <array>
#include <bitset>
#include <cassert>
#include <chrono>
#include <cinttypes>
#include <cstdio>
#include <cstring>
#include <fstream>
#include <iterator>
#include <limits>
#include <memory>
#include <numeric>
#include <sstream>

// GLM
#include <glm/gtc/matrix_transform.hpp>
#include <glm/gtc/type_ptr.hpp>
#include <glm/vec2.hpp>
#include <glm/vec3.hpp>

// Jade
#include "camera.h"
#include "debug_utility.h"
#include "file_utility.h"
#include "log_manager.h"
#include "math_utility.h"
#include "push_constants.h"
#include "stb_image_wrapper.h"
#include "vulkan_utility.h"

// This directory is relative to the path that the executable is running in.
const std::string Jade::Application::data_path{"data"};

namespace Jade
{

Application::Application() : //-V730
    m_random_engine{static_cast<unsigned>(std::chrono::system_clock::now().time_since_epoch().count())},
    m_distribution{0.0f, 1.0f},
    m_invert_y_matrix{Jade::Math_utility::invert_y_matrix()}
{

}

Application::~Application()
{
    // Destroy resources in reverse order of creation
    // Creation order
    // m_glfw_window
    // m_vulkan_instance
    // m_surface
    // m_debug_report_callback
    // m_vulkan_device
    // m_swap_chain
    // m_swap_chain_image_views
    // m_scene
    // m_uniform_buffer
    // m_staging_uniform_buffer
    // m_render_pass
    // m_shader_modules
    // m_descriptor_pool
    // m_descriptor_set_layout
    // m_descriptor_set
    // m_pipeline_layout
    // m_pipeline
    // m_depth_stencil
    // m_command_pool
    // m_vec_frame
    if (nullptr != m_vulkan_device)
    {
        vkDeviceWaitIdle(m_vulkan_device);
        for (auto iter = m_vec_frame.cbegin(); iter != m_vec_frame.cend(); ++iter)
        {
            if (nullptr != m_command_pool)
            {
                iter->destroy(m_vulkan_device, m_command_pool);
            }
        }
        
        if (nullptr != m_command_pool)
        {
            vkDestroyCommandPool(m_vulkan_device, m_command_pool, nullptr);
        }

        m_depth_stencil.destroy(m_vulkan_device);
        if (nullptr != m_pipeline)
        {
            vkDestroyPipeline(m_vulkan_device, m_pipeline, nullptr);
        }
        
        if (nullptr != m_pipeline_layout)
        {
            vkDestroyPipelineLayout(m_vulkan_device, m_pipeline_layout, nullptr);
        }

        // It is not necessary to call vkFreeDescriptorSets to free m_descriptor_set unless
        // VkDescriptorPoolCreateInfo.flags contains VK_DESCRIPTOR_POOL_CREATE_FREE_DESCRIPTOR_SET_BIT.

        if (nullptr != m_descriptor_set_layout)
        {
            vkDestroyDescriptorSetLayout(m_vulkan_device, m_descriptor_set_layout, nullptr);
        }

        if (nullptr != m_descriptor_pool)
        {
            vkDestroyDescriptorPool(m_vulkan_device, m_descriptor_pool, nullptr);
        }

        for (auto iter = m_shader_modules.cbegin(); iter != m_shader_modules.cend(); ++iter)
        {
            vkDestroyShaderModule(m_vulkan_device, *iter, nullptr);
        }
        
        if (nullptr != m_render_pass)
        {
            vkDestroyRenderPass(m_vulkan_device, m_render_pass, nullptr);
        }

        m_staging_uniform_buffer.destroy(m_vulkan_device);
        m_uniform_buffer.destroy(m_vulkan_device);
        m_scene.destroy(m_vulkan_device);
        for (auto iter = m_swapchain_image_views.cbegin(); iter != m_swapchain_image_views.cend(); ++iter)
        {
            auto image_view = *iter;
            if (nullptr != image_view)
            {
                vkDestroyImageView(m_vulkan_device, image_view, nullptr);
            }
        }

        if (nullptr != m_swapchain)
        {
            vkDestroySwapchainKHR(m_vulkan_device, m_swapchain, nullptr);
        }
        
        vkDestroyDevice(m_vulkan_device, nullptr);
    }
#ifdef _DEBUG
    if (nullptr != m_debug_report_callback)
    {
        if (nullptr != m_vulkan_instance)
        {
            auto proc_address = glfwGetInstanceProcAddress(m_vulkan_instance,
                "vkDestroyDebugReportCallbackEXT");
            if (nullptr != proc_address)
            {
                auto Jade_vkDestroyDebugReportCallbackEXT = reinterpret_cast<PFN_vkDestroyDebugReportCallbackEXT>(proc_address);
                Jade_vkDestroyDebugReportCallbackEXT(m_vulkan_instance,
                    m_debug_report_callback,
                    nullptr);
            }
        }
    }
#endif
    if (nullptr != m_vulkan_instance)
    {
        if (nullptr != m_surface)
        {
            vkDestroySurfaceKHR(m_vulkan_instance, m_surface, nullptr);
        }
        
        vkDestroyInstance(m_vulkan_instance, nullptr);
    }

    if (nullptr != m_glfw_window)
    {
        glfwDestroyWindow(m_glfw_window);
    }

    glfwTerminate();
}

bool Application::initialize(const std::vector<std::string> &arguments)
{
    // It is OK to call glfwSetErrorCallback before glfwInit
    glfwSetErrorCallback(Application::glfw_error_callback);
    if (GLFW_TRUE != glfwInit())
    {
        return false;
    }

    m_window_title = m_application_name;
    if (!arguments.empty())
    {
        m_window_title = arguments.at(0);
    }

    glfwWindowHint(GLFW_CLIENT_API, GLFW_NO_API);
    glfwWindowHint(GLFW_VISIBLE, GLFW_FALSE);
    m_glfw_window = glfwCreateWindow(800, 600, m_window_title.c_str(), nullptr, nullptr);
    if (nullptr == m_glfw_window)
    {
        return false;
    }

    glfwSetWindowUserPointer(m_glfw_window, this);
    glfwSetWindowCloseCallback(m_glfw_window, Application::glfw_window_close_callback);
    glfwSetFramebufferSizeCallback(m_glfw_window, Application::glfw_frame_buffer_size_callback);
    std::vector<std::string> instance_extension_names;
    uint32_t glfw_required_instance_extensions_count{};
    // The returned array will always contain VK_KHR_surface.
    auto glfw_required_instance_extensions = glfwGetRequiredInstanceExtensions(
        &glfw_required_instance_extensions_count);
    if (nullptr == glfw_required_instance_extensions)
    {
        return false;
    }

    std::copy(glfw_required_instance_extensions,
        glfw_required_instance_extensions + static_cast<ptrdiff_t>(glfw_required_instance_extensions_count),
        std::back_inserter(instance_extension_names));
    if (!create_instance(instance_extension_names, m_vulkan_instance))
    {
        return false;
    }

    if (VK_SUCCESS != glfwCreateWindowSurface(m_vulkan_instance, m_glfw_window, nullptr, &m_surface))
    {
        return false;
    }

    glfwSetKeyCallback(m_glfw_window, glfw_key_callback);
    glfwSetMouseButtonCallback(m_glfw_window, glfw_mouse_button_callback);
    glfwSetCursorPosCallback(m_glfw_window, glfw_mouse_cursor_position_callback);
#ifdef _DEBUG
    if (!create_debug_report_callback(m_vulkan_instance, m_debug_report_callback))
    {
        return false;
    }
#endif
    if (!create_device(m_physical_device, m_vulkan_device, m_graphics_queue_family_index, m_present_queue_family_index))
    {
        return false;
    }

    vkGetDeviceQueue(m_vulkan_device, m_graphics_queue_family_index, 0, &m_graphics_queue);
    vkGetDeviceQueue(m_vulkan_device, m_present_queue_family_index, 0, &m_present_queue);
    vkGetPhysicalDeviceProperties(m_physical_device, &m_physical_device_properties);
    vkGetPhysicalDeviceMemoryProperties(m_physical_device, &m_physical_device_memory_properties);
    int width{};
    int height{};
    glfwGetFramebufferSize(m_glfw_window, &width, &height);
    auto old_swapchain = m_swapchain;
    assert(nullptr == old_swapchain);
    if (!create_swapchain(
        static_cast<uint32_t>(width),
        static_cast<uint32_t>(height),
        old_swapchain,
        m_swapchain_image_format,
        m_swapchain_images,
        m_swapchain_image_views,
        m_swapchain))
    {
        return false;
    }

    if (!find_depth_stencil_image_format(m_depth_stencil_image_format))
    {
        return false;
    }

    {
        auto file_name = data_path + "/Colony5.iwf";
        if (!m_scene.load(file_name, 8, 1))
        {
            return false;
        }

        if (!m_scene.build_buffers(m_vulkan_device, m_physical_device_memory_properties))
        {
            return false;
        }
    }

    {
        update_descriptor_buffer_infos();
        auto size = m_descriptor_buffer_info_light_groups.offset + m_descriptor_buffer_info_light_groups.range;
        auto result = Jade::Vulkan_utility::create_buffer(m_vulkan_device,
            m_physical_device_memory_properties,
            VK_BUFFER_USAGE_UNIFORM_BUFFER_BIT | VK_BUFFER_USAGE_TRANSFER_DST_BIT,
            VK_MEMORY_PROPERTY_DEVICE_LOCAL_BIT,
            size,
            m_uniform_buffer);
        if (!result)
        {
            return false;
        }

        m_descriptor_buffer_info_lights.buffer = m_uniform_buffer.m_buffer;
        m_descriptor_buffer_info_materials.buffer = m_uniform_buffer.m_buffer;
        m_descriptor_buffer_info_light_groups.buffer = m_uniform_buffer.m_buffer;
        result = Jade::Vulkan_utility::create_buffer(m_vulkan_device,
            m_physical_device_memory_properties,
            VK_BUFFER_USAGE_TRANSFER_SRC_BIT,
            VK_MEMORY_PROPERTY_HOST_VISIBLE_BIT,
            size,
            m_staging_uniform_buffer);
        if (!result)
        {
            return false;
        }
    }
    
    if (!create_render_pass(m_render_pass))
    {
        return false;
    }

    std::vector<std::string> file_names = {"vertex_shader.spv", "fragment_shader.spv"};
    if (!create_shader_modules(file_names, m_shader_modules))
    {
        return false;
    }

    if (!create_descriptor_pool(m_descriptor_pool))
    {
        return false;
    }

    if (!create_descriptor_set_layout(m_descriptor_set_layout))
    {
        return false;
    }

    if (!allocate_descriptor_set(m_descriptor_set))
    {
        return false;
    }

    if (!create_pipeline(m_pipeline_layout, m_pipeline))
    {
        return false;
    }

    if (!create_command_pool(m_command_pool))
    {
        return false;
    }

    std::vector<VkCommandBuffer> vec_command_buffer;
    if (!allocate_command_buffers(command_buffer_count, vec_command_buffer))
    {
        return false;
    }

    for (std::size_t i = 0; i != vec_command_buffer.size(); ++i)
    {
        Jade::Frame frame;
        frame.m_command_buffer = vec_command_buffer.at(i);
        m_vec_frame.push_back(frame);
    }

    std::vector<VkSemaphore> semaphores;
    if (!create_semaphores(static_cast<uint32_t>(m_vec_frame.size() * 2), semaphores))
    {
        return false;
    }

    for (std::size_t i = 0; i != m_vec_frame.size(); ++i)
    {
        auto &frame = m_vec_frame.at(i);
        auto j = i * 2;
        frame.m_image_available_semaphore = semaphores.at(j);
        frame.m_rendering_finished_semaphore = semaphores.at(j + 1);
    }

    std::vector<VkFence> vec_fence;
    if (!create_fences(static_cast<uint32_t>(m_vec_frame.size()), vec_fence))
    {
        return false;
    }

    for (std::size_t i = 0; i != vec_fence.size(); ++i)
    {
        auto &frame = m_vec_frame.at(i);
        frame.m_fence = vec_fence.at(i);
    }
    
    {
        const auto &frame = m_vec_frame.at(0);
        VkCommandBufferBeginInfo command_buffer_begin_info = {};
        command_buffer_begin_info.sType = VK_STRUCTURE_TYPE_COMMAND_BUFFER_BEGIN_INFO;
        command_buffer_begin_info.pNext = nullptr;
        command_buffer_begin_info.flags = VK_COMMAND_BUFFER_USAGE_ONE_TIME_SUBMIT_BIT;
        command_buffer_begin_info.pInheritanceInfo = nullptr;
        auto vk_result = vkBeginCommandBuffer(frame.m_command_buffer, &command_buffer_begin_info);
        if (VK_SUCCESS != vk_result)
        {
            return false;
        }

        if (!m_scene.copy_buffers(m_vulkan_device, m_graphics_queue, frame.m_command_buffer))
        {
            return false;
        }

        if (!copy_uniform_buffer(frame.m_command_buffer))
        {
            return false;
        }

        vk_result = vkEndCommandBuffer(frame.m_command_buffer);
        if (VK_SUCCESS != vk_result)
        {
            return false;
        }

        VkSubmitInfo submit_info = {};
        submit_info.sType = VK_STRUCTURE_TYPE_SUBMIT_INFO;
        submit_info.pNext = nullptr;
        submit_info.waitSemaphoreCount = 0;
        submit_info.pWaitSemaphores = nullptr;
        submit_info.pWaitDstStageMask = nullptr;
        submit_info.commandBufferCount = 1;
        submit_info.pCommandBuffers = &frame.m_command_buffer;
        submit_info.signalSemaphoreCount = 0;
        submit_info.pSignalSemaphores = nullptr;
        vk_result = vkQueueSubmit(m_graphics_queue, 1, &submit_info, nullptr);
        if (VK_SUCCESS != vk_result)
        {
            return false;
        }

        vk_result = vkDeviceWaitIdle(m_vulkan_device);
        if (VK_SUCCESS != vk_result)
        {
            return false;
        }
    }
    
    m_player.set_camera_type(Camera::Type::first_person_fly);
    m_player.set_first_person_fly_defaults();
    auto volume = Volume{glm::vec3{-3.0f, -5.0f, -3.0f}, glm::vec3{3.0f, 20.0f, 3.0f}};
    m_player.set_volume(volume);

    {
        auto camera = m_player.get_camera();
        // Lab Project Scene_Lighting sets the vertical field of view to 120, then calls D3DXMatrixPerspectiveFovLH
        // with a fovy argument of 120/2. The scene_lighting program does not do this.
        camera->set_vertical_field_of_view(glm::radians(60.0f));
        // The aspect ratio will be recalculated when glfwShowWindow is called in Application::run
        camera->set_aspect_ratio(4.0f / 3.0f);
        camera->set_near_view_plane(1.01f);
        camera->set_far_view_plane(5000.0f);
        camera->set_volume(volume);
    }
    
    m_player.set_position(glm::vec3{0.0f, -15.0f, 0.0f});
    update_descriptor_sets();
    return true;
}

void Application::run()
{
    // Calling glfwShowWindow causes Application::frame_buffer_resized to be called.
    glfwShowWindow(m_glfw_window);
    std::size_t frame_index{};
    m_timer.start(glfwGetTime());
    int last_frame_rate{};
    while (!m_window_should_close)
    {
        m_timer.tick();
        int width{};
        int height{};
        glfwGetFramebufferSize(m_glfw_window, &width, &height);
        // Assume that the application is minimized if width or height is zero.
        bool is_minimized = ((0 == width) || (0 == height));
        if (!is_minimized)
        {
            auto frame_rate = m_timer.get_frame_rate();
            if (frame_rate != last_frame_rate)
            {
                std::ostringstream oss;
                oss << m_window_title << " frames per second: " << frame_rate;
                glfwSetWindowTitle(m_glfw_window, oss.str().c_str());
            }
            
            process_input();
            animate_objects();
            if (!draw(static_cast<uint32_t>(width), static_cast<uint32_t>(height), m_vec_frame.at(frame_index)))
            {
                return;
            }

            ++frame_index;
            assert(m_vec_frame.size() == static_cast<std::size_t>(command_buffer_count));
            frame_index %= m_vec_frame.size();
        }
        
        glfwPollEvents();
    }
}

void Application::glfw_error_callback(int error, const char *description)
{
    auto &os = Log_manager::get_instance()->get_stream(Log_manager::Log_type::glfw_error);
    os << jade_function_signature << ") error: " << error << " description: " << description << std::endl;
    std::exit(0);
}

void Application::glfw_window_close_callback(GLFWwindow *window)
{
    auto user_data = glfwGetWindowUserPointer(window);
    if (nullptr != user_data)
    {
        auto application = reinterpret_cast<Application*>(user_data);
        application->m_window_should_close = true;
    }
}

void Application::glfw_frame_buffer_size_callback(GLFWwindow *window, int width, int height)
{
    auto user_data = glfwGetWindowUserPointer(window);
    if (nullptr != user_data)
    {
        auto application = reinterpret_cast<Application*>(user_data);
        application->frame_buffer_resized(width, height);
    }
}

void Application::glfw_key_callback(GLFWwindow *window, int key, int scan_code, int action, int modifiers)
{
    auto user_data = glfwGetWindowUserPointer(window);
    if (nullptr != user_data)
    {
        auto application = reinterpret_cast<Application*>(user_data);
        application->key_callback(window, key, scan_code, action, modifiers);
    }
}

void Application::glfw_mouse_button_callback(GLFWwindow *window, int button, int action, int modifiers)
{
    auto user_data = glfwGetWindowUserPointer(window);
    if (nullptr != user_data)
    {
        auto application = reinterpret_cast<Application*>(user_data);
        application->mouse_button_callback(window, button, action, modifiers);
    }
}

void Application::glfw_mouse_cursor_position_callback(GLFWwindow *window, double x, double y)
{
    auto user_data = glfwGetWindowUserPointer(window);
    if (nullptr != user_data)
    {
        auto application = reinterpret_cast<Application*>(user_data);
        application->mouse_cursor_position_callback(window, x, y);
    }
}

// Returns true if all of the instance layers in instance_layer_names are supported.
bool Application::check_instance_layers(const std::vector<std::string> &instance_layer_names)
{
    uint32_t property_count{};
    auto result = vkEnumerateInstanceLayerProperties(&property_count, nullptr);
    if (VK_SUCCESS != result)
    {
        return false;
    }

    std::vector<VkLayerProperties> layer_properties(static_cast<std::size_t>(property_count));
    result = vkEnumerateInstanceLayerProperties(&property_count, layer_properties.data());
    if (VK_SUCCESS != result)
    {
        return false;
    }

    for (auto iter = instance_layer_names.cbegin(); iter != instance_layer_names.cend(); ++iter)
    {
        auto find_result = std::find_if(layer_properties.cbegin(),
            layer_properties.cend(),
            [iter](const VkLayerProperties &layer_property)
        { return 0 == std::strcmp(layer_property.layerName, iter->c_str()); });
        if (layer_properties.cend() == find_result)
        {
            return false;
        }
    }

    return true;
}

// Returns true if every extension in extension_names is supported by the Vulkan
// implementation.
bool Application::check_instance_extensions(const std::vector<std::string> &extension_names)
{
    uint32_t property_count{};
    auto result = vkEnumerateInstanceExtensionProperties(nullptr,
        &property_count,
        nullptr);
    if (VK_SUCCESS != result)
    {
        return false;
    }

    std::vector<VkExtensionProperties> properties(static_cast<std::size_t>(property_count));
    result = vkEnumerateInstanceExtensionProperties(nullptr,
        &property_count,
        properties.data());
    if (VK_SUCCESS != result)
    {
        return false;
    }

    for (auto iter = extension_names.cbegin(); iter != extension_names.cend(); ++iter)
    {
        auto j = std::find_if(properties.cbegin(),
            properties.cend(),
            [iter](const VkExtensionProperties &property) { return 0 == std::strcmp(iter->c_str(), property.extensionName); });
        if (properties.cend() == j)
        {
            return false;
        }
    }

    return true;
}

// Returns true if every extension in extension_names is supported by the Vulkan implementation.
bool Application::check_device_extensions(VkPhysicalDevice device,
    const std::vector<std::string> &extension_names)
{
    uint32_t property_count{};
    if (VK_SUCCESS != vkEnumerateDeviceExtensionProperties(device, nullptr, &property_count, nullptr))
    {
        return false;
    }

    std::vector<VkExtensionProperties> properties(static_cast<std::size_t>(property_count));
    if (VK_SUCCESS != vkEnumerateDeviceExtensionProperties(device, nullptr, &property_count, properties.data()))
    {
        return false;
    }

    for (auto iter = extension_names.cbegin(); iter != extension_names.cend(); ++iter)
    {
        auto j = std::find_if(properties.cbegin(),
            properties.cend(),
            [iter](const VkExtensionProperties &property) { return 0 == std::strcmp(property.extensionName, iter->c_str()); });
        if (properties.cend() == j)
        {
            return false;
        }
    }

    return true;
}

VkBool32 Application::vulkan_debug_report_callback(VkDebugReportFlagsEXT flags,
    VkDebugReportObjectTypeEXT object_type,
    uint64_t object,
    size_t location,
    int32_t message_code,
    const char *layer_prefix,
    const char *message,
    void *user_data)
{
    auto &os = Log_manager::get_instance()->get_stream(Log_manager::Log_type::vulkan_debug_report_all);
    os << "flags: " << flags << std::endl;
    os << "object_type: " << object_type << std::endl;
    os << "object: " << object << std::endl;
    os << "location: " << location << std::endl; //-V128
    os << "message_code: " << message_code << std::endl;
    os << "layer_prefix: " << layer_prefix << std::endl;
    os << message << std::endl;
    os << std::endl;
    switch (flags)
    {
    case VK_DEBUG_REPORT_WARNING_BIT_EXT:
    case VK_DEBUG_REPORT_PERFORMANCE_WARNING_BIT_EXT:
    case VK_DEBUG_REPORT_ERROR_BIT_EXT:
        std::exit(EXIT_FAILURE);
        break;
    }

    // Tell the calling layer not to abort the Vulkan call so that the
    // application behaves the same with and without validation layers enabled.
    return VK_FALSE;
}

void Application::key_callback(GLFWwindow *window, int key, int scan_code, int action, int modifiers)
{
    switch (key)
    {
    
    case GLFW_KEY_LEFT:
    case GLFW_KEY_RIGHT:
    case GLFW_KEY_UP:
    case GLFW_KEY_DOWN:
    case GLFW_KEY_PAGE_UP:
    case GLFW_KEY_PAGE_DOWN:
        m_key_map[key] = action;
        break;
    }
}

void Application::mouse_button_callback(GLFWwindow *window, int button, int action, int modifiers)
{
    switch (button)
    {
    case GLFW_MOUSE_BUTTON_LEFT:
        switch (action)
        {
        case GLFW_PRESS:
            // Hide the mouse cursor and lock it to window. When the cursor is disabled GLFW handles cursor
            // re-centering and provides the application with a virtual cursor position.
            glfwSetInputMode(window, GLFW_CURSOR, GLFW_CURSOR_DISABLED);
            glfwGetCursorPos(window, &m_previous_mouse_cursor_position.x, &m_previous_mouse_cursor_position.y);
            m_mouse_cursor_shift_vector = glm::dvec2{0.0};
            break;
        case GLFW_RELEASE:
            glfwSetInputMode(window, GLFW_CURSOR, GLFW_CURSOR_NORMAL);
            break;
        default:
            break;
        }
        break;
    default:
        break;
    }

    m_mouse_button_map[button] = action;
}

// x: x position relative to the left edge of the client area
// y: y position relative to the top edge of the client area
void Application::mouse_cursor_position_callback(GLFWwindow *window, double x, double y)
{
    if (is_mouse_button_pressed(GLFW_MOUSE_BUTTON_LEFT))
    {
        auto mouse_cursor_position = glm::dvec2{x, y};
        m_mouse_cursor_shift_vector = mouse_cursor_position - m_previous_mouse_cursor_position;
        // Divide by 12 to reduce rotation values.
        // Note: The Game Institute project Scene_Lighting used 3.0
        m_mouse_cursor_shift_vector /= 12.0;
        // According to the GLFW documentation:
        // If the cursor is disabled (with GLFW_CURSOR_DISABLED) then the cursor position is unbounded and limited only
        // by the minimum and maximum values of a double.
        m_previous_mouse_cursor_position = mouse_cursor_position;
    }
}

void Application::frame_buffer_resized(int new_width, int new_height)
{
    auto delta = std::fabs(0.0f - new_height);
    if (delta >= 0.01f)
    {
        auto aspect_ratio = static_cast<float>(new_width) / static_cast<float>(new_height);
        auto camera = m_player.get_camera();
        camera->set_aspect_ratio(aspect_ratio);
    }
    
    // Assume that the application is minimized if width or height is zero.
    if ((0 == new_width) || (0 == new_height))
    {
        return;
    }

    if (!recreate_swap_chain(static_cast<uint32_t>(new_width), static_cast<uint32_t>(new_height)))
    {
        std::exit(EXIT_FAILURE);
    }

    // recreate_swap_chain calls vkDeviceWaitIdle, so m_vulkan_device should be idle now.
    m_depth_stencil.destroy(m_vulkan_device);
    if (!create_depth_stencil_image(m_vulkan_device,
        m_physical_device_memory_properties,
        m_depth_stencil_image_format,
        static_cast<uint32_t>(new_width),
        static_cast<uint32_t>(new_height),
        m_depth_stencil))
    {
        std::exit(EXIT_FAILURE);
    }
}

// instance_extensions: List of extensions that the newly created VkInstance must support.
bool Application::create_instance(const std::vector<std::string> &instance_extensions, VkInstance &out_instance) const
{
    if (!check_instance_layers(m_instance_layer_names))
    {
        return false;
    }

    if (!check_instance_extensions(instance_extensions))
    {
        return false;
    }

    VkApplicationInfo application_info = {};
    application_info.sType = VK_STRUCTURE_TYPE_APPLICATION_INFO;
    application_info.pNext = nullptr;
    application_info.pApplicationName = m_application_name.c_str();
    application_info.applicationVersion = VK_MAKE_VERSION(1, 0, 0);
    application_info.pEngineName = m_engine_name.c_str();
    application_info.engineVersion = VK_MAKE_VERSION(1, 0, 0);
    application_info.apiVersion = VK_API_VERSION_1_0;
    VkInstanceCreateInfo instance_create_info = {};
    instance_create_info.sType = VK_STRUCTURE_TYPE_INSTANCE_CREATE_INFO;
    instance_create_info.pNext = nullptr;
    instance_create_info.flags = 0;
    instance_create_info.pApplicationInfo = &application_info;
    std::vector<const char*> instance_layers_char;
#ifdef _DEBUG
    std::transform(m_instance_layer_names.cbegin(),
        m_instance_layer_names.cend(),
        std::back_inserter(instance_layers_char),
        [](const std::string &layer) { return layer.c_str(); });
#endif
    instance_create_info.enabledLayerCount = static_cast<uint32_t>(instance_layers_char.size());
    instance_create_info.ppEnabledLayerNames = instance_layers_char.data();
    std::vector<const char*> instance_extensions_char;
    std::transform(instance_extensions.cbegin(),
        instance_extensions.cend(),
        std::back_inserter(instance_extensions_char),
        [](const std::string &extension) { return extension.c_str(); });
#ifdef _DEBUG
    instance_extensions_char.push_back("VK_EXT_debug_report");
#endif
    instance_create_info.enabledExtensionCount = static_cast<uint32_t>(instance_extensions_char.size());
    instance_create_info.ppEnabledExtensionNames = instance_extensions_char.data();
    auto result = vkCreateInstance(&instance_create_info,
        nullptr,
        &out_instance);
    return VK_SUCCESS == result;
}

// Returns true if physical_device is a suitable physical device e.g.
// - has a queue family that supports graphics
// - has a queue family that supports presenting to m_surface
//
// Each VkPhysicalDevice can have multiple queue families. Each queue family can
// have multiple queues.
//
// device_extensions - list of extensions that physical_device must support
// present_surface - handle to surface that physical_device must be able to
// present to.
bool Application::check_physical_device(VkPhysicalDevice physical_device,
    const std::vector<std::string> &device_extensions,
    VkSurfaceKHR present_surface,
    uint32_t &out_graphics_queue_family_index,
    uint32_t &out_present_queue_family_index) const
{
    out_graphics_queue_family_index = std::numeric_limits<uint32_t>::max();
    out_present_queue_family_index = std::numeric_limits<uint32_t>::max();
    if (!check_device_extensions(physical_device, device_extensions))
    {
        return false;
    }

    uint32_t queue_family_property_count{};
    vkGetPhysicalDeviceQueueFamilyProperties(physical_device, &queue_family_property_count, nullptr);
    if (0 == queue_family_property_count)
    {
        return false;
    }

    std::vector<VkQueueFamilyProperties> queue_family_properties(static_cast<std::size_t>(queue_family_property_count));
    vkGetPhysicalDeviceQueueFamilyProperties(physical_device,
        &queue_family_property_count,
        queue_family_properties.data());
    uint32_t queue_family_index{};
    std::vector<VkBool32> present_supported_vec;
    std::vector<bool> graphics_supported_vec;
    std::vector<bool> both_supported_vec;
    for (auto iter = queue_family_properties.cbegin(); iter != queue_family_properties.cend(); ++iter)
    {
        VkBool32 present_supported{VK_FALSE};
        // Check if the queue family queue_family_index can present to m_surface
        if (VK_SUCCESS != vkGetPhysicalDeviceSurfaceSupportKHR(physical_device,
            queue_family_index,
            present_surface,
            &present_supported))
        {
            return false;
        }
        
        present_supported_vec.push_back(present_supported);
        auto graphics_supported = (iter->queueFlags & VK_QUEUE_GRAPHICS_BIT);
		// Fix for warning C4800: forcing value to bool
		graphics_supported_vec.push_back((graphics_supported != 0) ? true : false);
        auto both_supported = (graphics_supported && (VK_TRUE == present_supported));
        both_supported_vec.push_back(both_supported);
        ++queue_family_index;
    }

    // Prefer to use a queue family that supports both graphics and present
    for (std::size_t i = 0; i != both_supported_vec.size(); ++i)
    {
        // Pick the first queue family found that supports both graphics and
        // present.
        if (both_supported_vec.at(i))
        {
            out_graphics_queue_family_index = static_cast<uint32_t>(i);
            out_present_queue_family_index = static_cast<uint32_t>(i);
            return true;
        }
    }

    for (std::size_t i = 0; i != present_supported_vec.size(); ++i)
    {
        auto present_supported = present_supported_vec.at(i);
        // Pick the first queue family found that supports present.
        if (VK_TRUE == present_supported)
        {
            out_present_queue_family_index = static_cast<uint32_t>(i);
            break;
        }
    }

    for (std::size_t i = 0; i != graphics_supported_vec.size(); ++i)
    {
        // Pick the first queue family found that supports graphics.
        if (graphics_supported_vec.at(i))
        {
            out_graphics_queue_family_index = static_cast<uint32_t>(i);
        }
    }

    if (std::numeric_limits<uint32_t>::max() == out_graphics_queue_family_index)
    {
        return false;
    }

    if (std::numeric_limits<uint32_t>::max() == out_present_queue_family_index)
    {
        return false;
    }

    return true;
}

bool Application::create_debug_report_callback(VkInstance instance,
    VkDebugReportCallbackEXT &out_callback) const
{
    VkDebugReportCallbackCreateInfoEXT debug_report_callback_create_info = {};
    debug_report_callback_create_info.sType = VK_STRUCTURE_TYPE_DEBUG_REPORT_CALLBACK_CREATE_INFO_EXT;
    debug_report_callback_create_info.pNext = nullptr;
    debug_report_callback_create_info.flags = VK_DEBUG_REPORT_INFORMATION_BIT_EXT|
        VK_DEBUG_REPORT_WARNING_BIT_EXT|
        VK_DEBUG_REPORT_PERFORMANCE_WARNING_BIT_EXT|
        VK_DEBUG_REPORT_ERROR_BIT_EXT|
        VK_DEBUG_REPORT_DEBUG_BIT_EXT;
    debug_report_callback_create_info.pfnCallback = Application::vulkan_debug_report_callback;
    debug_report_callback_create_info.pUserData = nullptr;
    auto proc_address = glfwGetInstanceProcAddress(instance, "vkCreateDebugReportCallbackEXT");
    if (nullptr == proc_address)
    {
        return false;
    }
    
    auto Jade_vkCreateDebugReportCallbackEXT = reinterpret_cast<PFN_vkCreateDebugReportCallbackEXT>(proc_address);
    auto result = Jade_vkCreateDebugReportCallbackEXT(instance,
        &debug_report_callback_create_info,
        nullptr,
        &out_callback);
    return VK_SUCCESS == result;
}

// out_graphics_queue_family_index: the queue family index belonging to the selected VkPhysicalDevice to use for
// graphics.
//
// out_present_queue_family_index: the queue family index belonging to the selected VkPhysicalDevice to use for present.
//
// out_graphics_queue_family_index and out_present_queue_family_index can be the same.
bool Application::create_device(VkPhysicalDevice &out_physical_device,
    VkDevice &out_vulkan_device,
    uint32_t &out_graphics_queue_family_index,
    uint32_t &out_present_queue_family_index) const
{
    uint32_t physical_device_count{};
    std::vector<VkPhysicalDevice> physical_devices;
    if (VK_SUCCESS != vkEnumeratePhysicalDevices(m_vulkan_instance, &physical_device_count, nullptr))
    {
        return false;
    }

    physical_devices.resize(static_cast<std::size_t>(physical_device_count), nullptr);
    if (VK_SUCCESS != vkEnumeratePhysicalDevices(m_vulkan_instance, &physical_device_count, physical_devices.data()))
    {
        return false;
    }

    std::size_t physical_device_index{};
    for (auto iter = physical_devices.cbegin(); iter != physical_devices.cend(); ++iter)
    {
        auto check_physical_device_result = check_physical_device(*iter,
            m_device_extension_names,
            m_surface,
            out_graphics_queue_family_index,
            out_present_queue_family_index);
        if (check_physical_device_result)
        {
            break;
        }

        ++physical_device_index;
    }

    if (physical_devices.size() == physical_device_index)
    {
        return false;
    }

    if ((std::numeric_limits<uint32_t>::max() == out_graphics_queue_family_index) ||
        (std::numeric_limits<uint32_t>::max() == out_present_queue_family_index))
    {
        return false;
    }

    auto priority = 1.0f;
    std::vector<VkDeviceQueueCreateInfo> device_queue_create_infos;
    VkDeviceQueueCreateInfo device_queue_create_info = {};
    device_queue_create_info.sType = VK_STRUCTURE_TYPE_DEVICE_QUEUE_CREATE_INFO;
    device_queue_create_info.pNext = nullptr;
    device_queue_create_info.flags = 0;
    device_queue_create_info.queueFamilyIndex = out_graphics_queue_family_index;
    device_queue_create_info.queueCount = 1;
    device_queue_create_info.pQueuePriorities = &priority;
    device_queue_create_infos.push_back(device_queue_create_info);
    if (out_graphics_queue_family_index != out_present_queue_family_index)
    {
        device_queue_create_info.queueFamilyIndex = out_present_queue_family_index;
        device_queue_create_infos.push_back(device_queue_create_info);
    }

    VkDeviceCreateInfo device_create_info = {};
    device_create_info.sType = VK_STRUCTURE_TYPE_DEVICE_CREATE_INFO;
    device_create_info.pNext = nullptr;
    device_create_info.flags = 0;
    device_create_info.queueCreateInfoCount = static_cast<uint32_t>(device_queue_create_infos.size());
    device_create_info.pQueueCreateInfos = device_queue_create_infos.data();
    device_create_info.enabledLayerCount = 0;
    device_create_info.ppEnabledLayerNames = nullptr;
    std::vector<const char*> extension_names_char;
    std::transform(m_device_extension_names.cbegin(),
        m_device_extension_names.cend(),
        std::back_inserter(extension_names_char),
        [](const std::string &layer) { return layer.c_str(); });
    device_create_info.enabledExtensionCount = static_cast<uint32_t>(extension_names_char.size());
    device_create_info.ppEnabledExtensionNames = extension_names_char.data();
    device_create_info.pEnabledFeatures = nullptr;
    if (VK_SUCCESS != vkCreateDevice(physical_devices.at(physical_device_index),
        &device_create_info,
        nullptr,
        &out_vulkan_device))
    {
        return false;
    }

    out_physical_device = physical_devices.at(physical_device_index);
    return true;
}

// out_old_swapchain: create_swapchain will destroy out_old_swapchain after the new swapchain is created.
bool Application::create_swapchain(
    uint32_t width,
    uint32_t height,
    VkSwapchainKHR &out_old_swapchain,
    VkFormat &out_swapchain_image_format,
    std::vector<VkImage> &out_swapchain_images,
    std::vector<VkImageView> &out_swapchain_image_views,
    VkSwapchainKHR &out_swapchain) const
{
    uint32_t formats_count{};
    if (VK_SUCCESS != vkGetPhysicalDeviceSurfaceFormatsKHR(m_physical_device, m_surface, &formats_count, nullptr))
    {
        return false;
    }

    std::vector<VkSurfaceFormatKHR> formats(static_cast<std::size_t>(formats_count));
    if (VK_SUCCESS != vkGetPhysicalDeviceSurfaceFormatsKHR(m_physical_device,
        m_surface,
        &formats_count,
        formats.data()))
    {
        return false;
    }

    if (0 == formats_count)
    {
        return false;
    }

    // If surface has no preferred format, use VK_FORMAT_B8G8R8A8_UNORM
    if (1 == formats_count)
    {
        const auto &surface_format = formats.at(0);
        if (VK_FORMAT_UNDEFINED == surface_format.format)
        {
            out_swapchain_image_format = VK_FORMAT_B8G8R8A8_UNORM;
        }
        else
        {
            out_swapchain_image_format = surface_format.format;
        }
    }
    else
    {
        const auto &surface_format = formats.at(0);
        out_swapchain_image_format = surface_format.format;
    }

    VkSurfaceCapabilitiesKHR surface_capabilities = {};
    if (VK_SUCCESS != vkGetPhysicalDeviceSurfaceCapabilitiesKHR(m_physical_device, m_surface, &surface_capabilities))
    {
        return false;
    }

    VkExtent2D extent = {};
    // If width is 0xFFFF_FFFF, so is height.
    if (std::numeric_limits<uint32_t>::max() == surface_capabilities.currentExtent.width)
    {
        extent.width = width;
        extent.height = height;
        extent.width = glm::clamp(extent.width,
            surface_capabilities.minImageExtent.width,
            surface_capabilities.maxImageExtent.width);
        extent.height = glm::clamp(extent.height,
            surface_capabilities.minImageExtent.height,
            surface_capabilities.maxImageExtent.height);
    }
    else
    {
        extent = surface_capabilities.currentExtent;
    }

    auto number_of_images = surface_capabilities.minImageCount;
    VkSurfaceTransformFlagBitsKHR surface_transform{};
    if (surface_capabilities.supportedTransforms & VK_SURFACE_TRANSFORM_IDENTITY_BIT_KHR)
    {
        surface_transform = VK_SURFACE_TRANSFORM_IDENTITY_BIT_KHR;
    }
    else
    {
        surface_transform = surface_capabilities.currentTransform;
    }

    VkSwapchainCreateInfoKHR swap_chain_create_info = {};
    swap_chain_create_info.sType = VK_STRUCTURE_TYPE_SWAPCHAIN_CREATE_INFO_KHR;
    swap_chain_create_info.pNext = nullptr;
    swap_chain_create_info.surface = m_surface;
    swap_chain_create_info.minImageCount = number_of_images;
    swap_chain_create_info.imageFormat = out_swapchain_image_format;
    swap_chain_create_info.imageExtent = extent;
    swap_chain_create_info.preTransform = surface_transform;
    swap_chain_create_info.compositeAlpha = VK_COMPOSITE_ALPHA_OPAQUE_BIT_KHR;
    swap_chain_create_info.imageArrayLayers = 1;
    std::vector<VkPresentModeKHR> present_modes;
    uint32_t present_mode_count{};
    if (VK_SUCCESS != vkGetPhysicalDeviceSurfacePresentModesKHR(m_physical_device,
        m_surface,
        &present_mode_count,
        nullptr))
    {
        return false;
    }

    present_modes.resize(static_cast<std::size_t>(present_mode_count));
    if (VK_SUCCESS != vkGetPhysicalDeviceSurfacePresentModesKHR(m_physical_device,
        m_surface,
        &present_mode_count,
        present_modes.data()))
    {
        return false;
    }

    VkPresentModeKHR present_mode = VK_PRESENT_MODE_FIFO_KHR;

    {
        // VK_PRESENT_MODE_IMMEDIATE_KHR: fastest - tearing
        // VK_PRESENT_MODE_MAILBOX_KHR: next fastest - no tearing
        // VK_PRESENT_MODE_FIFO_KHR is always supported
        auto iter = std::find(present_modes.cbegin(), present_modes.cend(), VK_PRESENT_MODE_MAILBOX_KHR);
        present_mode = (present_modes.cend() == iter) ? VK_PRESENT_MODE_FIFO_KHR : (*iter);
    }
    
    swap_chain_create_info.presentMode = present_mode;
    swap_chain_create_info.oldSwapchain = out_old_swapchain;
    swap_chain_create_info.clipped = VK_TRUE;
    swap_chain_create_info.imageColorSpace = VK_COLORSPACE_SRGB_NONLINEAR_KHR;
    swap_chain_create_info.imageUsage = VK_IMAGE_USAGE_COLOR_ATTACHMENT_BIT|VK_IMAGE_USAGE_TRANSFER_SRC_BIT;
    swap_chain_create_info.imageSharingMode = VK_SHARING_MODE_EXCLUSIVE;
    swap_chain_create_info.queueFamilyIndexCount = 0;
    swap_chain_create_info.pQueueFamilyIndices = nullptr;
    std::vector<uint32_t> queue_family_indices = { m_graphics_queue_family_index, m_present_queue_family_index };
    if (m_graphics_queue_family_index != m_present_queue_family_index)
    {
        swap_chain_create_info.imageSharingMode = VK_SHARING_MODE_CONCURRENT;
        swap_chain_create_info.queueFamilyIndexCount = static_cast<uint32_t>(queue_family_indices.size());
        swap_chain_create_info.pQueueFamilyIndices = queue_family_indices.data();
    }

    if (VK_SUCCESS != vkCreateSwapchainKHR(m_vulkan_device, &swap_chain_create_info, nullptr, &out_swapchain))
    {
        return false;
    }

    if (nullptr != out_old_swapchain)
    {
        vkDestroySwapchainKHR(m_vulkan_device, out_old_swapchain, nullptr);
        out_old_swapchain = nullptr;
    }

    uint32_t swap_chain_image_count{};
    if (VK_SUCCESS != vkGetSwapchainImagesKHR(m_vulkan_device, out_swapchain, &swap_chain_image_count, nullptr))
    {
        return false;
    }

    out_swapchain_images.resize(static_cast<std::size_t>(swap_chain_image_count));
    if (VK_SUCCESS != vkGetSwapchainImagesKHR(m_vulkan_device,
        out_swapchain,
        &swap_chain_image_count,
        out_swapchain_images.data()))
    {
        return false;
    }

    out_swapchain_image_views.clear();
    for (uint32_t i = 0; i != swap_chain_image_count; ++i)
    {
        VkImageViewCreateInfo image_view_create_info = {};
        image_view_create_info.sType = VK_STRUCTURE_TYPE_IMAGE_VIEW_CREATE_INFO;
        image_view_create_info.pNext = nullptr;
        image_view_create_info.flags = 0;
        image_view_create_info.image = out_swapchain_images.at(static_cast<std::size_t>(i));
        image_view_create_info.viewType = VK_IMAGE_VIEW_TYPE_2D;
        image_view_create_info.format = out_swapchain_image_format;
        image_view_create_info.components.r = VK_COMPONENT_SWIZZLE_R;
        image_view_create_info.components.g = VK_COMPONENT_SWIZZLE_G;
        image_view_create_info.components.b = VK_COMPONENT_SWIZZLE_B;
        image_view_create_info.components.a = VK_COMPONENT_SWIZZLE_A;
        image_view_create_info.subresourceRange.aspectMask = VK_IMAGE_ASPECT_COLOR_BIT;
        image_view_create_info.subresourceRange.baseMipLevel = 0;
        image_view_create_info.subresourceRange.levelCount = 1;
        image_view_create_info.subresourceRange.baseArrayLayer = 0;
        image_view_create_info.subresourceRange.layerCount = 1;
        VkImageView image_view{nullptr};
        if (VK_SUCCESS != vkCreateImageView(m_vulkan_device, &image_view_create_info, nullptr, &image_view))
        {
            return false;
        }

        out_swapchain_image_views.push_back(image_view);
    }

    return true;
}

bool Application::create_render_pass(VkRenderPass &out_render_pass) const
{
    std::vector<VkAttachmentDescription> vec_attachment_description;
    
    {
        VkAttachmentDescription attachment_description = {};
        attachment_description.flags = 0;
        attachment_description.format = m_swapchain_image_format;
        attachment_description.samples = VK_SAMPLE_COUNT_1_BIT;
        attachment_description.loadOp = VK_ATTACHMENT_LOAD_OP_CLEAR;
        attachment_description.storeOp = VK_ATTACHMENT_STORE_OP_STORE;
        attachment_description.stencilLoadOp = VK_ATTACHMENT_LOAD_OP_DONT_CARE;
        attachment_description.stencilStoreOp = VK_ATTACHMENT_STORE_OP_DONT_CARE;
        attachment_description.initialLayout = VK_IMAGE_LAYOUT_UNDEFINED;
        attachment_description.finalLayout = VK_IMAGE_LAYOUT_PRESENT_SRC_KHR;
        vec_attachment_description.push_back(attachment_description);
    }

    {
        VkAttachmentDescription attachment_description = {};
        attachment_description.flags = 0;
        attachment_description.format = m_depth_stencil_image_format;
        attachment_description.samples = VK_SAMPLE_COUNT_1_BIT;
        attachment_description.loadOp = VK_ATTACHMENT_LOAD_OP_CLEAR;
        attachment_description.storeOp = VK_ATTACHMENT_STORE_OP_STORE;
        // This should be OK since the application does not do anything with the stencil buffer.
        attachment_description.stencilLoadOp = VK_ATTACHMENT_LOAD_OP_DONT_CARE;
        attachment_description.stencilStoreOp = VK_ATTACHMENT_STORE_OP_DONT_CARE;
        attachment_description.initialLayout = VK_IMAGE_LAYOUT_UNDEFINED;
        attachment_description.finalLayout = VK_IMAGE_LAYOUT_PRESENT_SRC_KHR;
        vec_attachment_description.push_back(attachment_description);
    }
    
    VkAttachmentReference attachment_reference = {};
    // attachment is the index of the element in VkRenderPassCreateInfo.pAttachments
    attachment_reference.attachment = 0;
    // layout is the layout used by the attachment during the subpass.
    attachment_reference.layout = VK_IMAGE_LAYOUT_COLOR_ATTACHMENT_OPTIMAL;
    VkAttachmentReference depth_stencil_attachment_reference = {};
    depth_stencil_attachment_reference.attachment = 1;
    depth_stencil_attachment_reference.layout = VK_IMAGE_LAYOUT_DEPTH_STENCIL_ATTACHMENT_OPTIMAL;
    VkSubpassDescription subpass_description = {};
    subpass_description.flags = 0;
    subpass_description.pipelineBindPoint = VK_PIPELINE_BIND_POINT_GRAPHICS;
    subpass_description.inputAttachmentCount = 0;
    subpass_description.pInputAttachments = nullptr;
    subpass_description.colorAttachmentCount = 1;
    subpass_description.pColorAttachments = &attachment_reference;
    subpass_description.pResolveAttachments = nullptr;
    subpass_description.pDepthStencilAttachment = &depth_stencil_attachment_reference;
    subpass_description.preserveAttachmentCount = 0;
    subpass_description.pPreserveAttachments = nullptr;
    std::vector<VkSubpassDependency> vec_subpass_dependency;

    {
        VkSubpassDependency subpass_dependency = {};
        subpass_dependency.srcSubpass = VK_SUBPASS_EXTERNAL;
        subpass_dependency.dstSubpass = 0;
        subpass_dependency.srcStageMask = VK_PIPELINE_STAGE_BOTTOM_OF_PIPE_BIT;
        subpass_dependency.dstStageMask = VK_PIPELINE_STAGE_COLOR_ATTACHMENT_OUTPUT_BIT;
        subpass_dependency.srcAccessMask = VK_ACCESS_MEMORY_READ_BIT;
        subpass_dependency.dstAccessMask = VK_ACCESS_COLOR_ATTACHMENT_WRITE_BIT;
        subpass_dependency.dependencyFlags = VK_DEPENDENCY_BY_REGION_BIT;
        vec_subpass_dependency.push_back(subpass_dependency);
    }

    {
        VkSubpassDependency subpass_dependency = {};
        subpass_dependency.srcSubpass = 0;
        subpass_dependency.dstSubpass = VK_SUBPASS_EXTERNAL;
        subpass_dependency.srcStageMask = VK_PIPELINE_STAGE_COLOR_ATTACHMENT_OUTPUT_BIT;
        subpass_dependency.dstStageMask = VK_PIPELINE_STAGE_BOTTOM_OF_PIPE_BIT;
        subpass_dependency.srcAccessMask = VK_ACCESS_COLOR_ATTACHMENT_WRITE_BIT;
        subpass_dependency.dstAccessMask = VK_ACCESS_MEMORY_READ_BIT;
        subpass_dependency.dependencyFlags = VK_DEPENDENCY_BY_REGION_BIT;
        vec_subpass_dependency.push_back(subpass_dependency);
    }

    VkRenderPassCreateInfo render_pass_create_info = {};
    render_pass_create_info.sType = VK_STRUCTURE_TYPE_RENDER_PASS_CREATE_INFO;
    render_pass_create_info.pNext = nullptr;
    render_pass_create_info.flags = 0;
    render_pass_create_info.attachmentCount = static_cast<uint32_t>(vec_attachment_description.size());
    render_pass_create_info.pAttachments = vec_attachment_description.data();
    render_pass_create_info.subpassCount = 1;
    render_pass_create_info.pSubpasses = &subpass_description;
    render_pass_create_info.dependencyCount = static_cast<uint32_t>(vec_subpass_dependency.size());
    render_pass_create_info.pDependencies = vec_subpass_dependency.data();
    return VK_SUCCESS == vkCreateRenderPass(m_vulkan_device, &render_pass_create_info, nullptr, &out_render_pass);
}

bool Application::create_framebuffer(VkDevice device,
    VkRenderPass render_pass,
    uint32_t width,
    uint32_t height,
    VkImageView image_view,
    VkImageView depth_stencil_view,
    VkFramebuffer &out_framebuffer) const
{
    VkFramebufferCreateInfo framebuffer_create_info = {};
    framebuffer_create_info.sType = VK_STRUCTURE_TYPE_FRAMEBUFFER_CREATE_INFO;
    framebuffer_create_info.pNext = nullptr;
    framebuffer_create_info.flags = 0;
    framebuffer_create_info.renderPass = render_pass;
    std::vector<VkImageView> vec_image_view = { image_view, depth_stencil_view };
    framebuffer_create_info.attachmentCount = static_cast<uint32_t>(vec_image_view.size());
    framebuffer_create_info.pAttachments = vec_image_view.data();
    framebuffer_create_info.width = width;
    framebuffer_create_info.height = height;
    framebuffer_create_info.layers = 1;
    return VK_SUCCESS == vkCreateFramebuffer(device, &framebuffer_create_info, nullptr, &out_framebuffer);
}

// shader_file_names: SPIR-V files
// out_shader_modules: the caller must destroy these using vkDestroyShaderModule
bool Application::create_shader_modules(const std::vector<std::string> &shader_file_names,
    std::vector<VkShaderModule> &out_shader_modules) const
{
    out_shader_modules.clear();
    for (auto iter = shader_file_names.cbegin(); iter != shader_file_names.cend(); ++iter)
    {
        std::vector<uint32_t> spirv;
        if (!File_utility::load_spirv(*iter, spirv))
        {
            return false;
        }

        VkShaderModuleCreateInfo shader_module_create_info = {};
        shader_module_create_info.sType = VK_STRUCTURE_TYPE_SHADER_MODULE_CREATE_INFO;
        shader_module_create_info.pNext = nullptr;
        shader_module_create_info.flags = 0;
        shader_module_create_info.codeSize = spirv.size() * sizeof(uint32_t);
        shader_module_create_info.pCode = spirv.data();
        VkShaderModule shader_module = nullptr;
        auto vk_result = vkCreateShaderModule(m_vulkan_device, &shader_module_create_info, nullptr, &shader_module);
        if (VK_SUCCESS != vk_result)
        {
            return false;
        }

        out_shader_modules.push_back(shader_module);
    }

    return true;
}

bool Application::create_pipeline(VkPipelineLayout &out_pipeline_layout, VkPipeline &out_pipeline) const
{
    std::vector<VkPipelineShaderStageCreateInfo> vec_pipeline_shader_stage_create_info;
    // Assumes that element i in m_shader_modules corresponds to element i in shader_stages
    std::vector<VkShaderStageFlagBits> shader_stages = {VK_SHADER_STAGE_VERTEX_BIT, VK_SHADER_STAGE_FRAGMENT_BIT};
    for (std::size_t i = 0; i != m_shader_modules.size(); ++i)
    {
        VkPipelineShaderStageCreateInfo pipeline_shader_stage_create_info = {};
        pipeline_shader_stage_create_info.sType = VK_STRUCTURE_TYPE_PIPELINE_SHADER_STAGE_CREATE_INFO;
        pipeline_shader_stage_create_info.pNext = nullptr;
        pipeline_shader_stage_create_info.pSpecializationInfo = nullptr;
        pipeline_shader_stage_create_info.flags = 0;
        pipeline_shader_stage_create_info.stage = shader_stages.at(i);
        pipeline_shader_stage_create_info.pName = "main";
        pipeline_shader_stage_create_info.module = m_shader_modules.at(i);
        vec_pipeline_shader_stage_create_info.push_back(pipeline_shader_stage_create_info);
    }

    VkVertexInputBindingDescription vertex_input_binding_description = {};
    vertex_input_binding_description.binding = 0;
    vertex_input_binding_description.stride = sizeof(Jade::Vertex);
    vertex_input_binding_description.inputRate = VK_VERTEX_INPUT_RATE_VERTEX;
    std::vector<VkVertexInputAttributeDescription> vec_vertex_input_attribute_description;

    {
        VkVertexInputAttributeDescription vertex_input_attribute_description;
        vertex_input_attribute_description.location = 0;
        vertex_input_attribute_description.binding = vertex_input_binding_description.binding;
        vertex_input_attribute_description.format = VK_FORMAT_R32G32B32_SFLOAT;
        vertex_input_attribute_description.offset = 0;
        vec_vertex_input_attribute_description.push_back(vertex_input_attribute_description);
    }

    {
        VkVertexInputAttributeDescription vertex_input_attribute_description;
        vertex_input_attribute_description.location = 1;
        vertex_input_attribute_description.binding = vertex_input_binding_description.binding;
        vertex_input_attribute_description.format = VK_FORMAT_R32G32B32_SFLOAT;
        vertex_input_attribute_description.offset = sizeof(glm::vec3);
        vec_vertex_input_attribute_description.push_back(vertex_input_attribute_description);
    }

    VkPipelineVertexInputStateCreateInfo pipeline_vertex_input_state_create_info = {};
    pipeline_vertex_input_state_create_info.sType = VK_STRUCTURE_TYPE_PIPELINE_VERTEX_INPUT_STATE_CREATE_INFO;
    pipeline_vertex_input_state_create_info.pNext = nullptr;
    pipeline_vertex_input_state_create_info.flags = 0;
    pipeline_vertex_input_state_create_info.vertexBindingDescriptionCount = 1;
    pipeline_vertex_input_state_create_info.pVertexBindingDescriptions = &vertex_input_binding_description;
    pipeline_vertex_input_state_create_info.vertexAttributeDescriptionCount = static_cast<uint32_t>(vec_vertex_input_attribute_description.size());
    pipeline_vertex_input_state_create_info.pVertexAttributeDescriptions = vec_vertex_input_attribute_description.data();
    VkPipelineInputAssemblyStateCreateInfo pipeline_input_assembly_state_create_info = {};
    pipeline_input_assembly_state_create_info.sType = VK_STRUCTURE_TYPE_PIPELINE_INPUT_ASSEMBLY_STATE_CREATE_INFO;
    pipeline_input_assembly_state_create_info.pNext = nullptr;
    pipeline_input_assembly_state_create_info.flags = 0;
    pipeline_input_assembly_state_create_info.topology = VK_PRIMITIVE_TOPOLOGY_TRIANGLE_LIST;
    pipeline_input_assembly_state_create_info.primitiveRestartEnable = VK_FALSE;
    VkPipelineViewportStateCreateInfo pipeline_viewport_state_create_info = {};
    pipeline_viewport_state_create_info.sType = VK_STRUCTURE_TYPE_PIPELINE_VIEWPORT_STATE_CREATE_INFO;
    pipeline_viewport_state_create_info.pNext = nullptr;
    pipeline_viewport_state_create_info.viewportCount = 1;
    pipeline_viewport_state_create_info.pViewports = nullptr;
    pipeline_viewport_state_create_info.scissorCount = 1;
    pipeline_viewport_state_create_info.pScissors = nullptr;
    VkPipelineRasterizationStateCreateInfo pipeline_rasterization_state_create_info = {};
    pipeline_rasterization_state_create_info.sType = VK_STRUCTURE_TYPE_PIPELINE_RASTERIZATION_STATE_CREATE_INFO;
    pipeline_rasterization_state_create_info.pNext = nullptr;
    pipeline_rasterization_state_create_info.flags = 0;
    pipeline_rasterization_state_create_info.depthClampEnable = VK_FALSE;
    pipeline_rasterization_state_create_info.rasterizerDiscardEnable = VK_FALSE;
    pipeline_rasterization_state_create_info.polygonMode = VK_POLYGON_MODE_FILL;
    pipeline_rasterization_state_create_info.cullMode = VK_CULL_MODE_BACK_BIT;
    pipeline_rasterization_state_create_info.frontFace = VK_FRONT_FACE_CLOCKWISE;
    pipeline_rasterization_state_create_info.depthBiasEnable = VK_FALSE;
    pipeline_rasterization_state_create_info.depthBiasConstantFactor = 0.0f;
    pipeline_rasterization_state_create_info.depthBiasClamp = 0.0f;
    pipeline_rasterization_state_create_info.depthBiasSlopeFactor = 0.0f;
    pipeline_rasterization_state_create_info.lineWidth = 1.0f;
    VkPipelineMultisampleStateCreateInfo pipeline_multisample_state_create_info = {};
    pipeline_multisample_state_create_info.sType = VK_STRUCTURE_TYPE_PIPELINE_MULTISAMPLE_STATE_CREATE_INFO;
    pipeline_multisample_state_create_info.pNext = nullptr;
    pipeline_multisample_state_create_info.flags = 0;
    pipeline_multisample_state_create_info.rasterizationSamples = VK_SAMPLE_COUNT_1_BIT;
    pipeline_multisample_state_create_info.sampleShadingEnable = VK_FALSE;
    pipeline_multisample_state_create_info.minSampleShading = 1.0f;
    pipeline_multisample_state_create_info.pSampleMask = nullptr;
    pipeline_multisample_state_create_info.alphaToCoverageEnable = VK_FALSE;
    pipeline_multisample_state_create_info.alphaToOneEnable = VK_FALSE;
    VkPipelineColorBlendAttachmentState pipeline_color_blend_attachment_state = {};
    pipeline_color_blend_attachment_state.blendEnable = VK_FALSE;
    pipeline_color_blend_attachment_state.srcColorBlendFactor = VK_BLEND_FACTOR_ONE;
    pipeline_color_blend_attachment_state.dstColorBlendFactor = VK_BLEND_FACTOR_ZERO;
    pipeline_color_blend_attachment_state.colorBlendOp = VK_BLEND_OP_ADD;
    pipeline_color_blend_attachment_state.srcAlphaBlendFactor = VK_BLEND_FACTOR_ONE;
    pipeline_color_blend_attachment_state.dstAlphaBlendFactor = VK_BLEND_FACTOR_ZERO;
    pipeline_color_blend_attachment_state.alphaBlendOp = VK_BLEND_OP_ADD;
    pipeline_color_blend_attachment_state.colorWriteMask = VK_COLOR_COMPONENT_R_BIT |
        VK_COLOR_COMPONENT_G_BIT |
        VK_COLOR_COMPONENT_B_BIT |
        VK_COLOR_COMPONENT_A_BIT;
    VkPipelineColorBlendStateCreateInfo pipeline_color_blend_state_create_info = {};
    pipeline_color_blend_state_create_info.sType = VK_STRUCTURE_TYPE_PIPELINE_COLOR_BLEND_STATE_CREATE_INFO;
    pipeline_color_blend_state_create_info.pNext = nullptr;
    pipeline_color_blend_state_create_info.flags = 0;
    pipeline_color_blend_state_create_info.logicOpEnable = VK_FALSE;
    pipeline_color_blend_state_create_info.logicOp = VK_LOGIC_OP_CLEAR;
    pipeline_color_blend_state_create_info.attachmentCount = 1;
    pipeline_color_blend_state_create_info.pAttachments = &pipeline_color_blend_attachment_state;
    std::fill(pipeline_color_blend_state_create_info.blendConstants,
        pipeline_color_blend_state_create_info.blendConstants + 4,
        0.0f);
    std::vector<VkDynamicState> vec_dynamic_state = {VK_DYNAMIC_STATE_VIEWPORT, VK_DYNAMIC_STATE_SCISSOR};
    VkPipelineDynamicStateCreateInfo pipeline_dynamic_state_create_info = {};
    pipeline_dynamic_state_create_info.sType = VK_STRUCTURE_TYPE_PIPELINE_DYNAMIC_STATE_CREATE_INFO;
    pipeline_dynamic_state_create_info.pNext = nullptr;
    pipeline_dynamic_state_create_info.flags = 0;
    pipeline_dynamic_state_create_info.dynamicStateCount = static_cast<uint32_t>(vec_dynamic_state.size());
    pipeline_dynamic_state_create_info.pDynamicStates = vec_dynamic_state.data();
    VkPushConstantRange push_constant_range = {};
    push_constant_range.stageFlags = VK_SHADER_STAGE_VERTEX_BIT;
    push_constant_range.offset = 0;
    push_constant_range.size = sizeof(Jade::Push_constants);
    VkPipelineLayoutCreateInfo pipeline_layout_create_info = {};
    pipeline_layout_create_info.sType = VK_STRUCTURE_TYPE_PIPELINE_LAYOUT_CREATE_INFO;
    pipeline_layout_create_info.pNext = nullptr;
    pipeline_layout_create_info.flags = 0;
    pipeline_layout_create_info.setLayoutCount = 1;
    pipeline_layout_create_info.pSetLayouts = &m_descriptor_set_layout;
    pipeline_layout_create_info.pushConstantRangeCount = 1;
    pipeline_layout_create_info.pPushConstantRanges = &push_constant_range;
    auto vk_result = vkCreatePipelineLayout(m_vulkan_device,
        &pipeline_layout_create_info,
        nullptr,
        &out_pipeline_layout);
    if (VK_SUCCESS != vk_result)
    {
        return false;
    }

    VkPipelineDepthStencilStateCreateInfo pipeline_depth_stencil_state_create_info = {};
    pipeline_depth_stencil_state_create_info.sType = VK_STRUCTURE_TYPE_PIPELINE_DEPTH_STENCIL_STATE_CREATE_INFO;
    pipeline_depth_stencil_state_create_info.pNext = nullptr;
    pipeline_depth_stencil_state_create_info.flags = 0;
    pipeline_depth_stencil_state_create_info.depthTestEnable = VK_TRUE;
    pipeline_depth_stencil_state_create_info.depthWriteEnable = VK_TRUE;
    pipeline_depth_stencil_state_create_info.depthCompareOp = VK_COMPARE_OP_LESS_OR_EQUAL;
    pipeline_depth_stencil_state_create_info.depthBoundsTestEnable = VK_FALSE;
    pipeline_depth_stencil_state_create_info.stencilTestEnable = VK_FALSE;
    VkStencilOpState stencil_op_state = {};
    stencil_op_state.failOp = VK_STENCIL_OP_KEEP;
    stencil_op_state.passOp = VK_STENCIL_OP_KEEP;
    stencil_op_state.compareOp = VK_COMPARE_OP_ALWAYS;
    stencil_op_state.compareMask = 0;
    stencil_op_state.reference = 0;
    stencil_op_state.depthFailOp = VK_STENCIL_OP_KEEP;
    stencil_op_state.writeMask = 0;
    pipeline_depth_stencil_state_create_info.back = stencil_op_state;
    pipeline_depth_stencil_state_create_info.front = stencil_op_state;
    VkGraphicsPipelineCreateInfo graphics_pipeline_create_info = {};
    graphics_pipeline_create_info.sType = VK_STRUCTURE_TYPE_GRAPHICS_PIPELINE_CREATE_INFO;
    graphics_pipeline_create_info.pNext = nullptr;
    graphics_pipeline_create_info.flags = 0;
    graphics_pipeline_create_info.stageCount = static_cast<uint32_t>(vec_pipeline_shader_stage_create_info.size());
    graphics_pipeline_create_info.pStages = vec_pipeline_shader_stage_create_info.data();
    graphics_pipeline_create_info.pVertexInputState = &pipeline_vertex_input_state_create_info;
    graphics_pipeline_create_info.pInputAssemblyState = &pipeline_input_assembly_state_create_info;
    graphics_pipeline_create_info.pTessellationState = nullptr;
    graphics_pipeline_create_info.pViewportState = &pipeline_viewport_state_create_info;
    graphics_pipeline_create_info.pRasterizationState = &pipeline_rasterization_state_create_info;
    graphics_pipeline_create_info.pMultisampleState = &pipeline_multisample_state_create_info;
    graphics_pipeline_create_info.pDepthStencilState = &pipeline_depth_stencil_state_create_info;
    graphics_pipeline_create_info.pColorBlendState = &pipeline_color_blend_state_create_info;
    graphics_pipeline_create_info.pDynamicState = &pipeline_dynamic_state_create_info;
    graphics_pipeline_create_info.layout = out_pipeline_layout;
    graphics_pipeline_create_info.renderPass = m_render_pass;
    graphics_pipeline_create_info.subpass = 0;
    graphics_pipeline_create_info.basePipelineHandle = nullptr;
    graphics_pipeline_create_info.basePipelineIndex = -1;
    vk_result = vkCreateGraphicsPipelines(m_vulkan_device,
        nullptr,
        1,
        &graphics_pipeline_create_info,
        nullptr,
        &out_pipeline);
    if (VK_SUCCESS != vk_result)
    {
        return false;
    }

    return true;
}

// Semaphores are used for synchronization. A semaphore has two states:
// - signaled
// - unsignaled
// A queue can wait on a particular semaphore to be signaled before proceeding to process command buffers submitted to
// it.
bool Application::create_semaphores(uint32_t create_count, std::vector<VkSemaphore> &out_semaphores) const
{
    assert(out_semaphores.empty());
    for (uint32_t i = 0; i != create_count; ++i)
    {
        VkSemaphoreCreateInfo create_info = {};
        create_info.sType = VK_STRUCTURE_TYPE_SEMAPHORE_CREATE_INFO;
        create_info.pNext = nullptr;
        create_info.flags = 0;
        VkSemaphore semaphore;
        auto result = vkCreateSemaphore(m_vulkan_device, &create_info, nullptr, &semaphore);
        if (VK_SUCCESS != result)
        {
            return false;
        }

        out_semaphores.push_back(semaphore);
    }

    return true;
}

bool Application::create_fences(uint32_t create_count, std::vector<VkFence> &out_vec_fence) const
{
    assert(out_vec_fence.empty());
    for (uint32_t i = 0; i != create_count; ++i)
    {
        VkFenceCreateInfo fence_create_info = {};
        fence_create_info.sType = VK_STRUCTURE_TYPE_FENCE_CREATE_INFO;
        fence_create_info.pNext = nullptr;
        fence_create_info.flags = VK_FENCE_CREATE_SIGNALED_BIT;
        VkFence fence{nullptr};
        auto result = vkCreateFence(m_vulkan_device, &fence_create_info, nullptr, &fence);
        if (VK_SUCCESS != result)
        {
            return false;
        }

        out_vec_fence.push_back(fence);
    }

    return true;
}

bool Application::create_command_pool(VkCommandPool &out_command_pool) const
{
    VkCommandPoolCreateInfo create_info = {};
    create_info.sType = VK_STRUCTURE_TYPE_COMMAND_POOL_CREATE_INFO;
    create_info.pNext = nullptr;
    create_info.flags = VK_COMMAND_POOL_CREATE_RESET_COMMAND_BUFFER_BIT|VK_COMMAND_POOL_CREATE_TRANSIENT_BIT;
    create_info.queueFamilyIndex = m_graphics_queue_family_index;
    auto result = vkCreateCommandPool(m_vulkan_device, &create_info, nullptr, &out_command_pool);
    return VK_SUCCESS == result;
}

bool Application::allocate_command_buffers(uint32_t count, std::vector<VkCommandBuffer> &out_command_buffers) const
{
    out_command_buffers.resize(static_cast<std::size_t>(count));
    VkCommandBufferAllocateInfo command_buffer_allocate_info = {};
    command_buffer_allocate_info.sType = VK_STRUCTURE_TYPE_COMMAND_BUFFER_ALLOCATE_INFO;
    command_buffer_allocate_info.pNext = nullptr;
    command_buffer_allocate_info.commandPool = m_command_pool;
    command_buffer_allocate_info.level = VK_COMMAND_BUFFER_LEVEL_PRIMARY;
    command_buffer_allocate_info.commandBufferCount = count;
    auto result = vkAllocateCommandBuffers(m_vulkan_device, &command_buffer_allocate_info, out_command_buffers.data());
    return VK_SUCCESS == result;
}

bool Application::record_command_buffer(uint32_t framebuffer_width,
    uint32_t framebuffer_height,
    VkFramebuffer framebuffer,
    VkImage swapchain_image,
    VkCommandBuffer command_buffer)
{
    VkCommandBufferBeginInfo command_buffer_begin_info = {};
    command_buffer_begin_info.sType = VK_STRUCTURE_TYPE_COMMAND_BUFFER_BEGIN_INFO;
    command_buffer_begin_info.pNext = nullptr;
    command_buffer_begin_info.flags = VK_COMMAND_BUFFER_USAGE_ONE_TIME_SUBMIT_BIT;
    command_buffer_begin_info.pNext = nullptr;
    auto vk_result = vkBeginCommandBuffer(command_buffer, &command_buffer_begin_info);
    if (VK_SUCCESS != vk_result)
    {
        return false;
    }

    if (m_graphics_queue_family_index != m_present_queue_family_index)
    {
        // Barrier from present to draw
        VkImageMemoryBarrier image_memory_barrier = {};
        image_memory_barrier.sType = VK_STRUCTURE_TYPE_IMAGE_MEMORY_BARRIER;
        image_memory_barrier.pNext = nullptr;
        // srcAccessMask: the first operation protected by the barrier
        // dstAccessMask: the second operation protected by the barrier
        image_memory_barrier.srcAccessMask = VK_ACCESS_MEMORY_READ_BIT;
        image_memory_barrier.dstAccessMask = VK_ACCESS_COLOR_ATTACHMENT_WRITE_BIT;
        image_memory_barrier.oldLayout = VK_IMAGE_LAYOUT_UNDEFINED;
        image_memory_barrier.newLayout = VK_IMAGE_LAYOUT_PRESENT_SRC_KHR;
        image_memory_barrier.srcQueueFamilyIndex = m_present_queue_family_index;
        image_memory_barrier.dstQueueFamilyIndex = m_graphics_queue_family_index;
        image_memory_barrier.image = swapchain_image;
        VkImageSubresourceRange image_subresource_range = {};
        image_subresource_range.aspectMask = VK_IMAGE_ASPECT_COLOR_BIT;
        image_subresource_range.baseMipLevel = 0;
        image_subresource_range.levelCount = 1;
        image_subresource_range.baseArrayLayer = 0;
        image_subresource_range.layerCount = 1;
        image_memory_barrier.subresourceRange = image_subresource_range;
        vkCmdPipelineBarrier(command_buffer,
            VK_PIPELINE_STAGE_COLOR_ATTACHMENT_OUTPUT_BIT, // which stage wrote to the resource last
            VK_PIPELINE_STAGE_COLOR_ATTACHMENT_OUTPUT_BIT, // which stage will read from the resource next
            0,
            0,
            nullptr,
            0,
            nullptr,
            1,
            &image_memory_barrier);
    }

    VkRenderPassBeginInfo render_pass_begin_info = {};
    render_pass_begin_info.sType = VK_STRUCTURE_TYPE_RENDER_PASS_BEGIN_INFO;
    render_pass_begin_info.pNext = nullptr;
    render_pass_begin_info.renderPass = m_render_pass;
    render_pass_begin_info.framebuffer = framebuffer;
    VkRect2D render_area = {};
    render_area.offset.x = 0;
    render_area.offset.y = 0;
    render_area.extent.width = framebuffer_width;
    render_area.extent.height = framebuffer_height;
    render_pass_begin_info.renderArea = render_area;
    std::vector<VkClearValue> vec_clear_value;
    
    {
        VkClearValue clear_value = {};
        // RGBA
        clear_value.color.float32[0] = 0.4f; //-V525
        clear_value.color.float32[1] = 0.4f;
        clear_value.color.float32[2] = 0.4f;
        clear_value.color.float32[3] = 1.0f;
        vec_clear_value.push_back(clear_value);
    }

    {
        VkClearValue clear_value = {};
        clear_value.depthStencil.depth = 1.0f;
        clear_value.depthStencil.stencil = 0;
        vec_clear_value.push_back(clear_value);
    }
    
    render_pass_begin_info.clearValueCount = static_cast<uint32_t>(vec_clear_value.size());
    render_pass_begin_info.pClearValues = vec_clear_value.data();
    vkCmdBeginRenderPass(command_buffer, &render_pass_begin_info, VK_SUBPASS_CONTENTS_INLINE);
    vkCmdBindPipeline(command_buffer, VK_PIPELINE_BIND_POINT_GRAPHICS, m_pipeline);
    VkViewport viewport = {};
    viewport.x = 0.0f;
    viewport.y = 0.0f;
    viewport.width = static_cast<float>(framebuffer_width);
    viewport.height = static_cast<float>(framebuffer_height);
    viewport.minDepth = 0.0f;
    viewport.maxDepth = 1.0f;
    vkCmdSetViewport(command_buffer, 0, 1, &viewport);
    VkRect2D scissor = {};
    scissor.offset.x = 0;
    scissor.offset.y = 0;
    scissor.extent.width = framebuffer_width;
    scissor.extent.height = framebuffer_height;
    vkCmdSetScissor(command_buffer, 0, 1, &scissor);
    vkCmdBindDescriptorSets(command_buffer,
        VK_PIPELINE_BIND_POINT_GRAPHICS,
        m_pipeline_layout,
        0,
        1,
        &m_descriptor_set,
        0,
        nullptr);
    Camera *camera = m_player.get_camera();
    auto model_view_projection_matrix = m_invert_y_matrix * camera->get_projection_matrix() * camera->get_view_matrix();
    m_scene.draw(command_buffer, m_pipeline_layout, model_view_projection_matrix);
    vkCmdEndRenderPass(command_buffer);
    if (m_graphics_queue_family_index != m_present_queue_family_index)
    {
        // Barrier from draw to present
        VkImageMemoryBarrier image_memory_barrier = {};
        image_memory_barrier.sType = VK_STRUCTURE_TYPE_IMAGE_MEMORY_BARRIER;
        image_memory_barrier.pNext = nullptr;
        // srcAccessMask: the first operation protected by the barrier
        // dstAccessMask: the second operation protected by the barrier
        image_memory_barrier.srcAccessMask = VK_ACCESS_COLOR_ATTACHMENT_WRITE_BIT;
        image_memory_barrier.dstAccessMask = VK_ACCESS_MEMORY_READ_BIT;
        image_memory_barrier.oldLayout = VK_IMAGE_LAYOUT_PRESENT_SRC_KHR;
        image_memory_barrier.newLayout = VK_IMAGE_LAYOUT_PRESENT_SRC_KHR;
        image_memory_barrier.srcQueueFamilyIndex = m_graphics_queue_family_index;
        image_memory_barrier.dstQueueFamilyIndex = m_present_queue_family_index;
        image_memory_barrier.image = swapchain_image;
        VkImageSubresourceRange image_subresource_range = {};
        image_subresource_range.aspectMask = VK_IMAGE_ASPECT_COLOR_BIT;
        image_subresource_range.baseMipLevel = 0;
        image_subresource_range.levelCount = 1;
        image_subresource_range.baseArrayLayer = 0;
        image_subresource_range.layerCount = 1;
        image_memory_barrier.subresourceRange = image_subresource_range;
        vkCmdPipelineBarrier(command_buffer,
            VK_PIPELINE_STAGE_COLOR_ATTACHMENT_OUTPUT_BIT, // which stage wrote to the resource last
            VK_PIPELINE_STAGE_BOTTOM_OF_PIPE_BIT, // which stage will read from the resource next
            0,
            0,
            nullptr,
            0,
            nullptr,
            1,
            &image_memory_barrier);
    }

    vk_result = vkEndCommandBuffer(command_buffer);
    if (VK_SUCCESS != vk_result)
    {
        return false;
    }

    return true;
}

bool Application::draw(uint32_t framebuffer_width, uint32_t framebuffer_height, Jade::Frame &out_frame)
{
    // Using three groups of rendering resources permits the application to continue recording the command buffer for
    // the next frame whilst waiting for the fence associated with the command buffer submitted in the previous frame
    // to be signalled (meaning that the command buffer can be used again). Otherwise, after submitting a command
    // buffer, the application cannot do any further work until Vulkan notifies it that the command buffer can be
    // recorded to again.
    
    // Wait until out_frame.m_command_buffer can be recorded to again
    auto vk_result = vkWaitForFences(m_vulkan_device,
        1,
        &out_frame.m_fence, // out_frame.m_fence is signalled when out_frame.m_command_buffer can be reused
        VK_TRUE,
        std::numeric_limits<uint64_t>::max());
    switch (vk_result)
    {
    case VK_SUCCESS:
        break;
    
    case VK_TIMEOUT:
        // This should never happen since the timeout argument to vkWaitForFences is UINT64_MAX
        return false;
        break;
    
    default:
        // Failure
        return false;
        break;
    }
    
    vk_result = vkResetFences(m_vulkan_device, 1, &out_frame.m_fence);
    if (VK_SUCCESS != vk_result)
    {
        return false;
    }
    
    uint32_t image_index{};
    vk_result = vkAcquireNextImageKHR(m_vulkan_device,
        m_swapchain,
        std::numeric_limits<uint64_t>::max(),
        out_frame.m_image_available_semaphore, // will be signalled when the application can use the image
        nullptr,
        &image_index);
    switch (vk_result)
    {
    case VK_SUCCESS:
        break;
    
    case VK_SUBOPTIMAL_KHR:
    case VK_ERROR_OUT_OF_DATE_KHR:
        return recreate_swap_chain(framebuffer_width, framebuffer_height);
        break;

    default:
        // Failure
        return false;
        break;
    }
    
    if (nullptr != out_frame.m_framebuffer)
    {
        vkDestroyFramebuffer(m_vulkan_device, out_frame.m_framebuffer, nullptr);
        out_frame.m_framebuffer = nullptr;
    }

    if (!create_framebuffer(m_vulkan_device,
        m_render_pass,
        framebuffer_width,
        framebuffer_height,
        m_swapchain_image_views.at(static_cast<std::size_t>(image_index)),
        m_depth_stencil.m_image_view,
        out_frame.m_framebuffer))
    {
        return false;
    }
    
    if (!record_command_buffer(framebuffer_width,
        framebuffer_height,
        out_frame.m_framebuffer,
        m_swapchain_images.at(static_cast<std::size_t>(image_index)),
        out_frame.m_command_buffer))
    {
        return false;
    }

    // vkAcquireNextImageKHR: Wait until the next image is available (out_frame.m_image_available_semaphore is
    // signalled).
    // vkQueueSubmit: The command buffers are submitted when out_frame.m_image_available_semaphore is signalled.
    // vkQueuePresentKHR: Present the image at image_index when the command buffers have been executed
    // (out_frame.m_rendering_finished_semaphore is signalled).
    VkSubmitInfo submit_info = {};
    submit_info.sType = VK_STRUCTURE_TYPE_SUBMIT_INFO;
    submit_info.pNext = nullptr;
    submit_info.waitSemaphoreCount = 1;
    // Wait for image_available_semaphore to be signalled before the command buffers for this batch are executed.
    submit_info.pWaitSemaphores = &out_frame.m_image_available_semaphore;
    VkPipelineStageFlags wait_destination_stage_mask = VK_PIPELINE_STAGE_COLOR_ATTACHMENT_OUTPUT_BIT;
    // pWaitDstStageMask: pointer to an array with waitSemaphoreCount elements. pWaitSemaphores[i] will wait to be
    // signalled at pWaitDstStageMask[i]
    submit_info.pWaitDstStageMask = &wait_destination_stage_mask;
    submit_info.commandBufferCount = 1;
    submit_info.pCommandBuffers = &out_frame.m_command_buffer;
    submit_info.signalSemaphoreCount = 1;
    // Signal rendering_finished_semaphore when the command buffers for this batch have finished executing.
    submit_info.pSignalSemaphores = &out_frame.m_rendering_finished_semaphore;
    vk_result = vkQueueSubmit(m_graphics_queue, 1, &submit_info, out_frame.m_fence);
    if (VK_SUCCESS != vk_result)
    {
        return false;
    }
    
    VkPresentInfoKHR present_info = {};
    present_info.sType = VK_STRUCTURE_TYPE_PRESENT_INFO_KHR;
    present_info.pNext = nullptr;
    present_info.waitSemaphoreCount = 1;
    // Wait for rendering_finished_semaphore to be signalled before issuing the present request.
    present_info.pWaitSemaphores = &out_frame.m_rendering_finished_semaphore;
    present_info.swapchainCount = 1;
    present_info.pSwapchains = &m_swapchain;
    present_info.pImageIndices = &image_index;
    present_info.pResults = nullptr;
    vk_result = vkQueuePresentKHR(m_present_queue, &present_info);
    switch (vk_result)
    {
    case VK_SUCCESS:
        break;

    case VK_SUBOPTIMAL_KHR:
    case VK_ERROR_OUT_OF_DATE_KHR:
        return recreate_swap_chain(framebuffer_width, framebuffer_height);
        break;

    default:
        // Failure
        return false;
        break;
    }

    return true;
}

bool Application::recreate_swap_chain(uint32_t framebuffer_width, uint32_t framebuffer_height)
{
    auto vk_result = vkDeviceWaitIdle(m_vulkan_device);
    if (VK_SUCCESS != vk_result)
    {
        return false;
    }
    
    for (auto iter = m_swapchain_image_views.cbegin(); iter != m_swapchain_image_views.cend(); ++iter)
    {
        auto image_view = *iter;
        if (nullptr != image_view)
        {
            vkDestroyImageView(m_vulkan_device, image_view, nullptr);
        }
    }

    m_swapchain_image_views.clear();
    m_swapchain_images.clear();
    auto old_swapchain = m_swapchain;
    if (!create_swapchain(
        framebuffer_width,
        framebuffer_height,
        old_swapchain,
        m_swapchain_image_format,
        m_swapchain_images,
        m_swapchain_image_views,
        m_swapchain))
    {
        return false;
    }

    return true;
}

bool Application::create_2d_image(VkDevice device,
    const VkPhysicalDeviceMemoryProperties &physical_device_memory_properties,
    VkFormat image_format,
    VkImageTiling image_tiling,
    VkImageUsageFlags image_usage_flags,
    VkImageAspectFlags image_aspect_flags,
    VkMemoryPropertyFlags memory_property_flags,
    uint32_t image_width,
    uint32_t image_height,
    Jade::Image &out_image) const
{
    VkImageCreateInfo image_create_info = {};
    image_create_info.sType = VK_STRUCTURE_TYPE_IMAGE_CREATE_INFO;
    image_create_info.pNext = nullptr;
    image_create_info.flags = 0;
    image_create_info.imageType = VK_IMAGE_TYPE_2D;
    image_create_info.format = image_format;
    // extent is measured in texels
    image_create_info.extent.width = image_width;
    image_create_info.extent.height = image_height;
    image_create_info.extent.depth = 1;
    image_create_info.mipLevels = 1;
    image_create_info.arrayLayers = 1;
    image_create_info.samples = VK_SAMPLE_COUNT_1_BIT;
    image_create_info.tiling = VK_IMAGE_TILING_OPTIMAL;
    image_create_info.usage = image_usage_flags;
    image_create_info.sharingMode = VK_SHARING_MODE_EXCLUSIVE;
    image_create_info.queueFamilyIndexCount = 0;
    image_create_info.pQueueFamilyIndices = nullptr;
    image_create_info.initialLayout = VK_IMAGE_LAYOUT_UNDEFINED;
    auto vk_result = vkCreateImage(device, &image_create_info, nullptr, &out_image.m_image);
    if (VK_SUCCESS != vk_result)
    {
        return false;
    }

    vkGetImageMemoryRequirements(device, out_image.m_image, &out_image.m_memory_requirements);
    uint32_t memory_type_index{};
    if (!Vulkan_utility::find_memory_type(physical_device_memory_properties,
        out_image.m_memory_requirements,
        memory_property_flags,
        memory_type_index))
    {
        return false;
    }

    VkMemoryAllocateInfo memory_allocate_info = {};
    memory_allocate_info.sType = VK_STRUCTURE_TYPE_MEMORY_ALLOCATE_INFO;
    memory_allocate_info.pNext = nullptr;
    memory_allocate_info.allocationSize = out_image.m_memory_requirements.size;
    memory_allocate_info.memoryTypeIndex = memory_type_index;
    vk_result = vkAllocateMemory(device, &memory_allocate_info, nullptr, &out_image.m_device_memory);
    if (VK_SUCCESS != vk_result)
    {
        return false;
    }

    vk_result = vkBindImageMemory(device, out_image.m_image, out_image.m_device_memory, 0);
    if (VK_SUCCESS != vk_result)
    {
        return false;
    }

    VkImageViewCreateInfo image_view_create_info = {};
    image_view_create_info.sType = VK_STRUCTURE_TYPE_IMAGE_VIEW_CREATE_INFO;
    image_view_create_info.pNext = nullptr;
    image_view_create_info.flags = 0;
    image_view_create_info.image = out_image.m_image;
    image_view_create_info.viewType = VK_IMAGE_VIEW_TYPE_2D;
    image_view_create_info.format = image_create_info.format;
    image_view_create_info.components.r = VK_COMPONENT_SWIZZLE_IDENTITY;
    image_view_create_info.components.g = VK_COMPONENT_SWIZZLE_IDENTITY;
    image_view_create_info.components.b = VK_COMPONENT_SWIZZLE_IDENTITY;
    image_view_create_info.components.a = VK_COMPONENT_SWIZZLE_IDENTITY;
    image_view_create_info.subresourceRange.aspectMask = image_aspect_flags;
    image_view_create_info.subresourceRange.baseMipLevel = 0;
    image_view_create_info.subresourceRange.levelCount = 1;
    image_view_create_info.subresourceRange.baseArrayLayer = 0;
    image_view_create_info.subresourceRange.layerCount = 1;
    vk_result = vkCreateImageView(device, &image_view_create_info, nullptr, &out_image.m_image_view);
    if (VK_SUCCESS != vk_result)
    {
        return false;
    }

    return true;
}

bool Application::create_depth_stencil_image(VkDevice device,
    const VkPhysicalDeviceMemoryProperties &physical_device_memory_properties,
    VkFormat image_format,
    uint32_t image_width,
    uint32_t image_height,
    Jade::Image &out_image) const
{
    auto image_usage_flags = VK_IMAGE_USAGE_DEPTH_STENCIL_ATTACHMENT_BIT;
    auto image_aspect_flags = VK_IMAGE_ASPECT_DEPTH_BIT|VK_IMAGE_ASPECT_STENCIL_BIT;
    auto memory_property_flags = 0;
    auto image_tiling = VK_IMAGE_TILING_OPTIMAL;
    VkImageFormatProperties image_format_properties = {};
    auto vk_result = vkGetPhysicalDeviceImageFormatProperties(m_physical_device,
        image_format,
        VK_IMAGE_TYPE_2D,
        image_tiling,
        image_usage_flags,
        0,
        &image_format_properties);
    // If vkGetPhysicalDeviceImageFormatProperties returns VK_ERROR_FORMAT_NOT_SUPPORTED, could try again with
    // different arguments.
    if (VK_SUCCESS != vk_result)
    {
        return false;
    }

    return create_2d_image(m_vulkan_device,
        physical_device_memory_properties,
        image_format,
        image_tiling,
        image_usage_flags,
        image_aspect_flags,
        memory_property_flags,
        image_width,
        image_height,
        out_image);
}

void Application::process_input()
{
    unsigned direction = 0;
    for (auto iter = m_key_map.cbegin(); iter != m_key_map.cend(); ++iter)
    {
        switch (iter->first)
        {
        
        case GLFW_KEY_LEFT:
        {
            if ((GLFW_PRESS == iter->second) || (GLFW_REPEAT == iter->second))
            {
                direction |= Player::Direction::left;
            }
        }
        break;

        case GLFW_KEY_RIGHT:
        {
            if ((GLFW_PRESS == iter->second) || (GLFW_REPEAT == iter->second))
            {
                direction |= Player::Direction::right;
            }
        }
        break;

        case GLFW_KEY_UP:
        {
            if ((GLFW_PRESS == iter->second) || (GLFW_REPEAT == iter->second))
            {
                direction |= Player::Direction::forward;
            }
        }
        break;

        case GLFW_KEY_DOWN:
        {
            if ((GLFW_PRESS == iter->second) || (GLFW_REPEAT == iter->second))
            {
                direction |= Player::Direction::back;
            }
        }
        break;

        case GLFW_KEY_PAGE_UP:
        {
            if ((GLFW_PRESS == iter->second) || (GLFW_REPEAT == iter->second))
            {
                direction |= Player::Direction::up;
            }
        }
        break;

        case GLFW_KEY_PAGE_DOWN:
        {
            if ((GLFW_PRESS == iter->second) || (GLFW_REPEAT == iter->second))
            {
                direction |= Player::Direction::down;
            }
        }
        break;

        default:
            break;
        }
    }

    double x{}; // pitch
    double y{}; // yaw
    double z{}; // roll
    if (is_mouse_button_pressed(GLFW_MOUSE_BUTTON_LEFT))
    {
        // If the right mouse button is also pressed
        if (is_mouse_button_pressed(GLFW_MOUSE_BUTTON_RIGHT))
        {
            // The vertical movement of the mouse is used to calculate an angle to pitch the camera
            x = m_mouse_cursor_shift_vector.y;
            y = 0.0;
            // The horizontal movement of the mouse is used to calculate an angle to roll the camera
            z = -m_mouse_cursor_shift_vector.x;
        }
        else
        {
            // The vertical movement of the mouse is used to calculate an angle to pitch the camera
            x = m_mouse_cursor_shift_vector.y;
            // The horizontal movement of the mouse is used to calculate an angle to yaw the camera
            y = m_mouse_cursor_shift_vector.x;
            z = 0.0;
        }
    }
    
    if (x || y || z)
    {
        m_player.rotate(glm::radians(static_cast<float>(x)),
            glm::radians(static_cast<float>(y)),
            glm::radians(static_cast<float>(z)));
    }
    
    if (0 != direction)
    {
        auto distance = 800.0f * static_cast<float>(m_timer.get_time_elapsed());
        m_player.move_velocity(static_cast<Player::Direction>(direction), distance);
    }

    m_player.update(static_cast<float>(m_timer.get_time_elapsed()));
}

void Application::animate_objects()
{
}

bool Application::is_mouse_button_pressed(int button) const
{
    auto iter = m_mouse_button_map.find(button);
    return (m_mouse_button_map.cend() == iter) ? false : (GLFW_PRESS == iter->second);
}

bool Application::find_depth_stencil_image_format(VkFormat &out_format) const
{
    auto image_usage_flags = VK_IMAGE_USAGE_DEPTH_STENCIL_ATTACHMENT_BIT;
    auto image_aspect_flags = VK_IMAGE_ASPECT_DEPTH_BIT | VK_IMAGE_ASPECT_STENCIL_BIT;
    auto memory_property_flags = 0;
    auto image_tiling = VK_IMAGE_TILING_OPTIMAL;
    VkImageFormatProperties image_format_properties = {};
    std::vector<VkFormat> image_formats = {VK_FORMAT_D24_UNORM_S8_UINT, VK_FORMAT_D16_UNORM_S8_UINT};
    for (auto iter = image_formats.cbegin(); iter != image_formats.cend(); ++iter)
    {
        auto vk_result = vkGetPhysicalDeviceImageFormatProperties(m_physical_device,
            *iter,
            VK_IMAGE_TYPE_2D,
            image_tiling,
            image_usage_flags,
            0,
            &image_format_properties);
        // vkGetPhysicalDeviceImageFormatProperties can return VK_ERROR_FORMAT_NOT_SUPPORTED
        if (VK_SUCCESS == vk_result)
        {
            out_format = *iter;
            return true;
        }
    }

    return false;
}

bool Application::create_descriptor_set_layout(VkDescriptorSetLayout &out_descriptor_set_layout) const
{
    std::vector<VkDescriptorSetLayoutBinding> bindings;
    
    {
        VkDescriptorSetLayoutBinding descriptor_set_layout_binding = {};
        descriptor_set_layout_binding.binding = descriptor_binding_lights;
        descriptor_set_layout_binding.descriptorType = VK_DESCRIPTOR_TYPE_UNIFORM_BUFFER;
        descriptor_set_layout_binding.descriptorCount = 1;
        descriptor_set_layout_binding.stageFlags = VK_SHADER_STAGE_VERTEX_BIT;
        descriptor_set_layout_binding.pImmutableSamplers = nullptr;
        bindings.push_back(descriptor_set_layout_binding);
    }

    {
        VkDescriptorSetLayoutBinding descriptor_set_layout_binding = {};
        descriptor_set_layout_binding.binding = descriptor_binding_materials;
        descriptor_set_layout_binding.descriptorType = VK_DESCRIPTOR_TYPE_UNIFORM_BUFFER;
        descriptor_set_layout_binding.descriptorCount = 1;
        descriptor_set_layout_binding.stageFlags = VK_SHADER_STAGE_VERTEX_BIT;
        descriptor_set_layout_binding.pImmutableSamplers = nullptr;
        bindings.push_back(descriptor_set_layout_binding);
    }

    {
        VkDescriptorSetLayoutBinding descriptor_set_layout_binding = {};
        descriptor_set_layout_binding.binding = descriptor_binding_light_groups;
        descriptor_set_layout_binding.descriptorType = VK_DESCRIPTOR_TYPE_UNIFORM_BUFFER;
        descriptor_set_layout_binding.descriptorCount = 1;
        descriptor_set_layout_binding.stageFlags = VK_SHADER_STAGE_VERTEX_BIT;
        descriptor_set_layout_binding.pImmutableSamplers = nullptr;
        bindings.push_back(descriptor_set_layout_binding);
    }

    VkDescriptorSetLayoutCreateInfo descriptor_set_layout_create_info = {};
    descriptor_set_layout_create_info.sType = VK_STRUCTURE_TYPE_DESCRIPTOR_SET_LAYOUT_CREATE_INFO;
    descriptor_set_layout_create_info.pNext = nullptr;
    descriptor_set_layout_create_info.flags = 0;
    descriptor_set_layout_create_info.bindingCount = static_cast<uint32_t>(bindings.size());
    descriptor_set_layout_create_info.pBindings = bindings.data();
    auto vk_result = vkCreateDescriptorSetLayout(m_vulkan_device,
        &descriptor_set_layout_create_info,
        nullptr,
        &out_descriptor_set_layout);
    return VK_SUCCESS == vk_result;
}

bool Application::create_descriptor_pool(VkDescriptorPool &out_descriptor_pool) const
{
    VkDescriptorPoolSize descriptor_pool_size = {};
    descriptor_pool_size.type = VK_DESCRIPTOR_TYPE_UNIFORM_BUFFER;
    descriptor_pool_size.descriptorCount = 3;
    VkDescriptorPoolCreateInfo descriptor_pool_create_info = {};
    descriptor_pool_create_info.sType = VK_STRUCTURE_TYPE_DESCRIPTOR_POOL_CREATE_INFO;
    descriptor_pool_create_info.pNext = nullptr;
    descriptor_pool_create_info.flags = 0;
    descriptor_pool_create_info.maxSets = 1;
    descriptor_pool_create_info.poolSizeCount = 1;
    descriptor_pool_create_info.pPoolSizes = &descriptor_pool_size;
    return VK_SUCCESS == vkCreateDescriptorPool(m_vulkan_device,
        &descriptor_pool_create_info,
        nullptr,
        &out_descriptor_pool);
}

bool Application::allocate_descriptor_set(VkDescriptorSet &out_descriptor_set) const
{
    VkDescriptorSetAllocateInfo descriptor_set_allocate_info = {};
    descriptor_set_allocate_info.sType = VK_STRUCTURE_TYPE_DESCRIPTOR_SET_ALLOCATE_INFO;
    descriptor_set_allocate_info.pNext = nullptr;
    descriptor_set_allocate_info.descriptorPool = m_descriptor_pool;
    descriptor_set_allocate_info.descriptorSetCount = 1;
    descriptor_set_allocate_info.pSetLayouts = &m_descriptor_set_layout;
    return VK_SUCCESS == vkAllocateDescriptorSets(m_vulkan_device, &descriptor_set_allocate_info, &out_descriptor_set);
}

void Application::update_descriptor_sets() const
{
    std::vector<VkWriteDescriptorSet> writes;
    // See Application::update_descriptor_buffer_infos
    std::vector<VkDescriptorBufferInfo> buffer_infos = {m_descriptor_buffer_info_lights,
        m_descriptor_buffer_info_materials,
        m_descriptor_buffer_info_light_groups};
    std::vector<uint32_t> bindings = {descriptor_binding_lights,
        descriptor_binding_materials,
        descriptor_binding_light_groups};
    for (std::size_t i = 0; i != bindings.size(); ++i)
    {
        VkWriteDescriptorSet write_descriptor_set = {};
        write_descriptor_set.sType = VK_STRUCTURE_TYPE_WRITE_DESCRIPTOR_SET;
        write_descriptor_set.pNext = nullptr;
        write_descriptor_set.dstSet = m_descriptor_set;
        write_descriptor_set.dstBinding = bindings.at(i);
        write_descriptor_set.dstArrayElement = 0;
        write_descriptor_set.descriptorCount = 1;
        write_descriptor_set.descriptorType = VK_DESCRIPTOR_TYPE_UNIFORM_BUFFER;
        write_descriptor_set.pImageInfo = nullptr;
        write_descriptor_set.pBufferInfo = &buffer_infos.at(i);
        write_descriptor_set.pTexelBufferView = nullptr;
        writes.push_back(write_descriptor_set);
    }
    
    vkUpdateDescriptorSets(m_vulkan_device, static_cast<uint32_t>(writes.size()), writes.data(), 0, nullptr);
}

// command_buffer: Assumes that vkBeginCommandBuffer has already been called on command_buffer.
bool Application::copy_uniform_buffer(VkCommandBuffer command_buffer) const
{
    void *data{nullptr};
    auto vk_result = vkMapMemory(m_vulkan_device,
        m_staging_uniform_buffer.m_device_memory,
        0,
        m_staging_uniform_buffer.m_memory_requirements.size,
        0,
        &data);
    if (VK_SUCCESS != vk_result)
    {
        return false;
    }

    {
        auto begin_data_char = reinterpret_cast<char*>(data);
        auto data_char = begin_data_char;
        data_char += m_descriptor_buffer_info_lights.offset;
        const auto &lights = m_scene.get_lights();
        std::memcpy(data_char, lights.data(), lights.size() * sizeof(Jade::D3d9_light));
        data_char = begin_data_char;
        data_char += m_descriptor_buffer_info_materials.offset;
        for (auto iter = m_scene.get_materials().cbegin(); iter != m_scene.get_materials().cend(); ++iter)
        {
            std::memcpy(data_char, &(*iter), sizeof(Jade::D3d9_material));
            data_char += Jade::D3d9_material::std_140_size;
        }

        const std::size_t maximum_light_group_size = static_cast<std::size_t>(m_scene.get_maximum_light_group_size());
        for (std::size_t light_group_index = 0; light_group_index != m_scene.get_light_groups().size();
            ++light_group_index)
        {
            const auto &light_group = m_scene.get_light_groups().at(light_group_index);
            data_char = begin_data_char;
            data_char += m_descriptor_buffer_info_light_groups.offset;
            // Enough space is allocated in the uniform buffer for maximum_light_group_size elements per light group.
            auto i = light_group_index * maximum_light_group_size * sizeof(glm::vec4);
            data_char += i;
            assert(light_group.m_lights.size() <= maximum_light_group_size);
            for (auto iter = light_group.m_lights.cbegin(); iter != light_group.m_lights.cend(); ++iter)
            {
                uint32_t light_index = static_cast<uint32_t>(*iter);
                std::memcpy(data_char, &light_index, sizeof(uint32_t));
                // std140: each array element is aligned to a 16 byte boundary
                data_char += sizeof(glm::vec4);
            }
        }
    }
    
    VkMappedMemoryRange mapped_memory_range = {};
    mapped_memory_range.sType = VK_STRUCTURE_TYPE_MAPPED_MEMORY_RANGE;
    mapped_memory_range.pNext = nullptr;
    mapped_memory_range.memory = m_staging_uniform_buffer.m_device_memory;
    mapped_memory_range.offset = 0;
    mapped_memory_range.size = m_staging_uniform_buffer.m_memory_requirements.size;
    vk_result = vkFlushMappedMemoryRanges(m_vulkan_device, 1, &mapped_memory_range);
    if (VK_SUCCESS != vk_result)
    {
        return false;
    }

    vkUnmapMemory(m_vulkan_device, m_staging_uniform_buffer.m_device_memory);
    VkBufferCopy buffer_copy = {};
    buffer_copy.srcOffset = 0;
    buffer_copy.dstOffset = 0;
    buffer_copy.size = m_uniform_buffer.m_memory_requirements.size;
    vkCmdCopyBuffer(command_buffer, m_staging_uniform_buffer.m_buffer, m_uniform_buffer.m_buffer, 1, &buffer_copy);
    VkBufferMemoryBarrier buffer_memory_barrier = {};
    buffer_memory_barrier.sType = VK_STRUCTURE_TYPE_BUFFER_MEMORY_BARRIER;
    buffer_memory_barrier.pNext = nullptr;
    buffer_memory_barrier.srcAccessMask = VK_ACCESS_TRANSFER_WRITE_BIT;
    buffer_memory_barrier.dstAccessMask = VK_ACCESS_VERTEX_ATTRIBUTE_READ_BIT;
    buffer_memory_barrier.srcQueueFamilyIndex = VK_QUEUE_FAMILY_IGNORED;
    buffer_memory_barrier.dstQueueFamilyIndex = VK_QUEUE_FAMILY_IGNORED;
    buffer_memory_barrier.buffer = m_uniform_buffer.m_buffer;
    buffer_memory_barrier.offset = 0;
    buffer_memory_barrier.size = m_uniform_buffer.m_memory_requirements.size;
    vkCmdPipelineBarrier(command_buffer,
        VK_PIPELINE_STAGE_TRANSFER_BIT,
        VK_PIPELINE_STAGE_VERTEX_INPUT_BIT,
        0,
        0,
        nullptr,
        1,
        &buffer_memory_barrier,
        0,
        nullptr);
    return true;
}

// m_uniform_buffer is created after update_descriptor_buffer_infos is called.
void Application::update_descriptor_buffer_infos()
{
    m_descriptor_buffer_info_lights.buffer = nullptr;
    m_descriptor_buffer_info_lights.offset = 0;
    auto size = m_scene.get_lights().size() * Jade::D3d9_light::std_140_size;
    m_descriptor_buffer_info_lights.range = Vulkan_utility::calculate_buffer_size(
        m_physical_device_properties.limits.minUniformBufferOffsetAlignment,
        static_cast<VkDeviceSize>(size));
    m_descriptor_buffer_info_materials.buffer = nullptr;
    m_descriptor_buffer_info_materials.offset = m_descriptor_buffer_info_lights.range;
    size = m_scene.get_materials().size() * Jade::D3d9_material::std_140_size;
    m_descriptor_buffer_info_materials.range = Vulkan_utility::calculate_buffer_size(
        m_physical_device_properties.limits.minUniformBufferOffsetAlignment,
        static_cast<VkDeviceSize>(size));
    m_descriptor_buffer_info_light_groups.buffer = nullptr;
    m_descriptor_buffer_info_light_groups.offset = m_descriptor_buffer_info_materials.offset + m_descriptor_buffer_info_materials.range;
    // According to the std140 layout rules each array element is aligned to a 16 byte boundary (even though each light
    // index can be stored in a uint32_t).
    const auto maximum_light_group_size = static_cast<std::size_t>(m_scene.get_maximum_light_group_size());
    size = m_scene.get_light_groups().size() * maximum_light_group_size * sizeof(glm::vec4);
    m_descriptor_buffer_info_light_groups.range = Vulkan_utility::calculate_buffer_size(
        m_physical_device_properties.limits.minUniformBufferOffsetAlignment,
        static_cast<VkDeviceSize>(size));
}

}