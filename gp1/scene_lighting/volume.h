#pragma once

// GLM
#include <glm/vec3.hpp>

namespace Jade
{

class Volume
{
public:
    Volume() = default;
    Volume(const glm::vec3 &minimum, const glm::vec3 &maximum) : m_minimum{minimum}, m_maximum{maximum}
    {

    }
    
    glm::vec3 m_minimum;
    glm::vec3 m_maximum;
};

}
