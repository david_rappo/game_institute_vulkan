#pragma once

// C++ Standard Library
#include <cstdio>
#include <fstream>
#include <memory>
#include <string>
#include <vector>

#define jade_function_signature __FUNCSIG__

namespace Jade
{

class Log_manager
{
public:
    enum class Log_type
    {
        default_log,
        glfw_error,
        vulkan_debug_report_all
    };

    Log_manager(const Log_manager&) = delete;
    Log_manager& operator=(const Log_manager&) = delete;
    
    static Log_manager *get_instance();

    std::ostream &get_stream(Log_type log_type);

private:
    static constexpr std::size_t file_count{3};
    static std::unique_ptr<Log_manager> s_instance;

    std::vector<std::ofstream> m_streams;
    
    Log_manager();
};

}
