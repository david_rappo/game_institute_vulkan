#pragma once

// C++ Standard Library
#include <string>
#include <vector>

namespace Jade
{

// See documentation for stbi_load.
class Stb_image
{
public:
    class Pixel
    {
    public:
        unsigned char m_red{};
        unsigned char m_green{};
        unsigned char m_blue{};
        unsigned char m_alpha{};
    };

    Stb_image();
    Stb_image(const Stb_image&) = delete;
    Stb_image& operator=(const Stb_image&) = delete;
    
    bool load(const std::string &file_name, int number_of_components);
    
    // If m_number_of_components is 3, Pixel::m_alpha is unused.
    // If m_number_of_components is 2, only Pixel::m_red and Pixel::m_alpha are used.
    // If m_number_of_components is 1, only Pixel::m_red is used.
    //
    // scanline 0 is the first row in the image (from the top)
    // scanline 1 is the second row in the image.
    // etc.
    // x is the index of a pixel in scanline e.g. x = 0 is the leftmost pixel in scanline.
    Pixel get_pixel(int x, int scanline) const;
    
    int get_number_of_components() const
    {
        return m_number_of_components;
    }

    int get_number_of_scanlines() const
    {
        return m_number_of_scanlines;
    }

    int get_width() const
    {
        return m_width;
    }

    const std::vector<unsigned char> &get_data() const
    {
        return m_data;
    }

private:
    // 1 - 4
    // 1: gray
    // 2: gray, alpha
    // 3: red, green, blue
    // 4: red, green, blue, alpha
    int m_number_of_components{};
    int m_number_of_scanlines{};
    // If m_width is 256 and m_number_of_components is 4 then each scanline has 256 pixels where a pixel is 4 bytes
    // If m_width is 256 and m_number_of_components is 3 then each scanline has 256 pixels where a pixel is 3 bytes
    // etc.
    int m_width{};
    std::vector<unsigned char> m_data;

};

}