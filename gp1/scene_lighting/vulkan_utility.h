#pragma

// Vulkan
#include "vulkan/vulkan.h"

namespace Jade
{

class Buffer;

}

namespace Jade
{

namespace Vulkan_utility
{

bool create_buffer(
    VkDevice vulkan_device,
    const VkPhysicalDeviceMemoryProperties &physical_device_memory_properties,
    VkBufferUsageFlags buffer_usage_flags,
    VkMemoryPropertyFlags memory_property_flags,
    VkDeviceSize buffer_size,
    Jade::Buffer &out_buffer);

bool find_memory_type(const VkPhysicalDeviceMemoryProperties &physical_device_memory_properties,
    const VkMemoryRequirements &memory_requirements,
    VkFlags requirements,
    uint32_t &memory_type_index);

VkDeviceSize calculate_buffer_size(VkDeviceSize minimum_offset_alignment, VkDeviceSize buffer_size);

}

}