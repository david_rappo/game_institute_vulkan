// This is a personal academic project. Dear PVS-Studio, please check it.
// PVS-Studio Static Code Analyzer for C, C++ and C#: http://www.viva64.com
#include "player.h"

// C++ Standard Library
#include <algorithm>
#include <cassert>
#include <cmath>

// GLM
#include <glm/glm.hpp>
#include <glm/gtc/matrix_transform.hpp>
#include <glm/vec4.hpp>

// Jade
#include "first_person_fly_camera.h"
#include "log_manager.h"
#include "math_utility.h"

const float Jade::Player::minimum_pitch{glm::radians(-89.0f)};
const float Jade::Player::maximum_pitch{glm::radians(89.0f)};
const float Jade::Player::minimum_yaw{0.0f};
const float Jade::Player::maximum_yaw{glm::radians(360.0f)};
const float Jade::Player::minimum_roll{glm::radians(-20.0f)};
const float Jade::Player::maximum_roll{glm::radians(20.0f)};

namespace Jade
{

Player::Player() = default;

Player::~Player() = default;

void Player::set_camera_type(Camera::Type type)
{
    // scene_lighting only supports one type of camera
    if (nullptr != m_camera)
    {
        return;
    }

    m_camera.reset(new First_person_fly_camera);
}

// Set the position of the camera relative to the player.
void Player::set_camera_offset(const glm::vec3 &camera_offset)
{
    m_camera_offset = camera_offset;
    m_camera->set_position(m_position + m_camera_offset);
}

void Player::set_position(const glm::vec3 &position)
{
    auto v = position - m_position;
    move(v);
}

void Player::rotate(float x, float y, float z)
{
    // scene_lighting only supports one type of camera
    assert(Camera::Type::first_person_fly == m_camera->get_type());
    if (x)
    {
        m_pitch += x;
        if (m_pitch > maximum_pitch)
        {
            x -= (m_pitch - maximum_pitch);
            m_pitch = maximum_pitch;
        }

        if (m_pitch < minimum_pitch)
        {
            x -= (m_pitch + maximum_pitch);
            m_pitch = minimum_pitch;
        }
    }

    if (y)
    {
        m_yaw += y;
        if (m_yaw > maximum_yaw)
        {
            m_yaw -= maximum_yaw;
        }

        if (m_yaw < minimum_yaw)
        {
            m_yaw += maximum_yaw;
        }
    }

    if (z)
    {
        m_roll += z;
        if (m_roll > maximum_roll)
        {
            z -= (m_roll - maximum_roll);
            m_roll = maximum_roll;
        }

        if (m_roll < minimum_roll)
        {
            z -= (m_roll + maximum_roll);
            m_roll = minimum_roll;
        }
    }

    // rotate uses the player orientation vector to perform it's calculations
    m_camera->rotate(x, y, z, *this);
    if (y)
    {
        auto m = glm::rotate(glm::mat4{}, y, m_up);
        m_right = Math_utility::transform_normal(m, m_right);
        m_look = Math_utility::transform_normal(m, m_look);
    }
    
    Math_utility::regenerate_orientation_vectors(m_right, m_up, m_look, m_right, m_up, m_look);
}

void Player::update(float time_elapsed)
{
    m_velocity += (m_gravity * time_elapsed);
    auto direction = glm::normalize(m_velocity);
    auto scale = std::fabsf(direction.y);
    // This is the technique used in The Game Institute project Scene_Lighting to calculate the maximum velocity of the
    // player.
    scale = std::asinf(scale) / glm::half_pi<float>();
    auto maximum_velocity = m_maximum_velocity_xz + ((m_maximum_velocity_y - m_maximum_velocity_xz) * scale);
    auto length = glm::length(m_velocity);
    if (length > maximum_velocity)
    {
        // Clamp m_velocity to maximum_velocity
        auto percentage_of_length = (maximum_velocity / length);
        m_velocity *= percentage_of_length;
    }

    move(m_velocity * time_elapsed);
    m_camera->update(time_elapsed, m_camera_lag, *this);
    length = glm::length(m_velocity);
    auto friction = m_friction * time_elapsed;
    // Clamp friction to length
    if (friction > length)
    {
        friction = length;
    }

    auto reverse_velocity = -m_velocity;
    auto reverse_velocity_length = glm::length(reverse_velocity);
    auto delta = std::fabs(0.0f - reverse_velocity_length);
    if (delta >= 0.01f)
    {
        // Normalising reverse_velocity was returning <NaN, NaN, NaN> if reverse_velocity was zero. Appending
        // <NaN, NaN, NaN> to m_velocity corrupted m_velocity.
        reverse_velocity = glm::normalize(reverse_velocity);
        m_velocity += (reverse_velocity * friction);
    }
}

void Player::move(Direction direction, float distance)
{
    auto shift_vector = create_shift_vector(direction, distance);
    move(shift_vector);
}

void Player::move_velocity(Direction direction, float distance)
{
    auto shift_vector = create_shift_vector(direction, distance);
    move_velocity(shift_vector);
}

void Player::set_first_person_fly_defaults()
{
    m_friction = 400.0f;
    m_gravity = glm::vec3{0.0f, 0.0f, 0.0f};
    m_maximum_velocity_xz = 50.0f;
    m_maximum_velocity_y = 50.0f;
    set_camera_offset(glm::vec3{0.0f, 15.0f, 0.0f});
    m_camera_lag = 0.0f;
}

glm::mat4 Player::get_world_matrix() const
{
    // Direct3D world matrix
    //
    // right.x    right.y    right.z    0
    // up.x       up.y       up.z       0
    // look.x     look.y     look.z     0
    // position.x position.y position.z 1
    //
    // get_world_matrix returns the transpose of the Direct3D world matrix because GLSL uses column vectors
    auto column_0 = glm::vec4{m_right.x, m_right.y, m_right.z, 0.0f};
    auto column_1 = glm::vec4{m_up.x, m_up.y, m_up.z, 0.0f};
    auto column_2 = glm::vec4{m_look.x, m_look.y, m_look.z, 0.0f};
    auto column_3 = glm::vec4{m_position.x, m_position.y, m_position.z, 1.0f};
    return glm::mat4{column_0, column_1, column_2, column_3};
}

void Player::move(const glm::vec3 &v)
{
    m_position += v;
    m_camera->move(v);
}

void Player::move_velocity(const glm::vec3 &v)
{
    m_velocity += v;
}

glm::vec3 Player::create_shift_vector(Direction direction, float distance) const
{
    glm::vec3 shift_vector{0.0f};
    // No bits set
    if (0 == direction)
    {
        return shift_vector;
    }
    
    if (direction & Direction::forward)
    {
        shift_vector += (m_look * distance);
    }

    if (direction & Direction::back)
    {
        shift_vector += (-m_look * distance);
    }

    if (direction & Direction::right)
    {
        shift_vector += (m_right * distance);
    }

    if (direction & Direction::left)
    {
        shift_vector += (-m_right * distance);
    }

    if (direction & Direction::up)
    {
        shift_vector += (m_up * distance);
    }

    if (direction & Direction::down)
    {
        shift_vector += (-m_up * distance);
    }

    return shift_vector;
}

}