#pragma once

// C++ Standard Library
#include <cstddef>
#include <utility>
#include <vector>

// Jade
#include "property_group.h"
#include "vertex.h"

namespace Jade
{

class Light_group
{
public:
    // Each element in m_lights is an index into the global list of lights maintained by the Scene class
    std::vector<std::size_t> m_lights;
    std::vector<Jade::Property_group> m_property_groups;
    std::vector<Jade::Vertex> m_vertices;
};

}