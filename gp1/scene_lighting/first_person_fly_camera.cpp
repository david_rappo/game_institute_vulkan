// This is a personal academic project. Dear PVS-Studio, please check it.
// PVS-Studio Static Code Analyzer for C, C++ and C#: http://www.viva64.com
#include "first_person_fly_camera.h"

// GLM
#include <glm/glm.hpp>
#include <glm/gtc/matrix_transform.hpp>
#include <glm/mat4x4.hpp>

// Jade
#include "math_utility.h"
#include "player.h"

namespace Jade
{

First_person_fly_camera::First_person_fly_camera() = default;

First_person_fly_camera::First_person_fly_camera(const Camera *camera): Camera{camera}
{
}

Camera::Type First_person_fly_camera::get_type() const
{
    return Type::first_person_fly;
}

void First_person_fly_camera::rotate(float x, float y, float z, const Player &player)
{
    if (x)
    {
        // Pitch around the camera's right vector
        auto m = glm::rotate(glm::mat4{}, x, get_right());
        auto right = Math_utility::transform_normal(m, get_right());
        set_right(right);
        auto up = Math_utility::transform_normal(m, get_up());
        set_up(up);
        auto look = Math_utility::transform_normal(m, get_look());
        set_look(look);
    }

    if (y)
    {
        // Yaw around the player's up vector
        auto m = glm::rotate(glm::mat4{}, y, player.get_up());
        auto right = Math_utility::transform_normal(m, get_right());
        set_right(right);
        auto up = Math_utility::transform_normal(m, get_up());
        set_up(up);
        auto look = Math_utility::transform_normal(m, get_look());
        set_look(look);
    }

    if (z)
    {
        // Roll the camera position around the players look vector
        auto m = glm::rotate(glm::mat4{}, z, player.get_look());
        auto const &player_position = player.get_position();
        auto camera_position = get_position();
        camera_position -= player_position;
        camera_position = Math_utility::transform_coordinate(m, camera_position);
        camera_position += player_position;
        set_position(camera_position);
        // Roll around the player's look vector
        auto right = Math_utility::transform_normal(m, get_right());
        set_right(right);
        auto up = Math_utility::transform_normal(m, get_up());
        set_up(up);
        auto look = Math_utility::transform_normal(m, get_look());
        set_look(look);
    }

    set_view_matrix_dirty(true);
}

}