// This is a personal academic project. Dear PVS-Studio, please check it.
// PVS-Studio Static Code Analyzer for C, C++ and C#: http://www.viva64.com
#include "d3d9_plane.h"

// GLM
#include <glm/geometric.hpp>

namespace Jade
{

D3d9_plane D3d9_plane::normalize(const D3d9_plane &plane)
{
    auto v = glm::vec3{plane.m_a, plane.m_b, plane.m_c};
    auto length = glm::length(v);
    return D3d9_plane{glm::vec4{v, plane.m_d} / length};
}

}