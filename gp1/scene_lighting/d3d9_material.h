#pragma once

// GLM
#include <glm/vec4.hpp>

namespace Jade
{

// Stores the same data as D3DMATERIAL9
class D3d9_material
{
public:
    static const std::size_t std_140_size{80};
                          // std140 layout
    glm::vec4 m_diffuse;  // base alignment: 16 aligned offset: 0
    glm::vec4 m_ambient;  // base alignment: 16 aligned offset: 16
    glm::vec4 m_specular; // base alignment: 16 aligned offset: 32
    glm::vec4 m_emissive; // base alignment: 16 aligned offset: 48
    float m_power{};      // base alignment: 4  aligned offset: 64
                          //
                          // total size = 68
    // The largest member of the structure is 16 bytes.
    // 68 is not a multiple of 16.
    // The next closest size that is a multiple of 16 is 80.
};

}