// This is a personal academic project. Dear PVS-Studio, please check it.
// PVS-Studio Static Code Analyzer for C, C++ and C#: http://www.viva64.com
#include "scene.h"

// C++ Standard Library
#include <algorithm>
#include <cassert>
#include <cmath>
#include <cstddef>
#include <cstdlib>
#include <iterator>
#include <memory>
#include <utility>

// GLM
#include <glm/geometric.hpp>

// Third party - libIWF
#include "third-party/libIWF/iwfFile.h"
#include "third-party/libIWF/iwfObjects.h"
#include "third-party/libIWF/libIWF.h"

// Jade
#include "log_manager.h"
#include "push_constants.h"
#include "vertex.h"
#include "vulkan_utility.h"

namespace Jade
{

Scene::Scene() = default;

void Scene::destroy(VkDevice device)
{
    assert(nullptr != device);
    m_staging_vertex_buffer.destroy(device);
    m_staging_index_buffer.destroy(device);
    m_index_buffer.destroy(device);
    m_vertex_buffer.destroy(device);
}

bool Scene::load(const std::string &file_name, unsigned light_limit, unsigned reserved_lights)
{
    bool result = true;
    CFileIWF file;
    std::unique_ptr<CFileIWF, void(*)(CFileIWF*)> up_file(&file, [](CFileIWF *f) { f->ClearObjects(); });
    // CFileIWF::Load will throw an exception if it fails
    try
    {
        file.Load(file_name.c_str());
        process_entities(file);
        process_materials(file);
        m_light_limit = light_limit;
        m_reserved_lights = reserved_lights;
        if (0 == m_light_limit)
        {
            m_light_limit = static_cast<unsigned>(m_lights.size()) + m_reserved_lights;
        }
        
        process_meshes(file);
#ifdef _DEBUG
        auto &os = Log_manager::get_instance()->get_stream(Log_manager::Log_type::default_log);
        os << "Scene::load m_light_groups.size(): " << m_light_groups.size() << std::endl; //-V128
        os << "Scene::load m_materials.size(): " << m_materials.size() << std::endl; //-V128
        os << "Scene::load m_lights.size(): " << m_lights.size() << std::endl; //-V128
#endif
        m_vertices.clear();
        m_indices.clear();
        // Add each light groups vertices to the global list of vertices
        std::size_t begin_index = 0;
        for (std::size_t light_group_index = 0; light_group_index != m_light_groups.size(); ++light_group_index)
        {
            auto &light_group = m_light_groups.at(light_group_index);
            std::copy(light_group.m_vertices.cbegin(),
                light_group.m_vertices.cend(),
                std::back_inserter(m_vertices));
            for (std::size_t property_group_index = 0; property_group_index != light_group.m_property_groups.size();
                ++property_group_index)
            {
                auto &property_group = light_group.m_property_groups.at(property_group_index);
                // Increment property_group.m_begin_vertex by begin_index because the property groups vertices are now
                // stored in m_vertices (instead of it's parent light group).
                property_group.m_begin_vertex += begin_index;
            }

            begin_index += light_group.m_vertices.size();
        }

        // Add each Property_group's indices to m_indices
        begin_index = 0;
        for (std::size_t light_group_index = 0; light_group_index != m_light_groups.size(); ++light_group_index)
        {
            auto &light_group = m_light_groups.at(light_group_index);
            for (std::size_t property_group_index = 0; property_group_index != light_group.m_property_groups.size();
                ++property_group_index)
            {
                auto &property_group = light_group.m_property_groups.at(property_group_index);
                std::copy(property_group.m_indices.cbegin(),
                    property_group.m_indices.cend(),
                    std::back_inserter(m_indices));
                // Store the offset into the scene list of indices of the first index used by this Property_group.
                // Assignment is OK because Property_group::m_begin_index is not calculated/set by process_meshes.
                property_group.m_begin_index = begin_index;
                begin_index += property_group.m_indices.size();
            }
        }
    }
    catch (...)
    {
        result = false;
    }
    
    return result;
}

bool Scene::build_buffers(VkDevice vulkan_device,
    const VkPhysicalDeviceMemoryProperties &physical_device_memory_properties)
{
    destroy(vulkan_device);
    const auto vertex_buffer_size = static_cast<VkDeviceSize>(m_vertices.size() * sizeof(Jade::Vertex));
    auto result = Jade::Vulkan_utility::create_buffer(vulkan_device,
        physical_device_memory_properties,
        VK_BUFFER_USAGE_VERTEX_BUFFER_BIT | VK_BUFFER_USAGE_TRANSFER_DST_BIT,
        VK_MEMORY_PROPERTY_DEVICE_LOCAL_BIT,
        vertex_buffer_size,
        m_vertex_buffer);
    if (!result)
    {
        return false;
    }

    result = Jade::Vulkan_utility::create_buffer(vulkan_device,
        physical_device_memory_properties,
        VK_BUFFER_USAGE_TRANSFER_SRC_BIT,
        VK_MEMORY_PROPERTY_HOST_VISIBLE_BIT,
        vertex_buffer_size,
        m_staging_vertex_buffer);
    if (!result)
    {
        return false;
    }

    const auto index_buffer_size = static_cast<VkDeviceSize>(m_indices.size()) * Property_group::index_size;
    result = Jade::Vulkan_utility::create_buffer(vulkan_device,
        physical_device_memory_properties,
        VK_BUFFER_USAGE_INDEX_BUFFER_BIT | VK_BUFFER_USAGE_TRANSFER_DST_BIT,
        VK_MEMORY_PROPERTY_DEVICE_LOCAL_BIT,
        index_buffer_size,
        m_index_buffer);
    if (!result)
    {
        return false;
    }

    result = Jade::Vulkan_utility::create_buffer(vulkan_device,
        physical_device_memory_properties,
        VK_BUFFER_USAGE_TRANSFER_SRC_BIT,
        VK_MEMORY_PROPERTY_HOST_VISIBLE_BIT,
        index_buffer_size,
        m_staging_index_buffer);
    if (!result)
    {
        return false;
    }

    return true;
}

// command_buffer: Assumes that vkBeginCommandBuffer has already been called on command_buffer.
bool Scene::copy_buffers(VkDevice vulkan_device, VkQueue graphics_queue, VkCommandBuffer command_buffer) const
{
    void *data{nullptr};
    auto vk_result = vkMapMemory(vulkan_device,
        m_staging_vertex_buffer.m_device_memory,
        0,
        m_staging_vertex_buffer.m_memory_requirements.size,
        0,
        &data);
    if (VK_SUCCESS != vk_result)
    {
        return false;
    }

    std::memcpy(data, m_vertices.data(), m_vertices.size() * sizeof(Jade::Vertex));
    VkMappedMemoryRange mapped_memory_range = {};
    mapped_memory_range.sType = VK_STRUCTURE_TYPE_MAPPED_MEMORY_RANGE;
    mapped_memory_range.pNext = nullptr;
    mapped_memory_range.memory = m_staging_vertex_buffer.m_device_memory;
    mapped_memory_range.offset = 0;
    mapped_memory_range.size = m_staging_vertex_buffer.m_memory_requirements.size;
    vk_result = vkFlushMappedMemoryRanges(vulkan_device, 1, &mapped_memory_range);
    if (VK_SUCCESS != vk_result)
    {
        return false;
    }

    vkUnmapMemory(vulkan_device, m_staging_vertex_buffer.m_device_memory);
    VkBufferCopy buffer_copy = {};
    buffer_copy.srcOffset = 0;
    buffer_copy.dstOffset = 0;
    buffer_copy.size = m_vertex_buffer.m_memory_requirements.size;
    vkCmdCopyBuffer(command_buffer, m_staging_vertex_buffer.m_buffer, m_vertex_buffer.m_buffer, 1, &buffer_copy);
    VkBufferMemoryBarrier buffer_memory_barrier = {};
    buffer_memory_barrier.sType = VK_STRUCTURE_TYPE_BUFFER_MEMORY_BARRIER;
    buffer_memory_barrier.pNext = nullptr;
    buffer_memory_barrier.srcAccessMask = VK_ACCESS_TRANSFER_WRITE_BIT;
    buffer_memory_barrier.dstAccessMask = VK_ACCESS_VERTEX_ATTRIBUTE_READ_BIT;
    buffer_memory_barrier.srcQueueFamilyIndex = VK_QUEUE_FAMILY_IGNORED;
    buffer_memory_barrier.dstQueueFamilyIndex = VK_QUEUE_FAMILY_IGNORED;
    buffer_memory_barrier.buffer = m_vertex_buffer.m_buffer;
    buffer_memory_barrier.offset = 0;
    buffer_memory_barrier.size = m_vertex_buffer.m_memory_requirements.size;
    vkCmdPipelineBarrier(command_buffer,
        VK_PIPELINE_STAGE_TRANSFER_BIT,
        VK_PIPELINE_STAGE_VERTEX_INPUT_BIT,
        0,
        0,
        nullptr,
        1,
        &buffer_memory_barrier,
        0,
        nullptr);
    vk_result = vkMapMemory(vulkan_device,
        m_staging_index_buffer.m_device_memory,
        0,
        m_staging_index_buffer.m_memory_requirements.size,
        0,
        &data);
    if (VK_SUCCESS != vk_result)
    {
        return false;
    }

    const auto index_buffer_size = m_indices.size() * static_cast<VkDeviceSize>(Property_group::index_size);
    std::memcpy(data, m_indices.data(), index_buffer_size);
    mapped_memory_range = {};
    mapped_memory_range.sType = VK_STRUCTURE_TYPE_MAPPED_MEMORY_RANGE;
    mapped_memory_range.pNext = nullptr;
    mapped_memory_range.memory = m_staging_index_buffer.m_device_memory;
    mapped_memory_range.offset = 0;
    mapped_memory_range.size = m_staging_index_buffer.m_memory_requirements.size;
    vk_result = vkFlushMappedMemoryRanges(vulkan_device, 1, &mapped_memory_range);
    if (VK_SUCCESS != vk_result)
    {
        return false;
    }

    vkUnmapMemory(vulkan_device, m_staging_index_buffer.m_device_memory);
    buffer_copy = {};
    buffer_copy.srcOffset = 0;
    buffer_copy.dstOffset = 0;
    buffer_copy.size = m_index_buffer.m_memory_requirements.size;
    vkCmdCopyBuffer(command_buffer, m_staging_index_buffer.m_buffer, m_index_buffer.m_buffer, 1, &buffer_copy);
    buffer_memory_barrier = {};
    buffer_memory_barrier.sType = VK_STRUCTURE_TYPE_BUFFER_MEMORY_BARRIER;
    buffer_memory_barrier.pNext = nullptr;
    buffer_memory_barrier.srcAccessMask = VK_ACCESS_TRANSFER_WRITE_BIT;
    buffer_memory_barrier.dstAccessMask = VK_ACCESS_VERTEX_ATTRIBUTE_READ_BIT;
    buffer_memory_barrier.srcQueueFamilyIndex = VK_QUEUE_FAMILY_IGNORED;
    buffer_memory_barrier.dstQueueFamilyIndex = VK_QUEUE_FAMILY_IGNORED;
    buffer_memory_barrier.buffer = m_index_buffer.m_buffer;
    buffer_memory_barrier.offset = 0;
    buffer_memory_barrier.size = m_index_buffer.m_memory_requirements.size;
    vkCmdPipelineBarrier(command_buffer,
        VK_PIPELINE_STAGE_TRANSFER_BIT,
        VK_PIPELINE_STAGE_VERTEX_INPUT_BIT,
        0,
        0,
        nullptr,
        1,
        &buffer_memory_barrier,
        0,
        nullptr);

    return true;
}

void Scene::draw(VkCommandBuffer command_buffer, VkPipelineLayout pipeline_layout, const glm::mat4 &matrix) const
{
    for (std::size_t light_group_index = 0; light_group_index != m_light_groups.size(); ++light_group_index)
    {
        const auto &light_group = m_light_groups.at(light_group_index);
        for (auto iter = light_group.m_property_groups.cbegin(); iter != light_group.m_property_groups.cend(); ++iter)
        {
            if (Property_group::invalid_index != iter->m_property_index)
            {
                auto push_constants = Jade::Push_constants{matrix,
                    static_cast<uint32_t>(light_group.m_lights.size()),
                    static_cast<uint32_t>(light_group_index),
                    static_cast<uint32_t>(iter->m_property_index)};
                vkCmdPushConstants(command_buffer,
                    pipeline_layout,
                    VK_SHADER_STAGE_VERTEX_BIT,
                    0,
                    sizeof(Jade::Push_constants),
                    &push_constants);
                auto offset = static_cast<VkDeviceSize>(iter->m_begin_vertex * sizeof(Jade::Vertex));
                vkCmdBindVertexBuffers(command_buffer, 0, 1, &m_vertex_buffer.m_buffer, &offset);
                offset = iter->m_begin_index * Property_group::index_size;
                vkCmdBindIndexBuffer(command_buffer, m_index_buffer.m_buffer, offset, Property_group::vulkan_index_type);
                vkCmdDrawIndexed(command_buffer,
                    static_cast<uint32_t>(iter->m_indices.size()),
                    1,
                    0,
                    0,
                    0);
            }
        }
    }
}

void Scene::process_entities(const CFileIWF &file)
{
    m_lights.clear();
    for (auto iter = file.m_vpEntityList.cbegin(); iter != file.m_vpEntityList.cend(); ++iter)
    {
        const iwfEntity *entity = *iter;
        if ((ENTITY_LIGHT == entity->EntityTypeID) && (entity->DataSize != 0))
        {
            auto light_entity = reinterpret_cast<const LIGHTENTITY*>(entity->DataArea);
            // Ignore ambient lights
            if (LIGHTTYPE_AMBIENT != light_entity->LightType)
            {
                Jade::D3d9_light light;
                light.m_type = static_cast<Jade::D3d9_light::Type>(light_entity->LightType + 1);
                light.m_diffuse = glm::vec4{light_entity->DiffuseRed,
                    light_entity->DiffuseGreen,
                    light_entity->DiffuseBlue,
                    light_entity->DiffuseAlpha};
                light.m_ambient = glm::vec4{light_entity->AmbientRed,
                    light_entity->AmbientGreen,
                    light_entity->AmbientBlue,
                    light_entity->AmbientAlpha};
                light.m_specular = glm::vec4{light_entity->SpecularRed,
                    light_entity->SpecularGreen,
                    light_entity->SpecularBlue,
                    light_entity->SpecularAlpha};
                // entity->ObjectMatrix._RC
                // R: row (the first row is 1, the last row is 4)
                // C: column (the first column is 1, the last column is 4)
                light.m_position = glm::vec4{entity->ObjectMatrix._41,
                    entity->ObjectMatrix._42,
                    entity->ObjectMatrix._43,
                    1.0f};
                light.m_direction = glm::vec4{entity->ObjectMatrix._31,
                    entity->ObjectMatrix._32,
                    entity->ObjectMatrix._33,
                    0.0f};
                light.m_range = light_entity->Range;
                light.m_attenuation0 = light_entity->Attenuation0;
                light.m_attenuation1 = light_entity->Attenuation1;
                light.m_attenuation2 = light_entity->Attenuation2;
                light.m_falloff = light_entity->FallOff;
                light.m_theta = light_entity->Theta;
                light.m_phi = light_entity->Phi;
                m_lights.push_back(light);
            }
        }
    }
}

void Scene::process_materials(const CFileIWF &file)
{
    m_materials.clear();
    for (auto iter = file.m_vpMaterialList.cbegin(); iter != file.m_vpMaterialList.cend(); ++iter)
    {
        auto iwf_material = *iter;
        Jade::D3d9_material material;
        material.m_diffuse = glm::vec4{iwf_material->Diffuse.r,
            iwf_material->Diffuse.g,
            iwf_material->Diffuse.b,
            iwf_material->Diffuse.a};
        material.m_ambient = glm::vec4{iwf_material->Ambient.r,
            iwf_material->Ambient.g,
            iwf_material->Ambient.b,
            iwf_material->Ambient.a};
        material.m_emissive = glm::vec4{iwf_material->Emissive.r,
            iwf_material->Emissive.g,
            iwf_material->Emissive.b,
            iwf_material->Emissive.a};
        material.m_specular = glm::vec4{iwf_material->Specular.r,
            iwf_material->Specular.g,
            iwf_material->Specular.b,
            iwf_material->Specular.a};
        material.m_power = iwf_material->Power;
        m_materials.push_back(material);
    }
}

// IWF files exported from GILES store vertex components per face.
// iwfSurface::Vertices
// iwfSurface::Indices - Each index in Indices refers to a vertex in Vertices.
void Scene::process_meshes(const CFileIWF &file)
{
    m_light_groups.clear();
    const auto material_count = static_cast<int>(m_materials.size());
    // A material_index value of -1 means no material.
    for (int material_index = -1; material_index != material_count; ++material_index)
    {
        std::vector<const iwfSurface*> surfaces;
        for (auto mesh_iter = file.m_vpMeshList.cbegin(); mesh_iter != file.m_vpMeshList.cend(); ++mesh_iter)
        {
            auto iwf_mesh = *mesh_iter;
            for (unsigned surface_index = 0; surface_index != iwf_mesh->SurfaceCount; ++surface_index)
            {
                const iwfSurface *iwf_surface = iwf_mesh->Surfaces[static_cast<std::size_t>(surface_index)];
                bool has_material_component = iwf_surface->Components & SCOMPONENT_MATERIALS;
                if ((-1 == material_index) && ((!has_material_component) || (0 == iwf_surface->ChannelCount)))
                {
                    surfaces.push_back(iwf_surface);
                }
                else if (iwf_surface->MaterialIndices[0] == material_index)
                {
                    surfaces.push_back(iwf_surface);
                }
            }
        }
#ifdef _DEBUG
        auto &os = Log_manager::get_instance()->get_stream(Log_manager::Log_type::default_log);
        os << "Scene::process_meshes material_index: " << material_index;
        os << " surfaces.size(): " << surfaces.size() << std::endl; //-V128
#endif
        if (!surfaces.empty())
        {
            auto material_index_size_t = (-1 == material_index) ? Property_group::invalid_index : static_cast<std::size_t>(material_index);
            build_light_groups(surfaces, material_index_size_t);
        }
    }
}

// Scene vertex buffer
// - vertices using light group 0
//   - vertices using material 0
//   - vertices using material 1
//   - ...
//   - vertices using material N - 1
//   - vertices using light group 1
//   - vertices using material 0
//   - vertices using material 1
//   - ...
//   - vertices using material N - 1
//   ...
// - vertices using light group 1
// - ...
// - vertices using light group N - 1
void Scene::build_light_groups(const std::vector<const iwfSurface*> &surfaces, size_t material_index)
{
#ifdef _DEBUG
    auto &os = Log_manager::get_instance()->get_stream(Log_manager::Log_type::default_log);
    std::size_t surface_index = 0;
#endif
    for (auto iter = surfaces.cbegin(); iter != surfaces.cend(); ++iter)
    {
        // pair.first: index of light in m_lights
        // pair.second: contribution
        std::vector<std::pair<std::size_t, float>> light_contribution(m_lights.size());
        const iwfSurface *iwf_surface = *iter;
        // Calculate the contribution of each light in m_lights to iwf_surface.
        for (std::size_t light_index = 0; light_index != m_lights.size(); ++light_index)
        {
            const auto &light = m_lights.at(light_index);
            auto contribution = calculate_light_contribution(iwf_surface, light);
            light_contribution.at(light_index) = std::make_pair(light_index, contribution);
        }

        const auto maximum_light_group_size = static_cast<std::size_t>(get_maximum_light_group_size());
        // Sort the lights in light_contribution in descending order by contribution. That is, the first light in
        // light_contribution should contribute the most to the colour of the current surface. The second light in
        // light_contribution should contribute the second most to the colour of the current surface, and so on.
        std::sort(light_contribution.begin(),
            light_contribution.end(),
            [](const std::pair<std::size_t, float> &left, const std::pair<std::size_t, float> &right)
            {
                // Sort in descending order.
                return right.second < left.second;
            });
        std::vector<std::size_t> lights;
        assert(light_contribution.size() >= maximum_light_group_size);
        for (std::size_t i = 0; i != maximum_light_group_size; ++i)
        {
            if (light_contribution.at(i).second > 0.0f)
            {
                lights.push_back(light_contribution.at(i).first);
            }
        }
#ifdef _DEBUG
        os << "Scene::build_light_groups. List of lights to add to a light group for surface_index: " << surface_index; //-V128
        os << " lights: (";
        if (lights.empty())
        {
            os << "EMPTY)" << std::endl;
        }
        else
        {
            for (std::size_t i = 0; i != (lights.size() - 1); ++i)
            {
                os << lights.at(i) << ", "; //-V128
            }

            os << lights.back() << ")" << std::endl; //-V128
        }
#endif
        assert(lights.size() <= maximum_light_group_size);
        // Is there an existing Light_group representing lights?
        auto light_group_iter = std::find_if(m_light_groups.begin(),
            m_light_groups.end(),
            [&lights](const Jade::Light_group &light_group)
            {
                return (light_group.m_lights == lights);
            });
        if (m_light_groups.end() == light_group_iter)
        {
            // Create a new Light_group for lights
            Jade::Light_group light_group;
            light_group.m_lights = lights;
            m_light_groups.push_back(light_group);
            light_group_iter = m_light_groups.begin() + static_cast<std::ptrdiff_t>(m_light_groups.size() - 1);
        }

        // Now, light_group_iter points to the Light_group that iwf_surface should be added to.

        // Is there an existing Property_group (Property_group::Type::material) for material_index?
        auto property_group_material_iter = std::find_if(light_group_iter->m_property_groups.begin(),
            light_group_iter->m_property_groups.end(),
            [material_index](const Jade::Property_group &property_group)
        {
            return (property_group.m_type == Jade::Property_group::Type::material) &&
                (property_group.m_property_index == material_index);
        });
        if (light_group_iter->m_property_groups.end() == property_group_material_iter)
        {
            Jade::Property_group property_group;
            property_group.m_type = Jade::Property_group::Type::material;
            property_group.m_property_index = material_index;
            property_group.m_begin_vertex = light_group_iter->m_vertices.size();
            light_group_iter->m_property_groups.push_back(property_group);
            property_group_material_iter = light_group_iter->m_property_groups.begin() + static_cast<std::ptrdiff_t>(light_group_iter->m_property_groups.size() - 1);
        }

        // Now, property_group_material_iter points to the Property_group that iwf_surface should be added to.
        
        process_indices(*light_group_iter, iwf_surface, *property_group_material_iter);
        process_vertices(iwf_surface, *light_group_iter);
#ifdef _DEBUG
        ++surface_index;
#endif
    }
}

// Ignores:
// - the global ambient light color
// - emissive light (from materials)
// - specular light
float Scene::calculate_light_contribution(const iwfSurface* iwf_surface, const Jade::D3d9_light &light) const
{
    auto has_material = iwf_surface->Components & SCOMPONENT_MATERIALS;
    if ((0 == iwf_surface->ChannelCount) || (!has_material))
    {
        return 0.0f;
    }

    if (iwf_surface->MaterialIndices[0] < 0)
    {
        return 0.0f;
    }
    
    const auto material_index = static_cast<std::size_t>(iwf_surface->MaterialIndices[0]);
    const auto &material = m_materials.at(material_index);
    const auto vertex_count = static_cast<std::size_t>(iwf_surface->VertexCount);
    float maximum_contribution = 0.0f;
    for (std::size_t i = 0; i != vertex_count; ++i)
    {
        const auto &iwf_vertex = iwf_surface->Vertices[i];
        const auto vertex = glm::vec3{iwf_vertex.x, iwf_vertex.y, iwf_vertex.z};
        auto from_vertex_to_light = glm::vec3{light.m_position} - vertex;
        auto distance = glm::length(from_vertex_to_light);
        // Directional lights have infinite range.
        bool out_of_range = (D3d9_light::Type::directional != light.m_type) && (distance > light.m_range);
        if (!out_of_range)
        {
            from_vertex_to_light = glm::normalize(from_vertex_to_light);
            float attenuation{1.0f};
            // Directional lights do not attenuate with distance.
            if (D3d9_light::Type::directional != light.m_type)
            {
                attenuation = light.m_attenuation0 + (light.m_attenuation1 * distance) + (light.m_attenuation2 * distance * distance);
                attenuation = (attenuation > 0.0f) ? (1.0f / attenuation) : attenuation;
            }

            float spot{1.0f};
            if (D3d9_light::Type::spot == light.m_type)
            {
                auto cos_q = glm::dot(-from_vertex_to_light, glm::vec3{light.m_direction});
                auto inner_cone_cos_q = std::cosf(light.m_theta / 2.0f);
                auto outer_cone_cos_q = std::cosf(light.m_phi / 2.0f);
                if (cos_q > inner_cone_cos_q)
                {
                    // vertex is inside the inner cone
                    spot = 1.0f;
                }
                else if (cos_q <= outer_cone_cos_q)
                {
                    // vertex is outside the outer cone
                    spot = 0.0f;
                }
                else
                {
                    // vertex is somewhere between the inner cone and the outer cone
                    auto numerator = cos_q - outer_cone_cos_q;
                    auto denominator = inner_cone_cos_q - outer_cone_cos_q;
                    spot = std::powf(numerator / denominator, light.m_falloff);
                }
            }

            const auto vertex_normal = glm::vec3{iwf_vertex.Normal.x, iwf_vertex.Normal.y, iwf_vertex.Normal.z};
            auto cos_q = glm::dot(from_vertex_to_light, vertex_normal);
            cos_q = (cos_q < 0.0f) ? 0.0f : cos_q;
            auto diffuse = light.m_diffuse * material.m_diffuse * cos_q * attenuation * spot;
            diffuse.a = 0.0f;
            // The angle between the vertex normal and the vector from the vertex to the light does not affect the
            // intensity of ambient lights.
            auto ambient = light.m_ambient * material.m_ambient * attenuation * spot;
            ambient.a = 0.0f;
            auto color = ambient + diffuse;
            auto red_contribution = std::fabs(color.r);
            auto green_contribution = std::fabs(color.g);
            auto blue_contribution = std::fabs(color.b);
            std::vector<float> v = {red_contribution, green_contribution, blue_contribution};
            auto iter = std::max_element(v.cbegin(), v.cend());
            maximum_contribution = std::max<float>(*iter, maximum_contribution);
        }
    }

    return maximum_contribution;
}

void Scene::process_indices(const Jade::Light_group &light_group,
    const iwfSurface *iwf_surface,
    Jade::Property_group &out_property_group) const
{
    // Example
    // begin_vertex is the index of the first vertex belonging to the surface that is currently being processed
    // (iwf_surface) that is going to be added to light_group.m_vertices. process_vertices is called immediately after
    // process_indices to add the vertices in iwf_surface to light_group.
    // light_group.m_vertices = { 0, 1, 2 }
    // out_property_group.m_begin_vertex = 0
    // begin_vertex = 3 - 0
    // begin_vertex = 3
    assert(light_group.m_vertices.size() >= out_property_group.m_begin_vertex);
    // begin_vertex will be zero if out_property_group was added to light_group before calling process_indices because
    // out_property_group.m_begin_vertex will be equal to light_group.m_vertices.size().
    const std::size_t begin_vertex = light_group.m_vertices.size() - out_property_group.m_begin_vertex;
    if (iwf_surface->IndexCount > 0)
    {
        // INDICES_MASK_TYPE: 0xFC = 1111_1100
        const auto index_type = iwf_surface->IndexFlags & INDICES_MASK_TYPE;
        switch (index_type)
        {
        case INDICES_TRILIST:
        {
            for (std::size_t i = 0; i != static_cast<std::size_t>(iwf_surface->IndexCount); ++i)
            {
                auto index = static_cast<std::size_t>(iwf_surface->Indices[i]) + begin_vertex;
                out_property_group.m_indices.push_back(static_cast<uint16_t>(index));
            }
        }
            break;
        default:
            // IWF files exported by GILES only use INDICES_TRILIST. Since those are the only IWF files that are ever
            // going to be used the other index types are not handled.
            std::exit(EXIT_FAILURE);
            break;
        }
    }
    else
    {
        const auto vertex_type = iwf_surface->VertexFlags & VERTICES_MASK_TYPE;
        switch (vertex_type)
        {
        case VERTICES_TRIFAN:
        {
            // Convert a triangle fan to a triangle list
            // fan_triangle_count = fan_vertex_count - 2
            if (iwf_surface->VertexCount < 3)
            {
                std::exit(EXIT_FAILURE);
            }
            
            // Example
            // begin_vertex = 0, iwf_surface->VertexCount = 4
            // The first pass generates indices: { 0, 1, 2 }
            // The second pass generates indices: { 0, 2, 3 }
            for (std::size_t i = 0; i != static_cast<std::size_t>(iwf_surface->VertexCount - 2); ++i)
            {
                size_t index_0 = begin_vertex; // 0 + begin_vertex
                size_t index_1 = i + 1 + begin_vertex;
                size_t index_2 = i + 2 + begin_vertex;
                out_property_group.m_indices.push_back(static_cast<uint16_t>(index_0));
                out_property_group.m_indices.push_back(static_cast<uint16_t>(index_1));
                out_property_group.m_indices.push_back(static_cast<uint16_t>(index_2));
            }
        }
            break;

        default:
            // IWF files exported by GILES only use VERTICES_TRIFAN. Since those are the only IWF files that are ever
            // going to be used the other vertex types are not handled.
            std::exit(EXIT_FAILURE);
            break;
        }
    }
}

// process_vertices is called immediately after adding the indices for a surface to a Property_group.
void Scene::process_vertices(const iwfSurface *iwf_surface, Jade::Light_group &out_light_group) const
{
    for (std::size_t i = 0; i != static_cast<std::size_t>(iwf_surface->VertexCount); ++i)
    {
        const auto &iwf_vertex = iwf_surface->Vertices[i];
        const auto &normal = iwf_vertex.Normal;
        auto vertex = Jade::Vertex{glm::vec3{iwf_vertex.x, iwf_vertex.y, iwf_vertex.z},
            glm::vec3{normal.x, normal.y, normal.z}};
        out_light_group.m_vertices.push_back(vertex);
    }
}

}
