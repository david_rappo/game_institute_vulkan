#pragma once

// C++ Standard Library
#include <cstddef>
#include <limits>
#include <vector>

// Vulkan
#include "vulkan/vulkan.h"

namespace Jade
{

class Property_group
{
public:
    static const VkIndexType vulkan_index_type{VK_INDEX_TYPE_UINT16};
    static const VkDeviceSize index_size{sizeof(uint16_t)};
    // If m_data is equal to invalid_index this Property_group does not have a texture or material.
    static const std::size_t invalid_index = std::numeric_limits<std::size_t>::max();

    enum class Type
    {
        none = 0,
        material = 1,
        texture = 2
    };

    // If Property_group::Type is Property_group::Type::material m_property_index stores an index into a list of all
    // the materials used by the scene.
    //
    // If Property_group::Type is Property_group::Type::texture m_property_index stores an index into a list of all the
    // textures used by the scene.
    std::size_t m_property_index{};
    Type m_type{};
    // The first vertex used by this Property_group in it's parent Light_group.
    //
    // Example. The first six vertices belong to material 0 (m_0). The last three vertices belong to material 1 (m_1).
    // All surfaces that use m_0 will be processed before the surfaces that use m_1. So, when the Property_group for
    // m_1 is added to the light groups list of property groups m_begin_vertex is set to 6.
    //                            m_0               m_1
    // Light_group::m_vertices: { 0, 1, 2, 3, 4, 5, 6, 7, 8 }
    std::size_t m_begin_vertex{};
    // Stores the index of the first index in the scene list of indices used by this Property_group.
    std::size_t m_begin_index{};
    // Triangle list indices. m_begin_vertex needs to be added to each vertex index in m_indices e.g.
    // m_indices[0] + m_begin_vertex
    std::vector<uint16_t> m_indices;
};

}