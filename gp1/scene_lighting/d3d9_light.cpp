// This is a personal academic project. Dear PVS-Studio, please check it.
// PVS-Studio Static Code Analyzer for C, C++ and C#: http://www.viva64.com
#include "d3d9_light.h"

// C++ Standard Library
#include <iostream>

// Jade
#include "debug_utility.h"

namespace Jade
{

std::ostream &operator<<(std::ostream &out, const D3d9_light &light)
{
    using namespace Jade::Debug_utility;
    out << "light.m_type: " << static_cast<int32_t>(light.m_type);
    out << " light.m_position: " << light.m_position;
    out << " light.m_direction: " << light.m_direction;
    out << " light.m_diffuse: " << light.m_diffuse;
    out << " light.m_ambient: " << light.m_ambient;
    return out;
}

}