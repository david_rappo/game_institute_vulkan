readme.txt

Application: scene_lighting

About

Rewrite of Lab Project 5.2 Scene Lighting from Graphics Programming with DirectX 9 Part 1
(https://www.gameinstitute.com/) using Vulkan.

Loads a scene from a file stored in Interchangeable World Format (IWF). Geometry is batch rendered by light group, then
material. A light group stores the N most influential lights in the scene for a list of triangles. The triangles in
each light group are further grouped by material. The reason for doing this is to minimise the number of
vkCmdDrawIndexed calls required to draw the scene.

Controls

Keyboard Controls

Strafe left: left arrow key
Strafe right: right arrow key
Forward: up arrow key
Back: down arrow key
Page Up: Move player/camera up
Page Down: Move player/camera down

Mouse Controls

Rotate

Hold down the left mouse button only. Vertical movements pitch the camera. Horizontal movements yaw the camera.
Hold down the left mouse button and the right mouse button. Vertical movements pitch the camera. Horizontal movements
roll the camera.