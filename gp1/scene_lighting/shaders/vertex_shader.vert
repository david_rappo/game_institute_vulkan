#version 450
layout (location = 0) in vec3 in_position;
layout (location = 1) in vec3 in_normal;

layout (location = 0) out vec4 vs_out_color;

struct Light_struct
{

vec4 m_diffuse;
vec4 m_specular;
vec4 m_ambient;
vec4 m_position;
vec4 m_direction;
int m_type;
float m_range;
float m_falloff;
float m_attenuation0;
float m_attenuation1;
float m_attenuation2;
float m_theta;
float m_phi;

};

struct Material_struct
{

vec4 m_diffuse;
vec4 m_ambient;
vec4 m_specular;
vec4 m_emissive;
float m_power;

};

// Array of all the lights in the scene
layout (set = 0, binding = 0) uniform Uniform_buffer_lights
{

// Arrays must be declared with a size before being indexed with a variable.
Light_struct m_lights[28];

};

// Array of all the materials in the scene
layout (set = 0, binding = 1) uniform Uniform_buffer_materials
{

// Arrays must be declared with a size before being indexed with a variable.
Material_struct m_materials[84];

};

layout (set = 0, binding = 2) uniform Uniform_buffer_light_groups
{

// Array of light indices.
// light group 0 starts at m_light_groups[0]
// light group 1 starts at m_light_groups[maximum_light_group_size]
// light group 2 starts at m_light_groups[maximum_light_group_size * 2]
// ...
// 1106 = number_of_light_groups * maximum_light_group_size
uint m_light_groups[1106];

};

layout (push_constant) uniform Uniform_push_constant
{

// m_matrix = invert_y_matrix * projection_matrix * view_matrix * model_matrix
mat4 m_matrix;

// Stores the number of lights in each light group
uint m_light_group_size;

// index into Uniform_buffer_light_groups
uint m_light_group_index;

// index into Uniform_buffer_materials.m_materials
uint m_material_index;

} uniform_push_constant;

out gl_PerVertex
{

vec4 gl_Position;

};

const int light_type_point = 1;
const int light_type_spot = 2;
const int light_type_directional = 3;

const vec4 global_ambient = vec4(0.051f, 0.051f, 0.051f, 1.0f);
// The maximum number of light (indices) that a single light group can hold. See Scene::load. Most light groups contain
// fewer than four lights. So, using more doesn't improve the appearance of the scene.
const uint maximum_light_group_size = 7;

vec4 calculate_color(Light_struct light)
{
	vec3 from_vertex_to_light = light.m_position.xyz - in_position;
	float distance = length(from_vertex_to_light);
	// Directional lights have infinite range.
	bool out_of_range = (light_type_directional != light.m_type) && (distance > light.m_range);
	vec4 color = vec4(0.0f, 0.0f, 0.0f, 1.0f);
	if (!out_of_range)
	{
		from_vertex_to_light = normalize(from_vertex_to_light);
        float attenuation = 1.0f;
        // Directional lights do not attenuate with distance.
        if (light_type_directional != light.m_type)
        {
            attenuation = light.m_attenuation0 + (light.m_attenuation1 * distance) + (light.m_attenuation2 * distance * distance);
            attenuation = (attenuation > 0.0f) ? (1.0f / attenuation) : attenuation;
        }

		float spot = 1.0f;
        if (light_type_spot == light.m_type)
        {
            float cos_q = dot(-from_vertex_to_light, light.m_direction.xyz);
            float inner_cone_cos_q = cos(light.m_theta / 2.0f);
            float outer_cone_cos_q = cos(light.m_phi / 2.0f);
            if (cos_q > inner_cone_cos_q)
            {
                // vertex is inside the inner cone
                spot = 1.0f;
            }
            else if (cos_q <= outer_cone_cos_q)
            {
                // vertex is outside the outer cone
                spot = 0.0f;
            }
            else
            {
                // vertex is somewhere between the inner cone and the outer cone
                float numerator = cos_q - outer_cone_cos_q;
                float denominator = inner_cone_cos_q - outer_cone_cos_q;
                spot = pow(numerator / denominator, light.m_falloff);
            }
		}
		
		float cos_q = dot(from_vertex_to_light, in_normal);
		clamp(cos_q, 0.0f, 1.0f);
		Material_struct material = m_materials[uniform_push_constant.m_material_index];
        vec4 diffuse = light.m_diffuse * material.m_diffuse * cos_q * attenuation * spot;
        // The angle between the vertex normal and the vector from the vertex to the light does not affect the
        // intensity of ambient lights.
        vec4 ambient = light.m_ambient * material.m_ambient * attenuation * spot;
		color = (ambient + diffuse);
	}

	return color;
}

void main()
{
    Light_struct dynamic_light;
	dynamic_light.m_diffuse = vec4(1.0f, 0.0f, 0.0f, 1.0f);
	dynamic_light.m_specular = vec4(0.0f);
	dynamic_light.m_ambient = vec4(0.0f);
	dynamic_light.m_position = vec4(290.0f, 10.0f, 500.0f, 1.0f);
	dynamic_light.m_direction = vec4(0.0f);
	dynamic_light.m_type = light_type_point;
	dynamic_light.m_range = 150.0f;
	dynamic_light.m_falloff = 0.0f;
	dynamic_light.m_attenuation0 = 1.0f;
	dynamic_light.m_attenuation1 = 0.0f;
	dynamic_light.m_attenuation2 = 0.0f;
	dynamic_light.m_theta = 0.0f;
	dynamic_light.m_phi = 0.0f;
	// m_light_groups stores maximum_light_group_size lights per light group. The actual number of lights used by the
	// light group is stored in uniform_push_constant.m_light_group_size.
	uint begin = maximum_light_group_size * uniform_push_constant.m_light_group_index;
	uint end = begin + uniform_push_constant.m_light_group_size;
	vec4 color = vec4(0.0f, 0.0f, 0.0f, 1.0f);
	for (uint i = begin; i != end; ++i)
	{
		uint light_index = m_light_groups[i];
		Light_struct light = m_lights[light_index];
		color += calculate_color(light);
	}
	
	color += calculate_color(dynamic_light);
	Material_struct material = m_materials[uniform_push_constant.m_material_index];
	color += (global_ambient * material.m_ambient);
	color += material.m_emissive;
	gl_Position = uniform_push_constant.m_matrix * vec4(in_position, 1.0f);
	vs_out_color = vec4(color.rgb, 1.0f);
}