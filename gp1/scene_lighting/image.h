#pragma once

// Vulkan
#include "vulkan/vulkan.h"

namespace Jade
{

class Image
{
public:
    void destroy(VkDevice device);

    VkImage m_image{nullptr};
    VkDeviceMemory m_device_memory{nullptr};
    VkImageView m_image_view{nullptr};
    VkSampler m_sampler{nullptr};
    VkMemoryRequirements m_memory_requirements{};
};

}
