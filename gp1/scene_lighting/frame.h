#pragma once

// Vulkan
#include "vulkan/vulkan.h"

namespace Jade
{

class Frame
{
public:
    void destroy(VkDevice device, VkCommandPool command_pool) const;

    VkFramebuffer m_framebuffer{nullptr};
    VkCommandBuffer m_command_buffer{nullptr};
    VkSemaphore m_image_available_semaphore{nullptr};
    VkSemaphore m_rendering_finished_semaphore{nullptr};
    VkFence m_fence{nullptr};
};

}
