#pragma once

// Jade
#include "camera.h"

namespace Jade
{

class Player;

}

namespace Jade
{

class First_person_fly_camera : public Camera
{
public:
    First_person_fly_camera();
    explicit First_person_fly_camera(const Camera *camera);
    Type get_type() const override;
    void rotate(float x, float y, float z, const Player &player) override;
};

}