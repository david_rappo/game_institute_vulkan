#pragma once

// C++ Standard Library
#include <cstddef>
#include <string>
#include <vector>

// GLM
#include <glm/mat4x4.hpp>

// Vulkan
#include "vulkan/vulkan.h"

// Jade
#include "buffer.h"
#include "d3d9_light.h"
#include "d3d9_material.h"
#include "light_group.h"
#include "vertex.h"

// Third party - libIWF
class CFileIWF;
class iwfSurface;

namespace Jade
{

class Property_group;

}

namespace Jade
{

class Scene
{
public:
    Scene();
    Scene(const Scene&) = delete;
    Scene& operator=(const Scene&) = delete;
    
    void destroy(VkDevice device);
    bool load(const std::string &file_name, unsigned light_limit, unsigned reserved_lights);
    bool build_buffers(VkDevice vulkan_device,
        const VkPhysicalDeviceMemoryProperties &physical_device_memory_properties);
    bool copy_buffers(VkDevice vulkan_device, VkQueue graphics_queue, VkCommandBuffer command_buffer) const;
    void draw(VkCommandBuffer command_buffer, VkPipelineLayout pipeline_layout, const glm::mat4 &matrix) const;
    const std::vector<Jade::D3d9_light> &get_lights() const
    {
        return m_lights;
    }

    const std::vector<Jade::D3d9_material> &get_materials() const
    {
        return m_materials;
    }

    const std::vector<Jade::Light_group> &get_light_groups() const
    {
        return m_light_groups;
    }

    // The maximum number of light indices stored in each light group.
    unsigned get_maximum_light_group_size() const
    {
        return m_light_limit - m_reserved_lights;
    }

private:
    void process_entities(const CFileIWF &file);
    void process_materials(const CFileIWF &file);
    void process_meshes(const CFileIWF &file);
    void build_light_groups(const std::vector<const iwfSurface*> &surfaces, std::size_t material_index);
    float calculate_light_contribution(const iwfSurface* iwf_surface, const Jade::D3d9_light &light) const;
    void process_indices(const Jade::Light_group &light_group,
        const iwfSurface *iwf_surface,
        Jade::Property_group &out_property_group) const;
    void process_vertices(const iwfSurface *iwf_surface, Jade::Light_group &out_light_group) const;

    // Stores the maximum number of lights per light group
    unsigned m_light_limit{};
    // Each light group will have (m_light_limit - m_reserved_lights) lights
    unsigned m_reserved_lights{};
    // Stores all of the lights in the scene
    std::vector<Jade::D3d9_light> m_lights;
    // Stores all of the materials in the scene
    std::vector<Jade::D3d9_material> m_materials;
    // All of the triangles belonging to a particular Light_group use the same lights stored in the Light_group.
    std::vector<Jade::Light_group> m_light_groups;
    // Stores every vertex in the scene grouped by Light_group then Property_group.
    std::vector<Jade::Vertex> m_vertices;
    // Stores the indices for every triangle in the scene. The offsets into m_indices for every
    // Light_group/Property_group combination are stored in Property_group::m_begin_index.
    std::vector<uint16_t> m_indices;
    Jade::Buffer m_staging_vertex_buffer;
    Jade::Buffer m_staging_index_buffer;
    Jade::Buffer m_index_buffer;
    Jade::Buffer m_vertex_buffer;
};

}
