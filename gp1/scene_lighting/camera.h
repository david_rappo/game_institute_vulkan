#pragma once

// GLM
#include <glm/mat4x4.hpp>
#include <glm/vec3.hpp>

// Jade
#include "d3d9_plane.h"
#include "volume.h"

namespace Jade
{

class Player;

}

namespace Jade
{

class Camera
{
public:
    enum class Type
    {
        first_person_fly,
        undefined
    };
    
    Camera();
    explicit Camera(const Camera *camera);
    Camera(const Camera &camera) = delete;
    Camera& operator=(const Camera&) = delete;
    virtual ~Camera();

    void set_vertical_field_of_view(float vertical_field_of_view);
    void set_aspect_ratio(float aspect_ratio);
    void set_near_view_plane(float near_view_plane);
    void set_far_view_plane(float far_view_plane);
    const Volume &get_volume() const
    {
        return m_volume;
    }
    
    void set_volume(const Volume &volume)
    {
        m_volume = volume;
    }

    const glm::vec3 &get_position() const
    {
        return m_position;
    }
    
    void set_position(const glm::vec3 &position);
    const glm::mat4 &get_view_matrix();
    const glm::mat4 &get_projection_matrix();
    const glm::vec3 &get_right() const
    {
        return m_right;
    }

    const glm::vec3 &get_up() const
    {
        return m_up;
    }

    const glm::vec3 &get_look() const
    {
        return m_look;
    }
    
    bool bounds_in_frustum(const glm::vec3 &minimum, const glm::vec3 &maximum);
    virtual Type get_type() const = 0;
    // x, y, and z are measured in radians
    virtual void rotate(float x, float y, float z, const Player &player) = 0;
    virtual void update(float time_elapsed, float camera_lag, const Player &player);
    virtual void move(const glm::vec3 &v);

protected:
    void set_right(const glm::vec3 &right)
    {
        m_right = right;
    }

    void set_up(const glm::vec3 &up)
    {
        m_up = up;
    }

    void set_look(const glm::vec3 &look)
    {
        m_look = look;
    }
    
    void set_view_matrix_dirty(bool view_matrix_dirty);
    void set_projection_matrix_dirty(bool projection_matrix_dirty);

private:
    void calculate_frustum_planes();

    // Measured in radians
    float m_vertical_field_of_view{};
    float m_near_view_plane{};
    float m_far_view_plane{};
    float m_aspect_ratio{};
    bool m_view_matrix_dirty{true};
    bool m_projection_matrix_dirty{true};
    // To extract the camera frustum planes the view matrix is multiplied by the projection matrix. Therefore, if
    // either the view matrix or the projection matrix is modified the frustum planes need to be recalculated.
    bool m_frustum_dirty{true};
    glm::vec3 m_position;
    glm::vec3 m_right{1.0f, 0.0f, 0.0f};
    glm::vec3 m_up{0.0f, 1.0f, 0.0f};
    glm::vec3 m_look{0.0f, 0.0f, 1.0f};
    glm::mat4 m_view_matrix;
    glm::mat4 m_projection_matrix;
    Volume m_volume;
    // Plane normals are stored facing outwards - not inwards.
    Jade::D3d9_plane m_left_frustum_plane;
    Jade::D3d9_plane m_right_frustum_plane;
    Jade::D3d9_plane m_top_frustum_plane;
    Jade::D3d9_plane m_bottom_frustum_plane;
    Jade::D3d9_plane m_near_frustum_plane;
    Jade::D3d9_plane m_far_frustum_plane;
};

}